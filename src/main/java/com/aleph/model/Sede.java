package com.aleph.model;

public class Sede
{
    private Integer _sedeId;
    private String _nombre;
    private String _direccion;
    private String _distrito;
    private String _nombreCliente;
    private int _idCliente;

    public Sede()
    {

    }

    public Integer get_sedeId()
    {
        return _sedeId;
    }

    public void set_sedeId(Integer _sedeId)
    {
        this._sedeId = _sedeId;
    }

    public String get_nombre()
    {
        return _nombre;
    }

    public void set_nombre(String _nombre)
    {
        this._nombre = _nombre;
    }

    public String get_direccion()
    {
        return _direccion;
    }

    public void set_direccion(String _direccion)
    {
        this._direccion = _direccion;
    }

    public String get_distrito()
    {
        return _distrito;
    }

    public void set_distrito(String _distrito)
    {
        this._distrito = _distrito;
    }

    public String get_nombreCliente()
    {
        return _nombreCliente;
    }

    public void set_nombreCliente(String _nombreCliente)
    {
        this._nombreCliente = _nombreCliente;
    }

    public int get_idCliente()
    {
        return _idCliente;
    }

    public void set_idCliente(int _idCliente)
    {
        this._idCliente = _idCliente;
    }

    @Override
    public String toString()
    {
        return this._nombre = _nombre;
    }
}