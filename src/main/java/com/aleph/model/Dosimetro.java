package com.aleph.model;

public class Dosimetro
{
    private String _dosimetroCodigo;
    private String _tipo;
    private String _estado;

    public Dosimetro()
    {
    }

    public String get_dosimetroCodigo()
    {
        return _dosimetroCodigo;
    }

    public void set_dosimetroCodigo(String _dosimetroCodigo)
    {
        this._dosimetroCodigo = _dosimetroCodigo;
    }

    public String get_tipo()
    {
        return _tipo;
    }

    public void set_tipo(String _tipo)
    {
        this._tipo = _tipo;
    }

    public String get_estado()
    {
        return _estado;
    }

    public void set_estado(String _estado)
    {
        this._estado = _estado;
    }
}
