package com.aleph.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class AnnealReader
{

    public static ArrayList<Anneal> ReadAnneal(File _archivoSeleccionado)
    {
        File _archivo = null;
        FileReader _fr = null;
        BufferedReader _br = null;
        //ArrayList<Anneal> _datosList = new ArrayList<>();
        Anneal _anneal;
        ArrayList<Anneal> _annealList = new ArrayList<>();

        try
        {
            /**
             * 1.Se abre el fichero
             * 2.A partir del fichero creamos un BufferedReader
             */
//            _archivo = new File("C:\\Users\\Carlos\\Desktop\\testprn\\anneal_test.prn");

            _archivo = _archivoSeleccionado;
            _fr = new FileReader(_archivo);
            _br = new BufferedReader(_fr);
            /**
             * Lectura del fichero
             */

            String _lineaFichero;
            boolean _repetido = false;
            //int _contador = 1;
            while ((_lineaFichero = _br.readLine()) != null)
            {
                _repetido = false;

                _anneal = new Anneal();
                if (_lineaFichero.length() >= 61)
                {
                    if (_lineaFichero.substring(0, 8).compareTo("Badge ID") == 0)
                    {

                    } else if (_lineaFichero.substring(0, 8).compareTo("--------") == 0)
                    {

                    } else
                    {
                        if (_annealList.size() > 0)
                        {
                            for (Anneal _dato : _annealList)
                            {
                                if (_lineaFichero.substring(0, 7).compareTo(_dato.get_badgeId()) == 0)
                                {
                                    _dato.set_e1(Double.parseDouble(_lineaFichero.substring(21, 29)));
                                    _dato.set_e2(Double.parseDouble(_lineaFichero.substring(31, 39)));
                                    _dato.set_e3(Double.parseDouble(_lineaFichero.substring(41, 49)));
                                    _dato.set_e4(Double.parseDouble(_lineaFichero.substring(51, 59)));

                                    if (_lineaFichero.length() == 61)
                                    {
                                        _dato.set_reason("");
                                    }
                                    if (_lineaFichero.length() == 63)
                                    {
                                        _dato.set_reason(_lineaFichero.substring(61, 63));
                                    }
                                    if (_lineaFichero.length() == 65)
                                    {
                                        _dato.set_reason(_lineaFichero.substring(61, 65));
                                    }
                                    if (_lineaFichero.length() == 67)
                                    {
                                        _dato.set_reason(_lineaFichero.substring(61, 67));
                                    }
                                    if (_lineaFichero.length() == 69)
                                    {
                                        _dato.set_reason(_lineaFichero.substring(61, 69));
                                    }
                                    if (_lineaFichero.length() == 71)
                                    {
                                        _dato.set_reason(_lineaFichero.substring(61, 71));
                                    }

                                    _repetido = true;
                                }
                            }
                            if (_repetido == false)
                            {
                                //_anneal.set_indice(_contador);
                                _anneal.set_badgeId(_lineaFichero.substring(0, 7));
                                _anneal.set_e1(Double.parseDouble(_lineaFichero.substring(21, 29)));
                                _anneal.set_e2(Double.parseDouble(_lineaFichero.substring(31, 39)));
                                _anneal.set_e3(Double.parseDouble(_lineaFichero.substring(41, 49)));
                                _anneal.set_e4(Double.parseDouble(_lineaFichero.substring(51, 59)));


                                if (_lineaFichero.length() == 61)
                                {
                                    _anneal.set_reason("");
                                }
                                if (_lineaFichero.length() == 63)
                                {
                                    _anneal.set_reason(_lineaFichero.substring(61, 63));
                                }
                                if (_lineaFichero.length() == 65)
                                {
                                    _anneal.set_reason(_lineaFichero.substring(61, 65));
                                }
                                if (_lineaFichero.length() == 67)
                                {
                                    _anneal.set_reason(_lineaFichero.substring(61, 67));
                                }
                                if (_lineaFichero.length() == 69)
                                {
                                    _anneal.set_reason(_lineaFichero.substring(61, 69));
                                }
                                if (_lineaFichero.length() == 71)
                                {
                                    _anneal.set_reason(_lineaFichero.substring(61, 71));
                                }

                                //_contador++;
                                _annealList.add(_anneal);
                            }
                            //_repetido = false;

                        } else
                        {
                            //_anneal.set_indice(_contador);
                            _anneal.set_badgeId(_lineaFichero.substring(0, 7));
                            _anneal.set_e1(Double.parseDouble(_lineaFichero.substring(21, 29)));
                            _anneal.set_e2(Double.parseDouble(_lineaFichero.substring(31, 39)));
                            _anneal.set_e3(Double.parseDouble(_lineaFichero.substring(41, 49)));
                            _anneal.set_e4(Double.parseDouble(_lineaFichero.substring(51, 59)));

                            if (_lineaFichero.length() == 61)
                            {
                                _anneal.set_reason("");
                            }
                            if (_lineaFichero.length() == 63)
                            {
                                _anneal.set_reason(_lineaFichero.substring(61, 63));
                            }
                            if (_lineaFichero.length() == 65)
                            {
                                _anneal.set_reason(_lineaFichero.substring(61, 65));
                            }
                            if (_lineaFichero.length() == 67)
                            {
                                _anneal.set_reason(_lineaFichero.substring(61, 67));
                            }
                            if (_lineaFichero.length() == 69)
                            {
                                _anneal.set_reason(_lineaFichero.substring(61, 69));
                            }
                            if (_lineaFichero.length() == 71)
                            {
                                _anneal.set_reason(_lineaFichero.substring(61, 71));
                            }

                            //_contador++;
                            _annealList.add(_anneal);

                        }

                    }
                }
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            /**
             * cerramos el fichero
             */
            try
            {
                if (null != _fr)
                {
                    _fr.close();
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return _annealList;
    }
}
