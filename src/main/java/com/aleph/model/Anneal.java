package com.aleph.model;

public class Anneal
{
    private int _indice;
    private String _badgeId;
    private Double _e1;
    private Double _e2;
    private Double _e3;
    private Double _e4;
    private String _reason;

    public Anneal()
    {
    }

    public Anneal(int _indice, String _badgeId, Double _e1, Double _e2, Double _e3, Double _e4, String _reason)
    {
        this._indice = _indice;
        this._badgeId = _badgeId;
        this._e1 = _e1;
        this._e2 = _e2;
        this._e3 = _e3;
        this._e4 = _e4;
        this._reason = _reason;
    }

    public int get_indice()
    {
        return _indice;
    }

    public void set_indice(int _indice)
    {
        this._indice = _indice;
    }

    public String get_badgeId()
    {
        return _badgeId;
    }

    public void set_badgeId(String _badgeId)
    {
        this._badgeId = _badgeId;
    }

    public Double get_e1()
    {
        return _e1;
    }

    public void set_e1(Double _e1)
    {
        this._e1 = _e1;
    }

    public Double get_e2()
    {
        return _e2;
    }

    public void set_e2(Double _e2)
    {
        this._e2 = _e2;
    }

    public Double get_e3()
    {
        return _e3;
    }

    public void set_e3(Double _e3)
    {
        this._e3 = _e3;
    }

    public Double get_e4()
    {
        return _e4;
    }

    public void set_e4(Double _e4)
    {
        this._e4 = _e4;
    }

    public String get_reason()
    {
        return _reason;
    }

    public void set_reason(String _reason)
    {
        this._reason = _reason;
    }
}
