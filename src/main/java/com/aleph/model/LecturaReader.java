package com.aleph.model;

import com.aleph.tools.DosimetroRecibido;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LecturaReader
{
    public static ArrayList<Lectura> readReport(File _archivoSeleccionado, ArrayList<DosimetroRecibido> _listDosimetroRecibido)
    {
        File _archivo = null;
        FileReader _fr = null;
        BufferedReader _br = null;
        Lectura _lectura;
        ArrayList<Lectura> _lecturaList = new ArrayList<>();

        try
        {
            _archivo = _archivoSeleccionado;
            _fr = new FileReader(_archivo);
            _br = new BufferedReader(_fr);

            String _lineaFichero;
            boolean _repetido;
            int _contador = 1;
            while ((_lineaFichero = _br.readLine()) != null)
            {
                _lectura = new Lectura();
                if (_lineaFichero.length() == 171)
                {
                    _repetido = false;
                    if (_lineaFichero.substring(0, 9).compareTo("Badge ID ") == 0)
                    {

                    } else if (_lineaFichero.substring(0, 9).compareTo("---------") == 0)
                    {

                    } else
                    {
                        if (_lecturaList.size() > 0)
                        {
                            for (Lectura _dato : _lecturaList)
                            {
                                if (_lineaFichero.substring(0, 7).compareTo(_dato.get_badgeId()) == 0)
                                {

                                    for (DosimetroRecibido _recibido : _listDosimetroRecibido)
                                    {
                                        if (_dato.get_badgeId().compareTo(_recibido.get_dosimetroId()) == 0)
                                        {
                                            //_dato.set_prestamoId(_recibido.get_prestamoId());
                                            if (_recibido.get_tipoExp() == 1)
                                            {
                                                if (_recibido.get_tipoRad().compareTo("P") == 0)
                                                {
                                                    if (isNumeric(_lineaFichero.substring(52, 59)) == true)
                                                        _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(52, 59)), 1));
                                                    else
                                                        _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
                                                    if (isNumeric(_lineaFichero.substring(84, 91)) == true)
                                                        _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(84, 91)), 1));
                                                    else
                                                        _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
                                                    if (isNumeric(_lineaFichero.substring(116, 123)) == true)
                                                        _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(116, 123)), 1));
                                                    else
                                                        _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));
                                                } else if (_recibido.get_tipoRad().compareTo("B") == 0)
                                                {
                                                    if (isNumeric(_lineaFichero.substring(60, 67)) == true)
                                                        _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(60, 67)), 1));
                                                    else
                                                        _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
                                                    if (isNumeric(_lineaFichero.substring(92, 99)) == true)
                                                        _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(92, 99)), 1));
                                                    else
                                                        _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
                                                    if (isNumeric(_lineaFichero.substring(124, 131)) == true)
                                                        _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(124, 131)), 1));
                                                    else
                                                        _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));
                                                } else if (_recibido.get_tipoRad().compareTo("N") == 0)
                                                {
                                                    if (isNumeric(_lineaFichero.substring(68, 75)) == true)
                                                        _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(68, 75)), 1));
                                                    else
                                                        _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
                                                    if (isNumeric(_lineaFichero.substring(100, 107)) == true)
                                                        _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(100, 107)), 1));
                                                    else
                                                        _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
                                                    if (isNumeric(_lineaFichero.substring(132, 139)) == true)
                                                        _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(132, 139)), 1));
                                                    else
                                                        _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));

                                                } else
                                                {
                                                    _dato.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
                                                    _dato.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
                                                    _dato.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));
                                                }
                                            } else
                                            {
                                                _dato.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));
                                            }
                                        }
                                    }
//                                    _dato.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
//                                    _dato.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
//                                    _dato.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));
                                    _dato.set_radiacion(_lineaFichero.substring(141, 156));

                                    _repetido = true;
                                }
                            }
                            if (_repetido == false)
                            {
                                _lectura.set_indice(_contador);
                                _lectura.set_badgeId(_lineaFichero.substring(0, 7));

                                for (DosimetroRecibido _recibido : _listDosimetroRecibido)
                                {
                                    if (_lectura.get_badgeId().compareTo(_recibido.get_dosimetroId()) == 0)
                                    {
                                        _lectura.set_prestamoId(_recibido.get_prestamoId());
                                        if (_recibido.get_tipoExp() == 1)
                                        {
                                            if (_recibido.get_tipoRad().compareTo("P") == 0)
                                            {
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(52, 59)), 1));
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(84, 91)), 1));
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(116, 123)), 1));
                                            } else if (_recibido.get_tipoRad().compareTo("B") == 0)
                                            {
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(60, 67)), 1));
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(92, 99)), 1));
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(124, 131)), 1));
                                            } else if (_recibido.get_tipoRad().compareTo("N") == 0)
                                            {
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(68, 75)), 1));
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(100, 107)), 1));
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(132, 139)), 1));
                                            } else
                                            {
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));
                                            }
                                        } else
                                        {
                                            _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));
                                        }
                                    }
                                }

//
//                                _lectura.set_efectiva(Double.parseDouble(_lineaFichero.substring(44, 51)));
//                                _lectura.set_cristalino(Double.parseDouble(_lineaFichero.substring(76, 83)));
//                                _lectura.set_piel(Double.parseDouble(_lineaFichero.substring(108, 115)));
                                _lectura.set_radiacion(_lineaFichero.substring(141, 156));

                                _contador++;
                                _lecturaList.add(_lectura);
                            }
                        } else
                        {
                            _lectura.set_indice(_contador);
                            _lectura.set_badgeId(_lineaFichero.substring(0, 7));

                            for (DosimetroRecibido _recibido : _listDosimetroRecibido)
                            {
                                if (_lectura.get_badgeId().compareTo(_recibido.get_dosimetroId()) == 0)
                                {
                                    _lectura.set_prestamoId(_recibido.get_prestamoId());

                                    if (_recibido.get_tipoExp() == 1)
                                    {
                                        if (_recibido.get_tipoRad().compareTo("P") == 0)
                                        {
                                            if (isNumeric(_lineaFichero.substring(52, 59)) == true)
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(52, 59)), 1));
                                            else
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
                                            if (isNumeric(_lineaFichero.substring(84, 91)) == true)
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(84, 91)), 1));
                                            else
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
                                            if (isNumeric(_lineaFichero.substring(116, 123)) == true)
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(116, 123)), 1));
                                            else
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));

                                        } else if (_recibido.get_tipoRad().compareTo("B") == 0)
                                        {
                                            if (isNumeric(_lineaFichero.substring(60, 67)) == true)
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(60, 67)), 1));
                                            else
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
                                            if (isNumeric(_lineaFichero.substring(92, 99)) == true)
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(92, 99)), 1));
                                            else
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
                                            if (isNumeric(_lineaFichero.substring(124, 131)) == true)
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(124, 131)), 1));
                                            else
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));
                                        } else if (_recibido.get_tipoRad().compareTo("N") == 0)
                                        {
                                            if (isNumeric(_lineaFichero.substring(68, 75)) == true)
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(68, 75)), 1));
                                            else
                                                _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
                                            if (isNumeric(_lineaFichero.substring(100, 107)) == true)
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(100, 107)), 1));
                                            else
                                                _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
                                            if (isNumeric(_lineaFichero.substring(132, 139)) == true)
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(132, 139)), 1));
                                            else
                                                _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));
                                        }
                                    }

                                    _lectura.set_radiacion(_lineaFichero.substring(141, 156));
                                    break;
                                } else
                                {
                                    _lectura.set_prestamoId(-10);

//                                    _lectura.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)), 1));
//                                    _lectura.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)), 1));
//                                    _lectura.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)), 1));

                                    _lectura.set_efectiva(formatearDecimales(-10.0, 1));
                                    _lectura.set_cristalino(formatearDecimales(-10.0, 1));
                                    _lectura.set_piel(formatearDecimales(-10.0, 1));

                                    _lectura.set_radiacion(_lineaFichero.substring(141, 156));
                                }
                            }

                            _contador++;
                            _lecturaList.add(_lectura);
                        }
                    }
                }
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            /**
             * cerramos el fichero
             */
            try
            {
                if (null != _fr)
                {
                    _fr.close();
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return _lecturaList;
    }

    public static ArrayList<Lectura> readReport2(File _archivoSeleccionado)
    {
        File _archivo = null;
        FileReader _fr = null;
        BufferedReader _br = null;
        Lectura _lectura;
        ArrayList<Lectura> _lecturaList = new ArrayList<>();

        try
        {
            _archivo = _archivoSeleccionado;
            _fr = new FileReader(_archivo);
            _br = new BufferedReader(_fr);

            //int i=0;

            String _lineaFichero;
            boolean _repetido;
            int _contador = 1;
            while ((_lineaFichero = _br.readLine()) != null)
            {
                _lectura = new Lectura();
                if (_lineaFichero.length() == 171)
                {
                    _repetido = false;
                    if (_lineaFichero.substring(0, 9).compareTo("Badge ID ") == 0)
                    {

                    } else if (_lineaFichero.substring(0, 9).compareTo("---------") == 0)
                    {

                    } else
                    {
                        //i++;
                        if (_lecturaList.size() > 0)
                        {

                            for (Lectura _dato : _lecturaList)
                            {
                                if (_lineaFichero.substring(0, 7).compareTo(_dato.get_badgeId()) == 0)
                                {
                                    //_dato.set_efectiva(formatearDecimales(Double.parseDouble(_lineaFichero.substring(44, 51)),1));
                                    //_dato.set_cristalino(formatearDecimales(Double.parseDouble(_lineaFichero.substring(76, 83)),1));
                                    //_dato.set_piel(formatearDecimales(Double.parseDouble(_lineaFichero.substring(108, 115)),1));
                                    //_dato.set_radiacion(_lineaFichero.substring(141, 156));

                                    _repetido = true;
                                }
                            }
                            if (_repetido == false)
                            {
                                _lectura.set_indice(_contador);
                                _lectura.set_badgeId(_lineaFichero.substring(0, 7));

                                //_lectura.set_efectiva(Double.parseDouble(_lineaFichero.substring(44, 51)));
                                //_lectura.set_cristalino(Double.parseDouble(_lineaFichero.substring(76, 83)));
                                //_lectura.set_piel(Double.parseDouble(_lineaFichero.substring(108, 115)));
                                //_lectura.set_radiacion(_lineaFichero.substring(141, 156));

                                _contador++;
                                _lecturaList.add(_lectura);
                            }
                        } else
                        {
                            _lectura.set_indice(_contador);
                            _lectura.set_badgeId(_lineaFichero.substring(0, 7));

                            //_lectura.set_efectiva(Double.parseDouble(_lineaFichero.substring(44, 51)));
                            //_lectura.set_cristalino(Double.parseDouble(_lineaFichero.substring(76, 83)));
                            //_lectura.set_piel(Double.parseDouble(_lineaFichero.substring(108, 115)));
                            //_lectura.set_radiacion(_lineaFichero.substring(141, 156));

                            _contador++;
                            _lecturaList.add(_lectura);
                        }
                    }
                }
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            /**
             * cerramos el fichero
             */
            try
            {
                if (null != _fr)
                {
                    _fr.close();
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return _lecturaList;
    }

    public static Double formatearDecimales(Double numero, Integer numeroDecimales)
    {
        return Math.round(numero * Math.pow(10, numeroDecimales)) / Math.pow(10, numeroDecimales);
    }

    public static boolean isNumeric(String cadena)
    {
        boolean resultado;

        try
        {
            Double.parseDouble(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion)
        {
            resultado = false;
        }

        return resultado;
    }
}
