package com.aleph.model;

import javafx.scene.control.CheckBox;

public class Poe
{
    private Integer _poeId;
    private String _dni;
    private String _nombres;
    private String _apellidos;
    private char _sexo;
    //variables extra
    private String _nombreCompleto;
    private String _tipoDos;
    private int _tipoExp;
    private String _tipoRad;
    private int _prestamoId;
    private String _dosimetroId;
    private CheckBox _cboxExterno;
    private boolean _externo;

    public Poe()
    {
        this._cboxExterno = new CheckBox();
    }

    public Integer get_poeId()
    {
        return _poeId;
    }

    public void set_poeId(Integer _poeId)
    {
        this._poeId = _poeId;
    }

    public String get_dni()
    {
        return _dni;
    }

    public void set_dni(String _dni)
    {
        this._dni = _dni;
    }

    public String get_nombres()
    {
        return _nombres;
    }

    public void set_nombres(String _nombres)
    {
        this._nombres = _nombres;
    }

    public String get_apellidos()
    {
        return _apellidos;
    }

    public void set_apellidos(String _apellidos)
    {
        this._apellidos = _apellidos;
    }

    public char get_sexo()
    {
        return _sexo;
    }

    public void set_sexo(char _sexo)
    {
        this._sexo = _sexo;
    }

    public String get_nombreCompleto()
    {
        return get_apellidos().toUpperCase() + " " + get_nombres();
    }

    public void set_nombreCompleto(String _nombreCompleto)
    {
        this._nombreCompleto = _nombreCompleto;
    }

    public String get_tipoDos()
    {
        return _tipoDos;
    }

    public void set_tipoDos(String _tipoDos)
    {
        this._tipoDos = _tipoDos;
    }

    public int get_tipoExp()
    {
        return _tipoExp;
    }

    public void set_tipoExp(int _tipoExp)
    {
        this._tipoExp = _tipoExp;
    }

    public String get_tipoRad()
    {
        return _tipoRad;
    }

    public void set_tipoRad(String _tipoRad)
    {
        this._tipoRad = _tipoRad;
    }

    public int get_prestamoId()
    {
        return _prestamoId;
    }

    public void set_prestamoId(int _prestamoId)
    {
        this._prestamoId = _prestamoId;
    }

    public String get_dosimetroId()
    {
        return _dosimetroId;
    }

    public void set_dosimetroId(String _dosimetroId)
    {
        this._dosimetroId = _dosimetroId;
    }

    public CheckBox get_cboxExterno()
    {
        return _cboxExterno;
    }

    public void set_cboxExterno(CheckBox _cboxExterno)
    {
        this._cboxExterno = _cboxExterno;
    }

    public boolean is_externo()
    {
        return _externo;
    }

    public void set_externo(boolean _externo)
    {
        this._externo = _externo;
    }
}