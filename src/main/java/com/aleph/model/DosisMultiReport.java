package com.aleph.model;

public class DosisMultiReport
{
    private String _poeId;
    private String _poeNom;
    private String _poeSexo;
    private String _poeTde;
    private String _rad;
    private String _mensualEfectiva;
    private String _mensualCristalino;
    private String _mensualPiel;
    private String _anualEfectiva;
    private String _anualCristalino;
    private String _anualPiel;
    private String _dosAjuste;
    private String _dosRep;
    private String _dosMes;
    private String _dosYear;
    private String _dosNumero;

    public DosisMultiReport()
    {
    }

    public String get_poeId()
    {
        return _poeId;
    }

    public void set_poeId(String _poeId)
    {
        this._poeId = _poeId;
    }

    public String get_poeNom()
    {
        return _poeNom;
    }

    public void set_poeNom(String _poeNom)
    {
        this._poeNom = _poeNom;
    }

    public String get_poeSexo()
    {
        return _poeSexo;
    }

    public void set_poeSexo(String _poeSexo)
    {
        this._poeSexo = _poeSexo;
    }

    public String get_poeTde()
    {
        return _poeTde;
    }

    public void set_poeTde(String _poeTde)
    {
        this._poeTde = _poeTde;
    }

    public String get_rad()
    {
        return _rad;
    }

    public void set_rad(String _rad)
    {
        this._rad = _rad;
    }

    public String get_mensualEfectiva()
    {
        return _mensualEfectiva;
    }

    public void set_mensualEfectiva(String _mensualEfectiva)
    {
        this._mensualEfectiva = _mensualEfectiva;
    }

    public String get_mensualCristalino()
    {
        return _mensualCristalino;
    }

    public void set_mensualCristalino(String _mensualCristalino)
    {
        this._mensualCristalino = _mensualCristalino;
    }

    public String get_mensualPiel()
    {
        return _mensualPiel;
    }

    public void set_mensualPiel(String _mensualPiel)
    {
        this._mensualPiel = _mensualPiel;
    }

    public String get_anualEfectiva()
    {
        return _anualEfectiva;
    }

    public void set_anualEfectiva(String _anualEfectiva)
    {
        this._anualEfectiva = _anualEfectiva;
    }

    public String get_anualCristalino()
    {
        return _anualCristalino;
    }

    public void set_anualCristalino(String _anualCristalino)
    {
        this._anualCristalino = _anualCristalino;
    }

    public String get_anualPiel()
    {
        return _anualPiel;
    }

    public void set_anualPiel(String _anualPiel)
    {
        this._anualPiel = _anualPiel;
    }

    public String get_dosAjuste()
    {
        return _dosAjuste;
    }

    public void set_dosAjuste(String _dosAjuste)
    {
        this._dosAjuste = _dosAjuste;
    }

    public String get_dosRep()
    {
        return _dosRep;
    }

    public void set_dosRep(String _dosRep)
    {
        this._dosRep = _dosRep;
    }

    public String get_dosMes()
    {
        return _dosMes;
    }

    public void set_dosMes(String _dosMes)
    {
        this._dosMes = _dosMes;
    }

    public String get_dosYear()
    {
        return _dosYear;
    }

    public void set_dosYear(String _dosYear)
    {
        this._dosYear = _dosYear;
    }

    public String get_dosNumero()
    {
        return _dosNumero;
    }

    public void set_dosNumero(String _dosNumero)
    {
        this._dosNumero = _dosNumero;
    }
}
