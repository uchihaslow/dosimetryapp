package com.aleph.model;

public class ReporteGuia
{
    private int _codigo;
    private String _nombre;
    private String _tde;
    private String _rad;
    private String _numero;

    public ReporteGuia()
    {
    }

    public ReporteGuia(int _codigo, String _nombre, String _tde, String _rad, String _numero)
    {
        this._codigo = _codigo;
        this._nombre = _nombre;
        this._tde = _tde;
        this._rad = _rad;
        this._numero = _numero;
    }

    public int get_codigo()
    {
        return _codigo;
    }

    public void set_codigo(int _codigo)
    {
        this._codigo = _codigo;
    }

    public String get_nombre()
    {
        return _nombre;
    }

    public void set_nombre(String _nombre)
    {
        this._nombre = _nombre;
    }

    public String get_tde()
    {
        return _tde;
    }

    public void set_tde(String _tde)
    {
        this._tde = _tde;
    }

    public String get_rad()
    {
        return _rad;
    }

    public void set_rad(String _rad)
    {
        this._rad = _rad;
    }

    public String get_numero()
    {
        return _numero;
    }

    public void set_numero(String _numero)
    {
        this._numero = _numero;
    }
}
