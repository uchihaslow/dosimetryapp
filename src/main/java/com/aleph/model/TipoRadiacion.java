package com.aleph.model;

public class TipoRadiacion
{
    private int _tiporadId;
    private boolean _rayosXgAlta;
    private boolean _rayosXgMedia;
    private boolean _rayosXgBaja;
    private boolean _neutrones;
    private boolean _beta;
    private boolean _rayosXg;

    public TipoRadiacion()
    {

    }

    public int get_tiporadId()
    {
        return _tiporadId;
    }

    public void set_tiporadId(int _tiporadId)
    {
        this._tiporadId = _tiporadId;
    }

    public boolean is_rayosXgAlta()
    {
        return _rayosXgAlta;
    }

    public void set_rayosXgAlta(boolean _rayosXgAlta)
    {
        this._rayosXgAlta = _rayosXgAlta;
    }

    public boolean is_rayosXgMedia()
    {
        return _rayosXgMedia;
    }

    public void set_rayosXgMedia(boolean _rayosXgMedia)
    {
        this._rayosXgMedia = _rayosXgMedia;
    }

    public boolean is_rayosXgBaja()
    {
        return _rayosXgBaja;
    }

    public void set_rayosXgBaja(boolean _rayosXgBaja)
    {
        this._rayosXgBaja = _rayosXgBaja;
    }

    public boolean is_neutrones()
    {
        return _neutrones;
    }

    public void set_neutrones(boolean _neutrones)
    {
        this._neutrones = _neutrones;
    }

    public boolean is_beta()
    {
        return _beta;
    }

    public void set_beta(boolean _beta)
    {
        this._beta = _beta;
    }

    public boolean is_rayosXg()
    {
        return _rayosXg;
    }

    public void set_rayosXg(boolean _rayosXg)
    {
        this._rayosXg = _rayosXg;
    }

    @Override
    public String toString()
    {
        return "TipoRadiacion{" +
                "_tiporadId=" + _tiporadId +
                ", _rayosXgAlta=" + _rayosXgAlta +
                ", _rayosXgMedia=" + _rayosXgMedia +
                ", _rayosXgBaja=" + _rayosXgBaja +
                ", _neutrones=" + _neutrones +
                ", _beta=" + _beta +
                ", _rayosXg=" + _rayosXg +
                '}';
    }
}
