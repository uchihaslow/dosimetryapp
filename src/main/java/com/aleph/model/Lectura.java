package com.aleph.model;

public class Lectura
{
    private int _indice;
    private int _prestamoId;
    private String _badgeId;
    private double _elem1;
    private double _elem2;
    private double _elem3;
    private double _elem4;
    private Double _efectiva;
    private Double _cristalino;
    private Double _piel;
    private String _radiacion;

    public Lectura()
    {
    }

    public Lectura(int _indice, String _badgeId)
    {
        this._indice = _indice;
        this._badgeId = _badgeId;
    }

    public int get_prestamoId()
    {
        return _prestamoId;
    }

    public void set_prestamoId(int _prestamoId)
    {
        this._prestamoId = _prestamoId;
    }

    public int get_indice()
    {
        return _indice;
    }

    public void set_indice(int _indice)
    {
        this._indice = _indice;
    }

    public String get_badgeId()
    {
        return _badgeId;
    }

    public void set_badgeId(String _badgeId)
    {
        this._badgeId = _badgeId;
    }

    public double get_elem1()
    {
        return _elem1;
    }

    public void set_elem1(double _elem1)
    {
        this._elem1 = _elem1;
    }

    public double get_elem2()
    {
        return _elem2;
    }

    public void set_elem2(double _elem2)
    {
        this._elem2 = _elem2;
    }

    public double get_elem3()
    {
        return _elem3;
    }

    public void set_elem3(double _elem3)
    {
        this._elem3 = _elem3;
    }

    public double get_elem4()
    {
        return _elem4;
    }

    public void set_elem4(double _elem4)
    {
        this._elem4 = _elem4;
    }

    public Double get_efectiva()
    {
        return _efectiva;
    }

    public void set_efectiva(Double _efectiva)
    {
        this._efectiva = _efectiva;
    }

    public Double get_cristalino()
    {
        return _cristalino;
    }

    public void set_cristalino(Double _cristalino)
    {
        this._cristalino = _cristalino;
    }

    public Double get_piel()
    {
        return _piel;
    }

    public void set_piel(Double _piel)
    {
        this._piel = _piel;
    }

    public String get_radiacion()
    {
        return _radiacion;
    }

    public void set_radiacion(String _radiacion)
    {
        this._radiacion = _radiacion;
    }
}
