package com.aleph.model;

import javafx.scene.control.CheckBox;

import java.util.Date;

public class Prestamo
{
    private Integer _prestamoId;
    private Date _fechaSalida;
    private Date _fechaLlegada;
    private int _estado;
    private int _tipoExpo;
    private String _tipoRad;
    private String _tipoDosi;
    private boolean _externo;

    public Prestamo()
    {
    }

    public Prestamo(Integer _prestamoId, Date _fechaSalida, Date _fechaLlegada, int _estado, int _tipoExpo, String _tipoRad, String _tipoDosi)
    {
        this._prestamoId = _prestamoId;
        this._fechaSalida = _fechaSalida;
        this._fechaLlegada = _fechaLlegada;
        this._estado = _estado;
        this._tipoExpo = _tipoExpo;
        this._tipoRad = _tipoRad;
        this._tipoDosi = _tipoDosi;
    }

    public Integer get_prestamoId()
    {
        return _prestamoId;
    }

    public void set_prestamoId(Integer _prestamoId)
    {
        this._prestamoId = _prestamoId;
    }

    public Date get_fechaSalida()
    {
        return _fechaSalida;
    }

    public void set_fechaSalida(Date _fechaSalida)
    {
        this._fechaSalida = _fechaSalida;
    }

    public Date get_fechaLlegada()
    {
        return _fechaLlegada;
    }

    public void set_fechaLlegada(Date _fechaLlegada)
    {
        this._fechaLlegada = _fechaLlegada;
    }

    public int get_estado()
    {
        return _estado;
    }

    public void set_estado(int _estado)
    {
        this._estado = _estado;
    }

    public int get_tipoExpo()
    {
        return _tipoExpo;
    }

    public void set_tipoExpo(int _tipoExpo)
    {
        this._tipoExpo = _tipoExpo;
    }

    public String get_tipoRad()
    {
        return _tipoRad;
    }

    public void set_tipoRad(String _tipoRad)
    {
        this._tipoRad = _tipoRad;
    }

    public String get_tipoDosi()
    {
        return _tipoDosi;
    }

    public void set_tipoDosi(String _tipoDosi)
    {
        this._tipoDosi = _tipoDosi;
    }

    public boolean is_externo()
    {
        return _externo;
    }

    public void set_externo(boolean _externo)
    {
        this._externo = _externo;
    }
}
