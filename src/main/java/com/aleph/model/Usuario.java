package com.aleph.model;

public class Usuario
{
    private Integer _usuarioId;
    private String _nombreUsuario;
    private String _clave;
    private int _access;

    public Usuario()
    {

    }

    public Usuario(String _nombreUsuario, String _clave)
    {
        this._nombreUsuario = _nombreUsuario;
        this._clave = _clave;
    }

    public Integer get_usuarioId()
    {
        return _usuarioId;
    }

    public void set_usuarioId(Integer _usuarioId)
    {
        this._usuarioId = _usuarioId;
    }

    public String get_nombreUsuario()
    {
        return _nombreUsuario;
    }

    public void set_nombreUsuario(String _nombreUsuario)
    {
        this._nombreUsuario = _nombreUsuario;
    }

    public String get_clave()
    {
        return _clave;
    }

    public void set_clave(String _clave)
    {
        this._clave = _clave;
    }

    public int get_access()
    {
        return _access;
    }

    public void set_access(int _access)
    {
        this._access = _access;
    }

    @Override
    public String toString()
    {
        return "Usuario [_usuarioId=" + _usuarioId + ", _nombreUsuario=" + _nombreUsuario + ", _clave=" + _clave + "]";
    }

}
