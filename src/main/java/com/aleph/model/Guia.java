package com.aleph.model;

import java.time.LocalDate;
import java.util.Date;

public class Guia
{
    private Integer _guiaId;
    private LocalDate _periodo;
    private int _estado;
    private String _nroOrdenPedido;
    private LocalDate _fechaOrdenPedido;
    private String _externo;

    public Guia()
    {
    }

    public Integer get_guiaId()
    {
        return _guiaId;
    }

    public void set_guiaId(Integer _guiaId)
    {
        this._guiaId = _guiaId;
    }

    public LocalDate get_periodo()
    {
        return _periodo;
    }

    public void set_periodo(LocalDate _periodo)
    {
        this._periodo = _periodo;
    }

    public int get_estado()
    {
        return _estado;
    }

    public void set_estado(int _estado)
    {
        this._estado = _estado;
    }

    public String get_nroOrdenPedido()
    {
        return _nroOrdenPedido;
    }

    public void set_nroOrdenPedido(String _nroOrdenPedido)
    {
        this._nroOrdenPedido = _nroOrdenPedido;
    }

    public LocalDate get_fechaOrdenPedido()
    {
        return _fechaOrdenPedido;
    }

    public void set_fechaOrdenPedido(LocalDate _fechaOrdenPedido)
    {
        this._fechaOrdenPedido = _fechaOrdenPedido;
    }

    public String get_externo()
    {
        return _externo;
    }

    public void set_externo(String _externo)
    {
        this._externo = _externo;
    }
}
