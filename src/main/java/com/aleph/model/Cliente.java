package com.aleph.model;

public class Cliente
{
    private Integer _clienteId;
    private String _ruc;
    private String _razonSocial;
    private String _direccion;
    private String _telefono;
    private String _email;
    private String _prioridad;

    public Cliente()
    {

    }

    public Cliente(String _ruc, String _razonSocial, String _direccion, String _telefono, String _email, String _prioridad)
    {
        this._ruc = _ruc;
        this._razonSocial = _razonSocial;
        this._direccion = _direccion;
        this._telefono = _telefono;
        this._email = _email;
        this._prioridad = _prioridad;
    }

    public Integer get_clienteId()
    {
        return _clienteId;
    }

    public void set_clienteId(Integer _clienteId)
    {
        this._clienteId = _clienteId;
    }

    public String get_ruc()
    {
        return _ruc;
    }

    public void set_ruc(String _ruc)
    {
        this._ruc = _ruc;
    }

    public String get_razonSocial()
    {
        return _razonSocial;
    }

    public void set_razonSocial(String _razonSocial)
    {
        this._razonSocial = _razonSocial;
    }

    public String get_direccion()
    {
        return _direccion;
    }

    public void set_direccion(String _direccion)
    {
        this._direccion = _direccion;
    }

    public String get_telefono()
    {
        return _telefono;
    }

    public void set_telefono(String _telefono)
    {
        this._telefono = _telefono;
    }

    public String get_email()
    {
        return _email;
    }

    public void set_email(String _email)
    {
        this._email = _email;
    }

    public String get_prioridad()
    {
        return _prioridad;
    }

    public void set_prioridad(String _prioridad)
    {
        this._prioridad = _prioridad;
    }
}
