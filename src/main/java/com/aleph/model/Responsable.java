package com.aleph.model;

public class Responsable
{
    private int _responsableId;
    private String _nombre;
    private String _celular;
    private String _email;
    private boolean _habilitado;

    public Responsable()
    {

    }

    public int get_responsableId()
    {
        return _responsableId;
    }

    public void set_responsableId(int _responsableId)
    {
        this._responsableId = _responsableId;
    }

    public String get_nombre()
    {
        return _nombre;
    }

    public void set_nombre(String _nombre)
    {
        this._nombre = _nombre;
    }

    public String get_celular()
    {
        return _celular;
    }

    public void set_celular(String _celular)
    {
        this._celular = _celular;
    }

    public String get_email()
    {
        return _email;
    }

    public void set_email(String _email)
    {
        this._email = _email;
    }

    public boolean is_habilitado()
    {
        return _habilitado;
    }

    public void set_habilitado(boolean _habilitado)
    {
        this._habilitado = _habilitado;
    }

    @Override
    public String toString()
    {
        return "Responsable{" +
                "_responsableId=" + _responsableId +
                ", _nombre='" + _nombre + '\'' +
                ", _celular='" + _celular + '\'' +
                ", _email='" + _email + '\'' +
                ", _habilitado=" + _habilitado +
                '}';
    }
}
