package com.aleph.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Magazine
{
    private int _id;
    private String _codigo;
    private LocalDateTime _fecha;

    public Magazine()
    {

    }

    public int get_id()
    {
        return _id;
    }

    public void set_id(int _id)
    {
        this._id = _id;
    }

    public String get_codigo()
    {
        return _codigo;
    }

    public void set_codigo(String _codigo)
    {
        this._codigo = _codigo;
    }

    public String get_fecha()
    {
        DateTimeFormatter _dtf = DateTimeFormatter.ofPattern("d MMMM, yyyy h:mm a");
        return _fecha.format(_dtf);
    }

    public void set_fecha(LocalDateTime _fecha)
    {
        this._fecha = _fecha;
    }
}
