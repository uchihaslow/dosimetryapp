package com.aleph.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Log
{
    private Integer _logId;
    private String _info;
    private LocalDateTime _fecha;

    public Log()
    {
    }

    public Integer get_logId()
    {
        return _logId;
    }

    public void set_logId(Integer _logId)
    {
        this._logId = _logId;
    }

    public String get_info()
    {
        return _info;
    }

    public void set_info(String _info)
    {
        this._info = _info;
    }

    public String get_fecha()
    {
        DateTimeFormatter _dtf = DateTimeFormatter.ofPattern("d MMMM, yyyy h:mm a");
        return _fecha.format(_dtf);
    }

    public void set_fecha(LocalDateTime _fecha)
    {
        this._fecha = _fecha;
    }
}