package com.aleph.model;

public class Paths
{
//    public static String _inicioPath = "/view/Inicio_bio.fxml";
    public static String _inicioPath = "/view/Inicio.fxml";
    public static String _menuPath = "/view/Menu.fxml";


    public static String _clientePath = "/view/Cliente.fxml";
    public static String _clienteAgregarPath = "/view/ClienteAgregar.fxml";
    public static String _clienteBuscarPath = "/view/ClienteBuscar.fxml";
    public static String _clienteDetallePath = "/view/ClienteDetalle.fxml";
    public static String _clienteEditarPath = "/view/ClienteEditar.fxml";


    public static String _sedePath = "/view/Sede.fxml";
    public static String _sedeAgregarPath = "/view/SedeAgregar.fxml";
    public static String _sedeBuscarPath = "/view/SedeBuscar.fxml";
    public static String _sedeBuscarClientePath = "/view/SedeBuscarCliente.fxml";
    public static String _sedeDetallePath = "/view/SedeDetalle.fxml";
    public static String _sedeEditarPath = "/view/SedeEditar.fxml";


    public static String _areaPath = "/view/Area.fxml";
    public static String _areaAgregarPath = "/view/AreaAgregar.fxml";
    public static String _areaBuscarClientePath = "/view/AreaBuscarCliente.fxml";
    public static String _areaEditarClientePath = "/view/AreaEditar.fxml";
    public static String _areaDetalleClientePath = "/view/AreaDetalle.fxml";


    public static String _poePath = "/view/Poe.fxml";
    public static String _poeAgregarPath = "/view/PoeAgregar.fxml";
    public static String _poeAsignacionPath = "/view/PoeAsignacion.fxml";
    public static String _poeBuscarPath = "/view/PoeBuscar.fxml";
    public static String _poeBuscarClientePath = "/view/PoeBuscarCliente.fxml";
    public static String _poeBuscarPoePath = "/view/PoeBuscarPoe.fxml";
    public static String _poeDetallePoePath = "/view/PoeDetalle.fxml";
    public static String _poeEditarPoePath = "/view/PoeEditar.fxml";


    public static String _usuarioPath = "/view/Usuario.fxml";
    public static String _usuarioAgregarPath = "/view/UsuarioAgregar.fxml";
    public static String _usuarioBuscarPath = "/view/UsuarioBuscar.fxml";
    public static String _usuarioDetallePath = "/view/UsuarioDetalle.fxml";
    public static String _usuarioSedePath = "/view/UsuarioSede.fxml";


    public static String _dosimetroPath = "/view/Dosimetro.fxml";
    public static String _dosimetroAgregarPath = "/view/DosimetroAgregar.fxml";
    public static String _dosimetroEditarPath = "/view/DosimetroEditar.fxml";
//    public static String _dosimetroBuscarPath = "/view/Dosimetro.fxml";


    public static String _guiaPath = "/view/Guia.fxml";
    public static String _guiaPorAsignarPath = "/view/GuiaPorAsignar.fxml";
    public static String _guiaAsignadosPath = "/view/GuiaAsignados.fxml";
    public static String _guiaAsignacionPath = "/view/GuiaAsignacion.fxml";
    public static String _guiaGuiaCambiosPath = "/view/GuiaCambios.fxml";
    public static String _rowAsignacion = "/view/RowAsignacion.fxml";
    public static String _rowCambios = "/view/RowCambios.fxml";
    public static String _cambiosAgregar = "/view/CambiosAgregar.fxml";


    public static String _procesamientoDatosPath = "/view/ProcesamientoDatos.fxml";
    public static String _annealPath = "/view/Anneal.fxml";
    public static String _lecturaPath = "/view/Lectura.fxml";
    public static String _reportePath = "/view/Reporte.fxml";
    public static String _reporteNoDevueltosPath = "/view/ReporteNoDevueltos.fxml";
    public static String _reporteDevueltos = "/view/ReporteDevueltos.fxml";
    public static String _informeIsoPath = "/view/InformeIso.fxml";
    public static String _recibirPath = "/view/Recibir.fxml";


    public static String _seguimientoPath = "/view/Seguimiento.fxml";
    //public static String _seguimientoBuscarPath = "/view/SeguimientoBuscar.fxml";
    //public static String _seguimientoRecepcionPath = "/view/SeguimientoRecepcion.fxml";


    public static String _ordenPedidoPath = "/view/OrdenPedido.fxml";
    public static String _ordenEstadoPath = "/view/OrdenEstado.fxml";
    public static String _ordenPedidoAgregarPath = "/view/OrdenPedidoAgregar.fxml";
    public static String _ordenPedidoBuscarClientePath = "/view/OrdenPedidoBuscarCliente.fxml";
    public static String _ordenPedidoRecibirOrden = "/view/OrdenPedidoRecibirOrden.fxml";


    public static String _magazinePath = "/view/Magazine.fxml";
    public static String _magazineAgregarPath = "/view/MagazineAgregar.fxml";
}