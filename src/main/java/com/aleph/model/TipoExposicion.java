package com.aleph.model;

public class TipoExposicion
{
    private int _tipoexpId;
    private boolean _cuerpoEntero;
    private boolean _dedoManoDerecha;
    private boolean _dedoManoIzquierda;
    private boolean _munecaDerecha;
    private boolean _munecaIzquierda;
    private boolean _otraParteCuerpo;

    public TipoExposicion()
    {
    }

    public int get_tipoexpId()
    {
        return _tipoexpId;
    }

    public void set_tipoexpId(int _tipoexpId)
    {
        this._tipoexpId = _tipoexpId;
    }

    public boolean is_cuerpoEntero()
    {
        return _cuerpoEntero;
    }

    public void set_cuerpoEntero(boolean _cuerpoEntero)
    {
        this._cuerpoEntero = _cuerpoEntero;
    }

    public boolean is_dedoManoDerecha()
    {
        return _dedoManoDerecha;
    }

    public void set_dedoManoDerecha(boolean _dedoManoDerecha)
    {
        this._dedoManoDerecha = _dedoManoDerecha;
    }

    public boolean is_dedoManoIzquierda()
    {
        return _dedoManoIzquierda;
    }

    public void set_dedoManoIzquierda(boolean _dedoManoIzquierda)
    {
        this._dedoManoIzquierda = _dedoManoIzquierda;
    }

    public boolean is_munecaDerecha()
    {
        return _munecaDerecha;
    }

    public void set_munecaDerecha(boolean _munecaDerecha)
    {
        this._munecaDerecha = _munecaDerecha;
    }

    public boolean is_munecaIzquierda()
    {
        return _munecaIzquierda;
    }

    public void set_munecaIzquierda(boolean _munecaIzquierda)
    {
        this._munecaIzquierda = _munecaIzquierda;
    }

    public boolean is_otraParteCuerpo()
    {
        return _otraParteCuerpo;
    }

    public void set_otraParteCuerpo(boolean _otraParteCuerpo)
    {
        this._otraParteCuerpo = _otraParteCuerpo;
    }
}
