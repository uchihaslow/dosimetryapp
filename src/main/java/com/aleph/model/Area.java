package com.aleph.model;

public class Area
{
    private Integer _areaId;
    private String _nombre;
    private String _contacto;

    public Area()
    {

    }

    public Integer get_areaId()
    {
        return _areaId;
    }

    public void set_areaId(Integer _areaId)
    {
        this._areaId = _areaId;
    }

    public String get_nombre()
    {
        return _nombre;
    }

    public void set_nombre(String _nombre)
    {
        this._nombre = _nombre;
    }

    public String get_contacto()
    {
        return _contacto;
    }

    public void set_contacto(String _contacto)
    {
        this._contacto = _contacto;
    }

    @Override
    public String toString()
    {
        return _nombre;
    }
}
