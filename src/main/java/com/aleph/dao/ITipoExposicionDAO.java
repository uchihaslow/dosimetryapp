package com.aleph.dao;

import com.aleph.model.TipoExposicion;

import java.util.List;

public interface ITipoExposicionDAO
{
    public void addTipoExposicion(TipoExposicion _tipoExposicion, int _poeId);

    public List<TipoExposicion> listTipoExposicion();

    public void removeTipoExposicion(int _idBuscado);

    public void updateTipoExposicion(TipoExposicion _tipoExposicion);

    public TipoExposicion findTipoExposicionByPoe(int _idPoe);
}
