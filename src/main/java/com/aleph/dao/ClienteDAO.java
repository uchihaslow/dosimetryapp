package com.aleph.dao;

import com.aleph.model.Cliente;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO extends PgSQLDatabase implements IClienteDAO
{

    @Override
    public int addCliente(Cliente _cliente)
    {
        int _error = 0;
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_cliente\n" +
                                    "    (\n" +
                                    "        ruc_text,\n" +
                                    "        razon_social_text,\n" +
                                    "        direccion_text,\n" +
                                    "        telefono_text,\n" +
                                    "        email_text,\n" +
                                    "        prioridad_text\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    );"
                    );
            _pst.setString(1, _cliente.get_ruc());
            _pst.setString(2, _cliente.get_razonSocial());
            _pst.setString(3, _cliente.get_direccion());
            _pst.setString(4, _cliente.get_telefono());
            _pst.setString(5, _cliente.get_email());
            _pst.setString(6, _cliente.get_prioridad());
            _pst.executeUpdate();
            _error = 1;
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Cliente METODO addCliente() :" + e.getMessage());
            System.err.println("Code  : " + e.getErrorCode());
            System.err.println("Causa : " + e.getCause());
        } finally
        {
            cerrarConexion();
        }
        return _error;
    }

    @Override
    public List<Cliente> listCliente()
    {
        ArrayList<Cliente> _clienteList = new ArrayList<Cliente>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    ds_cliente.pk_cliente_id_int,\n" +
                                    "    ds_cliente.ruc_text,\n" +
                                    "    ds_cliente.razon_social_text,\n" +
                                    "    ds_cliente.direccion_text,\n" +
                                    "    ds_cliente.telefono_text,\n" +
                                    "    ds_cliente.email_text,\n" +
                                    "    ds_cliente.prioridad_text\n" +
                                    "FROM\n" +
                                    "    ds_cliente\n" +
                                    "WHERE\n" +
                                    "    ds_cliente.habilitado_bool = true ;"
                    );
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Cliente _cliente = new Cliente();
                _cliente.set_clienteId(_rs.getInt("pk_cliente_id_int"));
                _cliente.set_ruc(_rs.getString("ruc_text"));
                _cliente.set_razonSocial(_rs.getString("razon_social_text"));
                _cliente.set_direccion(_rs.getString("direccion_text"));
                _cliente.set_telefono(_rs.getString("telefono_text"));
                _cliente.set_email(_rs.getString("email_text"));
                _cliente.set_prioridad(_rs.getString("prioridad_text"));
                _clienteList.add(_cliente);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Cliente METODO listCliente : " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _clienteList;
    }

    @Override
    public void removeCliente(int _idBuscado)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_cliente\n" +
                                    "SET\n" +
                                    "    habilitado_bool = false\n" +
                                    "WHERE\n" +
                                    "    pk_cliente_id_int = ?"
                    );
            _pst.setInt(1, _idBuscado);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Cliente METODO removeCliente" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void updateCliente(Cliente _cliente)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_cliente\n" +
                                    "SET\n" +
                                    "    razon_social_text = ?,\n" +
                                    "    direccion_text = ?,\n" +
                                    "    telefono_text = ?,\n" +
                                    "    email_text = ?,\n" +
                                    "    prioridad_text = ?\n" +
                                    "WHERE\n" +
                                    "    pk_cliente_id_int = ?;"
                    );
            _pst.setString(1, _cliente.get_razonSocial());
            _pst.setString(2, _cliente.get_direccion());
            _pst.setString(3, _cliente.get_telefono());
            _pst.setString(4, _cliente.get_email());
            _pst.setString(5, _cliente.get_prioridad());
            _pst.setInt(6, _cliente.get_clienteId());
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase ClienteDAO error updateCliente() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<Cliente> findClienteById(int _idBuscado)
    {
        ArrayList<Cliente> _clienteList = new ArrayList<Cliente>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    ds_cliente.pk_cliente_id_int,\n" +
                                    "    ds_cliente.ruc_text,\n" +
                                    "    ds_cliente.razon_social_text,\n" +
                                    "    ds_cliente.direccion_text,\n" +
                                    "    ds_cliente.telefono_text,\n" +
                                    "    ds_cliente.email_text,\n" +
                                    "    ds_cliente.prioridad_text\n" +
                                    "FROM\n" +
                                    "    ds_cliente\n" +
                                    "WHERE\n" +
                                    "    ds_cliente.pk_cliente_id_int = ?\n" +
                                    "AND ds_cliente.habilitado_bool = true ;"
                    );
            _pst.setInt(1, _idBuscado);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Cliente _cliente = new Cliente();
                _cliente.set_clienteId(_rs.getInt("pk_cliente_id_int"));
                _cliente.set_ruc(_rs.getString("ruc_text"));
                _cliente.set_razonSocial(_rs.getString("razon_social_text"));
                _cliente.set_direccion(_rs.getString("direccion_text"));
                _cliente.set_telefono(_rs.getString("telefono_text"));
                _cliente.set_email(_rs.getString("email_text"));
                _cliente.set_prioridad(_rs.getString("prioridad_text"));
                _clienteList.add(_cliente);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Cliente METODO findClienteById" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _clienteList;
    }

    @Override
    public List<Cliente> findClienteByNombre(String _nombreBuscado)
    {
        ArrayList<Cliente> _clienteList = new ArrayList<Cliente>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    ds_cliente.pk_cliente_id_int,\n" +
                                    "    ds_cliente.ruc_text,\n" +
                                    "    ds_cliente.razon_social_text,\n" +
                                    "    ds_cliente.direccion_text,\n" +
                                    "    ds_cliente.telefono_text,\n" +
                                    "    ds_cliente.email_text,\n" +
                                    "    ds_cliente.prioridad_text\n" +
                                    "FROM\n" +
                                    "    ds_cliente\n" +
                                    "WHERE\n" +
                                    "    LOWER(ds_cliente.razon_social_text) LIKE LOWER(?)\n" +
                                    "AND ds_cliente.habilitado_bool = true ;"
                    );
            _pst.setString(1, "%" + _nombreBuscado + "%");
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Cliente _cliente = new Cliente();
                _cliente.set_clienteId(_rs.getInt("pk_cliente_id_int"));
                _cliente.set_ruc(_rs.getString("ruc_text"));
                _cliente.set_razonSocial(_rs.getString("razon_social_text"));
                _cliente.set_direccion(_rs.getString("direccion_text"));
                _cliente.set_telefono(_rs.getString("telefono_text"));
                _cliente.set_email(_rs.getString("email_text"));
                _cliente.set_prioridad(_rs.getString("prioridad_text"));
                _clienteList.add(_cliente);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Cliente METODO findClienteByNombre : " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _clienteList;
    }

    @Override
    public List<Cliente> findClienteByRuc(String _rucBuscado)
    {
        ArrayList<Cliente> _clienteList = new ArrayList<Cliente>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    ds_cliente.pk_cliente_id_int,\n" +
                                    "    ds_cliente.ruc_text,\n" +
                                    "    ds_cliente.razon_social_text,\n" +
                                    "    ds_cliente.direccion_text,\n" +
                                    "    ds_cliente.telefono_text,\n" +
                                    "    ds_cliente.email_text,\n" +
                                    "    ds_cliente.prioridad_text\n" +
                                    "FROM\n" +
                                    "    ds_cliente\n" +
                                    "WHERE\n" +
                                    "    ds_cliente.ruc_text LIKE ?\n" +
                                    "AND ds_cliente.habilitado_bool = true ;"
                    );
            _pst.setString(1, "%" + _rucBuscado + "%");
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Cliente _cliente = new Cliente();
                _cliente.set_clienteId(_rs.getInt("pk_cliente_id_int"));
                _cliente.set_ruc(_rs.getString("ruc_text"));
                _cliente.set_razonSocial(_rs.getString("razon_social_text"));
                _cliente.set_direccion(_rs.getString("direccion_text"));
                _cliente.set_telefono(_rs.getString("telefono_text"));
                _cliente.set_email(_rs.getString("email_text"));
                _cliente.set_prioridad(_rs.getString("prioridad_text"));
                _clienteList.add(_cliente);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Cliente METODO findClienteByRuc" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _clienteList;
    }
}