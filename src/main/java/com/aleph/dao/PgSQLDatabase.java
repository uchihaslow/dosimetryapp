package com.aleph.dao;

import com.aleph.controller.AlertBox;
import javafx.scene.control.Alert;

import java.sql.*;

public class PgSQLDatabase
{
    /**
     * Conexión Remota
     */

//    private static final String URL = "jdbc:postgresql://192.168.1.175:5432/";
//    private static final String URL = "jdbc:postgresql://192.168.56.101:5432/";
//    private static final String URL = "jdbc:postgresql://192.168.1.150:5432/";
//    private static final String URL = "jdbc:postgresql://192.168.0.105:5432/";

//    private static final String DATABASE = "postgres";
//    private static final String SCHEMA = "?currentSchema=prod2";
//    private static final String SCHEMA = "?currentSchema=prod3";
//    private static final String DRIVER = "org.postgresql.Driver";
//    private static final String USERNAME = "postgres";
//    private static final String PASSWORD = "aleph";
//    private static final String PASSWORD = "upao";

    /**
     * Conexión Cloud
     *
     * @param DATABASE se le agrega ?sslmode=require de lo contrario no conectará con la database en la nube
     */
//    private static final String URL = "jdbc:postgresql://ec2-79-125-125-97.eu-west-1.compute.amazonaws.com:5432/";
//    private static final String DATABASE = "dc48ffkhif4iok?sslmode=require";
//    private static final String DRIVER = "org.postgresql.Driver";
//    private static final String USERNAME = "iwbruxlidvlkij";
//    private static final String PASSWORD = "a99ba276a01f11c972ca2d0f973f4af12ee056310fe69b2cb4ecd3374f3ac6de";


    /**
     * Conexión con AmazonRDS_Desarrollo
     */
//    private static final String URL = "jdbc:postgresql://dosysdev.cf6eh8v7mzoh.us-east-2.rds.amazonaws.com/";
//        private static final String URL = "jdbc:postgresql://18.218.236.127/";

    //Esquema de base de datos por defecto
//    private static final String SCHEMA = "?currentSchema=public";

    //Esquema
//    private static final String SCHEMA = "?currentSchema=dev1";

// Esquema Test
//    private static final String SCHEMA = "?currentSchema=prod1";

    // Esquema Aleph
//    private static final String SCHEMA = "?currentSchema=prod2";

    //Esquema Bioelectron
//    private static final String SCHEMA = "?currentSchema=prod3";

    //
//    private static final String DATABASE = "dosys";
//    private static final String DRIVER = "org.postgresql.Driver";
//    private static final String USERNAME = "root";
//    private static final String PASSWORD = "4l3phgr0up";


    /**
     * Conexión con Aleph Server
     */
    //private static final String URL = "jdbc:postgresql://192.168.0.55:6100/";
    private static final String URL = "jdbc:postgresql://190.117.249.211:6100/";
//    private static final String URL = "jdbc:postgresql://192.168.0.55:6100/";

    // Esquema Aleph
    //private static final String SCHEMA = "?currentSchema=alephgroup";

    //Esquema Bioelectron
    //private static final String SCHEMA = "?currentSchema=bioelectron";

    //Esquema Pruebas
    private static final String SCHEMA = "?currentSchema=pruebas";

    //alephgroup2 update database
//    private static final String SCHEMA = "?currentSchema=alephgroup2";

    private static final String DATABASE = "dosys";
    private static final String DRIVER = "org.postgresql.Driver";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "PgGr0up274/*-";


    protected Connection _conn;
    protected Statement _stm;

    public void abrirConexion()
    {
        _conn = null;
        try
        {
            Class.forName(DRIVER);
            _conn = DriverManager.getConnection(URL + DATABASE + SCHEMA, USERNAME, PASSWORD);
            _stm = _conn.createStatement();
            System.out.println("Base de datos conectada");
        } catch (SQLException e)
        {
            System.err.println("Error 1 : " + e);
            new AlertBox("Error de conexión, consultar con su proveedor", "Error de Conexión", new Alert(Alert.AlertType.ERROR));
        } catch (ClassNotFoundException e)
        {
            System.err.println("Error 2 : " + e);
        }
    }

    public void cerrarConexion()
    {
        try
        {
            if (_conn != null)
            {
                _conn.close();
                System.out.println("Base de datos cerrada");
            }
        } catch (SQLException e)
        {
            System.err.println("Error 3 al cerrar la conexion : " + e);
        }
    }
}
