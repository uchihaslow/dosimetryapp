package com.aleph.dao;

import com.aleph.model.Dosimetro;
import javafx.collections.ObservableList;

import java.util.List;

public interface IDosimetroDAO
{
    public void addDosimetro(Dosimetro _dosimetro);

    public void removeDosimetro(String _dosimetroCodigo);

    public void updateDosimetro(Dosimetro _dosimetro);

    public List<Dosimetro> findDosimetroByCodigo(String _codigo);

    public void changeToAsignado(String _dosimetroId);

    public ObservableList NoDevueltos(int _guiaId);

    public ObservableList Devueltos(int _guiaId);
}
