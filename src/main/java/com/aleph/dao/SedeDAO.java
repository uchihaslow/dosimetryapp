package com.aleph.dao;

import com.aleph.model.Sede;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SedeDAO extends PgSQLDatabase implements ISedeDAO
{

    @Override
    public int addSede(Sede _sede)
    {
        int _codigo_ultimo_sede_id = 0;

        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_sede\n" +
                                    "    (\n" +
                                    "        nombre_text,\n" +
                                    "        direccion_text,\n" +
                                    "        distrito_text,\n" +
                                    "        fk_cliente_id_int\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    )\n" +
                                    "    RETURNING pk_sede_id_int;"
                    );
            _pst.setString(1, _sede.get_nombre());
            _pst.setString(2, _sede.get_direccion());
            _pst.setString(3, _sede.get_distrito());
            _pst.setInt(4, _sede.get_idCliente());
            _pst.execute();
            ResultSet _codigo_ultimo_sede = _pst.getResultSet();
            if (_codigo_ultimo_sede.next())
            {
                _codigo_ultimo_sede_id = _codigo_ultimo_sede.getInt(1);
            }

        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Sede METODO addSede() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _codigo_ultimo_sede_id;
    }

    @Override
    public List<Sede> listSede()
    {
        return null;
    }

    @Override
    public void removeSede(int _idBuscado)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_sede\n" +
                                    "SET\n" +
                                    "    habilitado_bool = false\n" +
                                    "WHERE\n" +
                                    "    pk_sede_id_int = ?"
                    );
            _pst.setInt(1, _idBuscado);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Sede METODO removeSede" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void updateSede(Sede _sede)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_sede\n" +
                                    "SET\n" +
                                    "    nombre_text = ?,\n" +
                                    "    direccion_text = ?,\n" +
                                    "    distrito_text = ?\n" +
                                    "WHERE\n" +
                                    "    pk_sede_id_int = ?;\n"
                    );
            _pst.setString(1, _sede.get_nombre());
            _pst.setString(2, _sede.get_direccion());
            _pst.setString(3, _sede.get_distrito());
            _pst.setInt(4, _sede.get_sedeId());
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase SedeDAO error updateSede() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<Sede> findSedeByCliente(int _idCliente)
    {
        ArrayList<Sede> _sedeList = new ArrayList<Sede>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    ("SELECT\n" +
                            "    sd.pk_sede_id_int,\n" +
                            "    sd.nombre_text\n" +
                            "FROM\n" +
                            "    ds_cliente as cl\n" +
                            "INNER JOIN\n" +
                            "    ds_sede as sd\n" +
                            "ON\n" +
                            "    cl.pk_cliente_id_int = sd.fk_cliente_id_int\n" +
                            "WHERE\n" +
                            "    cl.pk_cliente_id_int = ?\n" +
                            "    AND cl.habilitado_bool = true\n" +
                            "    AND sd.habilitado_bool = true "
                    );
            _pst.setInt(1, _idCliente);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Sede _sede = new Sede();
                _sede.set_sedeId(_rs.getInt("pk_sede_id_int"));
                _sede.set_nombre(_rs.getString("nombre_text"));
                _sedeList.add(_sede);
            }
        } catch (SQLException e)
        {
            System.out.println("ERORR CLASE: SedeDAO METODO: findSedeByCliente :" + e);
        } finally
        {
            cerrarConexion();
        }
        return _sedeList;
    }

    @Override
    public List<Sede> findSedeByNombre(String _sedeNom)
    {
        ArrayList<Sede> _sedeList = new ArrayList<Sede>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    cl.razon_social_text,\n" +
                                    "    sd.pk_sede_id_int,\n" +
                                    "    sd.nombre_text,\n" +
                                    "    sd.direccion_text,\n" +
                                    "    sd.distrito_text\n" +
                                    "FROM\n" +
                                    "    ds_cliente as cl\n" +
                                    "INNER JOIN\n" +
                                    "    ds_sede as sd\n" +
                                    "ON\n" +
                                    "    cl.pk_cliente_id_int = sd.fk_cliente_id_int\n" +
                                    "WHERE\n" +
                                    "    LOWER(sd.nombre_text) LIKE LOWER(?)\n" +
                                    "AND sd.habilitado_bool = true\n" +
                                    "AND cl.habilitado_bool = true ;"
                    );
            _pst.setString(1, "%" + _sedeNom + "%");
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Sede _sede = new Sede();
                _sede.set_nombreCliente(_rs.getString("razon_social_text"));
                _sede.set_sedeId(_rs.getInt("pk_sede_id_int"));
                _sede.set_nombre(_rs.getString("nombre_text"));
                _sede.set_direccion(_rs.getString("direccion_text"));
                _sede.set_distrito(_rs.getString("distrito_text"));
                _sedeList.add(_sede);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Sede METODO findSedeByNombre : " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _sedeList;
    }

    @Override
    public List<Sede> findSedeByDireccion(String _direccionBuscada)
    {
        ArrayList<Sede> _sedeList = new ArrayList<Sede>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    cl.razon_social_text,\n" +
                                    "    sd.pk_sede_id_int,\n" +
                                    "    sd.nombre_text,\n" +
                                    "    sd.direccion_text,\n" +
                                    "    sd.distrito_text\n" +
                                    "FROM\n" +
                                    "    ds_cliente as cl\n" +
                                    "INNER JOIN\n" +
                                    "    ds_sede as sd\n" +
                                    "ON\n" +
                                    "    cl.pk_cliente_id_int = sd.fk_cliente_id_int\n" +
                                    "WHERE\n" +
                                    "    LOWER(sd.direccion_text) LIKE LOWER(?)\n" +
                                    "AND sd.habilitado_bool = true\n" +
                                    "AND cl.habilitado_bool = true ;"
                    );
            _pst.setString(1, "%" + _direccionBuscada + "%");
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Sede _sede = new Sede();
                _sede.set_nombreCliente(_rs.getString("razon_social_text"));
                _sede.set_sedeId(_rs.getInt("pk_sede_id_int"));
                _sede.set_nombre(_rs.getString("nombre_text"));
                _sede.set_direccion(_rs.getString("direccion_text"));
                _sede.set_distrito(_rs.getString("distrito_text"));
                _sedeList.add(_sede);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Sede METODO findSedeByDireccion : " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _sedeList;
    }

    @Override
    public List<Sede> findSedeByClienteNom(String _clienteNom)
    {
        ArrayList<Sede> _sedeList = new ArrayList<Sede>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT \n" +
                                    "    cl.razon_social_text, \n" +
                                    "    sd.pk_sede_id_int, \n" +
                                    "    sd.nombre_text, \n" +
                                    "    sd.direccion_text, \n" +
                                    "    sd.distrito_text \n" +
                                    "FROM \n" +
                                    "    ds_cliente as cl \n" +
                                    "INNER JOIN \n" +
                                    "    ds_sede as sd \n" +
                                    "ON \n" +
                                    "    cl.pk_cliente_id_int = sd.fk_cliente_id_int \n" +
                                    "WHERE \n" +
                                    "    LOWER(cl.razon_social_text) LIKE LOWER(?) \n" +
                                    "AND sd.habilitado_bool = true \n" +
                                    "AND cl.habilitado_bool = true ;"
                    );
            _pst.setString(1, "%" + _clienteNom + "%");
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Sede _sede = new Sede();
                _sede.set_nombreCliente(_rs.getString("razon_social_text"));
                _sede.set_sedeId(_rs.getInt("pk_sede_id_int"));
                _sede.set_nombre(_rs.getString("nombre_text"));
                _sede.set_direccion(_rs.getString("direccion_text"));
                _sede.set_distrito(_rs.getString("distrito_text"));
                _sedeList.add(_sede);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Sede METODO findSedeByClienteNom : " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _sedeList;
    }
}
