package com.aleph.dao;

import com.aleph.model.Log;

import java.util.List;

public interface ILogDAO
{
    public void addLog(String _info);

    public List<Log> listLog();
}
