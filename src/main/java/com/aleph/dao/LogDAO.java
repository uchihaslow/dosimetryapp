package com.aleph.dao;

import com.aleph.model.Log;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class LogDAO extends PgSQLDatabase implements ILogDAO
{
    @Override
    public void addLog(String _info)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_log\n" +
                                    "    (\n" +
                                    "        nombre_log_text\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?\n" +
                                    "    );\n"
                    );
            _pst.setString(1, _info);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE LogDAO METODO addlog() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<Log> listLog()
    {
        ArrayList<Log> _logList = new ArrayList<Log>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    pk_log_id_int,\n" +
                                    "    nombre_log_text,\n" +
                                    "    fecha_tstamp\n" +
                                    "FROM\n" +
                                    "    ds_log;\n"
                    );
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Log _log = new Log();
                _log.set_logId(_rs.getInt("pk_log_id_int"));
                _log.set_info(_rs.getString("nombre_log_text"));
                _log.set_fecha(_rs.getObject("fecha_tstamp", LocalDateTime.class));
                _logList.add(_log);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Log METODO listLog" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _logList;
    }

}
