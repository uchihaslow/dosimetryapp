package com.aleph.dao;

import com.aleph.model.Dosimetro;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DosimetroDAO extends PgSQLDatabase implements IDosimetroDAO
{

    @Override
    public void addDosimetro(Dosimetro _dosimetro)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_dosimetro\n" +
                                    "    (\n" +
                                    "        pk_dosimetro_id_text,\n" +
                                    "        tipo_text\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    );"
                    );
            _pst.setString(1, _dosimetro.get_dosimetroCodigo());
            _pst.setString(2, _dosimetro.get_tipo());
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE DosimetroDAO METODO addDosimetro() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void removeDosimetro(String _dosimetroCodigo)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_dosimetro\n" +
                                    "SET\n" +
                                    "    habilitado_bool = false\n" +
                                    "WHERE\n" +
                                    "    pk_dosimetro_id_text = ?;"
                    );
            _pst.setString(1, _dosimetroCodigo);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            e.printStackTrace();
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void updateDosimetro(Dosimetro _dosimetro)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_dosimetro\n" +
                                    "SET\n" +
                                    "    tipo_text = ?\n" +
                                    "WHERE\n" +
                                    "    pk_dosimetro_id_text = ?;"
                    );
            _pst.setString(1, _dosimetro.get_tipo());
            _pst.setString(2, _dosimetro.get_dosimetroCodigo());
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase DosimetroDAO error updateDosimetro() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<Dosimetro> findDosimetroByCodigo(String _codigo)
    {
        ArrayList<Dosimetro> _dosimetroList = new ArrayList<>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    dos.pk_dosimetro_id_text,\n" +
                                    "    dos.tipo_text\n" +
                                    "FROM\n" +
                                    "    ds_dosimetro as dos\n" +
                                    "WHERE\n" +
                                    "    dos.pk_dosimetro_id_text LIKE ?\n" +
                                    "AND dos.habilitado_bool = true ;"
                    );
            _pst.setString(1, "%" + _codigo + "%");
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Dosimetro _dosimetro = new Dosimetro();
                _dosimetro.set_dosimetroCodigo(_rs.getString("pk_dosimetro_id_text"));
                _dosimetro.set_tipo(_rs.getString("tipo_text"));
                _dosimetroList.add(_dosimetro);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE DosimetroDAO METODO findDosimetroByCodigo : " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _dosimetroList;
    }

    public void changeToAsignado(String _dosimetroId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    public.ds_dosimetro\n" +
                                    "SET\n" +
                                    "    estado_text = 'ASIGNADO'\n" +
                                    "WHERE\n" +
                                    "    pk_dosimetro_id_text = ?;\n"
                    );
            _pst.setString(1, _dosimetroId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase DosimetroDAO error changeToAsignado() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public ObservableList NoDevueltos(int _guiaId)
    {
        ObservableList _dosimetrosList = FXCollections.observableArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement(SQL_NO_DEVUELTOS);
            _pst.setInt(1, _guiaId);
            _rs = _pst.executeQuery();
            //DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("MMM/yyyy");
            DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            while (_rs.next())
            {
                ObservableList<String> _row = FXCollections.observableArrayList();
                _row.add(_rs.getString("ruc_text"));//ruc
                _row.add(_rs.getString("razon_social_text"));//razon social
//                _row.add(_rs.getDate("fecha_orden_pedido_date").toLocalDate().format(dtf3).toUpperCase());//fecha OP
                _row.add(_rs.getString("fecha_orden_pedido_date"));//fecha OP
                _row.add(_rs.getString("dni_text"));//dni
                _row.add(_rs.getString("nombres_text"));//nombres
                _row.add(_rs.getString("apellidos_text"));//apellidos
                _row.add(_rs.getString("fk_dosimetro_id_text"));//dosimetros
                _row.add(_rs.getString("prestamo_fecha_salida_date"));
                System.out.println("Row [1] added " + _row);
                _dosimetrosList.add(_row);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE GuiaDAO METODO NoDevueltos: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _dosimetrosList;
    }

    @Override
    public ObservableList Devueltos(int _guiaId)
    {
        ObservableList _dosimetrosList = FXCollections.observableArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement(SQL_DEVUELTOS);
            _pst.setInt(1, _guiaId);
            _rs = _pst.executeQuery();
            //DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("MMM/yyyy");
            DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            while (_rs.next())
            {
                ObservableList<String> _row = FXCollections.observableArrayList();
                _row.add(_rs.getString("ruc_text"));//ruc
                _row.add(_rs.getString("razon_social_text"));//razon social
//                _row.add(_rs.getDate("fecha_orden_pedido_date").toLocalDate().format(dtf3).toUpperCase());//fecha OP
                _row.add(_rs.getString("fecha_orden_pedido_date"));//fecha OP
                _row.add(_rs.getString("dni_text"));//dni
                _row.add(_rs.getString("nombres_text"));//nombres
                _row.add(_rs.getString("apellidos_text"));//apellidos
                _row.add(_rs.getString("fk_dosimetro_id_text"));//dosimetros
                _row.add(_rs.getString("prestamo_fecha_llegada_date"));//fecha llegada
                System.out.println("Row [1] added " + _row);
                _dosimetrosList.add(_row);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE GuiaDAO METODO NoDevueltos: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _dosimetrosList;
    }


    //region SQL QUERY

    private static final String SQL_NO_DEVUELTOS =
            "SELECT \n" +
                    "    cli.ruc_text,\n" +
                    "    cli.razon_social_text,\n" +
                    "    guia.fecha_orden_pedido_date,\n" +
                    "    poe.dni_text,\n" +
                    "    poe.nombres_text,\n" +
                    "    poe.apellidos_text,\n" +
                    "    pres.fk_dosimetro_id_text,\n" +
                    "    pres.prestamo_fecha_salida_date\n" +
                    "FROM \n" +
                    "    ds_cliente cli\n" +
                    "    JOIN ds_sede sede\n" +
                    "    ON cli.pk_cliente_id_int = sede.fk_cliente_id_int\n" +
                    "    JOIN ds_area area \n" +
                    "    ON sede.pk_sede_id_int = area.fk_sede_id_int\n" +
                    "    JOIN ds_guia guia\n" +
                    "    ON area.pk_area_id_int = guia.fk_area_id_int\n" +
                    "    JOIN ds_prestamo pres\n" +
                    "    ON guia.pk_guia_id_int = pres.fk_guia_id_int\n" +
                    "    JOIN ds_poe poe\n" +
                    "    ON poe.pk_poe_id_int = pres.fk_poe_id_int\n" +
                    "WHERE \n" +
                    "    guia.pk_guia_id_int = ?\n" +
                    "    AND pres.estado_int = 2";

    private static final String SQL_DEVUELTOS =
            "SELECT \n" +
                    "    cli.ruc_text,\n" +
                    "    cli.razon_social_text,\n" +
                    "    guia.fecha_orden_pedido_date,\n" +
                    "    poe.dni_text,\n" +
                    "    poe.nombres_text,\n" +
                    "    poe.apellidos_text,\n" +
                    "    pres.fk_dosimetro_id_text,\n" +
                    "    pres.prestamo_fecha_llegada_date\n" +
                    "FROM \n" +
                    "    ds_cliente cli\n" +
                    "    JOIN ds_sede sede\n" +
                    "    ON cli.pk_cliente_id_int = sede.fk_cliente_id_int\n" +
                    "    JOIN ds_area area \n" +
                    "    ON sede.pk_sede_id_int = area.fk_sede_id_int\n" +
                    "    JOIN ds_guia guia\n" +
                    "    ON area.pk_area_id_int = guia.fk_area_id_int\n" +
                    "    JOIN ds_prestamo pres\n" +
                    "    ON guia.pk_guia_id_int = pres.fk_guia_id_int\n" +
                    "    JOIN ds_poe poe\n" +
                    "    ON poe.pk_poe_id_int = pres.fk_poe_id_int\n" +
                    "WHERE \n" +
                    "    guia.pk_guia_id_int = ?\n" +
                    "    AND pres.estado_int = 3";

    //endregion

}