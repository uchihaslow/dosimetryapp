package com.aleph.dao;

import com.aleph.model.Dosimetro;
import com.aleph.model.Magazine;

import java.util.ArrayList;
import java.util.List;

public interface IMagazineDAO
{
    public void addDosimetroLimpio(String _dosimetroId);

    public void deshabilitarDosimetro(String _dosimetroId);

    public ArrayList<String> findDosimetrosLimpios(int _cantidad);

    public boolean findDosimetroExist(String _codigo);

    public ArrayList<Magazine> ListDosimetros();

    public List<Magazine> findDosimetroByCodigo(String _codigo);

    public int CantDosimHabilitados();

}