package com.aleph.dao;

import com.aleph.model.TipoRadiacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TipoRadiacionDAO extends PgSQLDatabase implements ITipoRadiacionDAO
{

    @Override
    public void addTipoRadiacion(TipoRadiacion _tipoRadiacion, int _areaId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_tipo_radiacion\n" +
                                    "    (\n" +
                                    "        rayos_xg_alta_bool,\n" +
                                    "        rayos_xg_media_bool,\n" +
                                    "        rayos_xg_baja_bool,\n" +
                                    "        neutrones_bool,\n" +
                                    "        beta_bool,\n" +
                                    "        rayos_xg_bool,\n" +
                                    "        fk_area_id_int\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    );"
                    );
            _pst.setBoolean(1, _tipoRadiacion.is_rayosXgAlta());
            _pst.setBoolean(2, _tipoRadiacion.is_rayosXgMedia());
            _pst.setBoolean(3, _tipoRadiacion.is_rayosXgBaja());
            _pst.setBoolean(4, _tipoRadiacion.is_neutrones());
            _pst.setBoolean(5, _tipoRadiacion.is_beta());
            _pst.setBoolean(6, _tipoRadiacion.is_rayosXg());
            _pst.setInt(7, _areaId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE TipoRadiacion METODO addTipoRadiacion() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<TipoRadiacion> listTipoRadiacion()
    {
        return null;
    }

    @Override
    public void removeTipoRadiacion(int _idBuscado)
    {

    }

    @Override
    public void updateTipoRadiacion(TipoRadiacion _tipoRadiacion)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_tipo_radiacion\n" +
                                    "SET\n" +
                                    "    rayos_xg_alta_bool = ?,\n" +
                                    "    rayos_xg_media_bool = ?,\n" +
                                    "    rayos_xg_baja_bool = ?,\n" +
                                    "    neutrones_bool = ?,\n" +
                                    "    beta_bool = ?,\n" +
                                    "    rayos_xg_bool = ?\n" +
                                    "WHERE\n" +
                                    "    pk_tipo_radiacion_int = ?;\n"
                    );
            _pst.setBoolean(1, _tipoRadiacion.is_rayosXgAlta());
            _pst.setBoolean(2, _tipoRadiacion.is_rayosXgMedia());
            _pst.setBoolean(3, _tipoRadiacion.is_rayosXgBaja());
            _pst.setBoolean(4, _tipoRadiacion.is_neutrones());
            _pst.setBoolean(5, _tipoRadiacion.is_beta());
            _pst.setBoolean(6, _tipoRadiacion.is_rayosXg());
            _pst.setInt(7, _tipoRadiacion.get_tiporadId());
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase TipoRadiacion error updateTipoRadiacion() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public TipoRadiacion findTipoRadiacionByArea(int _idArea)
    {
        TipoRadiacion _tipoRadiacion = new TipoRadiacion();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    trad.pk_tipo_radiacion_int,\n" +
                                    "    trad.rayos_xg_alta_bool,\n" +
                                    "    trad.rayos_xg_media_bool,\n" +
                                    "    trad.rayos_xg_baja_bool,\n" +
                                    "    trad.neutrones_bool,\n" +
                                    "    trad.beta_bool,\n" +
                                    "    trad.rayos_xg_bool\n" +
                                    "FROM\n" +
                                    "    ds_tipo_radiacion trad\n" +
                                    "WHERE\n" +
                                    "    trad.fk_area_id_int = ?\n" +
                                    "AND trad.habilitado_bool = true ;"
                    );
            _pst.setInt(1, _idArea);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _tipoRadiacion.set_tiporadId(_rs.getInt("pk_tipo_radiacion_int"));
                _tipoRadiacion.set_rayosXgAlta(_rs.getBoolean("rayos_xg_alta_bool"));
                _tipoRadiacion.set_rayosXgMedia(_rs.getBoolean("rayos_xg_media_bool"));
                _tipoRadiacion.set_rayosXgBaja(_rs.getBoolean("rayos_xg_baja_bool"));
                _tipoRadiacion.set_neutrones(_rs.getBoolean("neutrones_bool"));
                _tipoRadiacion.set_beta(_rs.getBoolean("beta_bool"));
                _tipoRadiacion.set_rayosXg(_rs.getBoolean("rayos_xg_bool"));
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE TipoRadiacion METODO findTipoRadiacionByArea" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _tipoRadiacion;
    }
}
