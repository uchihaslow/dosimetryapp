package com.aleph.dao;

import com.aleph.model.Guia;
import javafx.collections.ObservableList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public interface IGuiaDAO
{
    public int addGuia(Guia _guia, int _areaId);

    public List<Guia> listGuia();

    public ObservableList listGuiaSinAsignar();

    public ObservableList listGuiaAsignadas();

    public void removeGuia(int _idBuscado);

    public void updateGuia(Guia _guia);

    public void aprobarOrden(int _guiaId);

    public List<Guia> findGuiaById(int _idBuscado);

    public List<Guia> findGuiaByCliente(String _clienteBuscado);

    public List<Guia> findGuiaByRuc(String _rucBuscado);

    public boolean findOrdenPedido(String _nroOrdenPedido);

    public void changeToEnviado(int _guiaId);

    public void uploadFile(File _file, int _guiaId);

    public void downloadFile(int _guiaId, File _file);

    public List<Object> listaDosimetrosPorGuia(ArrayList<Integer> _listaGuias);

    public List<Object> listaSobresPorGuia(ArrayList<Integer> _listaGuias);
}
