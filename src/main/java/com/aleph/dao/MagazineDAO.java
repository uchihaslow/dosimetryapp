package com.aleph.dao;

import com.aleph.model.Dosimetro;
import com.aleph.model.Magazine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MagazineDAO extends PgSQLDatabase implements IMagazineDAO
{
    @Override
    public void addDosimetroLimpio(String _dosimetroId)
    {
        abrirConexion();

        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_magazine\n" +
                                    "    (\n" +
                                    "        fk_dosimetro_id_text\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?\n" +
                                    "    );"
                    );
            _pst.setString(1, _dosimetroId);
            _pst.execute();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Magazine METODO addDosimetroLimpio() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void deshabilitarDosimetro(String _dosimetroId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_magazine\n" +
                                    "SET\n" +
                                    "    habilitado_bool = false\n" +
                                    "WHERE\n" +
                                    "    fk_dosimetro_id_text LIKE ?\n" +
                                    "AND\n" +
                                    "    habilitado_bool = true\n"
                    );
            _pst.setString(1, _dosimetroId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE DosimetroDAO METODO deshabilitarDosimetro" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public ArrayList<String> findDosimetrosLimpios(int _cantidad)
    {
        ArrayList<String> _dosimetroList = new ArrayList<>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    mag.fk_dosimetro_id_text\n" +
                                    "FROM\n" +
                                    "    ds_magazine mag\n" +
                                    "WHERE\n" +
                                    "    mag.habilitado_bool = true\n" +
                                    "ORDER BY\n" +
                                    "    mag.pk_magazine_id_int ASC\n" +
                                    "LIMIT ?"
                    );
            _pst.setInt(1, _cantidad);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _dosimetroList.add(_rs.getString("fk_dosimetro_id_text"));
            }
        } catch (SQLException e)
        {
            System.out.println("ERORR CLASE: MagazineDAO METODO: findDosimetrosLimpios :" + e);
        } finally
        {
            cerrarConexion();
        }
        return _dosimetroList;
    }

    @Override
    public boolean findDosimetroExist(String _codigo)
    {
        boolean _existe = false;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT EXISTS\n" +
                                    "(\n" +
                                    "    SELECT 1\n" +
                                    "    FROM ds_magazine mg\n" +
                                    "    WHERE mg.habilitado_bool = true\n" +
                                    "    AND mg.fk_dosimetro_id_text LIKE ?\n" +
                                    ")"
                    );
            _pst.setString(1, _codigo);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _existe = _rs.getBoolean(1);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE: MagazineDAO METODO: findDosimetroExist :" + e);
        } finally
        {
            cerrarConexion();
        }
        return _existe;
    }

    @Override
    public ArrayList<Magazine> ListDosimetros()
    {
        ArrayList<Magazine> _magazineList = new ArrayList<>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    mg.pk_magazine_id_int,\n" +
                                    "    mg.fecha_almacenamiento_tstamp,\n" +
                                    "    mg.habilitado_bool,\n" +
                                    "    mg.fk_dosimetro_id_text\n" +
                                    "FROM\n" +
                                    "    ds_magazine AS mg\n" +
                                    "WHERE mg.habilitado_bool = true\n"+
                                    "ORDER BY\n" +
                                    "    mg.pk_magazine_id_int ASC\n"
                    );
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Magazine _magazine = new Magazine();
                _magazine.set_id(_rs.getInt("pk_magazine_id_int"));
                _magazine.set_codigo(_rs.getString("fk_dosimetro_id_text"));
                _magazine.set_fecha(_rs.getObject("fecha_almacenamiento_tstamp", LocalDateTime.class));
                _magazineList.add(_magazine);
            }
        } catch (SQLException e)
        {
            System.out.println("Clase: MagazineDAO Metodo: ListDosimetros" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _magazineList;
    }

    @Override
    public List<Magazine> findDosimetroByCodigo(String _codigo)
    {
        ArrayList<Magazine> _magazineList = new ArrayList<>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    mg.pk_magazine_id_int,\n" +
                                    "    mg.fecha_almacenamiento_tstamp,\n" +
                                    "    mg.fk_dosimetro_id_text\n" +
                                    "FROM\n" +
                                    "    ds_magazine AS mg\n" +
                                    "WHERE\n" +
                                    "    mg.fk_dosimetro_id_text LIKE ?\n" +
                                    "AND mg.habilitado_bool = true\n"+
                                    "ORDER BY\n" +
                                    "    mg.pk_magazine_id_int ASC\n"
                    );
            _pst.setString(1, "%" + _codigo + "%");
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Magazine _magazine = new Magazine();
                _magazine.set_id(_rs.getInt("pk_magazine_id_int"));
                _magazine.set_codigo(_rs.getString("fk_dosimetro_id_text"));
                _magazine.set_fecha(_rs.getObject("fecha_almacenamiento_tstamp", LocalDateTime.class));
                _magazineList.add(_magazine);
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        } finally
        {
            cerrarConexion();
        }
        return _magazineList;
    }

    @Override
    public int CantDosimHabilitados()
    {
        int _cantidad = 0;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    COUNT(1)\n" +
                                    "FROM\n" +
                                    "    ds_magazine mg\n" +
                                    "WHERE\n" +
                                    "    mg.habilitado_bool = true"
                    );
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _cantidad = _rs.getInt(1);
            }

        } catch (SQLException e)
        {
            System.err.println("error cant datos" + e);
        } finally
        {
            cerrarConexion();
        }
        return _cantidad;
    }


}
