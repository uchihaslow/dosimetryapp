package com.aleph.dao;

import com.aleph.model.Poe;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PoeDAO extends PgSQLDatabase implements IPoeDAO
{
    @Override
    public void addPoe(Poe _poe)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_poe\n" +
                                    "    (\n" +
                                    "        dni_text,\n" +
                                    "        nombres_text,\n" +
                                    "        apellidos_text,\n" +
                                    "        sexo_char\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    )\n"
                    );
            _pst.setString(1, _poe.get_dni());
            _pst.setString(2, _poe.get_nombres());
            _pst.setString(3, _poe.get_apellidos());
            _pst.setString(4, String.valueOf(_poe.get_sexo()));
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Poe METODO addPoe() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void addPoeWithArea(int _poeId, int _areaId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_area_ds_poe\n" +
                                    "    (\n" +
                                    "        fk_area_id_int,\n" +
                                    "        fk_poe_id_int\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    );\n"
                    );
            _pst.setInt(1, _areaId);
            _pst.setInt(2, _poeId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Poe METODO addPoeWithArea() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<Poe> listPoe()
    {
        return null;
    }

    @Override
    public void removePoe(int _idBuscado)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_poe\n" +
                                    "SET\n" +
                                    "    habilitado_bool = false\n" +
                                    "WHERE\n" +
                                    "    pk_poe_id_int = ?;"
                    );
            _pst.setInt(1, _idBuscado);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE PoeDAO METODO removePoe" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void updatePoe(Poe _poe)
    {
        try
        {
            abrirConexion();
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_poe\n" +
                                    "SET\n" +
                                    "    dni_text = ?,\n" +
                                    "    nombres_text = ?,\n" +
                                    "    apellidos_text = ?,\n" +
                                    "    sexo_char = ?\n" +
                                    "WHERE\n" +
                                    "    pk_poe_id_int = ?;"
                    );
            _pst.setString(1, _poe.get_dni());
            _pst.setString(2, _poe.get_nombres());
            _pst.setString(3, _poe.get_apellidos());
            _pst.setString(4, String.valueOf(_poe.get_sexo()));
            _pst.setInt(5, _poe.get_poeId());
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase Poe error updatePoe() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<Poe> findPoeByArea(int _idArea)
    {
        ArrayList<Poe> _poeList = new ArrayList<Poe>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    poe.pk_poe_id_int,\n" +
                                    "    poe.nombres_text,\n" +
                                    "    poe.apellidos_text,\n" +
                                    "    poe.dni_text,\n" +
                                    "    poe.sexo_char\n" +//corregir
                                    "FROM\n" +
                                    "    ds_area ar \n" +
                                    "INNER JOIN\n" +
                                    "    ds_area_ds_poe arpoe\n" +
                                    "ON\n" +
                                    "\tar.pk_area_id_int = arpoe.fk_area_id_int\n" +
                                    "INNER JOIN\n" +
                                    "    ds_poe poe\n" +
                                    "ON\n" +
                                    "\tarpoe.fk_poe_id_int = poe.pk_poe_id_int\n" +
                                    "WHERE\n" +
                                    "    ar.pk_area_id_int = ?\n" +
                                    "AND ar.habilitado_bool = true\n" +
                                    "AND poe.habilitado_bool = true \n" +
                                    "AND arpoe.habilitado_bool = true ;"
                    );
            _pst.setInt(1, _idArea);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Poe _poe = new Poe();
                _poe.set_poeId(_rs.getInt("pk_poe_id_int"));
                _poe.set_nombres(_rs.getString("nombres_text"));
                _poe.set_apellidos(_rs.getString("apellidos_text"));
                _poe.set_dni(_rs.getString("dni_text"));
                _poe.set_sexo(_rs.getString("sexo_char").charAt(0));
                //_poe.set_sexo("M".charAt(0));
                _poeList.add(_poe);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE PoeDAO METODO findPoeByArea : " + e);
        } finally
        {
            cerrarConexion();
        }
        return _poeList;
    }

    @Override
    public List<Poe> findPoeByNombre(String _nombreBuscado)
    {
        ArrayList<Poe> _poeList = new ArrayList<Poe>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "      poe.pk_poe_id_int,\n" +
                                    "      poe.dni_text,\n" +
                                    "      poe.nombres_text,\n" +
                                    "      poe.apellidos_text,\n" +
                                    "      poe.sexo_char\n" +
                                    "FROM\n" +
                                    "      ds_poe poe\n" +
                                    "WHERE\n" +
                                    "      LOWER(REPLACE((CONCAT(poe.nombres_text,poe.apellidos_text)),' ','')) LIKE LOWER(REPLACE(?,' ',''))\n" +
                                    "      AND poe.habilitado_bool = true"
                    );
            _pst.setString(1, "%" + _nombreBuscado + "%");
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Poe _poe = new Poe();
                _poe.set_poeId(_rs.getInt("pk_poe_id_int"));
                _poe.set_dni(_rs.getString("dni_text"));
                _poe.set_nombres(_rs.getString("nombres_text"));
                _poe.set_apellidos(_rs.getString("apellidos_text"));
                _poe.set_sexo(_rs.getString("sexo_char").charAt(0));
                _poeList.add(_poe);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE PoeDAO METODO findPoeByNombre : " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _poeList;
    }

    @Override
    public boolean findPoeByDniExist(String _dniBuscado)
    {
        boolean _existe = false;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT EXISTS\n" +
                                    "(\n" +
                                    "        SELECT 1\n" +
                                    "        FROM ds_poe poe\n" +
                                    "        WHERE poe.dni_text LIKE ?\n" +
                                    "        AND poe.habilitado_bool = true\n" +
                                    ")"
                    );
            _pst.setString(1, _dniBuscado);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _existe = _rs.getBoolean(1);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE: PoeDAO METODO: findPoeByDniExist :" + e);
        } finally
        {
            cerrarConexion();
        }
        return _existe;
    }

    public List<Poe> findPoeByDni(String _dniBuscado)
    {
        ArrayList<Poe> _poeList = new ArrayList<Poe>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "      poe.pk_poe_id_int,\n" +
                                    "      poe.dni_text,\n" +
                                    "      poe.nombres_text,\n" +
                                    "      poe.apellidos_text,\n" +
                                    "      poe.sexo_char\n" +
                                    "FROM\n" +
                                    "      ds_poe poe\n" +
                                    "WHERE\n" +
                                    "      poe.dni_text LIKE ?" +
                                    "      AND poe.habilitado_bool = true"
                    );
            _pst.setString(1, "%" + _dniBuscado + "%");
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Poe _poe = new Poe();
                _poe.set_poeId(_rs.getInt("pk_poe_id_int"));
                _poe.set_dni(_rs.getString("dni_text"));
                _poe.set_nombres(_rs.getString("nombres_text"));
                _poe.set_apellidos(_rs.getString("apellidos_text"));
                _poe.set_sexo(_rs.getString("sexo_char").charAt(0));
                _poeList.add(_poe);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE PoeDAO METODO findPoeByDni : " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _poeList;
    }

    @Override
    public List<Poe> findPoeByGuia(int _idGuia)
    {
        ArrayList<Poe> _poeList = new ArrayList<Poe>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "pres.pk_prestamo_id_int,\n" +
                                    "poe.pk_poe_id_int,\n" +
                                    "poe.dni_text,\n" +
                                    "poe.nombres_text,\n" +
                                    "poe.apellidos_text,\n" +
                                    "pres.tipo_dosimetro_text,\n" +
                                    "pres.tipo_exposicion_int,\n" +
                                    "pres.tipo_radiacion_text,\n" +
                                    "pres.externo_bool,\n" +
                                    "pres.fk_dosimetro_id_text\n" +
                                    "FROM  ds_prestamo pres\n" +
                                    "INNER JOIN  ds_poe poe\n" +
                                    "ON    pres.fk_poe_id_int = poe.pk_poe_id_int\n" +
                                    "WHERE\n" +
                                    "        pres.fk_guia_id_int = ?\n" +
                                    "        AND pres.estado_int <> 6\n" +
                                    "ORDER BY poe.apellidos_text ASC"
                    );
            _pst.setInt(1, _idGuia);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Poe _poe = new Poe();
                _poe.set_prestamoId(_rs.getInt("pk_prestamo_id_int"));
                _poe.set_poeId(_rs.getInt("pk_poe_id_int"));
                _poe.set_nombres(_rs.getString("nombres_text"));
                _poe.set_apellidos(_rs.getString("apellidos_text"));
                _poe.set_dni(_rs.getString("dni_text"));
                _poe.set_tipoDos(_rs.getString("tipo_dosimetro_text"));
                _poe.set_tipoExp(_rs.getInt("tipo_exposicion_int"));
                _poe.set_tipoRad(_rs.getString("tipo_radiacion_text"));
                _poe.set_externo(_rs.getBoolean("externo_bool"));
                _poe.set_dosimetroId(_rs.getString("fk_dosimetro_id_text"));
                _poeList.add(_poe);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE PoeDAO METODO findPoeByGuia : " + e);
        } finally
        {
            cerrarConexion();
        }
        return _poeList;
    }

    @Override
    public List<Poe> findUsuariosFaltantesByGuia(int idGuia)
    {
        ArrayList<Poe> _poeList = new ArrayList<Poe>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT \n" +
                                    "        poe.pk_poe_id_int,\n" +
                                    "        poe.dni_text,\n" +
                                    "        poe.apellidos_text,\n" +
                                    "        poe.nombres_text\n" +
                                    "FROM ds_poe poe\n" +
                                    "INNER JOIN ds_area_ds_poe ap\n" +
                                    "ON poe.pk_poe_id_int = ap.fk_poe_id_int\n" +
                                    "INNER JOIN ds_area a\n" +
                                    "ON ap.fk_area_id_int = a.pk_area_id_int\n" +
                                    "INNER JOIN ds_guia g\n" +
                                    "ON a.pk_area_id_int = g.fk_area_id_int\n" +
                                    "WHERE g.pk_guia_id_int = ?\n" +
                                    "AND ap.habilitado_bool = true\n" +
                                    "AND poe.pk_poe_id_int NOT IN\n" +
                                    "        (\n" +
                                    "                SELECT \n" +
                                    "                p2.fk_poe_id_int\n" +
                                    "                FROM ds_guia g2\n" +
                                    "                INNER JOIN ds_prestamo p2\n" +
                                    "                ON g2.pk_guia_id_int = p2.fk_guia_id_int\n" +
                                    "                WHERE g2.pk_guia_id_int = g.pk_guia_id_int\n" +
                                    "                AND p2.estado_int = 2\n" +
                                    "        )\n" +
                                    "ORDER BY poe.apellidos_text"
                    );
            _pst.setInt(1, idGuia);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Poe _poe = new Poe();
                _poe.set_poeId(_rs.getInt("pk_poe_id_int"));
                _poe.set_nombres(_rs.getString("nombres_text"));
                _poe.set_apellidos(_rs.getString("apellidos_text"));
                _poe.set_dni(_rs.getString("dni_text"));
                _poeList.add(_poe);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE PoeDAO METODO findUsuariosFaltantesByGuia : " + e);
        } finally
        {
            cerrarConexion();
        }
        return _poeList;
    }

    @Override
    public List<Poe> findPoeByGuia2(int idGuia)
    {
        ArrayList<Poe> _poeList = new ArrayList<Poe>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT \n" +
                                    "        poe.pk_poe_id_int,\n" +
                                    "        poe.dni_text,\n" +
                                    "        poe.apellidos_text,\n" +
                                    "        poe.nombres_text\n" +
                                    "FROM ds_poe poe\n" +
                                    "INNER JOIN ds_area_ds_poe ap\n" +
                                    "ON poe.pk_poe_id_int = ap.fk_poe_id_int\n" +
                                    "INNER JOIN ds_area a\n" +
                                    "ON ap.fk_area_id_int = a.pk_area_id_int\n" +
                                    "INNER JOIN ds_guia g\n" +
                                    "ON a.pk_area_id_int = g.fk_area_id_int\n" +
                                    "WHERE g.pk_guia_id_int = ?\n" +
                                    "AND ap.habilitado_bool = true\n" +
                                    "ORDER BY poe.apellidos_text"
                    );
            _pst.setInt(1, idGuia);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Poe _poe = new Poe();
                _poe.set_poeId(_rs.getInt("pk_poe_id_int"));
                _poe.set_nombres(_rs.getString("nombres_text"));
                _poe.set_apellidos(_rs.getString("apellidos_text"));
                _poe.set_dni(_rs.getString("dni_text"));
                _poeList.add(_poe);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE PoeDAO METODO findUsuariosFaltantesByGuia : " + e);
        } finally
        {
            cerrarConexion();
        }
        return _poeList;
    }
}
