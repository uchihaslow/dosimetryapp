package com.aleph.dao;

import com.aleph.model.DosisMultiReport;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ReporteDAO extends PgSQLDatabase implements IReporteDAO
{
    @Override
    public ObservableList listReporteSinFinalizar()
    {
        ObservableList _reporteSinFinalizarList = FXCollections.observableArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "        pres.fk_guia_id_int,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p\n" +
                                    "                WHERE\n" +
                                    "                p.estado_int = 4\n" +
                                    "                AND p.fk_guia_id_int = pres.fk_guia_id_int\n" +
                                    "        ) AS leidos,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p2\n" +
                                    "                WHERE\n" +
                                    "                p2.estado_int = 3\n" +
                                    "                AND p2.fk_guia_id_int = pres.fk_guia_id_int\n" +
                                    "        )AS devueltos,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p3\n" +
                                    "                WHERE\n" +
                                    "                p3.estado_int = 2\n" +
                                    "                AND p3.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        )AS no_devueltos,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p4\n" +
                                    "                WHERE\n" +
                                    "                (\n" +
                                    "                        p4.estado_int = 2\n" +
                                    "                        OR p4.estado_int = 3\n" +
                                    "                        OR p4.estado_int = 4\n" +
                                    "                )\n" +
                                    "                AND p4.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        )AS total\n" +
                                    "FROM ds_prestamo pres\n" +
                                    "INNER JOIN ds_guia guia\n" +
                                    "ON guia.pk_guia_id_int = pres.fk_guia_id_int\n" +
                                    "WHERE \n" +
                                    "    guia.estado_int = 2\n" +
                                    "    OR guia.estado_int = 3\n" +
                                    "GROUP BY pres.fk_guia_id_int\n" +
                                    "ORDER BY pres.fk_guia_id_int ASC"
                    );
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                ObservableList<String> _row = FXCollections.observableArrayList();
                _row.add(_rs.getString("fk_guia_id_int"));
                _row.add(_rs.getString("leidos"));
                _row.add(_rs.getString("devueltos"));
                _row.add(_rs.getString("no_devueltos"));
                _row.add(_rs.getString("total"));
                System.out.println("Row [1] added " + _row);
                _reporteSinFinalizarList.add(_row);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE ReporteDAO METODO listReporteSinFinalizar: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _reporteSinFinalizarList;
    }

    @Override
    public ObservableList listReporteFinalizado()
    {
        ObservableList _reporteFinalizadoList = FXCollections.observableArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "        pres.fk_guia_id_int,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p\n" +
                                    "                WHERE\n" +
                                    "                p.estado_int = 4\n" +
                                    "                AND\n" +
                                    "                p.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        ) AS leidos,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p\n" +
                                    "                WHERE\n" +
                                    "                p.estado_int = 3\n" +
                                    "                AND\n" +
                                    "                p.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        )AS faltantes\n" +
                                    "FROM\n" +
                                    "ds_prestamo pres\n" +
                                    "INNER JOIN\n" +
                                    "ds_guia guia\n" +
                                    "ON guia.pk_guia_id_int = pres.fk_guia_id_int\n" +
                                    "WHERE\n" +
                                    "(\n" +
                                    "        pres.estado_int = 4\n" +
                                    "        OR\n" +
                                    "        pres.estado_int = 3\n" +
                                    ")\n" +
                                    "AND guia.estado_int = 4\n" +
                                    "GROUP BY pres.fk_guia_id_int\n" +
                                    "ORDER BY pres.fk_guia_id_int ASC"
                    );
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                ObservableList<String> _row = FXCollections.observableArrayList();
                _row.add(_rs.getString("fk_guia_id_int"));
                _row.add(_rs.getString("leidos"));
                _row.add(_rs.getString("faltantes"));
                System.out.println("Row [1] added " + _row);
                _reporteFinalizadoList.add(_row);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE ReporteDAO METODO listReporteFinalizado: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _reporteFinalizadoList;
    }

    @Override
    public ObservableList listReporteSinFinalizarByIdGuia(int _idGuia)
    {
        ObservableList _reporteSinFinalizarList = FXCollections.observableArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "        pres.fk_guia_id_int,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p\n" +
                                    "                WHERE\n" +
                                    "                p.estado_int = 4\n" +
                                    "                AND\n" +
                                    "                p.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        ) AS leidos,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p2\n" +
                                    "                WHERE\n" +
                                    "                p2.estado_int = 3\n" +
                                    "                AND\n" +
                                    "                p2.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        )AS no_leidos,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p3\n" +
                                    "                WHERE\n" +
                                    "                p3.estado_int = 2\n" +
                                    "                AND\n" +
                                    "                p3.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        )AS no_devueltos,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p4\n" +
                                    "                WHERE\n" +
                                    "                (\n" +
                                    "                        p4.estado_int = 2\n" +
                                    "                        OR p4.estado_int = 3\n" +
                                    "                        OR p4.estado_int = 4\n" +
                                    "                )\n" +
                                    "                AND p4.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        )AS total\n" +
                                    "FROM\n" +
                                    "ds_prestamo pres\n" +
                                    "INNER JOIN\n" +
                                    "ds_guia guia\n" +
                                    "ON guia.pk_guia_id_int = pres.fk_guia_id_int\n" +
                                    "WHERE\n" +
                                    "guia.estado_int = 2\n" +
                                    "AND guia.pk_guia_id_int = ?\n" +
                                    "GROUP BY pres.fk_guia_id_int\n" +
                                    "ORDER BY pres.fk_guia_id_int ASC"
                    );
            _pst.setInt(1, _idGuia);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                ObservableList<String> _row = FXCollections.observableArrayList();
                _row.add(_rs.getString("fk_guia_id_int"));
                _row.add(_rs.getString("leidos"));
                _row.add(_rs.getString("no_leidos"));
                _row.add(_rs.getString("no_devueltos"));
                _row.add(_rs.getString("total"));
                System.out.println("Row [1] added " + _row);
                _reporteSinFinalizarList.add(_row);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE ReporteDAO METODO listReporteSinFinalizarByIdGuia: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _reporteSinFinalizarList;
    }

    @Override
    public ObservableList listReporteFinalizadoByIdGuia(int _idGuia)
    {
        ObservableList _reporteFinalizadoList = FXCollections.observableArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "        pres.fk_guia_id_int,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p\n" +
                                    "                WHERE\n" +
                                    "                p.estado_int = 4\n" +
                                    "                AND\n" +
                                    "                p.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        ) AS leidos,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_prestamo p\n" +
                                    "                WHERE\n" +
                                    "                p.estado_int = 3\n" +
                                    "                AND\n" +
                                    "                p.fk_guia_id_int=pres.fk_guia_id_int\n" +
                                    "        )AS faltantes\n" +
                                    "FROM\n" +
                                    "ds_prestamo pres\n" +
                                    "INNER JOIN\n" +
                                    "ds_guia guia\n" +
                                    "ON guia.pk_guia_id_int = pres.fk_guia_id_int\n" +
                                    "WHERE\n" +
                                    "(\n" +
                                    "        pres.estado_int = 4\n" +
                                    "        OR\n" +
                                    "        pres.estado_int = 3\n" +
                                    ")\n" +
                                    "AND guia.estado_int = 4\n" +
                                    "AND guia.pk_guia_id_int = ?\n" +
                                    "GROUP BY pres.fk_guia_id_int\n" +
                                    "ORDER BY pres.fk_guia_id_int ASC"
                    );
            _pst.setInt(1, _idGuia);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                ObservableList<String> _row = FXCollections.observableArrayList();
                _row.add(_rs.getString("fk_guia_id_int"));
                _row.add(_rs.getString("leidos"));
                _row.add(_rs.getString("faltantes"));
                System.out.println("Row [1] added " + _row);
                _reporteFinalizadoList.add(_row);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE ReporteDAO METODO listReporteFinalizadoByIdGuia: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _reporteFinalizadoList;
    }

    @Override
    public ArrayList listDatosComunesReporte(int _idGuia)
    {
//        ObservableList _datosCabecera = FXCollections.observableArrayList();
        ArrayList _datosCabecera = new ArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "\n" +
                                    "        sede.direccion_text,\n" +
                                    "        cli.pk_cliente_id_int,\n" +
                                    "        cli.razon_social_text,\n" +
                                    "        guia.pk_guia_id_int,\n" +
                                    "        guia.periodo_date\n" +
                                    "\n" +
                                    "FROM \n" +
                                    "        ds_guia guia\n" +
                                    "        INNER JOIN ds_area area\n" +
                                    "        ON guia.fk_area_id_int= area.pk_area_id_int\n" +
                                    "        INNER JOIN ds_sede sede\n" +
                                    "        ON area.fk_sede_id_int = sede.pk_sede_id_int\n" +
                                    "        INNER JOIN ds_cliente cli\n" +
                                    "        ON sede.fk_cliente_id_int = cli.pk_cliente_id_int\n" +
                                    "WHERE\n" +
                                    "        guia.pk_guia_id_int = ?"
                    );
            _pst.setInt(1, _idGuia);
            _rs = _pst.executeQuery();
            //DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("MMM/yyyy");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMM");
            DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy");
            while (_rs.next())
            {
//                ObservableList<String> _row = FXCollections.observableArrayList();
//                _row.add(_rs.getString("direccion_text"));
//                _row.add(_rs.getString("pk_cliente_id_int"));
//                _row.add(_rs.getString("razon_social_text"));
//                _row.add(_rs.getString("pk_guia_id_int"));
//                _row.add(_rs.getDate("periodo_date").toLocalDate().format(dtf).toUpperCase());//MES
//                _row.add(_rs.getDate("periodo_date").toLocalDate().format(dtf2).toUpperCase());//AÑO

                _datosCabecera.add(_rs.getString("direccion_text"));
                _datosCabecera.add(_rs.getString("pk_cliente_id_int"));
                _datosCabecera.add(_rs.getString("razon_social_text"));
                _datosCabecera.add(_rs.getString("pk_guia_id_int"));
                _datosCabecera.add(_rs.getDate("periodo_date").toLocalDate().format(dtf).toUpperCase());//MES
                _datosCabecera.add(_rs.getDate("periodo_date").toLocalDate().format(dtf2).toUpperCase());//AÑO
                System.out.println("Row [1] added " + _datosCabecera);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE GuiaDAO METODO listDatosComunesReporte: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _datosCabecera;
    }

    @Override
    public List<DosisMultiReport> listDosisByPrestamo(int _idGuia)
    {
        List<DosisMultiReport> _dosisList = new ArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "        poe.pk_poe_id_int,\n" +
                                    "        poe.nombres_text,\n" +
                                    "        poe.apellidos_text,\n" +
                                    "        poe.sexo_char,\n" +
                                    "        pres.pk_prestamo_id_int,\n" +
                                    "        pres.tipo_dosimetro_text,\n" +
                                    "        pres.tipo_exposicion_int,\n" +
                                    "        pres.tipo_radiacion_text,\n" +
                                    "        lect.efectiva_num,\n" +
                                    "        lect.cristalino_num,\n" +
                                    "        lect.piel_num,\n" +
                                    "        (\n" +
                                    "                SELECT COUNT(1)\n" +
                                    "                FROM ds_poe poe2\n" +
                                    "                INNER JOIN ds_prestamo pres2\n" +
                                    "                ON poe2.pk_poe_id_int = pres2.fk_poe_id_int\n" +
                                    "                WHERE\n" +
                                    "                poe2.pk_poe_id_int = poe.pk_poe_id_int\n" +
                                    "                AND pres2.estado_int = 4\n" +
                                    "        )AS rep_cantidad,\n" +
                                    "        (\n" +
                                    "                SELECT SUM\n" +
                                    "                        (\n" +
                                    "                        \n" +
                                    "                                CASE WHEN lect3.efectiva_num > 0.10 THEN\n" +
                                    "                                        lect3.efectiva_num\n" +
                                    "                                ELSE\n" +
                                    "                                        0\n" +
                                    "                                END\n" +
                                    "                        )\n" +
                                    "                FROM ds_poe poe3\n" +
                                    "                INNER JOIN ds_prestamo pres3\n" +
                                    "                ON poe3.pk_poe_id_int = pres3.fk_poe_id_int\n" +
                                    "                INNER JOIN ds_lectura lect3\n" +
                                    "                ON lect3.fk_prestamo_id_int = pres3.pk_prestamo_id_int\n" +
                                    "                WHERE\n" +
                                    "                poe3.pk_poe_id_int = poe.pk_poe_id_int\n" +
                                    "                AND pres3.estado_int = 4\n" +
                                    "        )AS efectiva_anual,\n" +
                                    "        (\n" +
                                    "                SELECT SUM\n" +
                                    "                        (\n" +
                                    "                                CASE WHEN lect4.cristalino_num > 0.10 THEN\n" +
                                    "                                        lect4.cristalino_num\n" +
                                    "                                ELSE\n" +
                                    "                                        0\n" +
                                    "                                END\n" +
                                    "                        )\n" +
                                    "                FROM ds_poe poe4\n" +
                                    "                INNER JOIN ds_prestamo pres4\n" +
                                    "                ON poe4.pk_poe_id_int = pres4.fk_poe_id_int\n" +
                                    "                INNER JOIN ds_lectura lect4\n" +
                                    "                ON lect4.fk_prestamo_id_int = pres4.pk_prestamo_id_int\n" +
                                    "                WHERE\n" +
                                    "                poe4.pk_poe_id_int = poe.pk_poe_id_int\n" +
                                    "                AND pres4.estado_int = 4\n" +
                                    "        )AS cristalino_anual,\n" +
                                    "        (\n" +
                                    "                SELECT SUM\n" +
                                    "                        (\n" +
                                    "                                CASE WHEN lect5.piel_num > 0.10 THEN\n" +
                                    "                                        lect5.piel_num\n" +
                                    "                                ELSE\n" +
                                    "                                        0\n" +
                                    "                                END\n" +
                                    "                        )\n" +
                                    "                FROM ds_poe poe5\n" +
                                    "                INNER JOIN ds_prestamo pres5\n" +
                                    "                ON poe5.pk_poe_id_int = pres5.fk_poe_id_int\n" +
                                    "                INNER JOIN ds_lectura lect5\n" +
                                    "                ON lect5.fk_prestamo_id_int = pres5.pk_prestamo_id_int\n" +
                                    "                WHERE\n" +
                                    "                poe5.pk_poe_id_int = poe.pk_poe_id_int\n" +
                                    "                AND pres5.estado_int = 4\n" +
                                    "        )AS piel_anual,\n" +
                                    "        pres.fk_dosimetro_id_text\n" +
                                    "FROM \n" +
                                    "        ds_prestamo pres\n" +
                                    "        INNER JOIN ds_poe poe\n" +
                                    "        ON pres.fk_poe_id_int = poe.pk_poe_id_int\n" +
                                    "        INNER JOIN ds_lectura lect\n" +
                                    "        ON lect.fk_prestamo_id_int = pres.pk_prestamo_id_int\n" +
                                    "        INNER JOIN ds_guia guia\n" +
                                    "        ON pres.fk_guia_id_int = guia.pk_guia_id_int\n" +
                                    "WHERE\n" +
                                    "        pres.estado_int = 4\n" +
                                    "        AND lect.habilitado_bool = true\n" +
                                    "        AND pres.fk_guia_id_int = ?\n" +
                                    "        AND guia.periodo_date BETWEEN '20180101' AND '20181231'\n" +
                                    "ORDER BY pres.fk_guia_id_int ASC"
                    );
            _pst.setInt(1, _idGuia);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                DosisMultiReport _dosis = new DosisMultiReport();
                _dosis.set_poeId(_rs.getString("pk_poe_id_int"));
                _dosis.set_poeNom(_rs.getString("nombres_text") + ", " + _rs.getString("apellidos_text"));
                _dosis.set_poeSexo(_rs.getString("sexo_char"));
                _dosis.set_poeTde(_rs.getString("tipo_dosimetro_text") + _rs.getString("tipo_exposicion_int"));
                _dosis.set_rad(_rs.getString("tipo_radiacion_text"));
                _dosis.set_mensualEfectiva(_rs.getString("efectiva_num"));
                _dosis.set_mensualCristalino(_rs.getString("cristalino_num"));
                _dosis.set_mensualPiel(_rs.getString("piel_num"));
                _dosis.set_anualEfectiva(_rs.getString("efectiva_anual"));
                _dosis.set_anualCristalino(_rs.getString("cristalino_anual"));
                _dosis.set_anualPiel(_rs.getString("piel_anual"));
                _dosis.set_dosRep(_rs.getString("rep_cantidad"));
                _dosis.set_dosNumero(_rs.getString("fk_dosimetro_id_text"));

                _dosisList.add(_dosis);

                System.out.println("Row [1] added " + _dosis);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE ReporteDAO METODO listDosisByPrestamo: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _dosisList;
    }

    @Override
    public ObservableList listInformeSinFinalizarByGuia(int _idGuia)
    {
        {
            ObservableList _informeSinFinalizarList = FXCollections.observableArrayList();
            abrirConexion();
            try
            {
                ResultSet _rs;
                PreparedStatement _pst = _conn.prepareStatement
                        (
                                "SELECT\n" +
                                        "        guia.pk_guia_id_int,\n" +
                                        "        cli.razon_social_text,\n" +
                                        "        sede.direccion_text,\n" +
                                        "        guia.nro_orden_pedido_text\n" +
                                        "FROM\n" +
                                        "        ds_guia guia\n" +
                                        "        INNER JOIN ds_area area\n" +
                                        "        ON guia.fk_area_id_int = area.pk_area_id_int\n" +
                                        "        INNER JOIN ds_sede sede\n" +
                                        "        ON sede.pk_sede_id_int = area.fk_sede_id_int\n" +
                                        "        INNER JOIN ds_cliente cli\n" +
                                        "        ON cli.pk_cliente_id_int = sede.fk_cliente_id_int\n" +
                                        "WHERE\n" +
                                        "        (\n" +
                                        "                guia.estado_int = 2\n" +
                                        "                OR guia.estado_int = 4\n" +
                                        "        )\n" +
                                        "        AND guia.pk_guia_id_int = ?\n" +
                                        "        \n" +
                                        "GROUP BY\n" +
                                        "(\n" +
                                        "        guia.pk_guia_id_int,\n" +
                                        "        cli.razon_social_text,\n" +
                                        "        sede.direccion_text,\n" +
                                        "        guia.nro_orden_pedido_text\n" +
                                        ")\n" +
                                        "ORDER BY guia.pk_guia_id_int DESC"
                        );
                _pst.setInt(1, _idGuia);
                _rs = _pst.executeQuery();
                while (_rs.next())
                {
                    ObservableList<String> _row = FXCollections.observableArrayList();
                    _row.add(_rs.getString("pk_guia_id_int"));
                    _row.add(_rs.getString("razon_social_text"));
                    _row.add(_rs.getString("direccion_text"));
                    _row.add(_rs.getString("nro_orden_pedido_text"));
                    System.out.println("Row [1] added " + _row);
                    _informeSinFinalizarList.add(_row);
                }
            } catch (SQLException e)
            {
                System.err.println("ERROR CLASE ReporteDAO METODO listInformeSinFinalizarByGuia: " + e.getMessage());
            } finally
            {
                cerrarConexion();
            }
            return _informeSinFinalizarList;
        }
    }

    @Override
    public ObservableList listInformeSinFinalizar()
    {
        ObservableList _informeSinFinalizarList = FXCollections.observableArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "        guia.pk_guia_id_int,\n" +
                                    "        cli.razon_social_text,\n" +
                                    "        sede.direccion_text,\n" +
                                    "        guia.nro_orden_pedido_text\n" +
                                    "FROM\n" +
                                    "        ds_guia guia\n" +
                                    "        INNER JOIN ds_area area\n" +
                                    "        ON guia.fk_area_id_int = area.pk_area_id_int\n" +
                                    "        INNER JOIN ds_sede sede\n" +
                                    "        ON sede.pk_sede_id_int = area.fk_sede_id_int\n" +
                                    "        INNER JOIN ds_cliente cli\n" +
                                    "        ON cli.pk_cliente_id_int = sede.fk_cliente_id_int\n" +
                                    "WHERE\n" +
                                    "        guia.estado_int = 2\n" +
                                    "        OR guia.estado_int = 4\n" +
                                    "GROUP BY\n" +
                                    "(\n" +
                                    "        guia.pk_guia_id_int,\n" +
                                    "        cli.razon_social_text,\n" +
                                    "        sede.direccion_text,\n" +
                                    "        guia.nro_orden_pedido_text\n" +
                                    ")\n" +
                                    "ORDER BY guia.pk_guia_id_int DESC"
                    );
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                ObservableList<String> _row = FXCollections.observableArrayList();
                _row.add(_rs.getString("pk_guia_id_int"));
                _row.add(_rs.getString("razon_social_text"));
                _row.add(_rs.getString("direccion_text"));
                _row.add(_rs.getString("nro_orden_pedido_text"));
                System.out.println("Row [1] added " + _row);
                _informeSinFinalizarList.add(_row);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE ReporteDAO METODO listInformeSinFinalizar: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _informeSinFinalizarList;
    }

    /*
    @Override
    public ObservableList listGuiaReporteByPeriodo(List<String> _periodos)
    {
        Object[] _periodoList = _periodos.toArray();
        ObservableList _guiaReportList = FXCollections.observableArrayList();
        abrirConexion();

        try
        {

            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement(SQL_SELECT_GUIA_REPORTE_X_PERIODO);
            _pst.setArray(1, _conn.createArrayOf("text", _periodoList));
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                ObservableList<String> _row = FXCollections.observableArrayList();
                _row.add(_rs.getString("nro_guia"));//0
                _row.add(_rs.getString("periodo_text"));//1
                _row.add(_rs.getString("periodo"));//2
                _row.add(_rs.getString("razon_social"));//3
                _row.add(_rs.getString("nom_sede"));//4
                _row.add(_rs.getString("sede_direccion"));//5
                _row.add(_rs.getString("nom_area"));//6
                _row.add(_rs.getString("leidos"));//7
                _row.add(_rs.getString("devueltos"));//8
                _row.add(_rs.getString("no_devueltos"));//9
                _row.add(_rs.getString("total"));//10
                System.out.println("Row [1] added " + _row);
                _guiaReportList.add(_row);
            }

        } catch (SQLException e)
        {
            System.out.println("Error listGuiaReporteByPeriodo : " + e);
        }

        return _guiaReportList;
    }    */

    @Override
    public ObservableList listGuiaReporteByPeriodo(List<String> _periodos)
    {
        Object[] _periodoList = _periodos.toArray();
        ObservableList _guiaReportList = FXCollections.observableArrayList();
        abrirConexion();

        try
        {

            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement(SQL_SELECT_GUIA_REPORTE_X_PERIODO);
            _pst.setArray(1, _conn.createArrayOf("text", _periodoList));
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                ObservableList<Object> _row = FXCollections.observableArrayList();
                _row.add(_rs.getString("nro_guia"));//0
                _row.add(_rs.getString("periodo_text"));//1
                _row.add(_rs.getString("periodo"));//2
                _row.add(_rs.getString("razon_social"));//3
                _row.add(_rs.getString("nom_sede"));//4
                _row.add(_rs.getString("sede_direccion"));//5
                _row.add(_rs.getString("nom_area"));//6
                _row.add(_rs.getString("leidos"));//7
                _row.add(_rs.getString("devueltos"));//8
                _row.add(_rs.getString("no_devueltos"));//9
                _row.add(_rs.getString("total"));//10
                System.out.println("Row [1] added " + _row);
                _guiaReportList.add(_row);
            }

        } catch (SQLException e)
        {
            System.out.println("Error listGuiaReporteByPeriodo : " + e);
        }

        return _guiaReportList;
    }

    //region SQL_QUERY

    private static final String SQL_SELECT_GUIA_REPORTE_X_PERIODO =
            "SELECT\n" +
                    "    guia.pk_guia_id_int as nro_guia,\n" +
                    "    to_char(guia.periodo_date, 'TMMON/YYYY') as periodo_text,\n" +
                    "    guia.periodo_date as periodo,\n" +
                    "    clie.razon_social_text as razon_social,\n" +
                    "    sede.nombre_text as nom_sede,\n" +
                    "    sede.direccion_text as sede_direccion,\n" +
                    "    area.nombre_text as nom_area,\n" +
                    "    (\n" +
                    "        SELECT COUNT(1)\n" +
                    "        FROM ds_prestamo p\n" +
                    "        WHERE p.estado_int = 4\n" +
                    "        AND p.fk_guia_id_int=guia.pk_guia_id_int\n" +
                    "    ) AS leidos,\n" +
                    "    (\n" +
                    "        SELECT COUNT(1)\n" +
                    "        FROM ds_prestamo p2\n" +
                    "        WHERE p2.estado_int = 3\n" +
                    "        AND p2.fk_guia_id_int=guia.pk_guia_id_int\n" +
                    "    )AS devueltos,\n" +
                    "    (\n" +
                    "        SELECT COUNT(1)\n" +
                    "        FROM ds_prestamo p3\n" +
                    "        WHERE p3.estado_int = 2\n" +
                    "        AND p3.fk_guia_id_int=guia.pk_guia_id_int\n" +
                    "    )AS no_devueltos,\n" +
                    "    (\n" +
                    "        SELECT COUNT(1)\n" +
                    "        FROM ds_prestamo p4\n" +
                    "        WHERE ( p4.estado_int = 2 OR p4.estado_int = 3 OR p4.estado_int = 4)\n" +
                    "        AND p4.fk_guia_id_int=guia.pk_guia_id_int\n" +
                    "    )AS total\n" +
                    "    \n" +
                    "FROM ds_cliente clie\n" +
                    "INNER JOIN ds_sede sede\n" +
                    "ON clie.pk_cliente_id_int = sede.fk_cliente_id_int\n" +
                    "INNER JOIN ds_area area\n" +
                    "ON sede.pk_sede_id_int = area.fk_sede_id_int\n" +
                    "INNER JOIN ds_guia guia \n" +
                    "ON area.pk_area_id_int = guia.fk_area_id_int\n" +
                    "INNER JOIN ds_prestamo pres\n" +
                    "ON guia.pk_guia_id_int = pres.fk_guia_id_int\n" +
                    "WHERE\n" +
                    "    guia.estado_int = 2\n" +
                    "    AND guia.periodo_date = ANY(?::DATE[])\n" +
                    "GROUP BY \n" +
                    "    guia.pk_guia_id_int,\n" +
                    "    guia.periodo_date,\n" +
                    "    clie.razon_social_text,\n" +
                    "    sede.nombre_text,\n" +
                    "    sede.direccion_text,\n" +
                    "    area.nombre_text\n" +
                    "ORDER BY\n" +
                    "    guia.periodo_date ASC,\n" +
                    "    guia.pk_guia_id_int ASC\n";
    //endregion
}
