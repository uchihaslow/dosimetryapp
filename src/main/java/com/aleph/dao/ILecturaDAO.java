package com.aleph.dao;

import com.aleph.model.Lectura;

public interface ILecturaDAO
{
    public void addLectura(Lectura _lectura);
}
