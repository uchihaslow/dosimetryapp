package com.aleph.dao;

import com.aleph.model.DosisMultiReport;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public interface IReporteDAO
{
    public ObservableList listReporteSinFinalizar();

    public ObservableList listReporteFinalizado();

    public ObservableList listReporteSinFinalizarByIdGuia(int _idGuia);

    public ObservableList listReporteFinalizadoByIdGuia(int _idGuia);

    public ArrayList listDatosComunesReporte(int _idGuia);

    public List<DosisMultiReport> listDosisByPrestamo(int _idGuia);

    public ObservableList listInformeSinFinalizar();

    public ObservableList listInformeSinFinalizarByGuia(int _idGuia);

    public ObservableList listGuiaReporteByPeriodo(List<String> _periodos);
}
