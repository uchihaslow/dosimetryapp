package com.aleph.dao;

import com.aleph.model.Prestamo;
import com.aleph.tools.DosimetroRecibido;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class PrestamoDAO extends PgSQLDatabase implements IPrestamoDAO
{
    @Override
    public void addPrestamo(Prestamo _prestamo, int _guiaId, int _poeId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_prestamo\n" +
                                    "    (\n" +
                                    "        estado_int,\n" +
                                    "        tipo_exposicion_int,\n" +
                                    "        tipo_radiacion_text,\n" +
                                    "        tipo_dosimetro_text,\n" +
                                    "        externo_bool,\n" +
                                    "        fk_guia_id_int,\n" +
                                    "        fk_poe_id_int\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    );"
                    );
            _pst.setInt(1, _prestamo.get_estado());
            _pst.setInt(2, _prestamo.get_tipoExpo());
            _pst.setString(3, _prestamo.get_tipoRad());
            _pst.setString(4, _prestamo.get_tipoDosi());
            _pst.setBoolean(5,_prestamo.is_externo());
            _pst.setInt(6, _guiaId);
            _pst.setInt(7, _poeId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE PrestamoDAO METODO addPrestamo() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public ArrayList<Prestamo> findPrestamosByGuia(int _guiaId)
    {
        ArrayList<Prestamo> _prestamoList = new ArrayList<Prestamo>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "      pres.pk_prestamo_id_int\n" +
                                    "FROM  ds_prestamo pres\n" +
                                    "INNER JOIN  ds_poe poe\n" +
                                    "ON    pres.fk_poe_id_int = poe.pk_poe_id_int\n" +
                                    "WHERE\n" +
                                    "      pres.fk_guia_id_int = ?;"
                    );
            _pst.setInt(1, _guiaId);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Prestamo _prestamo = new Prestamo();
                _prestamo.set_prestamoId(_rs.getInt("pk_prestamo_id_int"));
                _prestamoList.add(_prestamo);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE PoeDAO METODO findPoeByGuia : " + e);
        } finally
        {
            cerrarConexion();
        }
        return _prestamoList;
    }

    @Override
    public boolean findPrestamoEnviadoExist(String _dosimetroId)
    {
        boolean _existe = false;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT EXISTS\n" +
                                    "(\n" +
                                    "    SELECT\n" +
                                    "    1\n" +
                                    "    FROM ds_prestamo p\n" +
                                    "    INNER JOIN ds_guia g\n" +
                                    "    ON g.pk_guia_id_int = p.fk_guia_id_int\n" +
                                    "    WHERE\n" +
                                    "    p.fk_dosimetro_id_text LIKE ?\n" +
                                    "    AND p.estado_int = 2\n" +
                                    "    AND g.estado_int = 2\n" +
                                    "    LIMIT 1\n" +
                                    ")"
                    );
            _pst.setString(1, _dosimetroId);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _existe = _rs.getBoolean(1);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE: PrestamoDAO METODO: findPrestamoEnviadoExist :" + e);
        } finally
        {
            cerrarConexion();
        }
        return _existe;
    }

    @Override
    public int findPrestamoEnviadoExist2(String _dosimetroId)
    {
        int _rpta = 0;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "COUNT(1)\n" +
                                    "FROM ds_prestamo p\n" +
                                    "INNER JOIN ds_guia g\n" +
                                    "ON g.pk_guia_id_int = p.fk_guia_id_int\n" +
                                    "WHERE\n" +
                                    "p.fk_dosimetro_id_text LIKE ?\n" +
                                    "AND \n" +
                                    "(\n" +
                                    "    p.estado_int = 2 \n" +
                                    "    OR p.estado_int = 7\n" +
                                    ")\n" +
                                    "AND g.estado_int = 2"
                    );
            _pst.setString(1, _dosimetroId);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _rpta = _rs.getInt(1);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE: PrestamoDAO METODO: findPrestamoEnviadoExist2 :" + e);
        } finally
        {
            cerrarConexion();
        }
        return _rpta;
    }

    @Override
    public boolean findPrestamoRecibidoExist(String _prestamoId)
    {
        boolean _existe = false;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT EXISTS\n" +
                                    "\t( \n" +
                                    "    \tSELECT 1\n" +
                                    "    \tFROM ds_prestamo pres\n" +
                                    "    \tWHERE pres.fk_dosimetro_id_text LIKE ?\n" +
                                    "    \tAND pres.estado_int = 3\n" +
                                    "\t)"
                    );
            _pst.setString(1, _prestamoId);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _existe = _rs.getBoolean(1);
            }
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE: PrestamoDAO METODO: findPrestamoRecibidoExist :" + e);
        } finally
        {
            cerrarConexion();
        }
        return _existe;
    }

    @Override
    public void addAsignacion(int _prestamoId, String _dosimetroId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_prestamo\n" +
                                    "SET\n" +
                                    "    prestamo_fecha_salida_date = CURRENT_DATE,\n" +
                                    "    estado_int = 2,\n" +//ENVIADO
                                    "    fk_dosimetro_id_text = ?\n" +
                                    "WHERE\n" +
                                    "    pk_prestamo_id_int = ?;\n"
                    );
            _pst.setString(1, _dosimetroId);
            _pst.setInt(2, _prestamoId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase PrestamoDAO error addAsignacion() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void deleteAsignacion(int _prestamoId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_prestamo\n" +
                                    "SET\n" +
                                    "    prestamo_fecha_llegada_date = CURRENT_DATE,\n" +
                                    "    estado_int = 6\n" +//ANULADO
                                    "WHERE\n" +
                                    "    pk_prestamo_id_int = ?;\n"
                    );
            _pst.setInt(1, _prestamoId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase PrestamoDAO error deleteAsignacion() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void changeEstadoLeido(String _dosimetroId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "   ds_prestamo\n" +
                                    "SET\n" +
                                    "   estado_int = 4\n" +
                                    "WHERE\n" +
                                    "   fk_dosimetro_id_text LIKE ?\n" +
                                    "   AND estado_int = 3"
                    );
            _pst.setString(1, _dosimetroId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase PrestamoDAO error changeEstadoLeido() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void changeEstadoRecibido(String _dosimetroId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "   ds_prestamo\n" +
                                    "SET\n" +
                                    "   prestamo_fecha_llegada_date = CURRENT_DATE,\n" +
                                    "   estado_int = 3\n" +
                                    "WHERE\n" +
                                    "   pk_prestamo_id_int =\n" +
                                    "   (\n" +
                                    "        SELECT\n" +
                                    "        p.pk_prestamo_id_int\n" +
                                    "        FROM ds_prestamo p\n" +
                                    "        INNER JOIN ds_guia g\n" +
                                    "        ON g.pk_guia_id_int = p.fk_guia_id_int\n" +
                                    "        WHERE\n" +
                                    "        p.fk_dosimetro_id_text LIKE ?\n" +
                                    "        AND p.estado_int = 2\n" +
                                    "        AND g.estado_int = 2\n" +
                                    "        LIMIT 1\n" +
                                    "    )"
                    );
            _pst.setString(1, _dosimetroId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase PrestamoDAO error changeEstadoLeido() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void cambioDosimetro(Prestamo _prestamo, int _guiaId, int _poeId, String _dosimetroId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_prestamo\n" +
                                    "    (\n" +
                                    "        estado_int,\n" +
                                    "        tipo_exposicion_int,\n" +
                                    "        tipo_radiacion_text,\n" +
                                    "        tipo_dosimetro_text,\n" +
                                    "        externo_bool,\n" +
                                    "        fk_guia_id_int,\n" +
                                    "        fk_poe_id_int,\n" +
                                    "        fk_dosimetro_id_text,\n" +
                                    "        prestamo_fecha_salida_date\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        CURRENT_DATE\n" +
                                    "    );"
                    );
            _pst.setInt(1, _prestamo.get_estado());
            _pst.setInt(2, _prestamo.get_tipoExpo());
            _pst.setString(3, _prestamo.get_tipoRad());
            _pst.setString(4, _prestamo.get_tipoDosi());
            _pst.setBoolean(5,_prestamo.is_externo());
            _pst.setInt(6, _guiaId);
            _pst.setInt(7, _poeId);
            _pst.setString(8, _dosimetroId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE PrestamoDAO METODO cambioDosimetro() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public ArrayList<DosimetroRecibido> listDosimetrosRecibidos(String _periodo)
    {
        ArrayList<DosimetroRecibido> _list = new ArrayList<>();
        abrirConexion();
        try
        {
            DateTimeFormatter _dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate _date = LocalDate.parse(_periodo, _dtf);

            _conn.setAutoCommit(false);
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    p.pk_prestamo_id_int,\n" +
                                    "    p.tipo_radiacion_text,\n" +
                                    "    p.tipo_dosimetro_text,\n" +
                                    "    p.tipo_exposicion_int,\n" +
                                    "    p.fk_dosimetro_id_text\n" +
                                    "FROM ds_guia g\n" +
                                    "INNER JOIN ds_prestamo p\n" +
                                    "ON g.pk_guia_id_int = p.fk_guia_id_int\n" +
                                    "WHERE\n" +
                                    "    g.periodo_date = ?\n" +
                                    "    AND g.estado_int = 2\n" +
                                    "    AND p.estado_int = 3"
                    );

            _pst.setObject(1, _date);

            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                DosimetroRecibido _item = new DosimetroRecibido();
                _item.set_prestamoId(_rs.getInt("pk_prestamo_id_int"));
                _item.set_tipoRad(_rs.getString("tipo_radiacion_text"));
                _item.set_tipoDos(_rs.getString("tipo_dosimetro_text"));
                _item.set_tipoExp(_rs.getInt("tipo_exposicion_int"));
                _item.set_dosimetroId(_rs.getString("fk_dosimetro_id_text"));

                _list.add(_item);
            }
            _conn.commit();
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE PoeDAO METODO findPoeByGuia : " + e);
            try
            {
                _conn.rollback();
            } catch (SQLException e2)
            {
                System.out.println("error rollback: " + e2);
            }
        } finally
        {
            cerrarConexion();
        }
        return _list;
    }
}
