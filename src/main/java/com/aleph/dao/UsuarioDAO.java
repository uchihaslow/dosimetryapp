package com.aleph.dao;

import com.aleph.model.Usuario;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UsuarioDAO extends PgSQLDatabase implements IUsuarioDAO
{
    @Override
    public void addUsuario(Usuario _usuario)
    {

    }

    @Override
    public List<Usuario> listUsuario()
    {

        return null;
    }

    @Override
    public void removeUsuario(Usuario _usuario)
    {

    }

    @Override
    public void updateUsuario(Usuario _usuario)
    {

    }

    @Override
    public Usuario login(String _username, String _password)
    {
        Usuario _usuario = null;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    ds_usuario.pk_usuario_id_int,\n" +
                                    "    ds_usuario.nombre_usuario_text,\n" +
                                    "    ds_usuario.clave_text,\n" +
                                    "    ds_usuario.acceso_int\n" +
                                    "FROM\n" +
                                    "    ds_usuario\n" +
                                    "WHERE\n" +
                                    "    ds_usuario.nombre_usuario_text = ?\n" +
                                    "AND ds_usuario.clave_text = ?\n" +
                                    "AND ds_usuario.habilitado_bool = true ;"
                    );
            _pst.setString(1, _username);
            _pst.setString(2, _password);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _usuario = new Usuario();
                _usuario.set_usuarioId(_rs.getInt("pk_usuario_id_int"));
                _usuario.set_nombreUsuario(_rs.getString("nombre_usuario_text"));
                _usuario.set_clave(_rs.getString("clave_text"));
                _usuario.set_access(_rs.getInt("acceso_int"));
            }
        } catch (SQLException e)
        {
            System.err.println("Error 4: Usuario " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _usuario;
    }

}
