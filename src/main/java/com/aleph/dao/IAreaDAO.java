package com.aleph.dao;

import com.aleph.model.Area;

import java.util.ArrayList;
import java.util.List;

public interface IAreaDAO
{
    public int addArea(Area _area, int _sedeId);

    public List<Area> listArea();

    public void removeArea(int _idBuscado);

    public void updateArea(Area _area);

    public List<Area> findAreaBySede(int _idCliente);

    public List<Area> findAreaByNombre(String _nombreBuscado);

    public ArrayList findAreaByPoe(int _poeId);

    public boolean findAreaByPoeExist(int _areaId, int _poeId);

    public void addAreaByPoe(int _areaId, int _poeId);

    public void updateAreaPoeHabilitar(int _areaId, int _poeId);

    public void updateAreaPoeDeshabilitar(int _areaId, int _poeId);
}
