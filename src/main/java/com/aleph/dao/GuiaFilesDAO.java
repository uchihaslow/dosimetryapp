package com.aleph.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class GuiaFilesDAO extends PgSQLDatabase implements IGuiaFilesDAO
{
    public void deshabilitarByGuia(int _guiaId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_guia_files\n" +
                                    "SET\n" +
                                    "    habilitado_bool = FALSE\n" +
                                    "WHERE   \n" +
                                    "    fk_guia_id_int = ?;"
                    );
            _pst.setInt(1, _guiaId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase GuiaDAO error deshabilitarByOne() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }
}
