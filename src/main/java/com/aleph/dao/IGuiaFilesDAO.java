package com.aleph.dao;

public interface IGuiaFilesDAO
{
    public void deshabilitarByGuia(int _guiaId);
}
