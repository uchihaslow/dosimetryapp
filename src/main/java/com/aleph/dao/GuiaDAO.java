package com.aleph.dao;

import com.aleph.model.Guia;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GuiaDAO extends PgSQLDatabase implements IGuiaDAO
{
    @Override
    public int addGuia(Guia _guia, int _areaId)
    {
        int _codigo_ultimo_guia_id = 0;
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_guia\n" +
                                    "    (\n" +
                                    "        periodo_date,\n" +
                                    "        estado_int,\n" +
                                    "        nro_orden_pedido_text,\n" +
                                    "        fecha_orden_pedido_date,\n" +
                                    "        externo_text,\n" +
                                    "        fk_area_id_int\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        CURRENT_DATE,\n" +
                                    "        ?\n," +
                                    "        ?\n" +
                                    "    )\n" +
                                    "    RETURNING pk_guia_id_int"
                    );
            _pst.setObject(1, _guia.get_periodo());
            _pst.setInt(2, _guia.get_estado());
            _pst.setString(3, _guia.get_nroOrdenPedido());
            _pst.setString(4, _guia.get_externo());
            _pst.setInt(5, _areaId);
            _pst.execute();
            ResultSet _codigo_ultimo_guia = _pst.getResultSet();
            if (_codigo_ultimo_guia.next())
            {
                _codigo_ultimo_guia_id = _codigo_ultimo_guia.getInt(1);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Guia METODO addGuia() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _codigo_ultimo_guia_id;
    }

    @Override
    public List<Guia> listGuia()
    {
        ArrayList<Guia> _guiaList = new ArrayList<Guia>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    ds_guia.pk_guia_id_int,\n" +
                                    "    ds_guia.nro_orden_pedido_text,\n" +
                                    "    ds_guia.fecha_orden_pedido_date\n" +
                                    "FROM\n" +
                                    "    ds_guia\n" +
                                    "WHERE\n" +
                                    "    ds_guia.habilitado_bool = true\n" +
                                    "AND ds_guia.estado_int = 0 ;"//GENERADO
                    );
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Guia _guia = new Guia();
                _guia.set_guiaId(_rs.getInt("pk_guia_id_int"));
                _guia.set_nroOrdenPedido(_rs.getString("nro_orden_pedido_text"));
                _guia.set_fechaOrdenPedido(_rs.getObject("fecha_orden_pedido_date", LocalDate.class));
                _guiaList.add(_guia);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE GuiaDAO METODO ListGuia: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _guiaList;
    }

    public ObservableList listGuiaSinAsignar()
    {
        ObservableList _guiaSinAsignarList = FXCollections.observableArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    guia.pk_guia_id_int,\n" +
                                    "    guia.nro_orden_pedido_text,\n" +
                                    "    sede.nombre_text,\n" +
                                    "    COUNT(pres.pk_prestamo_id_int) AS prestamo_cantidad,\n" +
                                    "    cliente.razon_social_text,\n" +
                                    "    area.nombre_text,\n" +
                                    "    sede.direccion_text,\n" +
                                    "    guia.periodo_date,\n" +
                                    "    cliente.ruc_text,\n" +
                                    "    guia.externo_text\n" +
                                    "FROM\n" +
                                    "    ds_cliente cliente\n" +
                                    "    INNER JOIN ds_sede sede\n" +
                                    "    ON  cliente.pk_cliente_id_int = sede.fk_cliente_id_int\n" +
                                    "    INNER JOIN ds_area area\n" +
                                    "    ON  sede.pk_sede_id_int = area.fk_sede_id_int\n" +
                                    "    INNER JOIN ds_guia guia\n" +
                                    "    ON  area.pk_area_id_int = guia.fk_area_id_int\n" +
                                    "    INNER JOIN ds_prestamo pres\n" +
                                    "    ON  guia.pk_guia_id_int = pres.fk_guia_id_int\n" +
                                    "WHERE\n" +
                                    "    guia.estado_int = 1 \n" +//EN PROCESO
                                    "    AND\n" +
                                    "    guia.estado_int <> 5 \n" +//ANULADO
                                    "    GROUP BY\n" +
                                    "    guia.pk_guia_id_int,\n" +
                                    "    guia.nro_orden_pedido_text,\n" +
                                    "    sede.nombre_text,\n" +
                                    "    cliente.razon_social_text,\n" +
                                    "    area.nombre_text,\n" +
                                    "    sede.direccion_text,\n" +
                                    "    guia.periodo_date,\n" +
                                    "    cliente.ruc_text,\n" +
                                    "    guia.externo_text\n" +
                                    "ORDER BY\n" +
                                    "    guia.pk_guia_id_int"
                    );
            _rs = _pst.executeQuery();
            DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("MMM/yyyy");
            while (_rs.next())
            {
                ObservableList<String> _listaRowGuia = FXCollections.observableArrayList();
                _listaRowGuia.add(_rs.getString(1));
                _listaRowGuia.add(_rs.getString(2));
                _listaRowGuia.add(_rs.getString(3));
                _listaRowGuia.add(_rs.getString(4));
                _listaRowGuia.add(_rs.getString(5));
                _listaRowGuia.add(_rs.getString(6));
                _listaRowGuia.add(_rs.getString(7));
                _listaRowGuia.add(_rs.getDate(8).toLocalDate().format(dtf3).toUpperCase());
                _listaRowGuia.add(_rs.getString(9));
                _listaRowGuia.add(_rs.getString(10));
                System.out.println("Row [1] added " + _listaRowGuia);
                _guiaSinAsignarList.add(_listaRowGuia);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE GuiaDAO METODO listGuiaSinAsignar: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _guiaSinAsignarList;
    }

    public ObservableList listGuiaAsignadas()
    {
        ObservableList _guiaAsignadoList = FXCollections.observableArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    guia.pk_guia_id_int,\n" +
                                    "    guia.nro_orden_pedido_text,\n" +
                                    "    sede.nombre_text,\n" +
                                    "    COUNT(pres.pk_prestamo_id_int) AS prestamo_cantidad,\n" +
                                    "    cliente.razon_social_text,\n" +
                                    "    area.nombre_text,\n" +
                                    "    sede.direccion_text,\n" +
                                    "    guia.periodo_date,\n" +
                                    "    cliente.ruc_text\n" +
                                    //"    guia.externo_text\n" +
                                    "FROM\n" +
                                    "    ds_cliente cliente\n" +
                                    "INNER JOIN ds_sede sede\n" +
                                    "ON  cliente.pk_cliente_id_int = sede.fk_cliente_id_int\n" +
                                    "INNER JOIN ds_area area\n" +
                                    "ON  sede.pk_sede_id_int = area.fk_sede_id_int\n" +
                                    "INNER JOIN ds_guia guia\n" +
                                    "ON  area.pk_area_id_int = guia.fk_area_id_int\n" +
                                    "INNER JOIN ds_prestamo pres\n" +
                                    "ON  guia.pk_guia_id_int = pres.fk_guia_id_int\n" +
                                    "WHERE\n" +
                                    "      guia.estado_int = 2" +
                                    "      AND pres.estado_int <> 6" +//NO ANULADO
                                    "GROUP BY\n" +
                                    "    guia.pk_guia_id_int,\n" +
                                    "    guia.nro_orden_pedido_text,\n" +
                                    "    sede.nombre_text,\n" +
                                    "    cliente.razon_social_text,\n" +
                                    "    area.nombre_text,\n" +
                                    "    sede.direccion_text,\n" +
                                    "    guia.periodo_date,\n" +
                                    "    cliente.ruc_text\n" +
                                    //"    guia.externo_text\n" +
                                    "ORDER BY\n" +
                                    "    guia.pk_guia_id_int"
                    );
            _rs = _pst.executeQuery();
            DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("MMM/yyyy");
            while (_rs.next())
            {
                ObservableList<Object> _listaRowGuia = FXCollections.observableArrayList();
                _listaRowGuia.add(_rs.getString(1));//guia_id
                _listaRowGuia.add(_rs.getString(2));//nro_orden_pedido
                _listaRowGuia.add(_rs.getString(3));//sede
                _listaRowGuia.add(_rs.getString(4));//cantidad
                _listaRowGuia.add(_rs.getString(5));//cliente
                _listaRowGuia.add(_rs.getString(6));//area
                _listaRowGuia.add(_rs.getString(7));//sede_direccion
                _listaRowGuia.add(_rs.getDate(8).toLocalDate().format(dtf3).toUpperCase());//periodo
                _listaRowGuia.add(_rs.getString(9));//cliente_ruc
                //_listaRowGuia.add(_rs.getString(10));//guia_externo
                System.out.println("Row [1] added " + _listaRowGuia);
                _guiaAsignadoList.add(_listaRowGuia);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE GuiaDAO METODO listGuiaAsignados: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _guiaAsignadoList;
    }

    @Override
    public void removeGuia(int _idBuscado)
    {

    }

    @Override
    public void updateGuia(Guia _guia)
    {

    }

    @Override
    public void aprobarOrden(int _guiaId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_guia\n" +
                                    "SET\n" +
                                    "    estado_int = 1\n" +//EN PROCESO
                                    "WHERE\n" +
                                    "    pk_guia_id_int = ?;\n"
                    );
            _pst.setInt(1, _guiaId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE GuiaDAO METODO aprobarOrden" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<Guia> findGuiaById(int _idBuscado)
    {
        return null;
    }

    @Override
    public List<Guia> findGuiaByCliente(String _clienteBuscado)
    {
        return null;
    }

    @Override
    public List<Guia> findGuiaByRuc(String _rucBuscado)
    {
        return null;
    }

    @Override
    public boolean findOrdenPedido(String _nroOrdenPedido)
    {
        boolean _encontrado = false;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    ds_guia.nro_orden_pedido_text\n" +
                                    "FROM\n" +
                                    "    ds_guia\n" +
                                    "WHERE\n" +
                                    "    ds_guia.nro_orden_pedido_text = ? ;\n"
                    );
            _pst.setString(1, _nroOrdenPedido);
            _rs = _pst.executeQuery();
            if (_rs.next())
            {
                _encontrado = true;
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Guia METODO findOrdenPedido : " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _encontrado;
    }

    public void changeToEnviado(int _guiaId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_guia\n" +
                                    "SET\n" +
                                    "    estado_int = 2\n" +//ENVIADO
                                    "WHERE\n" +
                                    "    pk_guia_id_int = ?;\n"
                    );
            _pst.setInt(1, _guiaId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase GuiaDAO error changeToEnviado() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    public void uploadFile(File _file, int _guiaId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_guia_files\n" +
                                    "    (\n" +
                                    "        file_bytea,\n" +
                                    "        fk_guia_id_int\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    );"
                    );
            FileInputStream _fis = new FileInputStream(_file);
            _pst.setBinaryStream(1, _fis, (int) _file.length());
            _pst.setInt(2, _guiaId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.out.println("error :" + e);
        } catch (FileNotFoundException e)
        {
            System.out.println("error 2 :" + e);
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void downloadFile(int _guiaId, File _file)
    {
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    gf.file_bytea\n" +
                                    "FROM\n" +
                                    "    ds_guia_files gf\n" +
                                    "WHERE\n" +
                                    "    gf.fk_guia_id_int = ?\n" +
                                    "    AND gf.habilitado_bool = true"
                    );
            _pst.setInt(1, _guiaId);
            _rs = _pst.executeQuery();
            FileOutputStream fos = new FileOutputStream(_file);
            byte[] _bytes = null;
            while (_rs.next())
            {
                _bytes = _rs.getBytes("file_bytea");
            }
            fos.write(_bytes);
            fos.close();
        } catch (SQLException e)
        {
            System.out.println("error :" + e);
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            cerrarConexion();
        }
        //return _archivo;
    }

    @Override
    public List<Object> listaDosimetrosPorGuia(ArrayList<Integer> _listaGuias)
    {
        Object[] _listaGuiasDosimetros = _listaGuias.toArray();
//        Object[] _listaEtiquetas;
        List<Object> DATA = null;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "        sp_ascii(c.razon_social_text) as razon_social,\n" +
                                    "        g.pk_guia_id_int as guia,\n" +
                                    "        EXTRACT(MONTH FROM g.periodo_date) as periodo_month,\n" +
                                    "        to_char(g.periodo_date, 'YYYY') as periodo_year,\n" +
                                    "        p.fk_dosimetro_id_text as codigo_dosimetro,\n" +
                                    "        concat(p.tipo_dosimetro_text, p.tipo_exposicion_int, '-', p.tipo_radiacion_text) as dosexprad,\n" +
                                    "        sp_ascii(CONCAT\n" +
                                    "        (\n" +
                                    "        UPPER(poe.apellidos_text),\n" +
                                    "        ' ',\n" +
                                    "        poe.nombres_text,\n" +
                                    "        CASE WHEN p.externo_bool = false THEN '' ELSE '-Ext' END\n" +
                                    "        )) as nombre_completo" +
                                    "        FROM ds_cliente c\n" +
                                    "INNER JOIN ds_sede s\n" +
                                    "ON c.pk_cliente_id_int = s.fk_cliente_id_int\n" +
                                    "INNER JOIN ds_area a\n" +
                                    "ON s.pk_sede_id_int = a.fk_sede_id_int\n" +
                                    "INNER JOIN ds_guia g\n" +
                                    "ON a.pk_area_id_int = g.fk_area_id_int\n" +
                                    "INNER JOIN ds_prestamo p\n" +
                                    "ON g.pk_guia_id_int = p.fk_guia_id_int\n" +
                                    "INNER JOIN ds_poe poe\n" +
                                    "ON poe.pk_poe_id_int = p.fk_poe_id_int \n" +
                                    "WHERE\n" +
                                    "        g.pk_guia_id_int = ANY(?::INT[])\n" +
                                    "        AND (g.estado_int = 2 OR g.estado_int = 3 OR g.estado_int = 4)\n" +
                                    "        AND p.estado_int <> 6 \n" +
                                    "ORDER BY \n" +
                                    "        g.pk_guia_id_int DESC,\n" +
                                    "        poe.apellidos_text DESC"
                    );
            _pst.setArray(1, _conn.createArrayOf("int4", _listaGuiasDosimetros));
            _rs = _pst.executeQuery();
            ArrayList<Object[]> _listaTemp = new ArrayList<>();
            while (_rs.next())
            {
                Object[] _row = new Object[0];
//                System.out.print(_rs.getString("razon_social"));
//                System.out.print("\t" + _rs.getString("guia"));
//                System.out.print("\t" + to_month(_rs.getInt("periodo_month")) + "/" + _rs.getString("periodo_year"));
//                System.out.print("\t" + _rs.getString("codigo_dosimetro"));
//                System.out.print("\t" + _rs.getString("dosexprad"));
//                System.out.print("\t" + _rs.getString("nombre_completo") + "\n");

                _row = appendValue(_row, _rs.getString("razon_social"));
                _row = appendValue(_row, _rs.getString("guia"));
                _row = appendValue(_row, to_month(_rs.getInt("periodo_month")) + "/" + _rs.getString("periodo_year"));
                _row = appendValue(_row, _rs.getString("codigo_dosimetro"));
                _row = appendValue(_row, _rs.getString("dosexprad"));
                _row = appendValue(_row, _rs.getString("nombre_completo"));

                System.out.println(_row[0] + " " + _row[1] + " " + _row[2] + " " + _row[3] + " " + _row[4] + " " + _row[5]);
                _listaTemp.add(_row);
            }

            DATA = Arrays.asList(_listaTemp.toArray());

            System.out.println("print ARRAY");

            for (int i = 0; i < DATA.size(); i++)
            {
                Object[] d = (Object[]) DATA.get(i);
                System.out.print(d[0] + "\t");
                System.out.print(d[1] + "\t");
                System.out.print(d[2] + "\t");
                System.out.print(d[3] + "\t");
                System.out.print(d[4] + "\t");
                System.out.print(d[5] + "\t");
                System.out.print("\n");
            }

        } catch (SQLException e)
        {
            System.out.println("Error Metodo listaDosimetrosPorGuia" + e);
        } finally
        {
            cerrarConexion();
        }
        return DATA;
    }

    @Override
    public List<Object> listaSobresPorGuia(ArrayList<Integer> _listaGuias)
    {
        Object[] _listaGuiasSobres = _listaGuias.toArray();
        List<Object> DATA = null;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "        g.pk_guia_id_int as guia,\n" +
                                    "        c.razon_social_text as razon_social,\n" +
                                    "        s.nombre_text as sede,\n" +
                                    "        s.direccion_text as sede_direccion,\n" +
                                    "        a.nombre_text as area,\n" +
                                    "        EXTRACT(MONTH FROM g.periodo_date) as periodo_month,\n" +
                                    "        to_char(g.periodo_date, 'YYYY') as periodo_year,\n" +
                                    "        a.contacto_text as contacto\n" +
                                    "FROM ds_cliente c\n" +
                                    "INNER JOIN ds_sede s\n" +
                                    "ON c.pk_cliente_id_int = s.fk_cliente_id_int\n" +
                                    "INNER JOIN ds_area a\n" +
                                    "ON s.pk_sede_id_int = a.fk_sede_id_int\n" +
                                    "INNER JOIN ds_guia g\n" +
                                    "ON a.pk_area_id_int = g.fk_area_id_int\n" +
                                    "WHERE\n" +
                                    "        g.pk_guia_id_int = ANY(?::INT[])\n" +
                                    "        AND (g.estado_int = 2 OR g.estado_int = 3 OR g.estado_int = 4)\n" +
                                    "ORDER BY\n" +
                                    "        g.pk_guia_id_int DESC"
                    );
            _pst.setArray(1, _conn.createArrayOf("int4", _listaGuiasSobres));
            _rs = _pst.executeQuery();
            ArrayList<Object[]> _listaTemp = new ArrayList<>();
            while (_rs.next())
            {
                Object[] _row = new Object[0];

                _row = appendValue(_row, _rs.getString("guia"));
                _row = appendValue(_row, _rs.getString("razon_social"));
                _row = appendValue(_row, _rs.getString("sede"));
                _row = appendValue(_row, _rs.getString("sede_direccion"));
                _row = appendValue(_row, _rs.getString("area"));
                _row = appendValue(_row, to_month(_rs.getInt("periodo_month")) + "/" + _rs.getString("periodo_year"));
                _row = appendValue(_row, _rs.getString("contacto"));


                System.out.println(_row[0] + " " + _row[1] + " " + _row[2] + " " + _row[3] + " " + _row[4] + " " + _row[5] + " " + _row[6]);
                _listaTemp.add(_row);
            }

            DATA = Arrays.asList(_listaTemp.toArray());

            System.out.println("print ARRAY");

            for (int i = 0; i < DATA.size(); i++)
            {
                Object[] d = (Object[]) DATA.get(i);
                System.out.print(d[0] + "\t");
                System.out.print(d[1] + "\t");
                System.out.print(d[2] + "\t");
                System.out.print(d[3] + "\t");
                System.out.print(d[4] + "\t");
                System.out.print(d[5] + "\t");
                System.out.print(d[6] + "\t");
                System.out.print("\n");
            }

        } catch (SQLException e)
        {
            System.out.println("Error Metodo listaSobresPorGuia " + e);
        } finally
        {
            cerrarConexion();
        }
        return DATA;
    }

    public static String to_month(int _mes)
    {
        String _charMonth;
        switch (_mes)
        {
            case 1:
                _charMonth = "ENE";
                break;
            case 2:
                _charMonth = "FEB";
                break;
            case 3:
                _charMonth = "MAR";
                break;
            case 4:
                _charMonth = "ABR";
                break;
            case 5:
                _charMonth = "MAY";
                break;
            case 6:
                _charMonth = "JUN";
                break;
            case 7:
                _charMonth = "JUL";
                break;
            case 8:
                _charMonth = "AGO";
                break;
            case 9:
                _charMonth = "SET";
                break;
            case 10:
                _charMonth = "OCT";
                break;
            case 11:
                _charMonth = "NOV";
                break;
            case 12:
                _charMonth = "DIC";
                break;
            default:
                _charMonth = "ERROR";
                break;
        }

        return _charMonth;
    }

    private Object[] appendValue(Object[] obj, Object newObj)
    {

        ArrayList<Object> temp = new ArrayList<Object>(Arrays.asList(obj));
        temp.add(newObj);
        return temp.toArray();
    }

    private static final String SQL_SELECT_GUIA_X_CRITERIOS = "";
}