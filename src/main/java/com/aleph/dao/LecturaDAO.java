package com.aleph.dao;

import com.aleph.model.Lectura;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LecturaDAO extends PgSQLDatabase implements ILecturaDAO
{
    @Override
    public void addLectura(Lectura _lectura)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO ds_lectura\n" +
                                    "(\n" +
                                    "      efectiva_num   ,\n" +
                                    "      cristalino_num ,\n" +
                                    "      piel_num       ,\n" +
                                    "      fk_prestamo_id_int\n" +
                                    ")\n" +
                                    "VALUES\n" +
                                    "(\n" +
                                    "      ? ,\n" +
                                    "      ? ,\n" +
                                    "      ? ,\n" +
                                    "      (\n" +
                                    "            SELECT pres.pk_prestamo_id_int\n" +
                                    "            FROM ds_prestamo pres\n" +
                                    "            WHERE pres.fk_dosimetro_id_text LIKE ?\n" +
                                    "            AND pres.estado_int = 3\n" +
                                    "      )\n" +
                                    ");"
                    );
            _pst.setDouble(1, _lectura.get_efectiva());
            _pst.setDouble(2, _lectura.get_cristalino());
            _pst.setDouble(3, _lectura.get_piel());
            _pst.setString(4, _lectura.get_badgeId());
            _pst.execute();
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE: LecturaDAO METODO: addLectura :" + e);
        }
        finally
        {
            cerrarConexion();
        }
    }
}
