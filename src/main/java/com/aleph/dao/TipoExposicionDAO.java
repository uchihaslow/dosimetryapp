package com.aleph.dao;

import com.aleph.model.TipoExposicion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TipoExposicionDAO extends PgSQLDatabase implements ITipoExposicionDAO
{

    @Override
    public void addTipoExposicion(TipoExposicion _tipoExposicion, int _poeId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_tipo_exposicion\n" +
                                    "    (\n" +
                                    "        cuerpo_entero_bool,\n" +
                                    "        dedo_mano_derecha_bool,\n" +
                                    "        dedo_mano_izquierda_bool,\n" +
                                    "        muneca_derecha_bool,\n" +
                                    "        muneca_izquierda_bool,\n" +
                                    "        otra_parte_cuerpo_bool,\n" +
                                    "        fk_poe_id_int\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    );\n"
                    );
            _pst.setBoolean(1, _tipoExposicion.is_cuerpoEntero());
            _pst.setBoolean(2, _tipoExposicion.is_dedoManoDerecha());
            _pst.setBoolean(3, _tipoExposicion.is_dedoManoIzquierda());
            _pst.setBoolean(4, _tipoExposicion.is_munecaDerecha());
            _pst.setBoolean(5, _tipoExposicion.is_munecaIzquierda());
            _pst.setBoolean(6, _tipoExposicion.is_otraParteCuerpo());
            _pst.setInt(7, _poeId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE TipoExposicion METODO addTipoExposicion() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<TipoExposicion> listTipoExposicion()
    {
        return null;
    }

    @Override
    public void removeTipoExposicion(int _idBuscado)
    {

    }

    @Override
    public void updateTipoExposicion(TipoExposicion _tipoExposicion)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_tipo_exposicion\n" +
                                    "SET\n" +
                                    "    cuerpo_entero_bool = ?,\n" +
                                    "    dedo_mano_derecha_bool = ?,\n" +
                                    "    dedo_mano_izquierda_bool = ?,\n" +
                                    "    muneca_derecha_bool = ?,\n" +
                                    "    muneca_izquierda_bool = ?,\n" +
                                    "    otra_parte_cuerpo_bool = ?\n" +
                                    "WHERE\n" +
                                    "    pk_tipo_exposicion_id_int = ?;"
                    );
            _pst.setBoolean(1, _tipoExposicion.is_cuerpoEntero());
            _pst.setBoolean(2, _tipoExposicion.is_dedoManoDerecha());
            _pst.setBoolean(3, _tipoExposicion.is_dedoManoIzquierda());
            _pst.setBoolean(4, _tipoExposicion.is_munecaDerecha());
            _pst.setBoolean(5, _tipoExposicion.is_munecaIzquierda());
            _pst.setBoolean(6, _tipoExposicion.is_otraParteCuerpo());
            _pst.setInt(7, _tipoExposicion.get_tipoexpId());
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase TipoExposicion error updateTipoExposicion() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public TipoExposicion findTipoExposicionByPoe(int _idPoe)
    {
        TipoExposicion _tipoExposicion = new TipoExposicion();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    texp.pk_tipo_exposicion_id_int,\n" +
                                    "    texp.cuerpo_entero_bool,\n" +
                                    "    texp.dedo_mano_derecha_bool,\n" +
                                    "    texp.dedo_mano_izquierda_bool,\n" +
                                    "    texp.muneca_derecha_bool,\n" +
                                    "    texp.muneca_izquierda_bool,\n" +
                                    "    texp.otra_parte_cuerpo_bool\n" +
                                    "FROM\n" +
                                    "    ds_tipo_exposicion texp\n" +
                                    "WHERE\n" +
                                    "    texp.fk_poe_id_int = ?\n" +
                                    "AND texp.habilitado_bool = true ;"
                    );
            _pst.setInt(1, _idPoe);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _tipoExposicion.set_tipoexpId(_rs.getInt("pk_tipo_exposicion_id_int"));
                _tipoExposicion.set_cuerpoEntero(_rs.getBoolean("cuerpo_entero_bool"));
                _tipoExposicion.set_dedoManoDerecha(_rs.getBoolean("dedo_mano_derecha_bool"));
                _tipoExposicion.set_dedoManoIzquierda(_rs.getBoolean("dedo_mano_izquierda_bool"));
                _tipoExposicion.set_munecaDerecha(_rs.getBoolean("muneca_derecha_bool"));
                _tipoExposicion.set_munecaIzquierda(_rs.getBoolean("muneca_izquierda_bool"));
                _tipoExposicion.set_otraParteCuerpo(_rs.getBoolean("otra_parte_cuerpo_bool"));
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE TipoExposicion METODO findTipoExposicionByPoe" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _tipoExposicion;
    }
}
