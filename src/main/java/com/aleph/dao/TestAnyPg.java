package com.aleph.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

public class TestAnyPg
{

    public static void main(String[] args)
    {
//        Integer[] array = new Integer[]{22150, 22154};
        ArrayList<Integer> list_array = new ArrayList<>();
        list_array.add(22150);
        list_array.add(22154);
        Object[] array =  list_array.toArray();
        //IntArray intArray = new IntArray(array);

        try
        {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://192.168.1.155:5432/postgres?currentSchema=prod2";
            Connection _conn = DriverManager.getConnection(url, "postgres", "aleph");
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            //"set lc_time='Spanish_Spain.1252';\n" +
                            "SELECT\n" +
                                    "        sp_ascii(c.razon_social_text) as razon_social,\n" +
                                    "        g.pk_guia_id_int as guia,\n" +
                                    "        EXTRACT(MONTH FROM g.periodo_date) as periodo_month,\n" +
                                    "        to_char(g.periodo_date, 'YYYY') as periodo_year,\n" +
                                    "        p.fk_dosimetro_id_text as codigo_dosimetro,\n" +
                                    "        concat(p.tipo_dosimetro_text, p.tipo_exposicion_int, '-', p.tipo_radiacion_text) as dosexprad,\n" +
                                    "        sp_ascii(concat(UPPER(poe.apellidos_text),' ',poe.nombres_text,g.externo_text)) as nombre_completo\n" +
                                    "FROM ds_cliente c\n" +
                                    "INNER JOIN ds_sede s\n" +
                                    "ON c.pk_cliente_id_int = s.fk_cliente_id_int\n" +
                                    "INNER JOIN ds_area a\n" +
                                    "ON s.pk_sede_id_int = a.fk_sede_id_int\n" +
                                    "INNER JOIN ds_guia g\n" +
                                    "ON a.pk_area_id_int = g.fk_area_id_int\n" +
                                    "INNER JOIN ds_prestamo p\n" +
                                    "ON g.pk_guia_id_int = p.fk_guia_id_int\n" +
                                    "INNER JOIN ds_poe poe\n" +
                                    "ON poe.pk_poe_id_int = p.fk_poe_id_int \n" +
                                    "WHERE\n" +
                                    "        g.pk_guia_id_int = ANY(?::INT[])\n" +
                                    "        AND (g.estado_int = 2 OR g.estado_int = 3 OR g.estado_int = 4)\n" +
                                    "        AND p.estado_int <> 6 \n" +
                                    "ORDER BY \n" +
                                    "        g.pk_guia_id_int DESC,\n" +
                                    "        poe.apellidos_text DESC"
                    );
            _pst.setArray(1, _conn.createArrayOf("int4", array));
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                System.out.println(_rs.getString("razon_social"));
                System.out.println(_rs.getString("guia"));
                System.out.println(to_month(_rs.getInt("periodo_month"))+"/"+_rs.getString("periodo_year") );
                System.out.println(_rs.getString("codigo_dosimetro"));
                System.out.println(_rs.getString("dosexprad"));
                System.out.println(_rs.getString("nombre_completo"));
            }
            _rs.close();
            _pst.close();
            _conn.close();
        } catch (SQLException e)
        {
            System.err.println("Error " + e.getMessage());
        } catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    public static String to_month(int _mes)
    {
        String _charMonth;
        switch (_mes)
        {
            case 1:
                _charMonth = "ENE";
                break;
            case 2:
                _charMonth = "FEB";
                break;
            case 3:
                _charMonth = "MAR";
                break;
            case 4:
                _charMonth = "ABR";
                break;
            case 5:
                _charMonth = "MAY";
                break;
            case 6:
                _charMonth = "JUN";
                break;
            case 7:
                _charMonth = "JUL";
                break;
            case 8:
                _charMonth = "AGO";
                break;
            case 9:
                _charMonth = "SET";
                break;
            case 10:
                _charMonth = "OCT";
                break;
            case 11:
                _charMonth = "NOV";
                break;
            case 12:
                _charMonth = "DIC";
                break;
            default:
                _charMonth = "ERROR";
                break;
        }

        return _charMonth;
    }

    private static Object[] appendValue(Object[] obj, Object newObj) {

        ArrayList<Object> temp = new ArrayList<Object>(Arrays.asList(obj));
        temp.add(newObj);
        return temp.toArray();
    }
}
