package com.aleph.dao;

import com.aleph.model.Area;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AreaDAO extends PgSQLDatabase implements IAreaDAO
{
    @Override
    public int addArea(Area _area, int _sedeId)
    {
        int _codigo_ultimo_area_id = 0;

        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_area\n" +
                                    "    (\n" +
                                    "        nombre_text,\n" +
                                    "        contacto_text,\n" +
                                    "        fk_sede_id_int\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    )\n" +
                                    "    RETURNING pk_area_id_int;"
                    );
            _pst.setString(1, _area.get_nombre());
            _pst.setString(2,_area.get_contacto());
            _pst.setInt(3, _sedeId);
            _pst.execute();

            ResultSet _codigo_ultimo_area = _pst.getResultSet();
            if (_codigo_ultimo_area.next())
            {
                _codigo_ultimo_area_id = _codigo_ultimo_area.getInt(1);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Area METODO addArea() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _codigo_ultimo_area_id;
    }

    @Override
    public List<Area> listArea()
    {
        return null;
    }

    @Override
    public void removeArea(int _idBuscado)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_area\n" +
                                    "SET\n" +
                                    "    habilitado_bool = false\n" +
                                    "WHERE\n" +
                                    "    pk_area_id_int = ?;"
                    );
            _pst.setInt(1, _idBuscado);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE AreaDAO METODO removeArea" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void updateArea(Area _area)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_area\n" +
                                    "SET\n" +
                                    "    nombre_text = ?,\n" +
                                    "    contacto_text = ?\n" +
                                    "WHERE\n" +
                                    "    pk_area_id_int = ?;\n"
                    );
            _pst.setString(1, _area.get_nombre());
            _pst.setString(2, _area.get_contacto());
            _pst.setInt(3, _area.get_areaId());
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase Area error updateArea() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<Area> findAreaBySede(int _idCliente)
    {
        ArrayList<Area> _areaList = new ArrayList<Area>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    ar.pk_area_id_int,\n" +
                                    "    ar.nombre_text,\n" +
                                    "    ar.contacto_text\n" +
                                    "FROM\n" +
                                    "    ds_sede as sd\n" +
                                    "INNER JOIN\n" +
                                    "    ds_area as ar\n" +
                                    "ON\n" +
                                    "    (\n" +
                                    "        sd.pk_sede_id_int = ar.fk_sede_id_int)\n" +
                                    "WHERE\n" +
                                    "    sd.pk_sede_id_int = ?\n" +
                                    "AND sd.habilitado_bool = true\n" +
                                    "AND ar.habilitado_bool = true "
                    );
            _pst.setInt(1, _idCliente);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Area _area = new Area();
                _area.set_areaId(_rs.getInt("pk_area_id_int"));
                _area.set_nombre(_rs.getString("nombre_text"));
                _area.set_contacto(_rs.getString("contacto_text"));
                _areaList.add(_area);
            }
        } catch (SQLException e)
        {
            System.out.println("ERORR CLASE: AreaDAO METODO: findAreaBySede :" + e);
        } finally
        {
            cerrarConexion();
        }
        return _areaList;
    }

    @Override
    public List<Area> findAreaByNombre(String _nombreBuscado)
    {
        return null;
    }

    @Override
    public ArrayList findAreaByPoe(int _poeId)
    {
        ArrayList _areaPoeList = new ArrayList();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "        area.pk_area_id_int as area_pk,\n" +
                                    "        area.nombre_text as area_nombre,\n" +
                                    "        sede.nombre_text as sede_nombre,\n" +
                                    "        cli.razon_social_text as cliente_nombre\n" +
                                    "FROM\n" +
                                    "        ds_poe poe\n" +
                                    "        INNER JOIN ds_area_ds_poe ap\n" +
                                    "        ON poe.pk_poe_id_int = ap.fk_poe_id_int\n" +
                                    "        INNER JOIN ds_area area\n" +
                                    "        ON area.pk_area_id_int = ap.fk_area_id_int\n" +
                                    "        INNER JOIN ds_sede sede\n" +
                                    "        ON area.fk_sede_id_int = sede.pk_sede_id_int\n" +
                                    "        INNER JOIN ds_cliente cli\n" +
                                    "        ON cli.pk_cliente_id_int = sede.fk_cliente_id_int\n" +
                                    "WHERE\n" +
                                    "        poe.pk_poe_id_int = ?\n" +
                                    "        AND ap.habilitado_bool = true\n" +
                                    "        AND area.habilitado_bool = true\n" +
                                    "        AND sede.habilitado_bool = true\n" +
                                    "        AND cli.habilitado_bool = true\n" +
                                    "        AND poe.habilitado_bool = true\n" +
                                    "        "
                    );
            _pst.setInt(1, _poeId);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                ObservableList<String> _listaRowGuia = FXCollections.observableArrayList();
                _listaRowGuia.add(_rs.getString("area_pk"));
                _listaRowGuia.add(_rs.getString("cliente_nombre"));
                _listaRowGuia.add(_rs.getString("sede_nombre"));
                _listaRowGuia.add(_rs.getString("area_nombre"));
                System.out.println("Row [1] added " + _listaRowGuia);
                _areaPoeList.add(_listaRowGuia);
            }
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE GuiaDAO METODO findAreaByPoe: " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _areaPoeList;
    }

    @Override
    public boolean findAreaByPoeExist(int _areaId, int _poeId)
    {
        boolean _exist = false;
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT EXISTS\n" +
                                    "(\n" +
                                    "        SELECT 1\n" +
                                    "        FROM ds_area_ds_poe ap\n" +
                                    "        WHERE\n" +
                                    "        ap.fk_poe_id_int = ?\n" +
                                    "        AND ap.fk_area_id_int = ?\n" +
                                    ")"
                    );
            _pst.setInt(1, _poeId);
            _pst.setInt(2, _areaId);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                _exist = _rs.getBoolean(1);
            }
        } catch (SQLException e)
        {
            System.out.println("ERORR CLASE: AreaDAO METODO: findAreaByPoeExist :" + e);
        } finally
        {
            cerrarConexion();
        }
        return _exist;
    }

    @Override
    public void addAreaByPoe(int _areaId, int _poeId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT INTO\n" +
                                    "ds_area_ds_poe\n" +
                                    "(\n" +
                                    "        fk_poe_id_int,\n" +
                                    "        fk_area_id_int\n" +
                                    ")\n" +
                                    "VALUES\n" +
                                    "(\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    ");\n"
                    );
            _pst.setInt(1, _poeId);
            _pst.setInt(2, _areaId);
            _pst.execute();
        } catch (SQLException e)
        {
            System.out.println("ERROR CLASE: AreaDAO METODO: addAreaByPoe :" + e);
        }
        finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void updateAreaPoeHabilitar(int _areaId, int _poeId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "        ds_area_ds_poe\n" +
                                    "SET\n" +
                                    "        habilitado_bool = true\n" +
                                    "WHERE\n" +
                                    "fk_poe_id_int = ?\n" +
                                    "AND fk_area_id_int = ?"
                    );
            _pst.setInt(1, _poeId);
            _pst.setInt(2, _areaId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE: AreaDAO METODO: updateAreaPoeHabilitar :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void updateAreaPoeDeshabilitar(int _areaId, int _poeId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "        ds_area_ds_poe\n" +
                                    "SET\n" +
                                    "        habilitado_bool = false\n" +
                                    "WHERE\n" +
                                    "fk_poe_id_int = ?\n" +
                                    "AND fk_area_id_int = ?"
                    );
            _pst.setInt(1, _poeId);
            _pst.setInt(2, _areaId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE: AreaDAO METODO: updateAreaPoeDeshabilitar :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }
}
