package com.aleph.dao;

import com.aleph.model.Sede;

import java.util.List;

public interface ISedeDAO
{
    public int addSede(Sede _sede);

    public List<Sede> listSede();

    public void removeSede(int _idBuscado);

    public void updateSede(Sede _sede);

    public List<Sede> findSedeByCliente(int _idCliente);

    public List<Sede> findSedeByNombre(String _sedeNom);

    public List<Sede> findSedeByDireccion(String _direccionBuscada);

    public List<Sede> findSedeByClienteNom(String _clienteNom);


}
