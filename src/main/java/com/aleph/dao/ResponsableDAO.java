package com.aleph.dao;

import com.aleph.model.Responsable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResponsableDAO extends PgSQLDatabase implements IResponsableDAO
{

    @Override
    public void addResponsable(Responsable _responsable, int _sedeId)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "INSERT\n" +
                                    "INTO\n" +
                                    "    ds_responsable\n" +
                                    "    (\n" +
                                    "        nombres_text,\n" +
                                    "        celular_text,\n" +
                                    "        email_text,\n" +
                                    "        fk_sede_id_int\n" +
                                    "    )\n" +
                                    "    VALUES\n" +
                                    "    (\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?,\n" +
                                    "        ?\n" +
                                    "    );"
                    );
            _pst.setString(1, _responsable.get_nombre());
            _pst.setString(2, _responsable.get_celular());
            _pst.setString(3, _responsable.get_email());
            _pst.setInt(4, _sedeId);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Responsable METODO addResponsable() :" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public List<Responsable> findResponsableById(int _sedeId)
    {
        List<Responsable> _listResponsable = new ArrayList<>();
        abrirConexion();
        try
        {
            ResultSet _rs;
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "SELECT\n" +
                                    "    ds_responsable.nombres_text,\n" +
                                    "    ds_responsable.celular_text,\n" +
                                    "    ds_responsable.email_text,\n" +
                                    "    ds_responsable.pk_responsable_id_int\n" +
                                    "FROM\n" +
                                    "    ds_responsable\n" +
                                    "WHERE\n" +
                                    "    ds_responsable.fk_sede_id_int = ?" +
                                    "AND ds_responsable.habilitado_bool = true;"
                    );
            _pst.setInt(1, _sedeId);
            _rs = _pst.executeQuery();
            while (_rs.next())
            {
                Responsable _responsable = new Responsable();
                _responsable.set_responsableId(_rs.getInt("pk_responsable_id_int"));
                _responsable.set_nombre(_rs.getString("nombres_text"));
                _responsable.set_celular(_rs.getString("celular_text"));
                _responsable.set_email(_rs.getString("email_text"));
                _responsable.set_habilitado(true);
                _listResponsable.add(_responsable);
            }

        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Responsable METODO findResponsableById" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
        return _listResponsable;
    }

    @Override
    public void removeResponsable(int _idBuscado)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_responsable\n" +
                                    "SET\n" +
                                    "    habilitado_bool = false,\n" +
                                    "WHERE\n" +
                                    "    pk_responsable_id_int = ?;"
                    );
            _pst.setInt(1, _idBuscado);
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("ERROR CLASE Responsable METODO removeResponsable" + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }

    @Override
    public void updateResponsable(Responsable _responsable)
    {
        abrirConexion();
        try
        {
            PreparedStatement _pst = _conn.prepareStatement
                    (
                            "UPDATE\n" +
                                    "    ds_responsable\n" +
                                    "SET\n" +
                                    "    nombres_text = ?,\n" +
                                    "    celular_text = ?,\n" +
                                    "    email_text = ?\n," +
                                    "    habilitado_bool = ?\n" +
                                    "WHERE\n" +
                                    "    pk_responsable_id_int = ?;"
                    );
            _pst.setString(1, _responsable.get_nombre());
            _pst.setString(2, _responsable.get_celular());
            _pst.setString(3, _responsable.get_email());
            _pst.setBoolean(4,_responsable.is_habilitado());
            _pst.setInt(5, _responsable.get_responsableId());
            _pst.executeUpdate();
        } catch (SQLException e)
        {
            System.err.println("Clase Responsable error updateResponsable() " + e.getMessage());
        } finally
        {
            cerrarConexion();
        }
    }
}
