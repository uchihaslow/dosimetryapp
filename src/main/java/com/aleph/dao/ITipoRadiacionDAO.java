package com.aleph.dao;

import com.aleph.model.TipoRadiacion;

import java.util.List;

public interface ITipoRadiacionDAO
{
    public void addTipoRadiacion(TipoRadiacion _tipoRadiacion, int _areaId);

    public List<TipoRadiacion> listTipoRadiacion();

    public void removeTipoRadiacion(int _idBuscado);

    public void updateTipoRadiacion(TipoRadiacion _tipoRadiacion);

    public TipoRadiacion findTipoRadiacionByArea(int _idArea);

}