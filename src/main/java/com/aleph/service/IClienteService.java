package com.aleph.service;

import com.aleph.model.Cliente;

import java.util.List;

public interface IClienteService
{
    public int addCliente(Cliente _cliente);

    public List<Cliente> listCliente();

    public void removeCliente(int _idBuscado);

    public void updateCliente(Cliente _cliente);

    public List<Cliente> findClienteById(int _idBuscado);

    public List<Cliente> findClienteByNombre(String _nombreBuscado);

    public List<Cliente> findClienteByRuc(String _rucBuscado);
}
