package com.aleph.service;

import com.aleph.dao.ITipoRadiacionDAO;
import com.aleph.dao.TipoRadiacionDAO;
import com.aleph.model.TipoRadiacion;

import java.util.List;

public class TipoRadiacionService implements ITipoRadiacionService
{
    ITipoRadiacionDAO _tipoRadiacionDAO = new TipoRadiacionDAO();

    @Override
    public void addTipoRadiacion(TipoRadiacion _tipoRadiacion, int _areaId)
    {
        _tipoRadiacionDAO.addTipoRadiacion(_tipoRadiacion, _areaId);
    }

    @Override
    public List<TipoRadiacion> listTipoRadiacion()
    {
        return null;
    }

    @Override
    public void removeTipoRadiacion(int _idBuscado)
    {

    }

    @Override
    public void updateTipoRadiacion(TipoRadiacion _tipoRadiacion)
    {
        _tipoRadiacionDAO.updateTipoRadiacion(_tipoRadiacion);
    }

    @Override
    public TipoRadiacion findTipoRadiacionByArea(int _idArea)
    {
        return _tipoRadiacionDAO.findTipoRadiacionByArea(_idArea);
    }
}
