package com.aleph.service;

import com.aleph.model.Responsable;

import java.util.List;

public interface IResponsableService
{
    public void addResponsable(Responsable _responsable, int _sedeId);

    public List<Responsable> findResponsableById(int _sedeId);

    public void removeResponsable(int _idBuscado);

    public void updateResponsable(Responsable _responsable);
}
