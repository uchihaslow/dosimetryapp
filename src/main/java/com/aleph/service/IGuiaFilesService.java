package com.aleph.service;

public interface IGuiaFilesService
{
    public void deshabilitarByGuia(int _guiaId);
}
