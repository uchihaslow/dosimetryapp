package com.aleph.service;

import com.aleph.model.Log;

import java.util.List;

public interface ILogService
{
    public void addLog(String _info);

    public List<Log> listLog();
}
