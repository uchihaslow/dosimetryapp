package com.aleph.service;

import com.aleph.dao.IPoeDAO;
import com.aleph.dao.PoeDAO;
import com.aleph.model.Poe;

import java.util.List;

public class PoeService implements IPoeService
{
    IPoeDAO _poeDAO = new PoeDAO();

    @Override
    public void addPoe(Poe _poe)
    {
        _poeDAO.addPoe(_poe);
    }

    @Override
    public void addPoeWithArea(int _poeId, int _areaId)
    {
        _poeDAO.addPoeWithArea(_poeId, _areaId);
    }

    @Override
    public List<Poe> listPoe()
    {
        return null;
    }

    @Override
    public void removePoe(int _idBuscado)
    {
        _poeDAO.removePoe(_idBuscado);
    }

    @Override
    public void updatePoe(Poe _poe)
    {
        _poeDAO.updatePoe(_poe);
    }

    @Override
    public List<Poe> findPoeByArea(int _idArea)
    {
        return _poeDAO.findPoeByArea(_idArea);
    }

    @Override
    public List<Poe> findPoeByNombre(String _nombreBuscado)
    {
        return _poeDAO.findPoeByNombre(_nombreBuscado);
    }

    @Override
    public boolean findPoeByDniExist(String _dniBuscado)
    {
        return _poeDAO.findPoeByDniExist(_dniBuscado);
    }

    @Override
    public List<Poe> findPoeByDni(String _dniBuscado)
    {
        return _poeDAO.findPoeByDni(_dniBuscado);
    }

    public List<Poe> findPoeByGuia(int idGuia)
    {
        return _poeDAO.findPoeByGuia(idGuia);
    }

    @Override
    public List<Poe> findPoeByGuia2(int idGuia)
    {
        return _poeDAO.findPoeByGuia2(idGuia);
    }

    @Override
    public List<Poe> findUsuariosFaltantesByGuia(int idGuia)
    {
        return _poeDAO.findUsuariosFaltantesByGuia(idGuia);
    }
}
