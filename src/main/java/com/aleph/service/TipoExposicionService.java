package com.aleph.service;

import com.aleph.dao.ITipoExposicionDAO;
import com.aleph.dao.TipoExposicionDAO;
import com.aleph.model.TipoExposicion;

import java.util.List;

public class TipoExposicionService implements ITipoExposicionService
{
    ITipoExposicionDAO _tipoExposicionDAO = new TipoExposicionDAO();

    @Override
    public void addTipoExposicion(TipoExposicion _tipoExposicion, int _poeId)
    {
        _tipoExposicionDAO.addTipoExposicion(_tipoExposicion,_poeId);
    }

    @Override
    public List<TipoExposicion> listTipoExposicion()
    {
        return null;
    }

    @Override
    public void removeTipoExposicion(int _idBuscado)
    {

    }

    @Override
    public void updateTipoExposicion(TipoExposicion _tipoExposicion)
    {
        _tipoExposicionDAO.updateTipoExposicion(_tipoExposicion);
    }

    @Override
    public TipoExposicion findTipoExposicionByPoe(int _idPoe)
    {
        return _tipoExposicionDAO.findTipoExposicionByPoe(_idPoe);
    }
}
