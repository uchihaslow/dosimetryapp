package com.aleph.service;

import com.aleph.dao.ILecturaDAO;
import com.aleph.dao.LecturaDAO;
import com.aleph.model.Lectura;

public class LecturaService implements ILecturaService
{
    ILecturaDAO _lecturaDAO = new LecturaDAO();

    @Override
    public void addLectura(Lectura _lectura)
    {
        _lecturaDAO.addLectura(_lectura);
    }
}
