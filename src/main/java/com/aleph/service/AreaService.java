package com.aleph.service;

import com.aleph.dao.AreaDAO;
import com.aleph.dao.IAreaDAO;
import com.aleph.model.Area;

import java.util.ArrayList;
import java.util.List;

public class AreaService implements IAreaService
{
    IAreaDAO _areaDAO = new AreaDAO();

    @Override
    public int addArea(Area _area, int _sedeId)
    {
        return _areaDAO.addArea(_area, _sedeId);
    }

    @Override
    public List<Area> listArea()
    {
        return null;
    }

    @Override
    public void removeArea(int _idBuscado)
    {
        _areaDAO.removeArea(_idBuscado);
    }

    @Override
    public void updateArea(Area _area)
    {
        _areaDAO.updateArea(_area);
    }

    @Override
    public List<Area> findAreaBySede(int _idSede)
    {
        return _areaDAO.findAreaBySede(_idSede);
    }

    @Override
    public List<Area> findAreaByNombre(String _nombreBuscado)
    {
        return null;
    }

    @Override
    public ArrayList findAreaByPoe(int _poeId)
    {
        return _areaDAO.findAreaByPoe(_poeId);
    }

    @Override
    public boolean findAreaByPoeExist(int _areaId, int _poeId)
    {
        return _areaDAO.findAreaByPoeExist(_areaId, _poeId);
    }

    @Override
    public void addAreaByPoe(int _areaId, int _poeId)
    {
        _areaDAO.addAreaByPoe(_areaId, _poeId);
    }

    @Override
    public void updateAreaPoeHabilitar(int _areaId, int _poeId)
    {
        _areaDAO.updateAreaPoeHabilitar(_areaId, _poeId);
    }

    @Override
    public void updateAreaPoeDeshabilitar(int _areaId, int _poeId)
    {
        _areaDAO.updateAreaPoeDeshabilitar(_areaId, _poeId);
    }
}
