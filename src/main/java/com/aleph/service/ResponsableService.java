package com.aleph.service;

import com.aleph.dao.IResponsableDAO;
import com.aleph.dao.ResponsableDAO;
import com.aleph.model.Responsable;

import java.util.List;

public class ResponsableService implements IResponsableService
{
    IResponsableDAO _responsableDAO = new ResponsableDAO();

    @Override
    public void addResponsable(Responsable _responsable, int _sedeId)
    {
        _responsableDAO.addResponsable(_responsable, _sedeId);
    }

    @Override
    public List<Responsable> findResponsableById(int _sedeId)
    {
        return _responsableDAO.findResponsableById(_sedeId);
    }

    @Override
    public void removeResponsable(int _idBuscado)
    {
        _responsableDAO.removeResponsable(_idBuscado);
    }

    @Override
    public void updateResponsable(Responsable _responsable)
    {
        _responsableDAO.updateResponsable(_responsable);
    }
}
