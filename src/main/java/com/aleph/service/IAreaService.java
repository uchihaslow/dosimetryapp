package com.aleph.service;

import com.aleph.model.Area;

import java.util.ArrayList;
import java.util.List;

public interface IAreaService
{
    public int addArea(Area _area, int sedeId);

    public List<Area> listArea();

    public void removeArea(int _idBuscado);

    public void updateArea(Area _area);

    public List<Area> findAreaBySede(int _idSede);

    public List<Area> findAreaByNombre(String _nombreBuscado);

    public ArrayList findAreaByPoe(int _poeId);

    public boolean findAreaByPoeExist(int _areaId, int _poeId);

    public void addAreaByPoe(int _areaId, int _poeId);

    public void updateAreaPoeHabilitar(int _areaId, int _poeId);

    public void updateAreaPoeDeshabilitar(int _areaId, int _poeId);
}
