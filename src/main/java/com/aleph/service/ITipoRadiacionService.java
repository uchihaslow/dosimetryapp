package com.aleph.service;

import com.aleph.model.TipoRadiacion;

import java.util.List;

public interface ITipoRadiacionService
{
    public void addTipoRadiacion(TipoRadiacion _tipoRadiacion, int areaId);

    public List<TipoRadiacion> listTipoRadiacion();

    public void removeTipoRadiacion(int _idBuscado);

    public void updateTipoRadiacion(TipoRadiacion _tipoRadiacion);

    public TipoRadiacion findTipoRadiacionByArea(int _idArea);
}
