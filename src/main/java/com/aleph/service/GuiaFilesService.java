package com.aleph.service;

import com.aleph.dao.GuiaFilesDAO;
import com.aleph.dao.IGuiaFilesDAO;

public class GuiaFilesService implements IGuiaFilesService
{
    private IGuiaFilesDAO _guiaFileDAO = new GuiaFilesDAO();

    @Override
    public void deshabilitarByGuia(int _guiaId)
    {
        _guiaFileDAO.deshabilitarByGuia(_guiaId);
    }
}
