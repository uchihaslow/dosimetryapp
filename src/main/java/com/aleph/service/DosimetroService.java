package com.aleph.service;

import com.aleph.dao.DosimetroDAO;
import com.aleph.dao.IDosimetroDAO;
import com.aleph.model.Dosimetro;
import javafx.collections.ObservableList;

import java.util.List;

public class DosimetroService implements IDosimetroService
{
    IDosimetroDAO _dosimetroDAO = new DosimetroDAO();

    @Override
    public void addDosimetro(Dosimetro _dosimetro)
    {
        _dosimetroDAO.addDosimetro(_dosimetro);
    }

    @Override
    public void removeDosimetro(String _dosimetroCodigo)
    {
        _dosimetroDAO.removeDosimetro(_dosimetroCodigo);
    }

    @Override
    public void updateDosimetro(Dosimetro _dosimetro)
    {
        _dosimetroDAO.updateDosimetro(_dosimetro);
    }

    @Override
    public List<Dosimetro> findDosimetroByCodigo(String _codigo)
    {
        return _dosimetroDAO.findDosimetroByCodigo(_codigo);
    }

    @Override
    public void changeToAsignado(String _dosimetroId)
    {
        _dosimetroDAO.changeToAsignado(_dosimetroId);
    }

    @Override
    public ObservableList NoDevueltos(int _guiaId)
    {
        return _dosimetroDAO.NoDevueltos(_guiaId);
    }

    @Override
    public ObservableList Devueltos(int _guiaId)
    {
        return _dosimetroDAO.Devueltos(_guiaId);
    }
}
