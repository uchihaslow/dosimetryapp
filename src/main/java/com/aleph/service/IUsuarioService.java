package com.aleph.service;

import com.aleph.model.Usuario;

import java.util.List;

public interface IUsuarioService
{
    public void addUsuario(Usuario _usuario);

    public List<Usuario> listUsuario();

    public void removeUsuario(Usuario _usuario);

    public void updateUsuario(Usuario _usuario);

    public Usuario login(String _username, String _password);
}
