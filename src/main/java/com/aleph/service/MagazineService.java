package com.aleph.service;

import com.aleph.dao.IMagazineDAO;
import com.aleph.dao.MagazineDAO;
import com.aleph.model.Magazine;

import java.util.ArrayList;
import java.util.List;

public class MagazineService implements IMagazineService
{
    IMagazineDAO _magazineDAO = new MagazineDAO();

    @Override
    public void addDosimetroLimpio(String _dosimetroId)
    {
        _magazineDAO.addDosimetroLimpio(_dosimetroId);
    }

    @Override
    public void deshabilitarDosimetro(String _dosimetroId)
    {
        _magazineDAO.deshabilitarDosimetro(_dosimetroId);
    }

    @Override
    public ArrayList<String> findDosimetrosLimpios(int _cantidad)
    {
        return _magazineDAO.findDosimetrosLimpios(_cantidad);
    }

    @Override
    public boolean findDosimetroExist(String _codigo)
    {
        return _magazineDAO.findDosimetroExist(_codigo);
    }

    @Override
    public ArrayList<Magazine> ListDosimetros()
    {
        return _magazineDAO.ListDosimetros();
    }

    @Override
    public List<Magazine> findDosimetroByCodigo(String _codigo)
    {
        return _magazineDAO.findDosimetroByCodigo(_codigo);
    }

    @Override
    public int CantDosimHabilitados()
    {
        return _magazineDAO.CantDosimHabilitados();
    }

}
