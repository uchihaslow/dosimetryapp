package com.aleph.service;

import com.aleph.model.Lectura;

public interface ILecturaService
{
    public void addLectura(Lectura _lectura);
}
