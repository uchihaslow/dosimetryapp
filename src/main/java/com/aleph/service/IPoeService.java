package com.aleph.service;

import com.aleph.model.Poe;

import java.util.List;

public interface IPoeService
{
    public void addPoe(Poe _poe);

    public void addPoeWithArea(int _poeId, int _areaId);

    public List<Poe> listPoe();

    public void removePoe(int _idBuscado);

    public void updatePoe(Poe _poe);

    public List<Poe> findPoeByArea(int _idArea);

    public List<Poe> findPoeByNombre(String _nombreBuscado);

    public boolean findPoeByDniExist(String _dniBuscado);

    public List<Poe> findPoeByDni(String _dniBuscado);

    public List<Poe> findPoeByGuia(int idGuia);

    public List<Poe> findPoeByGuia2(int idGuia);

    public List<Poe> findUsuariosFaltantesByGuia(int idGuia);
}
