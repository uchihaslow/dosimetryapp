package com.aleph.service;

import com.aleph.model.Prestamo;
import com.aleph.tools.DosimetroRecibido;

import java.util.ArrayList;

public interface IPrestamoService
{
    public void addPrestamo(Prestamo _prestamo, int _guiaId, int _poeId);

    public ArrayList<Prestamo> findPrestamosByGuia(int _guiaId);

    public boolean findPrestamoEnviadoExist(String dosimetroId);

    public int findPrestamoEnviadoExist2(String dosimetroId);

    public boolean findPrestamoRecibidoExist(String dosimetroId);

    public void addAsignacion(int _prestamoId, String _dosimetroId);

    public void changeEstadoLeido(String _dosimetroId);

    public void changeEstadoRecibido(String _dosimetroId);

    public void deleteAsignacion(int _prestamoId);

    public void cambioDosimetro(Prestamo _prestamo, int _guiaId, int _poeId, String _dosimetroId);

    public ArrayList<DosimetroRecibido> listDosimetrosRecibidos(String _periodo);
}
