package com.aleph.service;

import com.aleph.dao.IReporteDAO;
import com.aleph.dao.ReporteDAO;
import com.aleph.model.DosisMultiReport;
import com.aleph.service.IReporteService;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class ReporteService implements IReporteService
{
    IReporteDAO _reporteDAO = new ReporteDAO();

    @Override
    public ObservableList listReporteSinFinalizar()
    {
        return _reporteDAO.listReporteSinFinalizar();
    }

    @Override
    public ObservableList listReporteFinalizado()
    {
        return _reporteDAO.listReporteFinalizado();
    }

    @Override
    public ObservableList listReporteSinFinalizarByIdGuia(int _idGuia)
    {
        return _reporteDAO.listReporteSinFinalizarByIdGuia(_idGuia);
    }

    @Override
    public ObservableList listReporteFinalizadoByIdGuia(int _idGuia)
    {
        return _reporteDAO.listReporteFinalizadoByIdGuia(_idGuia);
    }

    @Override
    public ArrayList listDatosComunesReporte(int _idGuia)
    {
        return _reporteDAO.listDatosComunesReporte(_idGuia);
    }

    @Override
    public List<DosisMultiReport> listDosisByPrestamo(int _idGuia)
    {
        return _reporteDAO.listDosisByPrestamo(_idGuia);
    }

    @Override
    public ObservableList listInformeSinFinalizarByGuia(int _idGuia)
    {
        return _reporteDAO.listInformeSinFinalizarByGuia(_idGuia);
    }

    @Override
    public ObservableList listInformeSinFinalizar()
    {
        return _reporteDAO.listInformeSinFinalizar();
    }

    @Override
    public ObservableList listGuiaReporteByPeriodo(List<String> _periodos)
    {
        return _reporteDAO.listGuiaReporteByPeriodo(_periodos);
    }
}
