package com.aleph.service;

import com.aleph.dao.ILogDAO;
import com.aleph.dao.LogDAO;
import com.aleph.model.Log;

import java.util.List;

public class LogService implements ILogService
{
    ILogDAO _logDAO = new LogDAO();

    @Override
    public void addLog(String _info)
    {
        _logDAO.addLog(_info);
    }

    @Override
    public List<Log> listLog()
    {
        return _logDAO.listLog();
    }
}
