package com.aleph.service;

import com.aleph.dao.ISedeDAO;
import com.aleph.dao.SedeDAO;
import com.aleph.model.Sede;

import java.util.List;

public class SedeService implements ISedeService
{
    private ISedeDAO _sedeDAO = new SedeDAO();

    @Override
    public int addSede(Sede _sede)
    {
        return _sedeDAO.addSede(_sede);
    }

    @Override
    public List<Sede> listSede()
    {
        return null;
    }

    @Override
    public void removeSede(int _idBuscado)
    {
        _sedeDAO.removeSede(_idBuscado);
    }

    @Override
    public void updateSede(Sede _sede)
    {
        _sedeDAO.updateSede(_sede);
    }

    @Override
    public List<Sede> findSedeByCliente(int _idCliente)
    {
        return _sedeDAO.findSedeByCliente(_idCliente);
    }

    @Override
    public List<Sede> findSedeByNombre(String _sedeNom)
    {
        return _sedeDAO.findSedeByNombre(_sedeNom);
    }

    @Override
    public List<Sede> findSedeByDireccion(String _direccionBuscada)
    {
        return _sedeDAO.findSedeByDireccion(_direccionBuscada);
    }

    @Override
    public List<Sede> findSedeByClienteNom(String _clienteNom)
    {
        return  _sedeDAO.findSedeByClienteNom(_clienteNom);
    }
}
