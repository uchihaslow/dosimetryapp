package com.aleph.service;

import com.aleph.dao.ClienteDAO;
import com.aleph.dao.IClienteDAO;
import com.aleph.model.Cliente;

import java.util.List;

public class ClienteService implements IClienteService
{
    private IClienteDAO _clienteDAO = new ClienteDAO();

    @Override
    public int addCliente(Cliente _cliente)
    {
        return _clienteDAO.addCliente(_cliente);
    }

    @Override
    public List<Cliente> listCliente()
    {
        return _clienteDAO.listCliente();
    }

    @Override
    public void removeCliente(int _idBuscado)
    {
        _clienteDAO.removeCliente(_idBuscado);
    }

    @Override
    public void updateCliente(Cliente _cliente)
    {
        _clienteDAO.updateCliente(_cliente);
    }

    @Override
    public List<Cliente> findClienteById(int _idBuscado)
    {
        return null;
    }

    @Override
    public List<Cliente> findClienteByNombre(String _nombreBuscado)
    {
        return _clienteDAO.findClienteByNombre(_nombreBuscado);
    }

    @Override
    public List<Cliente> findClienteByRuc(String _rucBuscado)
    {
        return _clienteDAO.findClienteByRuc(_rucBuscado);
    }
}
