package com.aleph.service;

import com.aleph.dao.IUsuarioDAO;
import com.aleph.dao.UsuarioDAO;
import com.aleph.model.Usuario;

import java.util.List;

public class UsuarioService implements IUsuarioService
{

	private IUsuarioDAO _usuarioDAO = new UsuarioDAO();

	@Override
	public void addUsuario(Usuario _usuario)
	{

	}

	@Override
	public List<Usuario> listUsuario()
	{
		return null;
	}

	@Override
	public void removeUsuario(Usuario _usuario)
	{

	}

	@Override
	public void updateUsuario(Usuario _usuario)
	{

	}

	@Override
	public Usuario login(String _username, String _password)
	{
		return _usuarioDAO.login(_username, _password);
	}

}
