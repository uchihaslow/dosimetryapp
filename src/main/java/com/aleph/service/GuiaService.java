package com.aleph.service;

import com.aleph.dao.GuiaDAO;
import com.aleph.dao.IGuiaDAO;
import com.aleph.model.Guia;
import javafx.collections.ObservableList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GuiaService implements IGuiaService
{
    IGuiaDAO _guiaDAO = new GuiaDAO();

    @Override
    public int addGuia(Guia _guia, int _areaId)
    {
        return _guiaDAO.addGuia(_guia, _areaId);
    }

    @Override
    public List<Guia> listGuia()
    {
        return _guiaDAO.listGuia();
    }

    @Override
    public ObservableList listGuiaSinAsignar()
    {
        return _guiaDAO.listGuiaSinAsignar();
    }

    public ObservableList listGuiaAsignadas()
    {
        return _guiaDAO.listGuiaAsignadas();
    }

    @Override
    public void removeGuia(int _idBuscado)
    {

    }

    @Override
    public void updateGuia(Guia _guia)
    {

    }

    @Override
    public void aprobarOrden(int _guiaId)
    {
        _guiaDAO.aprobarOrden(_guiaId);
    }

    @Override
    public List<Guia> findGuiaById(int _idBuscado)
    {
        return null;
    }

    @Override
    public List<Guia> findGuiaByCliente(String _clienteBuscado)
    {
        return null;
    }

    @Override
    public List<Guia> findGuiaByRuc(String _rucBuscado)
    {
        return null;
    }

    @Override
    public boolean findOrdenPedido(String _nroOrdenPedido)
    {
        return _guiaDAO.findOrdenPedido(_nroOrdenPedido);
    }

    @Override
    public void changeToEnviado(int _guiaId)
    {
        _guiaDAO.changeToEnviado(_guiaId);
    }

    @Override
    public void uploadFile(File _file, int _guiaId)
    {
        _guiaDAO.uploadFile(_file, _guiaId);
    }

    @Override
    public void downloadFile(int _guiaId, File _file)
    {
        _guiaDAO.downloadFile(_guiaId, _file);
    }

    @Override
    public List<Object> listaDosimetrosPorGuia(ArrayList<Integer> _listaGuias)
    {
        return _guiaDAO.listaDosimetrosPorGuia(_listaGuias);
    }

    @Override
    public List<Object> listaSobresPorGuia(ArrayList<Integer> _listaGuias)
    {
        return _guiaDAO.listaSobresPorGuia(_listaGuias);
    }
}
