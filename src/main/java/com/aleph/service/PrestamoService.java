package com.aleph.service;

import com.aleph.dao.IPrestamoDAO;
import com.aleph.dao.PrestamoDAO;
import com.aleph.model.Prestamo;
import com.aleph.tools.DosimetroRecibido;

import java.util.ArrayList;

public class PrestamoService implements IPrestamoService
{
    IPrestamoDAO _prestamoDAO = new PrestamoDAO();

    @Override
    public void addPrestamo(Prestamo _prestamo, int _guiaId, int _poeId)
    {
        _prestamoDAO.addPrestamo(_prestamo, _guiaId, _poeId);
    }

    @Override
    public ArrayList<Prestamo> findPrestamosByGuia(int _guiaId)
    {
        return _prestamoDAO.findPrestamosByGuia(_guiaId);
    }

    @Override
    public boolean findPrestamoEnviadoExist(String _dosimetroId)
    {
        return _prestamoDAO.findPrestamoEnviadoExist(_dosimetroId);
    }

    @Override
    public int findPrestamoEnviadoExist2(String _dosimetroId)
    {
        return _prestamoDAO.findPrestamoEnviadoExist2(_dosimetroId);
    }

    @Override
    public boolean findPrestamoRecibidoExist(String dosimetroId)
    {
        return _prestamoDAO.findPrestamoRecibidoExist(dosimetroId);
    }

    @Override
    public void addAsignacion(int _prestamoId, String _dosimetroId)
    {
        _prestamoDAO.addAsignacion(_prestamoId, _dosimetroId);
    }

    @Override
    public void changeEstadoLeido(String _dosimetroId)
    {
        _prestamoDAO.changeEstadoLeido(_dosimetroId);
    }

    @Override
    public void changeEstadoRecibido(String _dosimetroId)
    {
        _prestamoDAO.changeEstadoRecibido(_dosimetroId);
    }

    @Override
    public void deleteAsignacion(int _prestamoId)
    {
        _prestamoDAO.deleteAsignacion(_prestamoId);
    }

    @Override
    public void cambioDosimetro(Prestamo _prestamo, int _guiaId, int _poeId, String _dosimetroId)
    {
        _prestamoDAO.cambioDosimetro(_prestamo, _guiaId, _poeId, _dosimetroId);
    }

    @Override
    public ArrayList<DosimetroRecibido> listDosimetrosRecibidos(String _periodo)
    {
        return _prestamoDAO.listDosimetrosRecibidos(_periodo);
    }
}