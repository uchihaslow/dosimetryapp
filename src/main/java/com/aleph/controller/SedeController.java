package com.aleph.controller;

import com.aleph.model.Paths;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.event.ActionEvent;

import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class SedeController
{
    @FXML
    private AnchorPane _anchorPaneSede;
    @FXML
    private Button _btnAgregarSede;
    @FXML
    private Button _btnBuscarSede;

    // Event Listener on Button[#_btnAgregarSede].onAction
    @FXML
    public void OnActionBtnAgregarSede(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._sedeAgregarPath));
        Parent _SedeAgregarPageParent = _loader.load();

        Stage _SedeAgregarModalStage = new Stage();
        Scene _SedeAgregarPageScene = new Scene(_SedeAgregarPageParent);
        _SedeAgregarModalStage.setScene(_SedeAgregarPageScene);
        _SedeAgregarModalStage.initModality(Modality.WINDOW_MODAL);
        _SedeAgregarModalStage.initOwner(_anchorPaneSede.getScene().getWindow());
        _SedeAgregarModalStage.setResizable(false);
        _SedeAgregarModalStage.setMaximized(false);
        _SedeAgregarModalStage.show();
    }

    // Event Listener on Button[#_btnBuscarSede].onAction
    @FXML
    public void OnActionBtnBuscarSede(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._sedeBuscarPath));
        Parent _SedeBuscarPageParent = _loader.load();

        Stage _SedeBuscarModalStage = new Stage();
        Scene _SedeBuscarPageScene = new Scene(_SedeBuscarPageParent);
        _SedeBuscarModalStage.setScene(_SedeBuscarPageScene);
        _SedeBuscarModalStage.initModality(Modality.WINDOW_MODAL);
        _SedeBuscarModalStage.initOwner(_anchorPaneSede.getScene().getWindow());
        _SedeBuscarModalStage.setResizable(false);
        _SedeBuscarModalStage.setMaximized(false);
        _SedeBuscarModalStage.show();
    }
}
