package com.aleph.controller;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class CustomRowAsignacion extends AnchorPane
{
    @FXML
    private TextField _txtNombrePoe;

    @FXML
    private TextField _txtCodigoDosimetro;

    @FXML
    private Label _labNumeracion;

    public CustomRowAsignacion()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/RowAsignacion.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        } catch (IOException exception)
        {
            throw new RuntimeException(exception);
        }
    }

    public String get_txtNombrePoe()
    {
        return textPropertyNombrePoe().get();
    }

    public void set_txtNombrePoe(String _value)
    {
        textPropertyNombrePoe().set(_value);
    }

    public StringProperty textPropertyNombrePoe()
    {
        return _txtNombrePoe.textProperty();
    }

    public String get_txtCodigoDosimetro()
    {
        return textPropertyCodigoDosimetro().get();
    }

    public void set_txtCodigoDosimetro(String _value)
    {
        textPropertyCodigoDosimetro().set(_value);
    }

    public StringProperty textPropertyCodigoDosimetro()
    {
        return _txtCodigoDosimetro.textProperty();
    }

    public String get_labNumeracion()
    {
        return textPropertyNumeracion().get();
    }

    public void set_labNumeracion(String _value)
    {
        textPropertyNumeracion().set(_value);
    }

    public StringProperty textPropertyNumeracion()
    {
        return _labNumeracion.textProperty();
    }
}
