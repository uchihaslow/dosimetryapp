package com.aleph.controller;

import com.aleph.model.Area;
import com.aleph.model.Paths;
import com.aleph.model.Sede;
import com.aleph.model.TipoRadiacion;
import com.aleph.service.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

import javafx.event.ActionEvent;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class AreaController
{
    private ISedeService _sedeService = new SedeService();
    private IAreaService _areaService = new AreaService();
    private ITipoRadiacionService _tipoRadiacionService = new TipoRadiacionService();

    @FXML
    private AnchorPane _anchorPaneArea;
    @FXML
    private Button _btnBuscarCliente;
    @FXML
    private Label _lblRuc;
    @FXML
    private Label _lblCliente;
    @FXML
    private ComboBox _cboxSede;
    @FXML
    private TableView<Area> _tvArea;
    @FXML
    private Button _btnAgregarArea;
    @FXML
    private Button _btnEliminarArea;
    @FXML
    private Button _btnEditarCliente;
    @FXML
    private Button _btnSeleccionarCliente;

    //variable auxiliar
    private int _clienteIdRecibido;

    // Event Listener on Button[#_btnBuscarCliente].onAction
    @FXML
    public void OnActionBtnBuscarCliente(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._areaBuscarClientePath));
            Stage _stage = new Stage();
            _stage.initOwner(_btnBuscarCliente.getScene().getWindow());
            _stage.setScene(new Scene((Parent) _loader.load()));

            _stage.initModality(Modality.WINDOW_MODAL);
            _stage.showAndWait();

            AreaBuscarClienteController _controller = _loader.getController();
            _lblRuc.setText(_controller.getRucSend());
            _lblCliente.setText(_controller.getRazonSocialSend());
            _clienteIdRecibido = _controller.getIdCliente();

            filtrarCBoxSedes(_clienteIdRecibido);
            _cboxSede.setDisable(false);
        } catch (IOException e)
        {
            System.out.println("CLASE Area Metodo OnActionBtnBuscarCliente" + e.getMessage());
        }
    }

    // Event Listener on Button[#_btnAgregarArea].onAction
    @FXML
    public void OnActionBtnAgregarArea(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._areaAgregarPath));
            Parent _areaAgregarPageParent = _loader.load();

            AreaAgregarController _controller = _loader.getController();
            _controller.setSede((Sede) _cboxSede.getSelectionModel().getSelectedItem());

            Stage _areaAgregarModalStage = new Stage();
            Scene _areaAgregarPageScene = new Scene(_areaAgregarPageParent);
            _areaAgregarModalStage.setScene(_areaAgregarPageScene);
            _areaAgregarModalStage.initModality(Modality.WINDOW_MODAL);
            _areaAgregarModalStage.initOwner(_anchorPaneArea.getScene().getWindow());
            _areaAgregarModalStage.setResizable(false);
            _areaAgregarModalStage.setMaximized(false);
            _areaAgregarModalStage.showAndWait();
            filtrarTVArea();

        } catch (IOException e)
        {
            System.out.println("ERROR : " + e);
        }
    }

    // Event Listener on Button[#_btnEliminarArea].onAction
    @FXML
    public void OnActionBtnEliminarArea(ActionEvent event)
    {
        Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
        _alert.setTitle("Cuadro de confirmación");
        _alert.setHeaderText("Eliminar Area");
        _alert.setContentText("¿Quiere elminar esta área?");

        Optional<ButtonType> _opcion = _alert.showAndWait();
        if (_opcion.get() == ButtonType.OK)
        {
            _areaService.removeArea(_tvArea.getSelectionModel().getSelectedItem().get_areaId());
        }
        filtrarTVArea();
    }

    // Event Listener on Button[#_btnEditarCliente].onAction
    @FXML
    public void OnActionBtnEditarArea(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._areaEditarClientePath));
            Parent _areaEditarPageParent = _loader.load();

            AreaEditarController _controller = _loader.getController();
            _controller.setSede((Sede) _cboxSede.getSelectionModel().getSelectedItem());
            _controller.setArea(_tvArea.getSelectionModel().getSelectedItem());
//            TipoRadiacion _tipoRadiacion = new TipoRadiacion();
//            _tipoRadiacion = _tipoRadiacionService.findTipoRadiacionByArea(_tvArea.getSelectionModel().getSelectedItem().get_areaId());
//            _controller.setTipoRadiacion(_tipoRadiacion);

            Stage _areaEditarModalStage = new Stage();
            Scene _areaEditarPageScene = new Scene(_areaEditarPageParent);
            _areaEditarModalStage.setScene(_areaEditarPageScene);
            _areaEditarModalStage.initModality(Modality.WINDOW_MODAL);
            _areaEditarModalStage.initOwner(_anchorPaneArea.getScene().getWindow());
            _areaEditarModalStage.setResizable(false);
            _areaEditarModalStage.setMaximized(false);
            _areaEditarModalStage.showAndWait();
            filtrarTVArea();

        } catch (IOException e)
        {
            System.out.println("ERROR : " + e);
        }
    }

    // Event Listener on Button[#_btnSeleccionarCliente].onAction
    @FXML
    public void OnActionBtnSeleccionarArea(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._areaDetalleClientePath));
            Parent _areaDetallePageParent = _loader.load();

            AreaDetalleController _controller = _loader.getController();
            _controller.setSede((Sede) _cboxSede.getSelectionModel().getSelectedItem());
            _controller.setArea(_tvArea.getSelectionModel().getSelectedItem());
//            TipoRadiacion _tipoRadiacion = new TipoRadiacion();
//            _tipoRadiacion = _tipoRadiacionService.findTipoRadiacionByArea(_tvArea.getSelectionModel().getSelectedItem().get_areaId());
//            _controller.setTipoRadiacion(_tipoRadiacion);

            Stage _areaDetalleModalStage = new Stage();
            Scene _areaDetallePageScene = new Scene(_areaDetallePageParent);
            _areaDetalleModalStage.setScene(_areaDetallePageScene);
            _areaDetalleModalStage.initModality(Modality.WINDOW_MODAL);
            _areaDetalleModalStage.initOwner(_anchorPaneArea.getScene().getWindow());
            _areaDetalleModalStage.setResizable(false);
            _areaDetalleModalStage.setMaximized(false);
            _areaDetalleModalStage.showAndWait();

        } catch (IOException e)
        {
            System.out.println("ERROR : " + e);
        }
    }

    // Event Listener on ComboBox[#_cboxSede].onAction
    @FXML
    public void OnActionCboxSede(ActionEvent event)
    {
        filtrarTVArea();
    }

    private void filtrarCBoxSedes(int _clienteId)
    {
        _cboxSede.getItems().clear();
        ObservableList<Sede> _sedeList = FXCollections.observableArrayList(_sedeService.findSedeByCliente(_clienteId));
        _cboxSede.getItems().addAll(_sedeList);
    }

    private void filtrarTVArea()
    {
        _tvArea.getItems().clear();
        ObservableList<Area> _areaList = FXCollections.observableList(_areaService.findAreaBySede(((Sede) _cboxSede.getSelectionModel().getSelectedItem()).get_sedeId()));
        _tvArea.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_areaId"));
        _tvArea.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_nombre"));
        _tvArea.setItems(_areaList);
    }
}
