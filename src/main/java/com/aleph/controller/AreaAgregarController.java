package com.aleph.controller;

import com.aleph.model.Area;
import com.aleph.model.Sede;
import com.aleph.model.TipoRadiacion;
import com.aleph.service.AreaService;
import com.aleph.service.IAreaService;
import com.aleph.service.ITipoRadiacionService;
import com.aleph.service.TipoRadiacionService;
import javafx.fxml.FXML;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import javafx.event.ActionEvent;

import javafx.scene.layout.AnchorPane;

import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

public class AreaAgregarController
{
    private IAreaService _areaService = new AreaService();
    private ITipoRadiacionService _tipoRadiacionService = new TipoRadiacionService();

    @FXML
    private AnchorPane _anchorPaneAgregarArea;
    @FXML
    private CheckBox _chbRayosXGAlta;
    @FXML
    private CheckBox _chbRayosXGMedia;
    @FXML
    private CheckBox _chbRayosXGBaja;
    @FXML
    private CheckBox _chbRayosXG;
    @FXML
    private CheckBox _chbNeutrones;
    @FXML
    private CheckBox _chbBeta;
    @FXML
    private TextField _txtNombreSede;
    @FXML
    private TextField _txtNombreArea;
    @FXML
    private Button _btnGuardarArea;
    @FXML
    private Button _btnCancelar;
    @FXML
    public TextField _txtContacto;

    //variables temporales
    TipoRadiacion _tipoRadiacionTemp = new TipoRadiacion();
    private int codigo_ultimo_area;
    private int _sedeIdTemp;

    // Event Listener on Button[#_btnGuardarArea].onAction
    @FXML
    public void OnActionBtnGuardarArea(ActionEvent event)
    {
        if (_txtNombreArea.getText().isEmpty())
        {
            new AlertBox("el campo nombre de area está vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else
        {
            try
            {
                Area _area = new Area();
                _area.set_nombre(_txtNombreArea.getText());
                _area.set_contacto(_txtContacto.getText());
                int _codigo_ultimo_area = _areaService.addArea(_area, _sedeIdTemp);

                //_tipoRadiacionService.addTipoRadiacion(_tipoRadiacionTemp, _codigo_ultimo_area);

                new AlertBox("Area registrada con éxito!!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
                OnActionBtnCancelar(event);

            } catch (RuntimeException e)
            {
                System.err.println("Error : " + e);
            }
        }
//        System.out.println(_tipoRadiacionTemp.toString());
    }

    // Event Listener on Button[#_btnCancelar].onAction
    @FXML
    public void OnActionBtnCancelar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneAgregarArea.getScene().getWindow();
        _stage.close();
    }

    public void OnActionCheckTipoRadiacion(ActionEvent event)
    {
//        if (_chbRayosXGAlta.isSelected())
//        {
//            _tipoRadiacionTemp.set_rayosXgAlta(true);
//        } else
//        {
//            _tipoRadiacionTemp.set_rayosXgAlta(false);
//        }
//        if (_chbRayosXGMedia.isSelected())
//        {
//            _tipoRadiacionTemp.set_rayosXgMedia(true);
//        } else
//        {
//            _tipoRadiacionTemp.set_rayosXgMedia(false);
//        }
//        if (_chbRayosXGBaja.isSelected())
//        {
//            _tipoRadiacionTemp.set_rayosXgBaja(true);
//        } else
//        {
//            _tipoRadiacionTemp.set_rayosXgBaja(false);
//        }
//        if (_chbRayosXG.isSelected())
//        {
//            _tipoRadiacionTemp.set_rayosXg(true);
//        } else
//        {
//            _tipoRadiacionTemp.set_rayosXg(false);
//        }
//        if (_chbNeutrones.isSelected())
//        {
//            _tipoRadiacionTemp.set_neutrones(true);
//        } else
//        {
//            _tipoRadiacionTemp.set_neutrones(false);
//        }
//        if (_chbBeta.isSelected())
//        {
//            _tipoRadiacionTemp.set_beta(true);
//        } else
//        {
//            _tipoRadiacionTemp.set_beta(false);
//        }
    }

    public void setSede(Sede _sede)
    {
        _sedeIdTemp = _sede.get_sedeId();
        _txtNombreSede.setText(_sede.get_nombre());
    }
}
