package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.model.Usuario;
import javafx.fxml.FXML;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuController
{
    private Usuario _usuarioTemp;

    @FXML
    private Button _btnCliente;

    @FXML
    private Button _btnSede;

    @FXML
    private Button _btnArea;

    @FXML
    private Button _btnPoe;

    @FXML
    private Button _btnDosimetro;

    @FXML
    private Button _btnGuia;

    @FXML
    private Button _btnEstadistica;

    @FXML
    private Button _btnProcesamiento;

    @FXML
    private Button _btnOrdenPedido;

    @FXML
    private AnchorPane _anchorPaneMenu;

    // Event Listener on Button.onAction
    @FXML
    public void OnActionBtnCliente(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._clientePath));
        Parent _clientePageParent = _loader.load();

        Stage _clienteModalStage = new Stage();
        Scene _clientePageScene = new Scene(_clientePageParent);
        _clienteModalStage.setScene(_clientePageScene);
        _clienteModalStage.initModality(Modality.WINDOW_MODAL);
        _clienteModalStage.initOwner(_anchorPaneMenu.getScene().getWindow());
        _clienteModalStage.setResizable(false);
        _clienteModalStage.setMaximized(false);
        _clienteModalStage.show();
    }

    // Event Listener on Button.onAction
    @FXML
    public void OnActionBtnSede(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._sedePath));
        Parent _sedePageParent = _loader.load();

        Stage _sedeModalStage = new Stage();
        Scene _sedePageScene = new Scene(_sedePageParent);
        _sedeModalStage.setScene(_sedePageScene);
        _sedeModalStage.initModality(Modality.WINDOW_MODAL);
        _sedeModalStage.initOwner(_anchorPaneMenu.getScene().getWindow());
        _sedeModalStage.setResizable(false);
        _sedeModalStage.setMaximized(false);
        _sedeModalStage.show();
    }

    public void OnActionBtnArea(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._areaPath));
        Parent _areaPageParent = _loader.load();

        Stage _areaModalStage = new Stage();
        Scene _areaPageScene = new Scene(_areaPageParent);
        _areaModalStage.setScene(_areaPageScene);
        _areaModalStage.initModality(Modality.WINDOW_MODAL);
        _areaModalStage.initOwner(_anchorPaneMenu.getScene().getWindow());
        _areaModalStage.setResizable(false);
        _areaModalStage.setMaximized(false);
        _areaModalStage.show();
    }

    @FXML
    public void OnActionBtnPoe(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._poePath));
        Parent _poePageParent = _loader.load();

        Stage _poeModalStage = new Stage();
        Scene _poePageScene = new Scene(_poePageParent);
        _poeModalStage.setScene(_poePageScene);
        _poeModalStage.initModality(Modality.WINDOW_MODAL);
        _poeModalStage.initOwner(_anchorPaneMenu.getScene().getWindow());
        _poeModalStage.setResizable(false);
        _poeModalStage.setMaximized(false);
        _poeModalStage.show();
    }

    // Event Listener on Button.onAction
    @FXML
    public void OnActionBtnDosimetro(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._dosimetroPath));
        Parent _dosimetroPageParent = _loader.load();

        Stage _dosimetroModalStage = new Stage();
        Scene _dosimetroPageScene = new Scene(_dosimetroPageParent);
        _dosimetroModalStage.setScene(_dosimetroPageScene);
        _dosimetroModalStage.initModality(Modality.WINDOW_MODAL);
        _dosimetroModalStage.initOwner(_anchorPaneMenu.getScene().getWindow());
        _dosimetroModalStage.setResizable(false);
        _dosimetroModalStage.setMaximized(false);
        _dosimetroModalStage.show();
    }

    // Event Listener on Button.onAction
    @FXML
    public void OnActionBtnGuia(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._guiaPath));
        Parent _guiaPageParent = _loader.load();

        GuiaController controller = _loader.getController();
        controller.setUsuario(_usuarioTemp);

        Stage _guiaModalStage = new Stage();
        Scene _guiaPageScene = new Scene(_guiaPageParent);
        _guiaModalStage.setScene(_guiaPageScene);
        _guiaModalStage.initModality(Modality.WINDOW_MODAL);
        _guiaModalStage.initOwner(_anchorPaneMenu.getScene().getWindow());
        _guiaModalStage.setResizable(false);
        _guiaModalStage.setMaximized(false);
        _guiaModalStage.show();
    }

    // Event Listener on Button.onAction
    @FXML
    public void OnActionBtnEstadistica(ActionEvent event)
    {

    }

    @FXML
    public void OnActionBtnOrdenPedido(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._ordenPedidoPath));
        Parent _ordenPedidoPageParent = _loader.load();

        OrdenPedidoController controller = _loader.getController();
        controller.setUsuario(_usuarioTemp);

        Stage _ordenPedidoModalStage = new Stage();
        Scene _ordenPedidoPageScene = new Scene(_ordenPedidoPageParent);
        _ordenPedidoModalStage.setScene(_ordenPedidoPageScene);
        _ordenPedidoModalStage.initModality(Modality.WINDOW_MODAL);
        _ordenPedidoModalStage.initOwner(_anchorPaneMenu.getScene().getWindow());
        _ordenPedidoModalStage.setResizable(false);
        _ordenPedidoModalStage.setMaximized(false);
        _ordenPedidoModalStage.show();
    }

    @FXML
    public void OnActionBtnProcesamiento(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._procesamientoDatosPath));
        Parent _procesamientoDatosPageParent = _loader.load();

        ProcesamientoDatosController controller = _loader.getController();
        controller.setUsuario(_usuarioTemp);

        Stage _procesamientoDatosModalStage = new Stage();
        Scene _procesamientoDatosPageScene = new Scene(_procesamientoDatosPageParent);
        _procesamientoDatosModalStage.setScene(_procesamientoDatosPageScene);
        _procesamientoDatosModalStage.initModality(Modality.WINDOW_MODAL);
        _procesamientoDatosModalStage.initOwner(_anchorPaneMenu.getScene().getWindow());
        _procesamientoDatosModalStage.setResizable(false);
        _procesamientoDatosModalStage.setMaximized(false);
        _procesamientoDatosModalStage.show();
    }

    public void setUsuario(Usuario _usuario)
    {
        _usuarioTemp = _usuario;

//        User SuperUsuario
        if (_usuarioTemp.get_access() == 1)
        {
//            _btnGuia.setDisable(true);
//            _btnProcesamiento.setDisable(true);
//            _btnSeguimiento.setDisable(true);
        }
//        User Laboratorio
        if (_usuarioTemp.get_access() == 2)
        {

//            _btnCliente.setDisable(true);
//            _btnSede.setDisable(true);
//            _btnArea.setDisable(true);
//            _btnPoe.setDisable(true);
//            _btnDosimetro.setDisable(true);
        }
//        User Administracion
        if (_usuarioTemp.get_access() == 3)
        {
            _btnEstadistica.setDisable(true);
            _btnDosimetro.setDisable(true);
        }
    }
}
