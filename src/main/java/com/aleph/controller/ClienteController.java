package com.aleph.controller;

import com.aleph.model.Paths;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.event.ActionEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class ClienteController
{
    @FXML
    private AnchorPane _AnchorPaneCliente;
    @FXML
    private Button _btnAgregarCliente;
    @FXML
    private Button _btnBuscarCliente;

    // Event Listener on Button[#_btnAgregarCliente].onAction
    @FXML
    public void OnActionBtnAgregarCliente(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._clienteAgregarPath));
        Parent _clienteAgregarPageParent = _loader.load();

        Stage _clienteAgregarModalStage = new Stage();
        Scene _clienteAgregarPageScene = new Scene(_clienteAgregarPageParent);
        _clienteAgregarModalStage.setScene(_clienteAgregarPageScene);
        _clienteAgregarModalStage.initModality(Modality.WINDOW_MODAL);
        _clienteAgregarModalStage.initOwner(_AnchorPaneCliente.getScene().getWindow());
        _clienteAgregarModalStage.setResizable(false);
        _clienteAgregarModalStage.setMaximized(false);
        _clienteAgregarModalStage.show();
    }

    // Event Listener on Button[#_btnBuscarCliente].onAction
    @FXML
    public void OnActionBtnBuscarCliente(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._clienteBuscarPath));
        Parent _clienteBuscarPageParent = _loader.load();

        Stage _clienteBuscarModalStage = new Stage();
        Scene _clienteBuscarPageScene = new Scene(_clienteBuscarPageParent);
        _clienteBuscarModalStage.setScene(_clienteBuscarPageScene);
        _clienteBuscarModalStage.initModality(Modality.WINDOW_MODAL);
        _clienteBuscarModalStage.initOwner(_AnchorPaneCliente.getScene().getWindow());
        _clienteBuscarModalStage.setResizable(false);
        _clienteBuscarModalStage.setMaximized(false);
        _clienteBuscarModalStage.show();
    }
}
