package com.aleph.controller;

import com.aleph.model.Paths;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class RunApp extends Application {
    @Override
    public void start(Stage _primaryStage) throws Exception {
        Parent _root = FXMLLoader.load(getClass().getResource(Paths._inicioPath));
        Scene _scene = new Scene(_root);
        _primaryStage.setScene(_scene);
        _primaryStage.centerOnScreen();
        _primaryStage.setResizable(false);
        _primaryStage.show();
    }

    public static void main(String[] _args) {
        launch(_args);
    }
}