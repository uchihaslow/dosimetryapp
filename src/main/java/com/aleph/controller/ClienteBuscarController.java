package com.aleph.controller;

import com.aleph.model.Cliente;
import com.aleph.model.Paths;
import com.aleph.service.ClienteService;
import com.aleph.service.IClienteService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

import javafx.event.ActionEvent;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class ClienteBuscarController
{
    private IClienteService _clienteService = new ClienteService();
    @FXML
    private AnchorPane _anchorPaneBuscarCliente;
    @FXML
    private TableView<Cliente> _tvCliente;
    @FXML
    private TextField _txtBuscarCliente;
    @FXML
    private Button _btnAgregarCliente;
    @FXML
    private Button _btnSeleccionarCliente;
    @FXML
    private RadioButton _rbRuc;
    @FXML
    private ToggleGroup _tgGrupoClientes;
    @FXML
    private RadioButton _rbRazonSocial;
    @FXML
    private Button _btnBuscarCliente;
    @FXML
    private Button _btnEliminarCliente;
    @FXML
    private Button _btnEditarCliente;

    @FXML
    public void initialize()
    {

//        LlenarTablaClientes();
//        _tvCliente.getColumns().get(0).setVisible(false);
    }

    // Event Listener on Button[#_btnAgregarCliente].onAction
    @FXML
    public void OnActionBtnAgregarCliente(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._clienteAgregarPath));
        Parent _clienteAgregarPageParent = _loader.load();

        Stage _clienteAgregarModalStage = new Stage();
        Scene _clienteAgregarPageScene = new Scene(_clienteAgregarPageParent);
        _clienteAgregarModalStage.setScene(_clienteAgregarPageScene);
        _clienteAgregarModalStage.initModality(Modality.WINDOW_MODAL);
        _clienteAgregarModalStage.initOwner(_anchorPaneBuscarCliente.getScene().getWindow());
        _clienteAgregarModalStage.setResizable(false);
        _clienteAgregarModalStage.setMaximized(false);
        _clienteAgregarModalStage.show();
    }

    // Event Listener on Button[#_btnSeleccionarCliente].onAction
    @FXML
    public void OnActionBtnSeleccionarCliente(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._clienteDetallePath));
            Parent _clienteDetallePageParent = _loader.load();

            ClienteDetalleController _controller = _loader.getController();
            _controller.setCliente(_tvCliente.getSelectionModel().getSelectedItem());

            Stage _clienteDetalleModalStage = new Stage();
            Scene _clienteDetallePageScene = new Scene(_clienteDetallePageParent);
            _clienteDetalleModalStage.setScene(_clienteDetallePageScene);
            _clienteDetalleModalStage.initModality(Modality.WINDOW_MODAL);
            _clienteDetalleModalStage.initOwner(_anchorPaneBuscarCliente.getScene().getWindow());
            _clienteDetalleModalStage.setResizable(false);
            _clienteDetalleModalStage.setMaximized(false);
            _clienteDetalleModalStage.show();
        } catch (Exception e)
        {
            System.out.println("ERROR OnActionBtnSeleccionarCliente : " + e);
        }
    }

    // Event Listener on Button[#_btnBuscarCliente].onAction
    @FXML
    public void OnActionBtnBuscarCliente(ActionEvent event)
    {
        Buscar();
    }

    // Event Listener on Button[#_btnEliminarCliente].onAction
    @FXML
    public void OnActionBtnEliminarCliente(ActionEvent event)
    {
        Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
        _alert.setTitle("Cuadro de confirmación");
        _alert.setHeaderText("Eliminar Cliente");
        _alert.setContentText("¿Quiere elminar este cliente?");

        Optional<ButtonType> _opcion = _alert.showAndWait();
        if (_opcion.get() == ButtonType.OK)
        {
            _clienteService.removeCliente(_tvCliente.getSelectionModel().getSelectedItem().get_clienteId());
        } else
        {

        }
    }

    // Event Listener on Button[#_btnEditarCliente].onAction
    @FXML
    public void OnActionBtnEditarCliente(ActionEvent event) throws IOException
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._clienteEditarPath));
            Parent _clienteEditarPageParent = _loader.load();

            ClienteEditarController _controller = _loader.getController();
            _controller.setCliente(_tvCliente.getSelectionModel().getSelectedItem());

            Stage _clienteEditarModalStage = new Stage();
            Scene _clienteEditarPageScene = new Scene(_clienteEditarPageParent);
            _clienteEditarModalStage.setScene(_clienteEditarPageScene);
            _clienteEditarModalStage.initModality(Modality.WINDOW_MODAL);
            _clienteEditarModalStage.initOwner(_anchorPaneBuscarCliente.getScene().getWindow());
            _clienteEditarModalStage.setResizable(false);
            _clienteEditarModalStage.setMaximized(false);
            _clienteEditarModalStage.show();
//            _tvCliente.getItems().clear();
        } catch (Exception e)
        {
            System.out.println("ERROR OnActionBtnEditarCliente : " + e);
        }

    }

    private void LlenarTablaClientes()
    {
        ObservableList<Cliente> _clienteList = FXCollections.observableArrayList(_clienteService.listCliente());
        _tvCliente.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_ruc"));
        _tvCliente.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_razonSocial"));
        _tvCliente.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_telefono"));
        _tvCliente.setItems(_clienteList);
    }

    public void OnKeyPressed(KeyEvent keyEvent)
    {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
        {
            Buscar();
        }
    }

    private void Buscar()
    {
        if (_rbRazonSocial.isSelected())
        {
            ObservableList<Cliente> _clienteList = FXCollections.observableArrayList(_clienteService.findClienteByNombre(_txtBuscarCliente.getText()));
            _tvCliente.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_ruc"));
            _tvCliente.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_razonSocial"));
            _tvCliente.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_telefono"));
            _tvCliente.setItems(_clienteList);
        }
        if (_rbRuc.isSelected())
        {
            ObservableList<Cliente> _clienteList = FXCollections.observableArrayList(_clienteService.findClienteByRuc(_txtBuscarCliente.getText()));
            _tvCliente.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_ruc"));
            _tvCliente.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_razonSocial"));
            _tvCliente.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_telefono"));
            _tvCliente.setItems(_clienteList);
        }
    }
}
