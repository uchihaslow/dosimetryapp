package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.model.Poe;
import com.aleph.service.IPoeService;
import com.aleph.service.PoeService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class PoeBuscarController
{
    @FXML
    public AnchorPane _anchorPaneBuscarPoe;
    @FXML
    public TableView<Poe> _tvPoe;
    @FXML
    public TableColumn _tcDni;
    @FXML
    public TableColumn _tcNombres;
    @FXML
    public TableColumn _tcApellidos;
    @FXML
    public TableColumn _tcId;
    @FXML
    public TextField _txtBuscarPoe;
    @FXML
    public Button _btnAgregar;
    @FXML
    public Button _btnSeleccionar;
    @FXML
    public RadioButton _rbNombre;
    @FXML
    public ToggleGroup tgPoe;
    @FXML
    public RadioButton _rbDni;
    @FXML
    public Button _btnBuscar;
    @FXML
    public Button _btnEliminar;
    @FXML
    public Button _btnEditar;

    IPoeService _poeSvc = new PoeService();

    public void OnActionBtnAgregar(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._poeAgregarPath));
        Parent _poeAgregarPageParent = _loader.load();

        Stage _poeAgregarModalStage = new Stage();
        Scene _poeAgregarPageScene = new Scene(_poeAgregarPageParent);
        _poeAgregarModalStage.setScene(_poeAgregarPageScene);
        _poeAgregarModalStage.initModality(Modality.NONE);
        _poeAgregarModalStage.initOwner(_anchorPaneBuscarPoe.getScene().getWindow());
        _poeAgregarModalStage.setResizable(false);
        _poeAgregarModalStage.setMaximized(false);
        _poeAgregarModalStage.showAndWait();
        OnActionBtnBuscar(actionEvent);
    }

    public void OnActionBtnSeleccionar(ActionEvent actionEvent)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._poeDetallePoePath));
            Parent _poeDetallePageParent = _loader.load();

            PoeDetalleController _controller = _loader.getController();
            _controller.setPoe(_tvPoe.getSelectionModel().getSelectedItem());

            Stage _poeDetalleModalStage = new Stage();
            Scene _poeDetallePageScene = new Scene(_poeDetallePageParent);
            _poeDetalleModalStage.setScene(_poeDetallePageScene);
            _poeDetalleModalStage.initModality(Modality.WINDOW_MODAL);
            _poeDetalleModalStage.initOwner(_anchorPaneBuscarPoe.getScene().getWindow());
            _poeDetalleModalStage.setResizable(false);
            _poeDetalleModalStage.setMaximized(false);
            _poeDetalleModalStage.show();
        } catch (Exception e)
        {
            System.out.println("ERROR OnActionBtnSeleccionar: " + e);
        }
    }

    public void OnActionBtnBuscar(ActionEvent actionEvent)
    {
        Buscar();
    }

    public void OnActionBtnEliminar(ActionEvent actionEvent)
    {
        Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
        _alert.setTitle("Cuadro de confirmación");
        _alert.setHeaderText("Eliminar Usuario");
        _alert.setContentText("¿Quiere elminar este usuario?");

        Optional<ButtonType> _opcion = _alert.showAndWait();
        if (_opcion.get() == ButtonType.OK)
        {
            _poeSvc.removePoe(_tvPoe.getSelectionModel().getSelectedItem().get_poeId());
        }
    }

    public void OnActionBtnEditar(ActionEvent actionEvent)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._poeEditarPoePath));
            Parent _poeEditarPageParent = _loader.load();

            PoeActualizarController _controller = _loader.getController();
            _controller.setPoe(_tvPoe.getSelectionModel().getSelectedItem());

            Stage _poeEditarModalStage = new Stage();
            Scene _poeEditarPageScene = new Scene(_poeEditarPageParent);
            _poeEditarModalStage.setScene(_poeEditarPageScene);
            _poeEditarModalStage.initModality(Modality.WINDOW_MODAL);
            _poeEditarModalStage.initOwner(_anchorPaneBuscarPoe.getScene().getWindow());
            _poeEditarModalStage.setResizable(false);
            _poeEditarModalStage.setMaximized(false);
            _poeEditarModalStage.showAndWait();
            OnActionBtnBuscar(actionEvent);
        } catch (Exception e)
        {
            System.out.println("ERROR OnActionBtnEditar : " + e);
        }
    }

    public void OnKeyPressed(KeyEvent keyEvent)
    {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
        {
            Buscar();
        }
    }

    private void Buscar()
    {
        if (_rbNombre.isSelected())
        {
            ObservableList<Poe> _poeList = FXCollections.observableArrayList(_poeSvc.findPoeByNombre(_txtBuscarPoe.getText()));
            _tcId.setCellValueFactory(new PropertyValueFactory("_poeId"));
            _tcDni.setCellValueFactory(new PropertyValueFactory("_dni"));
            _tcNombres.setCellValueFactory(new PropertyValueFactory("_nombres"));
            _tcApellidos.setCellValueFactory(new PropertyValueFactory("_apellidos"));
            _tvPoe.setItems(_poeList);
        }

        if (_rbDni.isSelected())
        {
            ObservableList<Poe> _poeList = FXCollections.observableArrayList(_poeSvc.findPoeByDni(_txtBuscarPoe.getText()));
            _tcId.setCellValueFactory(new PropertyValueFactory("_poeId"));
            _tcDni.setCellValueFactory(new PropertyValueFactory("_dni"));
            _tcNombres.setCellValueFactory(new PropertyValueFactory("_nombres"));
            _tcApellidos.setCellValueFactory(new PropertyValueFactory("_apellidos"));
            _tvPoe.setItems(_poeList);
        }
    }
}
