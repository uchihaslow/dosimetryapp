package com.aleph.controller;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.io.IOException;

public class CustomResponsable extends GridPane
{

    @FXML
    private TextField _txtContactoNombre;

    @FXML
    private TextField _txtContactoEmail;

    @FXML
    private TextField _txtContactoTelefono;

    @FXML
    private ComboBox _cboxContactoArea;

    @FXML
    private Label _labContactoId;

    @FXML
    private Button _btnDeleteContacto;

    @FXML
    private GridPane _gridPaneContactos;

    private boolean _habilitado;

    public CustomResponsable()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/RowResponsable.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        } catch (IOException exception)
        {
            throw new RuntimeException(exception);
        }
    }

    public void initialize()
    {
        LlenarComboContactoArea();
    }

    public String get_txtContactoNombre()
    {
        return textPropertyContactoNombre().get();
    }

    public void set_txtContactoNombre(String _value)
    {
        textPropertyContactoNombre().set(_value);
    }

    public StringProperty textPropertyContactoNombre()
    {
        return _txtContactoNombre.textProperty();
    }

    public String get_txtContactoEmail()
    {
        return textPropertyContactoEmail().get();
    }

    public void set_txtContactoEmail(String _value)
    {
        textPropertyContactoEmail().set(_value);
    }

    public StringProperty textPropertyContactoEmail()
    {
        return _txtContactoEmail.textProperty();
    }

    public String get_txtContactoTelefono()
    {
        return textPropertyContactoTelefono().get();
    }

    public void set_txtContactoTelefono(String _value)
    {
        textPropertyContactoTelefono().set(_value);
    }

    public StringProperty textPropertyContactoTelefono()
    {
        return _txtContactoTelefono.textProperty();
    }

    public String get_labContactoId()
    {
        return textPropertyLabContactoId().get();
    }

    public void set_labContactoId(String _value)
    {
        textPropertyLabContactoId().set(_value);
    }

    public StringProperty textPropertyLabContactoId()
    {
        return _labContactoId.textProperty();
    }

    public Button get_btnDeleteContacto()
    {
        return _btnDeleteContacto;
    }

    public void set_btnDeleteContacto(Button _btnDeleteContacto)
    {
        this._btnDeleteContacto = _btnDeleteContacto;
    }

    public GridPane get_gridPaneContactos()
    {
        return _gridPaneContactos;
    }

    public void set_gridPaneContactos(GridPane _gridPaneContactos)
    {
        this._gridPaneContactos = _gridPaneContactos;
    }

    public boolean is_habilitado()
    {
        return _habilitado;
    }

    public void set_habilitado(boolean _habilitado)
    {
        this._habilitado = _habilitado;
    }

    private void LlenarComboContactoArea()
    {
        _cboxContactoArea.getItems().clear();
        _cboxContactoArea.getItems().add("Contabilidad");
        _cboxContactoArea.getItems().add("OPR");
        _cboxContactoArea.getItems().add("Otros");
        _cboxContactoArea.getSelectionModel().select(2);

    }


}
