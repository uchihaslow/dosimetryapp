package com.aleph.controller;

import com.aleph.model.DosisMultiReport;
import com.aleph.model.Paths;
import com.aleph.service.ReporteService;
import com.aleph.service.GuiaService;
import com.aleph.service.IGuiaService;
import com.aleph.service.IReporteService;
import com.aleph.tools.TableUtils;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ReporteController
{

    //region Controles
    @FXML
    private ComboBox _cbPeriodo;
    @FXML
    private AnchorPane _anchorPaneReporte;
    @FXML
    private TextField _txtFiltro;
    @FXML
    private CheckBox _cboxFinalizado;
    @FXML
    private TableView _tvReporte;
    @FXML
    private TableColumn _tcNroGuia;
    @FXML
    private TableColumn _tcCliente;
    @FXML
    private TableColumn _tcSede;
    @FXML
    private TableColumn _tcArea;
    @FXML
    private TableColumn _tcLeidos;
    @FXML
    private TableColumn _tcDevueltos;
    @FXML
    private TableColumn _tcNoDevueltos;
    @FXML
    private TableColumn _tcTotal;
    @FXML
    private Button _btnBuscar;
    @FXML
    private Button _btnPrevisualizar;
    @FXML
    private Button _btnFinalizar;
    @FXML
    private Button _btnImprimir;
    @FXML
    private Button _btnDevueltos;
    @FXML
    private Button _btnNoDevueltos;
    @FXML
    private RadioButton _rbGuia;
    @FXML
    private RadioButton _rbNombre;
    @FXML
    private TableColumn _tcPeriodo;
    @FXML
    private TableColumn _tcDireccion;
    //endregion

    IGuiaService _guiaService = new GuiaService();
    IReporteService _reporteService = new ReporteService();

    private ArrayList<String> _cabecera = new ArrayList<>();
    private ObservableList<ObservableList> _listaReporte = FXCollections.observableArrayList();
    private SortedList<ObservableList> sortedData;

    @FXML
    public void initialize()
    {
        FillCboxMonths();
        _tcNroGuia.setStyle("-fx-alignment: center ;");
        _tcLeidos.setStyle("-fx-alignment: center ;");
        _tcDevueltos.setStyle("-fx-alignment: center ;");
        _tcNoDevueltos.setStyle("-fx-alignment: center ;");
        _tcTotal.setStyle("-fx-alignment: center ;");

        // enable multi-selection
        _tvReporte.getSelectionModel().setCellSelectionEnabled(true);
        _tvReporte.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // enable copy/paste
        TableUtils.installCopyPasteHandler(_tvReporte);

    }

    @FXML
    public void OnActionBtnPrevisualizar(ActionEvent actionEvent) throws JRException
    {
        int _guiaId = Integer.parseInt(((ObservableList<String>) _tvReporte.getSelectionModel().getSelectedItem()).get(0));
        _cabecera = _reporteService.listDatosComunesReporte(_guiaId);


        System.out.println("row" + _cabecera);

        String _direccion = _cabecera.get(0);
        String _clienteId = _cabecera.get(1);
        String _clienteNom = _cabecera.get(2);
        String _guiaNro = _cabecera.get(3);

        List<DosisMultiReport> _listaDosis = new ArrayList<>();

        _listaDosis = _reporteService.listDosisByPrestamo(_guiaId);

        for (DosisMultiReport _dosis : _listaDosis)
        {
            if (Double.parseDouble(_dosis.get_mensualEfectiva()) < 0.1)
            {
                _dosis.set_mensualEfectiva("M");
            }

            if (Double.parseDouble(_dosis.get_mensualCristalino()) < 0.1)
            {
                _dosis.set_mensualCristalino("M");
            }

            if (Double.parseDouble(_dosis.get_mensualPiel()) < 0.1)
            {
                _dosis.set_mensualPiel("M");
            }

            if (Double.parseDouble(_dosis.get_anualEfectiva()) < 0.1)
            {
                _dosis.set_anualEfectiva("M");
            }

            if (Double.parseDouble(_dosis.get_anualCristalino()) < 0.1)
            {
                _dosis.set_anualCristalino("M");
            }

            if (Double.parseDouble(_dosis.get_anualPiel()) < 0.1)
            {
                _dosis.set_anualPiel("M");
            }

            _dosis.set_dosAjuste("");
            _dosis.set_dosMes(_cabecera.get(4));//MES
            _dosis.set_dosYear(_cabecera.get(5));//AÑO
        }

//        for (int i = 0; i < 23; i++)
//        {
//            DosisMultiReport _dosis = new DosisMultiReport();
//            _dosis.set_poeId("1050" + i);
//            _dosis.set_poeNom("Usuario " + i);
//            _dosis.set_poeSexo("M");
//            _dosis.set_poeTde("K1");
//            _dosis.set_rad("P");
//            _dosis.set_mensualEfectiva("M");
//            _dosis.set_mensualCristalino("M");
//            _dosis.set_mensualPiel("M");
//            _dosis.set_anualEfectiva("");
//            _dosis.set_anualCristalino("");
//            _dosis.set_anualPiel("");
//            _dosis.set_dosAjuste("");
//            _dosis.set_dosRep("3");
//            _dosis.set_dosMes(_cabecera.get(4));//MES
//            _dosis.set_dosYear(_cabecera.get(5));//AÑO
//            _dosis.set_dosNumero("105484");
//            _listaDosis.add(_dosis);
//        }


        final Map<String, Object> _parameters = new HashMap<>();
        _parameters.put("param_clienteId", _clienteId);
        _parameters.put("param_clienteDir", _direccion);
        _parameters.put("param_clienteNom", _clienteNom);
        _parameters.put("param_guiaNro", _guiaNro);

        _parameters.put("param_dosis", _listaDosis);

        toPDF("reporte_dosimetria_v8.jrxml", _parameters);
    }

    @FXML
    public void OnActionBtnFinalizar(ActionEvent actionEvent)
    {
    }

    @FXML
    public void OnActionBtnImprimir(ActionEvent actionEvent)
    {
    }

    public void OnActionBtnBuscar(ActionEvent actionEvent)
    {
        if (_rbNombre.isSelected())
        {
            LlenarTvReporte(1);
        } else
        {
            LlenarTvReporte(2);
        }

        /*
        System.out.println(_cboxFinalizado.isSelected());
        if (_cboxFinalizado.isSelected())
        {
            _btnImprimir.setDisable(false);
            _btnFinalizar.setDisable(true);
            _btnPrevisualizar.setDisable(true);
            _tvReporte.getItems().clear();
            if (_txtBuscarGuia.getText() == null || _txtBuscarGuia.getText().trim().isEmpty())
            {
                LlenarTvReporte(3);
            } else
            {
                LlenarTvReporte(4);
            }
        } else
        {
            _btnImprimir.setDisable(true);
            _btnFinalizar.setDisable(false);
            _btnPrevisualizar.setDisable(false);
            _tvReporte.getItems().clear();
            if (_txtBuscarGuia.getText() == null || _txtBuscarGuia.getText().trim().isEmpty())
            {
                LlenarTvReporte(1);
            } else
            {
                LlenarTvReporte(2);
            }
        }*/
    }

    public void LlenarTvReporte(int _opcion)
    {
        //_listaReporte.clear();

        switch (_opcion)
        {
            case 1:
                _listaReporte = FXCollections.observableArrayList(_reporteService.listGuiaReporteByPeriodo(MonthsResolver(_cbPeriodo.getValue().toString())));
                break;
            case 2:

                break;
            default:
                System.out.println("error");
                break;
        }

        /*
        switch (_opcion)
        {
            case 1:
                _listaReporte = FXCollections.observableArrayList(_reporteService.listReporteSinFinalizar());
                break;
            case 2:
                _listaReporte = FXCollections.observableArrayList(_reporteService.listReporteSinFinalizarByIdGuia(Integer.parseInt(_txtBuscarGuia.getText())));
                break;
            case 3:
                _listaReporte = FXCollections.observableArrayList(_reporteService.listReporteFinalizado());
                break;
            case 4:
                _listaReporte = FXCollections.observableArrayList(_reporteService.listReporteFinalizadoByIdGuia(Integer.parseInt(_txtBuscarGuia.getText())));
                break;
            default:
                System.out.println("error");
                break;
        }*/


        _tcNroGuia.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(0).toString())
                );

        _tcPeriodo.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(1).toString())
                );

        _tcCliente.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(3).toString())
                );

        _tcSede.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(4).toString())
                );

        _tcDireccion.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(5).toString())
                );

        _tcArea.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(6).toString())
                );

        _tcLeidos.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(7).toString())
                );

        _tcDevueltos.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(8).toString())
                );

        _tcNoDevueltos.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(9).toString())
                );
        _tcTotal.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(10).toString())
                );

        _tvReporte.setItems(_listaReporte);
        filtro();
    }

    public void toPDF(String _reportName, Map<String, Object> _parameters) throws JRException
    {
//        final String _reportSource = getClass().getClassLoader().getResource("reportes/" + _reportName).getPath();

        //agregar esto cuando se va a compilar
        String _projectPath = System.getProperty("user.dir");
        final String _reportSource = (_projectPath + "/plantilla/" + _reportName);

        final JasperDesign _jd = JRXmlLoader.load(_reportSource);
        final JasperReport _report = JasperCompileManager.compileReport(_jd);
        final JasperPrint _print = JasperFillManager.fillReport(_report, _parameters, new JREmptyDataSource());
        final String _reporTarget = _reportSource.substring(0, _reportSource.lastIndexOf('/'))
                .concat("/")
                .concat(_reportName)
                .concat(".pdf");
        System.out.println("Ruta del archivo: " + _reporTarget);

        JasperExportManager.exportReportToPdfFile(_print, _reporTarget);
        JasperViewer.viewReport(_print, false);
        File _fichero = new File(_reporTarget);

        if (_fichero.delete())
        {
            System.out.println("El fichero ha sido borrado satisfactoriamente");
        } else
        {
            System.out.println("El fichero no puede ser borrado");
        }
    }

    public void OnActionBtnNoDevueltos(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._reporteNoDevueltosPath));
        Parent _noDevueltosPageParent = _loader.load();

        ReporteNoDevueltosController _controller = _loader.getController();

        _controller.cargarDatos(Integer.parseInt(((ObservableList<String>) _tvReporte.getSelectionModel().getSelectedItem()).get(0)));
//        _controller.cargarDatos(_tcNroGuia.get );

        Stage _noDevueltosModalStage = new Stage();
        Scene _noDevueltosPageScene = new Scene(_noDevueltosPageParent);
        _noDevueltosModalStage.setScene(_noDevueltosPageScene);
        _noDevueltosModalStage.initModality(Modality.WINDOW_MODAL);
        _noDevueltosModalStage.initOwner(_anchorPaneReporte.getScene().getWindow());
        _noDevueltosModalStage.setResizable(false);
        _noDevueltosModalStage.setMaximized(false);
        _noDevueltosModalStage.show();
    }

    public void OnActionBtnDevueltos(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._reporteDevueltos));
        Parent _noLeidosPageParent = _loader.load();

        ReporteDevueltosController _controller = _loader.getController();

        _controller.cargarDatos(Integer.parseInt(((ObservableList<String>) _tvReporte.getSelectionModel().getSelectedItem()).get(0)));
//        _controller.cargarDatos(_tcNroGuia.get );

        Stage _noLeidosModalStage = new Stage();
        Scene _noLeidosPageScene = new Scene(_noLeidosPageParent);
        _noLeidosModalStage.setScene(_noLeidosPageScene);
        _noLeidosModalStage.initModality(Modality.WINDOW_MODAL);
        _noLeidosModalStage.initOwner(_anchorPaneReporte.getScene().getWindow());
        _noLeidosModalStage.setResizable(false);
        _noLeidosModalStage.setMaximized(false);
        _noLeidosModalStage.show();
    }

    public void FillCboxMonths()
    {
        _cbPeriodo
                .getItems()
                .addAll("TODOS", "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
        _cbPeriodo.getSelectionModel().select(1);

    }

    private List<String> MonthsResolver(String _month)
    {
        List<String> _opcion = new ArrayList<>();

        switch (_month)
        {
            case "ENE":
                _opcion.add("2019-01-01");
                break;
            case "FEB":
                _opcion.add("2019-02-01");
                break;
            case "MAR":
                _opcion.add("2019-03-01");
                break;
            case "ABR":
                _opcion.add("2019-04-01");
                break;
            case "MAY":
                _opcion.add("2019-05-01");
                break;
            case "JUN":
                _opcion.add("2019-06-01");
                break;
            case "JUL":
                _opcion.add("2019-07-01");
                break;
            case "AGO":
                _opcion.add("2019-08-01");
                break;
            case "SEP":
                _opcion.add("2019-09-01");
                break;
            case "OCT":
                _opcion.add("2019-10-01");
                break;
            case "NOV":
                _opcion.add("2019-11-01");
                break;
            case "DIC":
                _opcion.add("2019-12-01");
                break;
            case "TODOS":
                _opcion.add("2019-01-01");
                _opcion.add("2019-02-01");
                _opcion.add("2019-03-01");
                _opcion.add("2019-04-01");
                _opcion.add("2019-05-01");
                _opcion.add("2019-06-01");
                _opcion.add("2019-07-01");
                _opcion.add("2019-08-01");
                _opcion.add("2019-09-01");
                _opcion.add("2019-10-01");
                _opcion.add("2019-11-01");
                _opcion.add("2019-12-01");
                break;
        }

        return _opcion;
    }

    public void OnActionBtnExportarExcel(ActionEvent actionEvent) throws Exception
    {
        ExportToExcel(_tvReporte);
    }

    private void filtro()
    {
        //case sensitive
        FilteredList<ObservableList> filteredData = new FilteredList<>(_listaReporte, p -> true);

        _txtFiltro.textProperty().addListener((observable, oldValue, newValue) ->
        {
            filteredData.setPredicate(observableList ->
            {
                if (newValue == null || newValue.isEmpty())
                {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                if (((String) observableList.get(0)).toLowerCase().indexOf(lowerCaseFilter) != -1)
                {
                    return true;
                } else if (((String) observableList.get(1)).toLowerCase().indexOf(lowerCaseFilter) != -1)
                {
                    return true;
                } else if (((String) observableList.get(3)).toLowerCase().indexOf(lowerCaseFilter) != -1)
                {
                    return true;
                } else if (((String) observableList.get(4)).toLowerCase().indexOf(lowerCaseFilter) != -1)
                {
                    return true;
                }
                return false;
            });
        });

        //SortedList<ObservableList> sortedData = new SortedList<>(filteredData);
        sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(_tvReporte.comparatorProperty());

        _tvReporte.setItems(sortedData);
    }

    public void ExportToExcel(TableView<Object> tablaExport) throws Exception
    {
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet();

        wb.setSheetName(0, "report_guias");

        Row row = sheet.createRow(0);
        //CellStyle style = wb.createCellStyle();
        //style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
        //style.setFillPattern(FillPatternType.BIG_SPOTS);

        for (int j = 0; j < tablaExport.getColumns().size(); j++)
        {
            row.createCell(j).setCellValue(tablaExport.getColumns().get(j).getText());
            //row.getCell(j).setCellStyle(style);
        }

        for (int i = 0; i < tablaExport.getItems().size(); i++)
        {
            row = sheet.createRow(i + 1);
            for (int j = 0; j < tablaExport.getColumns().size(); j++)
            {
                if (tablaExport.getColumns().get(j).getCellData(i) != null)
                {
                    if (StringUtils.isNumeric(tablaExport.getColumns().get(j).getCellData(i).toString()))
                        row.createCell(j).setCellValue(Integer.parseInt(tablaExport.getColumns().get(j).getCellData(i).toString()));
                    else
                        row.createCell(j).setCellValue(tablaExport.getColumns().get(j).getCellData(i).toString());
                } else
                {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xlsx)", "*.xlsx");
        fileChooser.getExtensionFilters().add(extFilter);

        LocalDateTime _now = LocalDateTime.now();
        DateTimeFormatter _dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd hh_mm_ss a");
        fileChooser.setInitialFileName("report_L_D_ND_" + _now.format(_dtf) + ".xlsx");
        File file = fileChooser.showSaveDialog(null);
        FileOutputStream out;

        if (file != null && (out = new FileOutputStream(file)) != null)
        {
            wb.write(out);
            out.close();
        }
    }
}