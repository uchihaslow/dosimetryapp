package com.aleph.controller;

import com.aleph.model.Guia;
import com.aleph.model.Usuario;
import com.aleph.service.GuiaService;
import com.aleph.service.IGuiaService;
import com.aleph.service.ILogService;
import com.aleph.service.LogService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.event.ActionEvent;

import javafx.scene.control.ButtonType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import javafx.scene.control.TableView;

import java.util.Optional;

public class OrdenPedidoRecibirOrdenController
{
    private IGuiaService _guiaService = new GuiaService();
    private ILogService _logService = new LogService();

    private Usuario _usuarioTemp;

    @FXML
    private AnchorPane _anchorPaneOrdenesPendientes;
    @FXML
    private TableView<Guia> _tvOrdenes;
    @FXML
    private Button _btnRecibir;

    @FXML
    public void initialize()
    {
        llenarTablaOrdenes();
    }

    // Event Listener on Button[#_btnRecibir].onAction
    @FXML
    public void OnActionBtnRecibir(ActionEvent event)
    {
        Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
        _alert.setTitle("Cuadro de confirmación");
        _alert.setHeaderText("Recibir Orden");
        _alert.setContentText("¿Quiere recibir esta Orden de Pedido?");

        Optional<ButtonType> _opcion = _alert.showAndWait();
        if (_opcion.get() == ButtonType.OK)
        {
            _guiaService.aprobarOrden(_tvOrdenes.getSelectionModel().getSelectedItem().get_guiaId());
            _logService.addLog("Recibido -> #" + _tvOrdenes.getSelectionModel().getSelectedItem().get_guiaId());
            llenarTablaOrdenes();
        } else
        {

        }
    }

    private void llenarTablaOrdenes()
    {
        //_tvOrdenes.getItems().removeAll();
        ObservableList<Guia> _guiaList = FXCollections.observableArrayList(_guiaService.listGuia());
        _tvOrdenes.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_guiaId"));
        _tvOrdenes.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_nroOrdenPedido"));
        _tvOrdenes.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_fechaOrdenPedido"));
        _tvOrdenes.setItems(_guiaList);
    }

    public void setUsuario(Usuario _usuario)
    {
        _usuarioTemp = _usuario;

//        User SuperUsuario
        if (_usuarioTemp.get_access() == 1)
        {
        }
//        User Laboratorio
        if (_usuarioTemp.get_access() == 2)
        {
        }
//        User Administracion
        if (_usuarioTemp.get_access() == 3)
        {
            _btnRecibir.setDisable(true);
        }
    }
}
