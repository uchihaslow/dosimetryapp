package com.aleph.controller;

import com.aleph.model.Poe;
import com.aleph.model.Prestamo;
import com.aleph.model.ReporteGuia;
import com.aleph.service.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import java.io.File;
import java.util.*;


public class GuiaAsignacionController
{
    IPrestamoService _prestamoService = new PrestamoService();
    IPoeService _poeService = new PoeService();
    IDosimetroService _dosimetroService = new DosimetroService();
    IMagazineService _magazineService = new MagazineService();
    IGuiaService _guiaService = new GuiaService();

    @FXML
    private AnchorPane _anchorPaneAsignacion;
    @FXML
    private Label _labNombreCliente;
    @FXML
    private Label _labNombreSede;
    @FXML
    private Label _labNombreArea;
    @FXML
    private Label _labNumeroCantDosimetros;
    @FXML
    private Label _labNumeroOrdenPedido;
    @FXML
    private Label _labNumeroGuia;
    @FXML
    private VBox _vboxAsignacion;
    @FXML
    private Button _btnAsignar;
    @FXML
    private Button _btnCancelar;
    @FXML
    private Button _btnPrint;

    private int _guiaId;

    private ObservableList<Poe> _poeList;

    private ObservableList<Prestamo> _prestamoAsignadoList;

    private ObservableList<String> _dosimetroLimpioList;

    private ObservableList<CustomRowAsignacion> _lista = FXCollections.observableArrayList();

    private List<ReporteGuia> _dosimetroReporte = new ArrayList<>();

    private String _guiaDireccion;

    private String _guiaPeriodo;

    private String _guiaRuc;


    // Event Listener on Button[#_btnAsignar].onAction
    @FXML
    public void OnActionBtnAsignar(ActionEvent event) throws JRException
    {
        Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
        _alert.setTitle("Cuadro de confirmación");
        _alert.setHeaderText("Realizar Asignación");
        _alert.setContentText("¿Quiere realizar la asignación?");


        Optional<ButtonType> _opcion = _alert.showAndWait();
        if (_opcion.get() == ButtonType.OK)
        {
            _guiaService.changeToEnviado(_guiaId);
            //for (int i = 0; i < _prestamoAsignadoList.size(); i++)
            for (int i = 0; i < _poeList.size(); i++)
            {
                _prestamoService.addAsignacion
                        (
//                                _prestamoAsignadoList.get(i).get_prestamoId(),
                                _poeList.get(i).get_prestamoId(),
                                _dosimetroLimpioList.get(i)
                        );
//                System.out.println(_prestamoAsignadoList.get(i).get_prestamoId());
                System.out.println(_poeList.get(i).get_prestamoId());
                System.out.println(_dosimetroLimpioList.get(i));
                _magazineService.deshabilitarDosimetro(_dosimetroLimpioList.get(i));
                //_dosimetroService.changeToAsignado(_dosimetroLimpioList.get(i));
            }


            /*
             *
             *
             *
             */
            String param_nroGuia = _labNumeroGuia.getText();
            String param_periodoServicio = _guiaPeriodo;
            String param_totalDosimetros = _labNumeroCantDosimetros.getText();

            List<InfoCliente> _infoClientesList = new ArrayList<>();
            InfoCliente _infoCliente = new InfoCliente();
            _infoCliente.set_razonSocial(_labNombreCliente.getText());
            _infoCliente.set_ruc(_guiaRuc);
            _infoClientesList.add(_infoCliente);


            List<InfoEspecifica> _infoEspecificaList = new ArrayList<>();
            InfoEspecifica _infoEspecifica = new InfoEspecifica();
            _infoEspecifica.set_sede(_labNombreSede.getText());
            if (_labNombreArea.getText().equals("NO APLICA"))
            {
                _infoEspecifica.set_area("----------------");
            } else
            {
                _infoEspecifica.set_area(_labNombreArea.getText());
            }
            _infoEspecificaList.add(_infoEspecifica);


            List<InfoDireccion> _infoDireccionList = new ArrayList<>();
            InfoDireccion _infoDireccion = new InfoDireccion();
            _infoDireccion.set_direccion(_guiaDireccion);
            _infoDireccionList.add(_infoDireccion);


            final Map<String, Object> _parameters = new HashMap<String, Object>();
            _parameters.put("param_nroGuia", param_nroGuia);
            _parameters.put("param_periodoServicio", param_periodoServicio);
            _parameters.put("param_infoCliente", _infoClientesList);
            _parameters.put("param_infoEspecifica", _infoEspecificaList);
            _parameters.put("param_infoDireccion", _infoDireccionList);
            _parameters.put("param_infoDosimetros", _dosimetroReporte);
            _parameters.put("param_totalDosimetros", param_totalDosimetros);

            toPdfUpload("guia_dosimetria_v9.jrxml", _parameters, _guiaId);


            /*
             *
             *
             *
             */


            new AlertBox("Asignación realizada", "Concluido", new Alert(Alert.AlertType.INFORMATION));

            Stage _stage = (Stage) _anchorPaneAsignacion.getScene().getWindow();
            _stage.close();
        }

    }

    // Event Listener on Button[#_btnCancelar].onAction
    @FXML
    public void OnActionBtnCancelar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneAsignacion.getScene().getWindow();
        _stage.close();
    }

    @FXML
    public void OnActionBtnMostrarPdf(ActionEvent event) throws JRException
    {
//        List<String> _clienteList = new ArrayList<>();
//        _clienteList.add(_labNombreCliente.getText());
//
//        List<String> _sedeList = new ArrayList<>();
//        _sedeList.add(_labNombreSede.getText());
//
//        List<String> _areaList = new ArrayList<>();
//        _areaList.add(_labNombreArea.getText());
//
//        final Map<String, Object> _parameters = new HashMap<String, Object>();
//        _parameters.put("Cliente", _clienteList);
//        _parameters.put("Sede", _sedeList);
//        _parameters.put("Area", _areaList);
//        _parameters.put("Dosimetros", _dosimetroReporte);
//
        String param_nroGuia = _labNumeroGuia.getText();
        String param_periodoServicio = _guiaPeriodo;
        String param_totalDosimetros = _labNumeroCantDosimetros.getText();

        List<InfoCliente> _infoClientesList = new ArrayList<>();
        InfoCliente _infoCliente = new InfoCliente();
        _infoCliente.set_razonSocial(_labNombreCliente.getText());
        _infoCliente.set_ruc(_guiaRuc);
        _infoClientesList.add(_infoCliente);


        List<InfoEspecifica> _infoEspecificaList = new ArrayList<>();
        InfoEspecifica _infoEspecifica = new InfoEspecifica();
        _infoEspecifica.set_sede(_labNombreSede.getText());
        if (_labNombreArea.getText().equals("NO APLICA"))
        {
            _infoEspecifica.set_area("----------------");
        } else
        {
            _infoEspecifica.set_area(_labNombreArea.getText());
        }
        _infoEspecificaList.add(_infoEspecifica);


        List<InfoDireccion> _infoDireccionList = new ArrayList<>();
        InfoDireccion _infoDireccion = new InfoDireccion();
        _infoDireccion.set_direccion(_guiaDireccion);
        _infoDireccionList.add(_infoDireccion);


        final Map<String, Object> _parameters = new HashMap<String, Object>();
        _parameters.put("param_nroGuia", param_nroGuia);
        _parameters.put("param_periodoServicio", param_periodoServicio);
        _parameters.put("param_infoCliente", _infoClientesList);
        _parameters.put("param_infoEspecifica", _infoEspecificaList);
        _parameters.put("param_infoDireccion", _infoDireccionList);
        _parameters.put("param_infoDosimetros", _dosimetroReporte);
        _parameters.put("param_totalDosimetros", param_totalDosimetros);

        toPDF("guia_dosimetria_v9.jrxml", _parameters);
    }

    public void setDatosComunesGuia(String _cliente, String _sede, String _area, String _guia, String _ordenPedido, String _cantidad, String _direccion, String _periodo, String _ruc, String _externo)
    {
        _labNombreCliente.setText(_cliente);
        _labNombreSede.setText(_sede);
        _labNombreArea.setText(_area);
        _labNumeroGuia.setText(_guia);
        _guiaId = Integer.parseInt(_guia);
        _labNumeroOrdenPedido.setText(_ordenPedido);
        _labNumeroCantDosimetros.setText(_cantidad);

        _guiaDireccion = _direccion;
        _guiaPeriodo = _periodo;
        _guiaRuc = _ruc;

        LlenarDatosPoes(_externo);
    }

    public void LlenarDatosPoes(String _ext)
    {
        //_prestamoAsignadoList = FXCollections.observableArrayList(_prestamoService.findPrestamosByGuia(_guiaId));
        _poeList = FXCollections.observableArrayList(_poeService.findPoeByGuia(_guiaId));
        int i = 0;
        for (Poe _poe : _poeList)
        {
            CustomRowAsignacion _row = new CustomRowAsignacion();
            _row.set_txtNombrePoe(_poe.get_apellidos().toUpperCase() + " " + _poe.get_nombres() + _ext);
            _row.set_txtCodigoDosimetro("dosimetro" + i);
            _row.set_labNumeracion("#" + (i + 1));
            ReporteGuia _report = new ReporteGuia();

            _report.set_codigo((i + 1));
            _report.set_nombre((_poe.get_apellidos().toUpperCase() + " " + _poe.get_nombres() + _ext));
//            _report.set_nombre("ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ");
            _report.set_tde(_poe.get_tipoDos() + _poe.get_tipoExp());
            _report.set_rad(_poe.get_tipoRad());

            _dosimetroReporte.add(_report);

            _lista.add(_row);
            i++;
        }

        _vboxAsignacion.getChildren().addAll(_lista);

        LlenarDosimetros(i);
    }

    public void LlenarDosimetros(int _cantidad)
    {
        _dosimetroLimpioList = FXCollections.observableArrayList(_magazineService.findDosimetrosLimpios(_cantidad));

        int i = 0;
        for (CustomRowAsignacion _row : _lista)
        {
            _row.set_txtCodigoDosimetro(_dosimetroLimpioList.get(i));
            _dosimetroReporte.get(i).set_numero(_dosimetroLimpioList.get(i));
            i++;
        }
    }

    public void toPDF(String _reportName, Map<String, Object> _parameters) throws JRException
    {

//        final String _reportSource = getClass().getClassLoader().getResource("reportes/" + _reportName).getPath();

//agregar esto cuando se va a compilar
        String _projectPath = System.getProperty("user.dir");
        final String _reportSource = (_projectPath + "/plantilla/" + _reportName);

        final JasperDesign _jd = JRXmlLoader.load(_reportSource);
        final JasperReport _report = JasperCompileManager.compileReport(_jd);
        final JasperPrint _print = JasperFillManager.fillReport(_report, _parameters, new JREmptyDataSource());
        final String _reporTarget = _reportSource.substring(0, _reportSource.lastIndexOf('/'))
                .concat("/")
                .concat(_reportName)
                .concat(".pdf");
        System.out.println("Ruta del archivo: " + _reporTarget);

        JasperExportManager.exportReportToPdfFile(_print, _reporTarget);
        JasperViewer.viewReport(_print, false);
        File _fichero = new File(_reporTarget);

        if (_fichero.delete())
        {
            System.out.println("El fichero ha sido borrado satisfactoriamente");
        } else
        {
            System.out.println("El fichero no puede ser borrado");
        }
    }

    public void toPdfUpload(String _reportName, Map<String, Object> _parameters, int _guiaId) throws JRException
    {
//        final String _reportSource = getClass().getClassLoader().getResource("reportes/" + _reportName).getPath();

        //agregar esto cuando se va a compilar
        String _projectPath = System.getProperty("user.dir");
        final String _reportSource = (_projectPath + "/plantilla/" + _reportName);

        final JasperDesign _jd = JRXmlLoader.load(_reportSource);
        final JasperReport _report = JasperCompileManager.compileReport(_jd);
        final JasperPrint _print = JasperFillManager.fillReport(_report, _parameters, new JREmptyDataSource());
        final String _reporTarget = _reportSource.substring(0, _reportSource.lastIndexOf('/'))
                .concat("/")
                .concat(_reportName)
                .concat(".pdf");
        System.out.println("Ruta del archivo: " + _reporTarget);

        JasperExportManager.exportReportToPdfFile(_print, _reporTarget);

        File _fichero = new File(_reporTarget);

        _guiaService.uploadFile(_fichero, _guiaId);

        if (_fichero.delete())
        {
            System.out.println("El fichero ha sido borrado satisfactoriamente");
        } else
        {
            System.out.println("El fichero no puede ser borrado");
        }
    }

    public class InfoCliente
    {
        private String _ruc;
        private String _razonSocial;

        public InfoCliente()
        {
        }

        public String get_ruc()
        {
            return _ruc;
        }

        public void set_ruc(String _ruc)
        {
            this._ruc = _ruc;
        }

        public String get_razonSocial()
        {
            return _razonSocial;
        }

        public void set_razonSocial(String _razonSocial)
        {
            this._razonSocial = _razonSocial;
        }
    }

    public class InfoEspecifica
    {
        String _sede;
        String _area;

        public InfoEspecifica()
        {
        }

        public String get_sede()
        {
            return _sede;
        }

        public void set_sede(String _sede)
        {
            this._sede = _sede;
        }

        public String get_area()
        {
            return _area;
        }

        public void set_area(String _area)
        {
            this._area = _area;
        }
    }

    public class InfoDireccion
    {
        String _direccion;

        public InfoDireccion()
        {
        }

        public String get_direccion()
        {
            return _direccion;
        }

        public void set_direccion(String _direccion)
        {
            this._direccion = _direccion;
        }
    }

    public class InfoDosimetros
    {
        String _codigo;
        String _nombre;
        String _tde;
        String _rad;
        String _numero;

        public InfoDosimetros()
        {
        }

        public String get_codigo()
        {
            return _codigo;
        }

        public void set_codigo(String _codigo)
        {
            this._codigo = _codigo;
        }

        public String get_nombre()
        {
            return _nombre;
        }

        public void set_nombre(String _nombre)
        {
            this._nombre = _nombre;
        }

        public String get_tde()
        {
            return _tde;
        }

        public void set_tde(String _tde)
        {
            this._tde = _tde;
        }

        public String get_rad()
        {
            return _rad;
        }

        public void set_rad(String _rad)
        {
            this._rad = _rad;
        }

        public String get_numero()
        {
            return _numero;
        }

        public void set_numero(String _numero)
        {
            this._numero = _numero;
        }
    }
}