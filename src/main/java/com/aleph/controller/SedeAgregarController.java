package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.model.Responsable;
import com.aleph.model.Sede;
import com.aleph.service.IResponsableService;
import com.aleph.service.ISedeService;
import com.aleph.service.ResponsableService;
import com.aleph.service.SedeService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class SedeAgregarController
{
    private ISedeService _sedeService = new SedeService();
    private IResponsableService _responsableService = new ResponsableService();

    @FXML
    private AnchorPane _anchorPaneAgregarSede;
    @FXML
    private TextField _txtContactoNombre1;
    @FXML
    private TextField _txtContactoEmail1;
    @FXML
    private TextField _txtContactoTelefono1;
    @FXML
    private TextField _txtCliente;
    @FXML
    private TextField _txtSedeNombre;
    @FXML
    private TextField _txtSedeDireccion;
    @FXML
    private Button _btnGuardarSede;
    @FXML
    private Button _btnCancelarSede;
    @FXML
    private Button _btnBuscarCliente;
    @FXML
    private Button _btnAgregarContactos;
    @FXML
    private ComboBox _cboxDistritos;
    @FXML
    public GridPane _gridPaneContacto;
    @FXML
    public VBox _vboxContactos;


    //Variables temporables
    private ObservableList<CustomResponsable> _lista = FXCollections.observableArrayList();
    private int _clienteIdTemp;

    @FXML
    public void initialize()
    {
        LlenarComboDistritos();
        cargarContactos();
    }

    // Event Listener on Button[#_btnGuardarSede].onAction
    @FXML
    public void OnActionBtnGuardarSede(ActionEvent event)
    {
        if (_txtCliente.getText().isEmpty())
        {
            new AlertBox("El cliente no debe ser vacio", "error", new Alert(Alert.AlertType.ERROR));
        } else if (_txtSedeNombre.getText().isEmpty())
        {
            new AlertBox("el campo nombre de sede está vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else if (_txtSedeDireccion.getText().isEmpty())
        {
            new AlertBox("el campo direccion está vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else
//            else if (_txtContactoNombre.getText().isEmpty())
//        {
//            new AlertBox("el campo nombre de contacto está vacío", "error", new Alert(Alert.AlertType.ERROR));
//        } else if (_txtContactoEmail.getText().isEmpty())
//        {
//            new AlertBox("el campo email de contacto está vacío", "error", new Alert(Alert.AlertType.ERROR));
//        } else if (_txtContactoTelefono.getText().isEmpty())
//        {
//            new AlertBox("el campo telefono de contacto está vacío", "error", new Alert(Alert.AlertType.ERROR));
//        } else
        {
            try
            {
//                boolean _error = false;
//
//                if (!_error)
//                {
                Sede _sede = new Sede();
                _sede.set_idCliente(_clienteIdTemp);
                _sede.set_nombre(_txtSedeNombre.getText());
                _sede.set_direccion(_txtSedeDireccion.getText());
                _sede.set_distrito((String) _cboxDistritos.getValue());

                int _codigo_ultimo_sede = _sedeService.addSede(_sede);

                for (int i = 0; i < _lista.size(); i++)
                {
                    Responsable _responsable = new Responsable();
                    _responsable.set_nombre(_lista.get(i).get_txtContactoNombre());
                    _responsable.set_email(_lista.get(i).get_txtContactoEmail());
                    _responsable.set_celular(_lista.get(i).get_txtContactoTelefono());
                    _responsableService.addResponsable(_responsable, _codigo_ultimo_sede);
                }


                new AlertBox("Sede registrada con éxito!!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
                OnActionBtnCancelarSede(event);
//                }
            } catch (RuntimeException e)
            {
                System.err.println("Error en 2: " + e);
            }
        }
    }

    // Event Listener on Button[#_btnCancelarSede].onAction
    @FXML
    public void OnActionBtnCancelarSede(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneAgregarSede.getScene().getWindow();
        _stage.close();
    }

    // Event Listener on Button[#_btnBuscarCliente].onAction
    @FXML
    public void OnActionBtnBuscarCliente(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._sedeBuscarClientePath));
            Stage _stage = new Stage();
            _stage.initOwner(_btnBuscarCliente.getScene().getWindow());
            _stage.setScene(new Scene(_loader.load()));

            _stage.initModality(Modality.WINDOW_MODAL);
            _stage.showAndWait();

            SedeBuscarClienteController _controller = _loader.getController();
            _txtCliente.setText(_controller.getRazonSocialSend());
            _clienteIdTemp = _controller.getIdCliente();

        } catch (IOException e)
        {
            System.out.println("CLASE SedeAgregar Metodo OnActionBtnBuscarCliente" + e.getMessage());
        }
    }

    @FXML
    public void OnActionBtnAgregarContacto(ActionEvent event) {
        cargarContactos();
    }

    private void LlenarComboDistritos()
    {
        _cboxDistritos.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxDistritos.getItems().add("Otros");
        _cboxDistritos.getItems().add("Ancon");
        _cboxDistritos.getItems().add("Ate");
        _cboxDistritos.getItems().add("Barranco");
        _cboxDistritos.getItems().add("Breña");
        _cboxDistritos.getItems().add("Carabayllo");
        _cboxDistritos.getItems().add("Chaclacayo");
        _cboxDistritos.getItems().add("Chorrillos");
        _cboxDistritos.getItems().add("Cieneguilla");
        _cboxDistritos.getItems().add("Comas");
        _cboxDistritos.getItems().add("El Agustino");
        _cboxDistritos.getItems().add("Huaycan");
        _cboxDistritos.getItems().add("Independencia");
        _cboxDistritos.getItems().add("Jesus Maria");
        _cboxDistritos.getItems().add("La Molina");
        _cboxDistritos.getItems().add("La Victoria");
        _cboxDistritos.getItems().add("Lima");
        _cboxDistritos.getItems().add("Lince");
        _cboxDistritos.getItems().add("Los Olivos");
        _cboxDistritos.getItems().add("Lurigancho");
        _cboxDistritos.getItems().add("Lurin");
        _cboxDistritos.getItems().add("Magdalena del Mar");
        _cboxDistritos.getItems().add("Miraflores");
        _cboxDistritos.getItems().add("Pachacamac");
        _cboxDistritos.getItems().add("Pucusana");
        _cboxDistritos.getItems().add("Pueblo Libre");
        _cboxDistritos.getItems().add("Puente Piedra");
        _cboxDistritos.getItems().add("Punta Hermosa");
        _cboxDistritos.getItems().add("Punta Negra");
        _cboxDistritos.getItems().add("Rimac");
        _cboxDistritos.getItems().add("San Bartolo");
        _cboxDistritos.getItems().add("San Borja");
        _cboxDistritos.getItems().add("San Isidro");
        _cboxDistritos.getItems().add("San Juan de Lurigancho");
        _cboxDistritos.getItems().add("San Juan de Miraflores");
        _cboxDistritos.getItems().add("San Luis");
        _cboxDistritos.getItems().add("San Martin de Porres");
        _cboxDistritos.getItems().add("San Miguel");
        _cboxDistritos.getItems().add("Santa Anita");
        _cboxDistritos.getItems().add("Santa Maria del Mar");
        _cboxDistritos.getItems().add("Santa Rosa");
        _cboxDistritos.getItems().add("Santiago de Surco");
        _cboxDistritos.getItems().add("Surquillo");
        _cboxDistritos.getItems().add("Villa El Salvador");
        _cboxDistritos.getItems().add("Villa Maria del Triunfo");
        _cboxDistritos.getItems().add("Bellavista");
        _cboxDistritos.getItems().add("Callao");
        _cboxDistritos.getItems().add("Carmen de La Legua-Reynoso");
        _cboxDistritos.getItems().add("La Perla");
        _cboxDistritos.getItems().add("La Punta");
        _cboxDistritos.getItems().add("Ventanilla");
        _cboxDistritos.getItems().add("Mi Peru");
        _cboxDistritos.getSelectionModel().select(0);
    }

    public void cargarContactos()
    {
        CustomResponsable _row = new CustomResponsable();
        _lista.add(_row);

        _vboxContactos.getChildren().addAll(_lista);
    }
}
