package com.aleph.controller;

import com.aleph.model.Dosimetro;
import com.aleph.service.DosimetroService;
import com.aleph.service.IDosimetroService;
import javafx.fxml.FXML;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import javafx.event.ActionEvent;

import javafx.scene.control.ComboBox;

import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class DosimetroAgregarController
{
    IDosimetroService _dosimetroService = new DosimetroService();

    @FXML
    private AnchorPane _anchorPaneDosimetroAgregar;
    @FXML
    private TextField _txtCodigo;
    @FXML
    private ComboBox _cboxTipo;
    @FXML
    private Button _btnGuardar;
    @FXML
    private Button _btnCancelar;

    //Variables temporales
    Dosimetro _dosimetroTemp = new Dosimetro();

    @FXML
    public void initialize()
    {
        LlenarComboTipo();
    }

    // Event Listener on Button[#_btnGuardar].onAction
    @FXML
    public void OnActionBtnGuardar(ActionEvent event)
    {
        if (_txtCodigo.getText().isEmpty())
        {
            new AlertBox("El Codigo del dosimetro no debe estar vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else
        {
            try
            {
                _dosimetroTemp.set_dosimetroCodigo(agregarCeros(_txtCodigo.getText(), 7));
                _dosimetroTemp.set_tipo((String) _cboxTipo.getValue());

                _dosimetroService.addDosimetro(_dosimetroTemp);

                new AlertBox("Dosimetro registrado con éxito!!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
                OnActionBtnCancelar(event);
            } catch (RuntimeException e)
            {
                System.err.println("Error en 1: " + e);
            }
        }
    }

    // Event Listener on Button[#_btnCancelar].onAction
    @FXML
    public void OnActionBtnCancelar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneDosimetroAgregar.getScene().getWindow();
        _stage.close();
    }

    private void LlenarComboTipo()
    {
        _cboxTipo.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxTipo.getItems().addAll("Cuerpo", "Anillo");
        _cboxTipo.getSelectionModel().select(0);
    }

    private static String agregarCeros(String _numero, int _longitud)
    {
        String ceros = "";

        int cantidad = _longitud - _numero.length();

        if (cantidad >= 1)
        {
            for (int i = 0; i < cantidad; i++)
            {
                ceros += "0";
            }

            return (ceros + _numero);
        } else
        {
            return _numero;
        }
    }
}
