package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.model.Sede;
import com.aleph.service.ISedeService;
import com.aleph.service.SedeService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

import javafx.event.ActionEvent;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class SedeBuscarController
{
    private ISedeService _sedeService = new SedeService();
    @FXML
    private AnchorPane _anchorPaneBuscarSede;
    @FXML
    private TableView<Sede> _tvSede;
    @FXML
    private TextField _txtBuscar;
    @FXML
    private Button _btnAgregarSede;
    @FXML
    private Button _btnDetalleSede;
    @FXML
    public RadioButton _rbClienteNom;
    @FXML
    private RadioButton _rbSedeNom;
    @FXML
    private RadioButton _rbDireccion;
    @FXML
    private ToggleGroup _tgSede;
    @FXML
    private Button _btnBuscarSede;
    @FXML
    private Button _btnEliminarSede;
    @FXML
    private Button _btnEditarSede;

    // Event Listener on Button[#_btnAgregarSede].onAction
    @FXML
    public void OnActionBtnAgregarSede(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._sedeAgregarPath));
        Parent _SedeAgregarPageParent = _loader.load();

        Stage _SedeAgregarModalStage = new Stage();
        Scene _SedeAgregarPageScene = new Scene(_SedeAgregarPageParent);
        _SedeAgregarModalStage.setScene(_SedeAgregarPageScene);
        _SedeAgregarModalStage.initModality(Modality.WINDOW_MODAL);
        _SedeAgregarModalStage.initOwner(_anchorPaneBuscarSede.getScene().getWindow());
        _SedeAgregarModalStage.setResizable(false);
        _SedeAgregarModalStage.setMaximized(false);
        _SedeAgregarModalStage.show();
    }

    // Event Listener on Button[#_btnDetalleSede].onAction
    @FXML
    public void OnActionBtnDetalleSede(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._sedeDetallePath));
            Parent _sedeDetallePageParent = _loader.load();

            SedeDetalleController _controller = _loader.getController();
            _controller.setSede(_tvSede.getSelectionModel().getSelectedItem());

            Stage _sedeDetalleModalStage = new Stage();
            Scene _sedeDetallePageScene = new Scene(_sedeDetallePageParent);
            _sedeDetalleModalStage.setScene(_sedeDetallePageScene);
            _sedeDetalleModalStage.initModality(Modality.WINDOW_MODAL);
            _sedeDetalleModalStage.initOwner(_anchorPaneBuscarSede.getScene().getWindow());
            _sedeDetalleModalStage.setResizable(false);
            _sedeDetalleModalStage.setMaximized(false);
            _sedeDetalleModalStage.show();
        } catch (Exception e)
        {
            System.out.println("ERROR : " + e);
        }

    }

    // Event Listener on Button[#_btnBuscarSede].onAction
    @FXML
    public void OnActionBtnBuscarSede(ActionEvent event)
    {
        Buscar();
    }

    // Event Listener on Button[#_btnEliminarSede].onAction
    @FXML
    public void OnActionBtnEliminarSede(ActionEvent event)
    {
        Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
        _alert.setTitle("Cuadro de confirmación");
        _alert.setHeaderText("Eliminar Sede");
        _alert.setContentText("¿Quiere elminar ests sede?");

        Optional<ButtonType> _opcion = _alert.showAndWait();
        if (_opcion.get() == ButtonType.OK)
        {
            _sedeService.removeSede(_tvSede.getSelectionModel().getSelectedItem().get_sedeId());
        } else
        {

        }
    }

    // Event Listener on Button[#_btnEditarSede].onAction
    @FXML
    public void OnActionBtnEditarSede(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._sedeEditarPath));
            Parent _sedeEditarPageParent = _loader.load();

            SedeEditarController _controller = _loader.getController();
            _controller.setSede(_tvSede.getSelectionModel().getSelectedItem());

            Stage _sedeEditarModalStage = new Stage();
            Scene _sedeEditarPageScene = new Scene(_sedeEditarPageParent);
            _sedeEditarModalStage.setScene(_sedeEditarPageScene);
            _sedeEditarModalStage.initModality(Modality.WINDOW_MODAL);
            _sedeEditarModalStage.initOwner(_anchorPaneBuscarSede.getScene().getWindow());
            _sedeEditarModalStage.setResizable(false);
            _sedeEditarModalStage.setMaximized(false);
            _sedeEditarModalStage.showAndWait();
            _tvSede.getItems().clear();
        } catch (Exception e)
        {
            System.out.println("ERROR : " + e);
        }
    }

    private void Buscar()
    {
        if (_rbSedeNom.isSelected())
        {
            ObservableList<Sede> _sedeList = FXCollections.observableArrayList(_sedeService.findSedeByNombre(_txtBuscar.getText()));
            _tvSede.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_nombreCliente"));
            _tvSede.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_nombre"));
            _tvSede.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_direccion"));
            _tvSede.setItems(_sedeList);
        }
        if (_rbDireccion.isSelected())
        {
            ObservableList<Sede> _sedeList = FXCollections.observableArrayList(_sedeService.findSedeByDireccion(_txtBuscar.getText()));
            _tvSede.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_nombreCliente"));
            _tvSede.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_nombre"));
            _tvSede.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_direccion"));
            _tvSede.setItems(_sedeList);
        }
        if (_rbClienteNom.isSelected())
        {
            ObservableList<Sede> _sedeList = FXCollections.observableArrayList(_sedeService.findSedeByClienteNom(_txtBuscar.getText()));
            _tvSede.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_nombreCliente"));
            _tvSede.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_nombre"));
            _tvSede.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_direccion"));
            _tvSede.setItems(_sedeList);
        }
    }

    public void OnKeyPressed(KeyEvent keyEvent)
    {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
        {
            Buscar();
        }
    }
}
