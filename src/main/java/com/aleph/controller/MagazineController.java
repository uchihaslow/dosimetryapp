package com.aleph.controller;

import com.aleph.model.Magazine;
import com.aleph.model.Paths;
import com.aleph.service.IMagazineService;
import com.aleph.service.MagazineService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class MagazineController
{
    public AnchorPane _anchorPaneDosimetroBuscar;
    public TextField _txtBuscar;
    public Button _btnAgregar;
    public Button _btnBuscar;
    public Button _btnEliminar;
    public TableColumn _tcCodigo;
    public TableColumn _tcFecha;
    public TableView<Magazine> _tvMagazine;

    IMagazineService _magazineService = new MagazineService();

    public void OnActionBtnAgregar(ActionEvent actionEvent)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._magazineAgregarPath));
            Stage _stage = new Stage();
            _stage.initOwner(_btnAgregar.getScene().getWindow());
            _stage.setScene(new Scene(_loader.load()));

            _stage.initModality(Modality.WINDOW_MODAL);
            _stage.showAndWait();

            FillMagazine();

        } catch (IOException e)
        {
            System.out.println("Clase: Magazine Metodo: OnActionBtnAgregar" + e.getMessage());
        }
    }

    public void OnActionBtnBuscar(ActionEvent actionEvent)
    {
        ObservableList<Magazine> _magazineList = FXCollections.observableArrayList(_magazineService.findDosimetroByCodigo(_txtBuscar.getText()));
        _tvMagazine.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_codigo"));
        _tvMagazine.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_fecha"));
        _tvMagazine.setItems(_magazineList);
    }

    public void OnActionBtnEliminar(ActionEvent actionEvent)
    {
        Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
        _alert.setTitle("Cuadro de confirmación");
        _alert.setHeaderText("Eliminar el dosimetro del magazine");
        _alert.setContentText("¿Quiere continuar?");

        Optional<ButtonType> _opcion = _alert.showAndWait();
        if (_opcion.get() == ButtonType.OK)
        {
            _magazineService.deshabilitarDosimetro(_tvMagazine.getSelectionModel().getSelectedItem().get_codigo());
            FillMagazine();
        }
    }

    public void FillMagazine()
    {
        ObservableList<Magazine> _magazineList = FXCollections.observableArrayList(_magazineService.ListDosimetros());
        _tvMagazine.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_codigo"));
        _tvMagazine.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_fecha"));
        _tvMagazine.setItems(_magazineList);
    }
}
