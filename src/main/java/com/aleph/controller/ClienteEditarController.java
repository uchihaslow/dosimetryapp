package com.aleph.controller;

import com.aleph.model.Cliente;
import com.aleph.service.ClienteService;
import com.aleph.service.IClienteService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ClienteEditarController
{
    private IClienteService _clienteService = new ClienteService();

    @FXML
    private AnchorPane _anchorPaneEditarCliente;
    @FXML
    private TextField _txtRuc;
    @FXML
    private TextField _txtRazonSocial;
    @FXML
    private TextField _txtDireccion;
    @FXML
    private TextField _txtTelefono;
    @FXML
    private TextField _txtEmail;
    @FXML
    private ComboBox _cboxPrioridad;
    @FXML
    private Button _btnGuardarCliente;
    @FXML
    private Button _btnCancelar;

    private int _clienteIdTemp;

    @FXML
    public void initialize()
    {

    }

    // Event Listener on Button[#_btnGuardarCliente].onAction
    @FXML
    public void OnActionBtnGuardarCliente(ActionEvent event)
    {
        if (_txtRuc.getText().isEmpty())
        {
            new AlertBox("El ruc del cliente no debe ser vacio", "error", new Alert(Alert.AlertType.ERROR));
        } else if (_txtRazonSocial.getText().isEmpty())
        {
            new AlertBox("La razón social no debe ser vacía", "error", new Alert(Alert.AlertType.ERROR));
        } else if (_txtDireccion.getText().isEmpty())
        {
            new AlertBox("La dirección no debe ser vacía", "error", new Alert(Alert.AlertType.ERROR));
        } else if (_txtTelefono.getText().isEmpty())
        {
            new AlertBox("El telefono no debe ser vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else if (_txtEmail.getText().isEmpty())
        {
            new AlertBox("El email no debe ser vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else if (_cboxPrioridad.getValue() == null)
        {
            new AlertBox("Tiene que seleccionar una prioridad", "error", new Alert(Alert.AlertType.ERROR));
        } else
        {
            try
            {
                boolean _error = false;

                String _rucForm = null;
                String _razonSocialForm;
                String _direccionForm;
                String _telefonoForm = null;
                String _emailForm;
                String _prioridadForm = null;

                if (!_txtRuc.getText().isEmpty() && _txtRuc.getText().length() == 11)
                {
                    _rucForm = _txtRuc.getText();
                } else
                {
                    new AlertBox("El RUC del cliente debe contar con 11 numeros", "Error", new Alert(Alert.AlertType.ERROR));
                    _error = true;
                }

                _razonSocialForm = _txtRazonSocial.getText();

                _direccionForm = _txtDireccion.getText();

                if (_txtTelefono.getText().length() < 12)
                {
                    _telefonoForm = _txtTelefono.getText();
                } else
                {
                    new AlertBox("El telefono debe contener como maximo 12 numeros  ", "Error", new Alert(Alert.AlertType.ERROR));
                    _error = true;
                }
                _emailForm = _txtEmail.getText();
                _prioridadForm = (String) _cboxPrioridad.getValue();
                if (!_error)
                {
                    try
                    {
                        Cliente _cliente = new Cliente();
                        _cliente.set_clienteId(_clienteIdTemp);
                        _cliente.set_ruc(_rucForm);
                        _cliente.set_razonSocial(_razonSocialForm);
                        _cliente.set_direccion(_direccionForm);
                        _cliente.set_telefono(_telefonoForm);
                        _cliente.set_email(_emailForm);
                        _cliente.set_prioridad(_prioridadForm);
                        _clienteService.updateCliente(_cliente);
                        new AlertBox("Cliente actualizado con éxito!!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
                        OnActionBtnCancelar(event);
                    } catch (RuntimeException e)
                    {
                        System.err.println("Error en 1: " + e);
                    }
                }
            } catch (NumberFormatException e)
            {
                System.err.println("Error en 2: " + e);
            }
        }
    }

    // Event Listener on Button[#_btnCancelar].onAction
    @FXML
    public void OnActionBtnCancelar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneEditarCliente.getScene().getWindow();
        _stage.close();
    }

    private void LlenarComboPrioridad(String _opcion)
    {
        _cboxPrioridad.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxPrioridad.getItems().addAll("Alta", "Media", "Baja");
        if (_opcion.equals("Alta"))
        {
            _cboxPrioridad.getSelectionModel().select(0);
        } else if (_opcion.equals("Media"))
        {
            _cboxPrioridad.getSelectionModel().select(1);
        } else if (_opcion.equals("Baja"))
        {
            _cboxPrioridad.getSelectionModel().select(2);
        }
    }

    public void setCliente(Cliente _cliente)
    {
        _txtRuc.setText(_cliente.get_ruc());
        _txtRazonSocial.setText(_cliente.get_razonSocial());
        _txtDireccion.setText(_cliente.get_direccion());
        _txtTelefono.setText(_cliente.get_telefono());
        _txtEmail.setText(_cliente.get_email());
        LlenarComboPrioridad(_cliente.get_prioridad());
        _clienteIdTemp = _cliente.get_clienteId();
    }
}
