package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.model.Usuario;
import com.aleph.service.GuiaService;
import com.aleph.service.IGuiaService;
import com.aleph.tools.TableUtils;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.event.ActionEvent;

import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import javafx.scene.control.TableView;

import javafx.scene.control.TableColumn;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class GuiaAsignadosController
{
    //region Controles

    @FXML
    private AnchorPane _anchorPaneGuiaAsignadas;
    @FXML
    private TableView _tvGuiaAsignados;
    @FXML
    private TableColumn _tcNroGuia;
    @FXML
    private TableColumn _tcPeriodo;
    @FXML
    private TableColumn _tcNroPedido;
    @FXML
    private TableColumn _tcRuc;
    @FXML
    private TableColumn _tcNombreCliente;
    @FXML
    private TableColumn _tcNombreSede;
    @FXML
    private TableColumn _tcNombreArea;
    @FXML
    private TableColumn _tcCantidadUsuarios;
    @FXML
    private TextField _txtFiltro;
    @FXML
    private Button _btnShowPdf;
    @FXML
    public Button _btnGenEtiquetas;
    @FXML
    public Button _btnGenSobre;
    @FXML
    public Button _btnCambios;

    //endregion

    private Usuario _usuarioTemp;
    IGuiaService _guiaService = new GuiaService();

    private ObservableList<ObservableList> _listaGuiaAsignados;
    private ObservableList<ObservableList> _listaSeleccionados = FXCollections.observableArrayList();
    private SortedList<ObservableList> sortedData;

    @FXML
    public void initialize()
    {
        _tvGuiaAsignados.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        _tvGuiaAsignados.getSelectionModel().getSelectedItems().addListener(new ListChangeListener()
        {
            @Override
            public void onChanged(Change c)
            {
                _listaSeleccionados.setAll(c.getList());
            }
        });

        LlenarTvGuiaAsignados();

        //case sensitive
        FilteredList<ObservableList> filteredData = new FilteredList<>(_listaGuiaAsignados, p -> true);

        _txtFiltro.textProperty().addListener((observable, oldValue, newValue) ->
        {
            filteredData.setPredicate(observableList ->
            {
                if (newValue == null || newValue.isEmpty())
                {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                if (((String) observableList.get(0)).toLowerCase().indexOf(lowerCaseFilter) != -1)
                {
                    return true;
                } else if (((String) observableList.get(4)).toLowerCase().indexOf(lowerCaseFilter) != -1)
                {
                    return true;
                } else if (((String) observableList.get(3)).toLowerCase().indexOf(lowerCaseFilter) != -1)
                {
                    return true;
                } else if (((String) observableList.get(2)).toLowerCase().indexOf(lowerCaseFilter) != -1)
                {
                    return true;
                } else if (((String) observableList.get(7)).toLowerCase().indexOf(lowerCaseFilter) != -1)
                {
                    return true;
                }
                return false;
            });
        });

        //SortedList<ObservableList> sortedData = new SortedList<>(filteredData);
        sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(_tvGuiaAsignados.comparatorProperty());

        _tvGuiaAsignados.setItems(sortedData);

        // enable multi-selection
        _tvGuiaAsignados.getSelectionModel().setCellSelectionEnabled(true);
        // enable copy/paste
        TableUtils.installCopyPasteHandler(_tvGuiaAsignados);
    }

    // Event Listener on Button[#_btnShowPdf].onAction
    @FXML
    public void OnActionBtnMostrarPdf(ActionEvent event) throws IOException //throws JRException
    {
        int _guiaDownload = Integer.parseInt(((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(0));

        /* PARA MOSTRAR UNA PANTALLA DONDE GUARDAR EL PDF

        FileChooser _fc = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
        _fc.getExtensionFilters().add(extFilter);
        _fc.setInitialFileName("Guia_Nro_" + _guiaDownload);

        File _file = _fc.showSaveDialog(null);


        _file = new File(_file.getAbsolutePath() + ".pdf");

        _guiaService.downloadFile(_guiaDownload, _file);

        */


        String _projectPath = System.getProperty("user.dir");
        final String _reportSource = (_projectPath + "/temp/");
        //final String _reportSource = (_projectPath + "/plantilla/" + "temp.pdf");
        //final String _reportSource = ("/media/windata/JavaProjects/DOSYS/plantilla/" + "temp.pdf");
        //final String _reportSource = ("/media/windata/JavaProjects/DOSYS/plantilla/");

//        if (new File(_reportSource).delete())
//        {
//            System.out.println("El fichero ha sido borrado satisfactoriamente");
//        } else
//        {
//            System.out.println("El fichero no puede ser borrado");
//        }

        File _directorio = new File(_reportSource);

        System.out.println("directory" + _directorio);

        File _file = File.createTempFile("temp_guia_" + _guiaDownload + "_", ".pdf", _directorio);

        System.out.println("Path Absolute" + _file.getAbsolutePath());

        _guiaService.downloadFile(_guiaDownload, _file);


        try
        {
            if (_file.exists())
            {
                if (Desktop.isDesktopSupported())
                {
                    new Thread
                            (
                                    () ->
                                    {
                                        try
                                        {
                                            Desktop.getDesktop().open(_file);
                                            //_file.deleteOnExit();

                                        } catch (IOException e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }
                            ).start();
                } else
                {
                    System.out.println("Awt Desktop is not supported!");
                }
            } else
            {
                System.out.println("File is not exists!");
            }
            _file.deleteOnExit();
            System.out.println("Done");
        } catch (Exception ex)
        {
            System.err.println("error: " + ex);
        }


//        JasperReport jreport = JasperPrintManager.printReport(_file.getAbsolutePath(),false);
//
//        final JasperPrint _print = JasperFillManager.fillReport(jreport, null);
//
//        JasperViewer.viewReport(_print, false);

//        if (_file.delete())
//        {
//            System.out.println("El fichero ha sido borrado satisfactoriamente");
//        } else
//        {
//            System.out.println("El fichero no puede ser borrado");
//        }

    }

    public void LlenarTvGuiaAsignados()
    {
        _listaGuiaAsignados = FXCollections.observableArrayList(_guiaService.listGuiaAsignadas());

        _tcNroGuia.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(0).toString())
                );

        _tcPeriodo.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(7).toString())
                );

        _tcNroPedido.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(1).toString())
                );

        _tcRuc.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(8).toString())
                );

        _tcNombreCliente.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(4).toString())
                );

        _tcNombreSede.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(2).toString())
                );

        _tcNombreArea.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(5).toString())
                );

        _tcCantidadUsuarios.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(3).toString())
                );

        _tvGuiaAsignados.setItems(_listaGuiaAsignados);

    }

    public void OnActionBtnGenEtiqueta(ActionEvent actionEvent) throws Exception
    {
        ArrayList<Integer> _listaGuias = new ArrayList<>();
        for (ObservableList i : _listaSeleccionados)
        {
            System.out.println(i.get(0));
            _listaGuias.add(Integer.valueOf(String.valueOf(i.get(0))));
        }
        List<Object> DATA = _guiaService.listaDosimetrosPorGuia(_listaGuias);
        ExportarDosimetrosExcel(DATA);
    }

    public void OnActionBtnGenSobre(ActionEvent actionEvent) throws Exception
    {
        ArrayList<Integer> _listaGuias = new ArrayList<>();
        for (ObservableList i : _listaSeleccionados)
        {
            System.out.println(i.get(0));
            _listaGuias.add(Integer.valueOf(String.valueOf(i.get(0))));
        }
        List<Object> DATA = _guiaService.listaSobresPorGuia(_listaGuias);
        ExportarSobreExcel(DATA);
    }

    public void OnActionCambios(ActionEvent actionEvent)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._guiaGuiaCambiosPath));
            Parent _guiaCambiosPageParent = _loader.load();

            GuiaCambiosController _controller = _loader.getController();

            _controller.setDatosComunesGuia
                    (
                            ((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(4),//Cliente
                            ((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(2),//Sede
                            ((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(5),//Area
                            ((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(0),//Guia Nro
                            ((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(1),//OrdenPedido
                            ((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(3),//Cantidad
                            ((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(6),//Direccion
                            ((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(7),//Periodo
                            ((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(8)//RUC
                            //((ObservableList<String>) _tvGuiaAsignados.getSelectionModel().getSelectedItem()).get(9)//Externo
                    );

            Stage _guiaCambiosModalStage = new Stage();
            Scene _guiaCambiosPageScene = new Scene(_guiaCambiosPageParent);
            _guiaCambiosModalStage.setScene(_guiaCambiosPageScene);
            _guiaCambiosModalStage.initModality(Modality.WINDOW_MODAL);
            _guiaCambiosModalStage.initOwner(_anchorPaneGuiaAsignadas.getScene().getWindow());
            _guiaCambiosModalStage.setResizable(false);
            _guiaCambiosModalStage.setMaximized(false);
            _guiaCambiosModalStage.showAndWait();
            LlenarTvGuiaAsignados();
            _tvGuiaAsignados.setItems(sortedData);
        } catch (Exception e)
        {
            System.out.println("ERROR OnActionCambios : " + e);
        }
    }

    public void ExportarDosimetrosExcel(List<Object> DATA) throws Exception
    {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, "lista_usuarios_por_guia");

        String[] headers = new String[]{
                "razon_social",
                "guia",
                "periodo",
                "codigo_dosimetro",
                "dosexprad",
                "nombre_completo"
        };

        CellStyle headerStyle = workbook.createCellStyle();
        //Font font = workbook.createFont();
        //font.setBold(true);
        //headerStyle.setFont(font);

        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        HSSFRow headerRow = sheet.createRow(0);
        for (int i = 0; i < headers.length; ++i)
        {
            String header = headers[i];
            HSSFCell cell = headerRow.createCell(i);
            cell.setCellStyle(headerStyle);
            cell.setCellValue(header);
        }

        for (int i = 0; i < DATA.size(); ++i)
        {
            HSSFRow dataRow = sheet.createRow(i + 1);

            Object[] d = (Object[]) DATA.get(i);
            String razon_social = (String) d[0];
            String guia = (String) d[1];
            String periodo = (String) d[2];
            String codigo_dosimetro = (String) d[3];
            String dosexprad = (String) d[4];
            String nombre_completo = (String) d[5];

            dataRow.createCell(0).setCellValue(razon_social);
            dataRow.createCell(1).setCellValue(guia);
            dataRow.createCell(2).setCellValue(periodo);
            dataRow.createCell(3).setCellValue(codigo_dosimetro);
            dataRow.createCell(4).setCellValue(dosexprad);
            dataRow.createCell(5).setCellValue(nombre_completo);
        }

        FileChooser fileChooser = new FileChooser();

        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xls)", "*.xls");
        fileChooser.getExtensionFilters().add(extFilter);

        fileChooser.setInitialFileName("ETIQUETA_DOSIMETROS.xls");

        File file = fileChooser.showSaveDialog(null);

        FileOutputStream out = new FileOutputStream(file);
        workbook.write(out);
        out.close();
    }

    public void ExportarSobreExcel(List<Object> DATA) throws Exception
    {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, "lista_guias");

        String[] headers = new String[]{
                "guia",
                "razon_social",
                "sede",
                "sede_direccion",
                "area",
                "periodo",
                "contacto"
        };

        CellStyle headerStyle = workbook.createCellStyle();

        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        HSSFRow headerRow = sheet.createRow(0);
        for (int i = 0; i < headers.length; ++i)
        {
            String header = headers[i];
            HSSFCell cell = headerRow.createCell(i);
            cell.setCellStyle(headerStyle);
            cell.setCellValue(header);
        }

        for (int i = 0; i < DATA.size(); ++i)
        {
            HSSFRow dataRow = sheet.createRow(i + 1);

            Object[] d = (Object[]) DATA.get(i);
            String guia = (String) d[0];
            String razon_social = (String) d[1];
            String sede = (String) d[2];
            String sede_direccion = (String) d[3];
            String area = (String) d[4];
            if (area.equals("NO APLICA"))
            {
                area = "";
            }
            String periodo = (String) d[5];
            String contacto = (String) d[6];

            dataRow.createCell(0).setCellValue(guia);
            dataRow.createCell(1).setCellValue(razon_social);
            dataRow.createCell(2).setCellValue(sede);
            dataRow.createCell(3).setCellValue(sede_direccion);
            dataRow.createCell(4).setCellValue(area);
            dataRow.createCell(5).setCellValue(periodo);
            dataRow.createCell(6).setCellValue(contacto);
        }

        FileChooser fileChooser = new FileChooser();

        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xls)", "*.xls");
        fileChooser.getExtensionFilters().add(extFilter);

        fileChooser.setInitialFileName("ETIQUETA_BOLSAS.xls");

        File file = fileChooser.showSaveDialog(null);

        FileOutputStream out = new FileOutputStream(file);
        workbook.write(out);
        out.close();
    }

    public void setUsuario(Usuario _usuario)
    {
        _usuarioTemp = _usuario;

//        User SuperUsuario
        if (_usuarioTemp.get_access() == 1)
        {
//            _btnGuia.setDisable(true);
//            _btnProcesamiento.setDisable(true);
//            _btnSeguimiento.setDisable(true);
        }
//        User Laboratorio
        if (_usuarioTemp.get_access() == 2)
        {

//            _btnCliente.setDisable(true);
//            _btnSede.setDisable(true);
//            _btnArea.setDisable(true);
//            _btnPoe.setDisable(true);
//            _btnDosimetro.setDisable(true);
        }
//        User Administracion
        if (_usuarioTemp.get_access() == 3)
        {
            _btnGenEtiquetas.setDisable(true);
            _btnGenSobre.setDisable(true);
            _btnCambios.setDisable(true);
        }
    }

    public void ExportToExcel(TableView<Object> tablaExport) throws Exception
    {
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet();

        wb.setSheetName(0, "report_guias");

        Row row = sheet.createRow(0);
        //CellStyle style = wb.createCellStyle();
        //style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
        //style.setFillPattern(FillPatternType.BIG_SPOTS);

        for (int j = 0; j < tablaExport.getColumns().size(); j++)
        {
            row.createCell(j).setCellValue(tablaExport.getColumns().get(j).getText());
            //row.getCell(j).setCellStyle(style);
        }

        for (int i = 0; i < tablaExport.getItems().size(); i++)
        {
            row = sheet.createRow(i + 1);
            for (int j = 0; j < tablaExport.getColumns().size(); j++)
            {
                if (tablaExport.getColumns().get(j).getCellData(i) != null)
                {
                    if (StringUtils.isNumeric(tablaExport.getColumns().get(j).getCellData(i).toString()))
                        row.createCell(j).setCellValue(Integer.parseInt(tablaExport.getColumns().get(j).getCellData(i).toString()));
                    else
                        row.createCell(j).setCellValue(tablaExport.getColumns().get(j).getCellData(i).toString());
                } else
                {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xlsx)", "*.xlsx");
        fileChooser.getExtensionFilters().add(extFilter);

        LocalDateTime _now = LocalDateTime.now();
        DateTimeFormatter _dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd hh_mm_ss a");
        fileChooser.setInitialFileName("guias_export_" + _now.format(_dtf) + ".xlsx");
        File file = fileChooser.showSaveDialog(null);
        FileOutputStream out;

        if (file != null && (out = new FileOutputStream(file)) != null)
        {
            wb.write(out);
            out.close();
        }
    }

    public void OnActionBtnExportarExcel(ActionEvent actionEvent) throws Exception
    {
        ExportToExcel(_tvGuiaAsignados);
    }
}
