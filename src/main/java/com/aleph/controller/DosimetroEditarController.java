package com.aleph.controller;

import com.aleph.model.Dosimetro;
import com.aleph.service.DosimetroService;
import com.aleph.service.IDosimetroService;
import javafx.fxml.FXML;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import javafx.event.ActionEvent;

import javafx.scene.control.ComboBox;

import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class DosimetroEditarController
{
    IDosimetroService _dosimetroService = new DosimetroService();

    @FXML
    private AnchorPane _anchorPaneDosimetroEditar;
    @FXML
    private TextField _txtCodigo;
    @FXML
    private ComboBox _cboxTipo;
    @FXML
    private Button _btnGuardar;
    @FXML
    private Button _btnCancelar;

    //Variables temporales
    Dosimetro _dosimetroTemp = new Dosimetro();

    @FXML
    public void initialize()
    {
        //LlenarComboTipo();
    }

    // Event Listener on Button[#_btnGuardar].onAction
    @FXML
    public void OnActionBtnGuardar(ActionEvent event)
    {
        if (_txtCodigo.getText().isEmpty())
        {
            new AlertBox("El Codigo del dosimetro no debe estar vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else
        {
            try
            {
                _dosimetroTemp.set_dosimetroCodigo(_txtCodigo.getText());
                _dosimetroTemp.set_tipo((String) _cboxTipo.getValue());

                _dosimetroService.updateDosimetro(_dosimetroTemp);

                new AlertBox("Dosimetro actualizado con éxito!!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
                OnActionBtnCancelar(event);
            } catch (RuntimeException e)
            {
                System.err.println("Error en 1: " + e);
            }
        }
    }

    // Event Listener on Button[#_btnCancelar].onAction
    @FXML
    public void OnActionBtnCancelar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneDosimetroEditar.getScene().getWindow();
        _stage.close();
    }

    private void LlenarComboTipo(String _opcion)
    {
        _cboxTipo.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxTipo.getItems().addAll("Cuerpo", "Anillo");
        if (_opcion.equals("Cuerpo"))
        {
            _cboxTipo.getSelectionModel().select(0);
        }
        if (_opcion.equals("Anillo"))
        {
            _cboxTipo.getSelectionModel().select(1);
        }
    }

    public void setDosimetro(Dosimetro _dosimetro)
    {
        _txtCodigo.setText(_dosimetro.get_dosimetroCodigo());
        LlenarComboTipo(_dosimetro.get_tipo());
    }
}
