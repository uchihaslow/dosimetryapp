package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.service.GuiaService;
import com.aleph.service.IGuiaService;
import com.aleph.service.IMagazineService;
import com.aleph.service.MagazineService;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

public class GuiaPorAsignarController
{
    @FXML
    public Label _labCantDosim;

    @FXML
    private AnchorPane _anchorPaneGuiaPorAsignar;

    @FXML
    private TableView _tvGuiaPorAsignar;

    @FXML
    private TableColumn _tcNroGuia;

    @FXML
    private TableColumn _tcNroPedido;

    @FXML
    private TableColumn _tcNombreSede;

    @FXML
    private TableColumn _tcCantidadUsuarios;

    @FXML
    private Button _btnAsignar;

    IMagazineService _magazineService = new MagazineService();
    IGuiaService _guiaService = new GuiaService();

    private ObservableList<ObservableList> _listaGuiaSinAsignar;
    private int _cantDosimetros;

    @FXML
    public void initialize()
    {
        LlenarTvGuiaSinAsignar();
        CantDosim();
    }

    @FXML
    public void OnActionBtnAsignar(ActionEvent event)
    {
        if (Integer.parseInt(((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(3)) <= _cantDosimetros)
        {
            try
            {
                FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._guiaAsignacionPath));
                Parent _guiaAsignacionPageParent = _loader.load();

                GuiaAsignacionController _controller = _loader.getController();

                _controller.setDatosComunesGuia
                        (
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(4),//Cliente
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(2),//Sede
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(5),//Area
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(0),//Guia Nro
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(1),//OrdenPedido
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(3),//Cantidad
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(6),//Direccion
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(7),//Periodo
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(8),//RUC
                                ((ObservableList<String>) _tvGuiaPorAsignar.getSelectionModel().getSelectedItem()).get(9)//Externo
                        );

                Stage _guiaAsignacionModalStage = new Stage();
                Scene _guiaAsignacionPageScene = new Scene(_guiaAsignacionPageParent);
                _guiaAsignacionModalStage.setScene(_guiaAsignacionPageScene);
                _guiaAsignacionModalStage.initModality(Modality.WINDOW_MODAL);
                _guiaAsignacionModalStage.initOwner(_anchorPaneGuiaPorAsignar.getScene().getWindow());
                _guiaAsignacionModalStage.setResizable(false);
                _guiaAsignacionModalStage.setMaximized(false);
                _guiaAsignacionModalStage.showAndWait();
                LlenarTvGuiaSinAsignar();
            } catch (Exception e)
            {
                System.out.println("ERROR OnActionBtnAsignar : " + e);
            }
        } else
        {
            Alert _alert = new Alert(Alert.AlertType.WARNING);
            _alert.setTitle("ADVERTENCIA");
            _alert.setHeaderText("Cantidad de dosimetros insuficiente");
            _alert.setContentText("La cantidad es: " + _cantDosimetros);
            _alert.showAndWait();
        }
    }

    public void LlenarTvGuiaSinAsignar()
    {
        _listaGuiaSinAsignar = FXCollections.observableArrayList(_guiaService.listGuiaSinAsignar());

        _tcNroGuia.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(0).toString())
                );

        _tcNroPedido.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(1).toString())
                );

        _tcNombreSede.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(2).toString())
                );

        _tcCantidadUsuarios.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(3).toString())
                );

        _tvGuiaPorAsignar.setItems(_listaGuiaSinAsignar);

        //_tcCantidadUsuarios.setVisible(false);
    }

    public void CantDosim()
    {
        _cantDosimetros = _magazineService.CantDosimHabilitados();
        _labCantDosim.setText(String.valueOf(_cantDosimetros));
    }
}