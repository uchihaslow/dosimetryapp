package com.aleph.controller;

import com.aleph.service.IReporteService;
import com.aleph.service.ReporteService;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class InformeIsoController
{

    public AnchorPane _anchorPaneInforme;

    public TableView _tvInforme;
    public TableColumn _tcNroGuia;
    public TableColumn _tcCliente;
    public Button _btnPrevisualizar;
    public Button _btnFinalizar;
    public Button _btnImprimir;
    public TextField _txtBuscarInforme;
    public Button _btnBuscar;
    public CheckBox _cboxFinalizado;
    public DatePicker _txtFechaRecepcion;
    public TextField _txtNroContrato;
    public TextField _txtTipoEnsayo;
    public TextField _txtMuestra;
    public DatePicker _txtFechaAnalisis;
    public DatePicker _txtFechaInforme;
    public TextArea _txtObservaciones;

    IReporteService _reporteService = new ReporteService();

    private ObservableList<ObservableList> _listaInforme;


    public void OnActionBtnPrevisualizar(ActionEvent actionEvent) throws JRException
    {
        String _nroInforme = ((ObservableList<String>) _tvInforme.getSelectionModel().getSelectedItem()).get(0);
        String _razonSocial = ((ObservableList<String>) _tvInforme.getSelectionModel().getSelectedItem()).get(1);
        String _direccion = ((ObservableList<String>) _tvInforme.getSelectionModel().getSelectedItem()).get(2);
        String _ordenPedido = (String)((ObservableList<String>) _tvInforme.getSelectionModel().getSelectedItem()).get(3);
        String _nroContrato = _txtNroContrato.getText();
        String _tipoEnsayo = _txtTipoEnsayo.getText();
        String _fechaRecepcion = _txtFechaRecepcion.getValue().toString();
        String _fechaAnalisis = _txtFechaAnalisis.getValue().toString();
        String _muestra = _txtMuestra.getText();
        String _observaciones = _txtObservaciones.getText();
        String _fechaInforme = _txtFechaInforme.getValue().toString();


        final Map<String, Object> _parameters = new HashMap<>();
        _parameters.put("param_nroInforme", _nroInforme);
        _parameters.put("param_razonSocial", _razonSocial);
        _parameters.put("param_direccion", _direccion);
        _parameters.put("param_ordenPedido", _ordenPedido);
        _parameters.put("param_nroContrato", _nroContrato);
        _parameters.put("param_tipoEnsayo", _tipoEnsayo);
        _parameters.put("param_fechaRecepcion", _fechaRecepcion);
        _parameters.put("param_fechaAnalisis", _fechaAnalisis);
        _parameters.put("param_muestra", _muestra);
        _parameters.put("param_observaciones", _observaciones);
        _parameters.put("param_fechaInforme", _fechaInforme);
        toPDF("informe_dosimetria_v3.jrxml", _parameters);
    }

    public void OnActionBtnFinalizar(ActionEvent actionEvent)
    {
    }

    public void OnActionBtnImprimir(ActionEvent actionEvent)
    {
    }

    public void OnActionBtnBuscar(ActionEvent actionEvent)
    {
        System.out.println(_cboxFinalizado.isSelected());
        if (_cboxFinalizado.isSelected())
        {
            _btnImprimir.setDisable(false);
            _btnFinalizar.setDisable(true);
            _btnPrevisualizar.setDisable(true);
            _tvInforme.getItems().clear();
            if (_txtBuscarInforme.getText() == null || _txtBuscarInforme.getText().trim().isEmpty())
            {
                LlenarTvInforme(3);
            } else
            {
                LlenarTvInforme(4);
            }
        } else
        {
            _btnImprimir.setDisable(true);
            _btnFinalizar.setDisable(false);
            _btnPrevisualizar.setDisable(false);
            _tvInforme.getItems().clear();
            if (_txtBuscarInforme.getText() == null || _txtBuscarInforme.getText().trim().isEmpty())
            {
                LlenarTvInforme(1);
            } else
            {
                LlenarTvInforme(2);
            }
        }
    }


    public void LlenarTvInforme(int _opcion)
    {
        switch (_opcion)
        {
            case 1:
                _listaInforme = FXCollections.observableArrayList(_reporteService.listInformeSinFinalizar());
                break;
            case 2:
                _listaInforme = FXCollections.observableArrayList(_reporteService.listInformeSinFinalizarByGuia(Integer.parseInt(_txtBuscarInforme.getText())));

                break;
//            case 3:
//                _listaInforme = null;
//                break;
//            case 4:
//                _listaInforme = null;
//                break;
            default:
                System.out.println("error");
                break;
        }

        _tcNroGuia.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(0).toString())
                );

        _tcCliente.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(1).toString())
                );

        _tvInforme.setItems(_listaInforme);
    }

    public void toPDF(String _reportName, Map<String, Object> _parameters) throws JRException
    {
//        final String _reportSource = getClass().getClassLoader().getResource("reportes/" + _reportName).getPath();

        //agregar esto cuando se va a compilar
        String _projectPath = System.getProperty("user.dir");
        final String _reportSource = (_projectPath + "/plantilla/" + _reportName);

        final JasperDesign _jd = JRXmlLoader.load(_reportSource);
        final JasperReport _report = JasperCompileManager.compileReport(_jd);
        final JasperPrint _print = JasperFillManager.fillReport(_report, _parameters, new JREmptyDataSource());
        final String _reporTarget = _reportSource.substring(0, _reportSource.lastIndexOf('/'))
                .concat("/")
                .concat(_reportName)
                .concat(".pdf");
        System.out.println("Ruta del archivo: " + _reporTarget);

        JasperExportManager.exportReportToPdfFile(_print, _reporTarget);
        JasperViewer.viewReport(_print, false);
        File _fichero = new File(_reporTarget);

        if (_fichero.delete())
        {
            System.out.println("El fichero ha sido borrado satisfactoriamente");
        } else
        {
            System.out.println("El fichero no puede ser borrado");
        }
    }
}
