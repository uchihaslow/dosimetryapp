package com.aleph.controller;

import com.aleph.model.Area;
import com.aleph.model.Paths;
import com.aleph.model.Poe;
import com.aleph.model.Sede;
import com.aleph.service.*;
import com.aleph.tools.ComboBoxAutoComplete;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Callback;

import java.io.IOException;
import java.util.ArrayList;

public class PoeAsignacion
{
    public AnchorPane _anchorPanePoeAsignacion;
    public Button _btnBuscarCliente;
    public Label _labRuc;
    public Label _labCliente;
    public ComboBox _cboxSede;
    public TableView _tvAreaPoe;
    public Button _btnAgregarArea;
    public Button _btnEliminarArea;
    public Button _btnAsignar;
    public ComboBox _cboxArea;
    public Button _btnBuscarPoe;
    public Label _labDni;
    public Label _labPoe;
    public TableColumn _tcAreaId;
    public TableColumn _tcCliente;
    public TableColumn _tcSede;
    public TableColumn _tcArea;
    public TitledPane _titlePaneSede;
    public TitledPane _titlePaneArea;

    //variable auxiliar
    private int _clienteIdRecibido;
    private ISedeService _sedeService = new SedeService();
    private IAreaService _areaService = new AreaService();
    private IPoeService _poeService = new PoeService();


    ObservableList<Sede> _sedeList;
    private ObservableList<ObservableList> _poeAreaList;

    Poe _poe;

    @FXML
    public void initialize()
    {
        //_cboxSede.setTooltip(new Tooltip());
        //new ComboBoxAutoComplete<String>(_cboxSede);
    }

    public void OnActionBtnBuscarCliente(ActionEvent actionEvent)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._poeBuscarClientePath));
            Stage _stage = new Stage();
            _stage.initOwner(_btnBuscarCliente.getScene().getWindow());
            _stage.setScene(new Scene((Parent) _loader.load()));

            _stage.initModality(Modality.WINDOW_MODAL);
            _stage.showAndWait();

            PoeBuscarClienteController _controller = _loader.getController();
            _labRuc.setText(_controller.getRucSend());
            _labCliente.setText(_controller.getRazonSocialSend());
            _clienteIdRecibido = _controller.getIdCliente();

            filtrarCBoxSedes(_clienteIdRecibido);
            Double positionX = _stage.getX() + _titlePaneSede.getBoundsInParent().getMinX();
            Double positionY = _stage.getY() + _titlePaneSede.getBoundsInParent().getMinY();
            new ComboBoxAutoComplete<String>(_cboxSede, positionX, positionY);
            _cboxSede.setDisable(false);
        } catch (IOException e)
        {
            System.out.println("CLASE Area Metodo OnActionBtnBuscarCliente" + e.getMessage());
        }
    }

    public void OnActionCboxSede(ActionEvent actionEvent)
    {
        filtrarCBoxArea();
        Window _stage = _cboxArea.getScene().getWindow();
        Double positionX = _stage.getX() + _titlePaneArea.getBoundsInParent().getMinX();
        Double positionY = _stage.getY() + _titlePaneArea.getBoundsInParent().getMinY();

        new ComboBoxAutoComplete<String>(_cboxArea, positionX, positionY);
        _cboxArea.setDisable(false);
    }

    public void OnActionBtnAgregarArea(ActionEvent actionEvent)
    {
        try
        {
            String _areaId = String.valueOf(((Area) _cboxArea.getSelectionModel().getSelectedItem()).get_areaId());
            String _clienteNom = _labCliente.getText();
            String _sedeNom = ((Sede) _cboxSede.getSelectionModel().getSelectedItem()).get_nombre();
            String _areaNom = ((Area) _cboxArea.getSelectionModel().getSelectedItem()).get_nombre();

            ObservableList<String> _newRow = FXCollections.observableArrayList();
            _newRow.add(_areaId);
            _newRow.add(_clienteNom);
            _newRow.add(_sedeNom);
            _newRow.add(_areaNom);
            //ArrayList<ObservableList<String>> xd = new ArrayList();
            //xd.add(_newRow);

            boolean _repetido = false;
            for (int i = 0; i < _poeAreaList.size(); i++)
            {
                if ((((ObservableList<String>) (_poeAreaList.get(i))).get(0)).equals(_areaId))
                {
                    _repetido = true;
                    break;
                } else
                {
                    _repetido = false;
                }
                //System.out.println(((ObservableList<String>)(_poeAreaList.get(i))).get(0));
            }

            if (_repetido == false)
                _poeAreaList.add(_newRow);

        } catch (Exception e)
        {
            System.out.println("error: " + e);
        }
    }

    public void OnActionBtnEliminarArea(ActionEvent actionEvent)
    {
        try
        {
            System.out.println(((ObservableList<String>) _tvAreaPoe.getSelectionModel().getSelectedItem()).get(0));
            String _areaId = ((ObservableList<String>) _tvAreaPoe.getSelectionModel().getSelectedItem()).get(0);

            for (int i = 0; i < _poeAreaList.size(); i++)
            {
                if ((((ObservableList<String>) (_poeAreaList.get(i))).get(0)).equals(_areaId))
                {
                    _poeAreaList.remove(i);
                }
            }
        } catch (Exception e)
        {
            System.out.println("error: " + e);
        }

    }

    public void OnActionBtnAsignar(ActionEvent actionEvent)
    {
        ArrayList _poeAreaBaseList = _areaService.findAreaByPoe(_poe.get_poeId());

        for (int i = 0; i < _poeAreaList.size(); i++)
        {
            boolean _encontrado = false;
            int _areaIdBase;
            int _areaId = _areaId = Integer.parseInt(String.valueOf(_poeAreaList.get(i).get(0)));

            for (int j = 0; j < _poeAreaBaseList.size(); j++)
            {
                _areaIdBase = Integer.parseInt(String.valueOf(((ObservableList<ObservableList>) _poeAreaBaseList.get(j)).get(0)));

                if (_areaId == _areaIdBase)
                {
                    System.out.println(_poeAreaList.get(i).get(0) + " : " + _areaIdBase);
                    System.out.println(true);
                    _encontrado = true;
                    break;
                } else
                {
                    System.out.println(_poeAreaList.get(i).get(0) + " : " + _areaIdBase);
                    System.out.println(false);
                    _encontrado = false;
                }
            }
            if (_encontrado == false)
            {
                if (_areaService.findAreaByPoeExist(_areaId, _poe.get_poeId()))
                {
                    _areaService.updateAreaPoeHabilitar(_areaId, _poe.get_poeId());
                    System.out.println("habilitado :" + _areaId + " - " + _poe.get_poeId());
                } else
                {
                    _areaService.addAreaByPoe(_areaId, _poe.get_poeId());
                    System.out.println("agregado :" + _areaId + " - " + _poe.get_poeId());
                }

            }
        }

        System.out.println("PARTE 2");

        for (int i = 0; i < _poeAreaBaseList.size(); i++)
        {
            boolean _encontrado = false;
            int _areaIdBase = Integer.parseInt(String.valueOf(((ObservableList<ObservableList>) _poeAreaBaseList.get(i)).get(0)));
            int _areaId;
            for (int j = 0; j < _poeAreaList.size(); j++)
            {
                _areaId = Integer.parseInt(String.valueOf(_poeAreaList.get(j).get(0)));

                if (_areaId == _areaIdBase)
                {
                    System.out.println(_poeAreaList.get(j).get(0) + " : " + _areaIdBase);
                    System.out.println(true);
                    _encontrado = true;
                    break;
                } else
                {
                    System.out.println(_poeAreaList.get(j).get(0) + " : " + _areaIdBase);
                    System.out.println(false);
                    _encontrado = false;
                }
            }
            if (_encontrado == false)
            {
                _areaService.updateAreaPoeDeshabilitar(_areaIdBase, _poe.get_poeId());
                System.out.println("eliminado :" + _areaIdBase + " - " + _poe.get_poeId());
            }
        }
        new AlertBox("Usuario Actualizado", "Concluido", new Alert(Alert.AlertType.INFORMATION));
    }

    public void OnActionCboxArea(ActionEvent actionEvent)
    {
    }

    public void OnActionBtnBuscarPoe(ActionEvent actionEvent)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._poeBuscarPoePath));
            Stage _stage = new Stage();
            _stage.initOwner(_btnBuscarPoe.getScene().getWindow());
            _stage.setScene(new Scene((Parent) _loader.load()));

            _stage.initModality(Modality.WINDOW_MODAL);
            _stage.showAndWait();

            PoeBuscarPoeController _controller = _loader.getController();
            _poe = _controller.get_poe();
            _labDni.setText(_poe.get_dni());
            _labPoe.setText(_poe.get_nombres() + ", " + _poe.get_apellidos());

            filtrarTVAreaPoe(_poe.get_poeId());
        } catch (IOException e)
        {
            System.out.println("CLASE Area Metodo OnActionBtnBuscarPoe" + e.getMessage());
        }
    }

    private void filtrarCBoxSedes(int _clienteId)
    {
        _cboxSede.getItems().clear();
        _sedeList = FXCollections.observableArrayList(_sedeService.findSedeByCliente(_clienteId));
        _cboxSede.getItems().addAll(_sedeList);
    }

    private void filtrarCBoxArea()
    {
        _cboxArea.getItems().clear();
        ObservableList<Area> _areaList = FXCollections.observableArrayList(_areaService.findAreaBySede(((Sede) _cboxSede.getSelectionModel().getSelectedItem()).get_sedeId()));
        _cboxArea.getItems().addAll(_areaList);
    }

    private void filtrarTVAreaPoe(int _poeId)
    {
        _tvAreaPoe.getItems().clear();
        _poeAreaList = FXCollections.observableArrayList(_areaService.findAreaByPoe(_poeId));

        _tcAreaId.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(0).toString())
                );
        _tcCliente.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(1).toString())
                );
        _tcSede.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(2).toString())
                );
        _tcArea.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(3).toString())
                );

        _tvAreaPoe.setItems(_poeAreaList);
    }
}