package com.aleph.controller;

import com.aleph.model.Paths;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class PoeController
{
    @FXML
    public Button _btnAgregar;
    @FXML
    public Button _btnAsignacion;
    @FXML
    public Button _btnBuscar;
    @FXML
    public AnchorPane _anchorPanePoe;

    @FXML
    public void OnActionBtnAgregar(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._poeAgregarPath));
        Parent _poeAgregarPageParent = _loader.load();

        Stage _poeAgregarModalStage = new Stage();
        Scene _poeAgregarPageScene = new Scene(_poeAgregarPageParent);
        _poeAgregarModalStage.setScene(_poeAgregarPageScene);
        _poeAgregarModalStage.initModality(Modality.NONE);
        _poeAgregarModalStage.initOwner(_anchorPanePoe.getScene().getWindow());
        _poeAgregarModalStage.setResizable(false);
        _poeAgregarModalStage.setMaximized(false);
        _poeAgregarModalStage.show();
    }

    @FXML
    public void OnActionBtnAsignacion(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._poeAsignacionPath));
        Parent _poeAsignacionPageParent = _loader.load();

        Stage _poeAsignacionModalStage = new Stage();
        Scene _poeAsignacionPageScene = new Scene(_poeAsignacionPageParent);
        _poeAsignacionModalStage.setScene(_poeAsignacionPageScene);
        _poeAsignacionModalStage.initModality(Modality.NONE);
        _poeAsignacionModalStage.initOwner(_anchorPanePoe.getScene().getWindow());
        _poeAsignacionModalStage.setResizable(false);
        _poeAsignacionModalStage.setMaximized(false);
        _poeAsignacionModalStage.show();
    }

    @FXML
    public void OnActionBtnBuscar(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._poeBuscarPath));
        Parent _poeBuscarPageParent = _loader.load();

        Stage _poeBuscarModalStage = new Stage();
        Scene _poeBuscarPageScene = new Scene(_poeBuscarPageParent);
        _poeBuscarModalStage.setScene(_poeBuscarPageScene);
        _poeBuscarModalStage.initModality(Modality.NONE);
        _poeBuscarModalStage.initOwner(_anchorPanePoe.getScene().getWindow());
        _poeBuscarModalStage.setResizable(false);
        _poeBuscarModalStage.setMaximized(false);
        _poeBuscarModalStage.show();
    }
}
