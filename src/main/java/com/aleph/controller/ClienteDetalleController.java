package com.aleph.controller;

import com.aleph.model.Cliente;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ClienteDetalleController
{
    @FXML
    private AnchorPane _anchorPaneDetalleCliente;
    @FXML
    private TextField _txtRuc;
    @FXML
    private TextField _txtRazonSocial;
    @FXML
    private TextField _txtDireccion;
    @FXML
    private TextField _txtTelefono;
    @FXML
    private TextField _txtEmail;
    @FXML
    private ComboBox _cboxPrioridad;

    @FXML
    public void OnActionBtnAceptar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneDetalleCliente.getScene().getWindow();
        _stage.close();
    }

    private void LlenarComboPrioridad(String _opcion)
    {
        _cboxPrioridad.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxPrioridad.getItems().addAll("Alta", "Media", "Baja");
        if (_opcion.equals("Alta"))
        {
            _cboxPrioridad.getSelectionModel().select(0);
        } else if (_opcion.equals("Media"))
        {
            _cboxPrioridad.getSelectionModel().select(1);
        } else if (_opcion.equals("Baja"))
        {
            _cboxPrioridad.getSelectionModel().select(2);
        }
    }

    public void setCliente(Cliente _cliente)
    {
        _txtRuc.setText(_cliente.get_ruc());
        _txtRazonSocial.setText(_cliente.get_razonSocial());
        _txtDireccion.setText(_cliente.get_direccion());
        _txtTelefono.setText(_cliente.get_telefono());
        _txtEmail.setText(_cliente.get_email());
        LlenarComboPrioridad(_cliente.get_prioridad());
    }
}
