package com.aleph.controller;

import com.aleph.service.IPrestamoService;
import com.aleph.service.PrestamoService;
import javafx.animation.PauseTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class RecibirController
{
    public TextField _txtNroDosimetro;
    public Label _labEstado;
    public AnchorPane _anchorPaneRecepcionDosim;
    public Label _labTitulo;
    public Text _txtContador;

    IPrestamoService _prestamoService = new PrestamoService();
    int contador = 0;

    @FXML
    public void initialize()
    {
        _labEstado.setStyle("-fx-border-color:dimgray; -fx-background-color: dimgray; -fx-text-fill: white; ");
        _txtContador.setText(String.valueOf(contador));

        PauseTransition pause = new PauseTransition(Duration.millis(250));

        _txtNroDosimetro.textProperty().addListener((observable, oldValue, newValue) ->
        {
            pause.setOnFinished(event -> recibirDosimetro(newValue));
            pause.playFromStart();
        });
    }

    public void recibirDosimetro(String _valorIngresado)
    {
        int _existe;
        _existe = _prestamoService.findPrestamoEnviadoExist2(agregarCeros(_valorIngresado, 7));

        if (_valorIngresado.isEmpty())
        {
            _txtNroDosimetro.setDisable(false);
            System.out.println(_txtNroDosimetro.isDisable());
            _labEstado.setText("Ingrese el id del dosímetro");
            _labEstado.setStyle
                    (
                            "-fx-border-color:dimgray; " +
                                    "-fx-background-color: dimgray; " +
                                    "-fx-text-fill:white;"
                    );
            System.out.println("El campo está vacío");
        } else if (_existe == 1)
        {
            _txtNroDosimetro.setDisable(true);
            _prestamoService.changeEstadoRecibido(agregarCeros(_valorIngresado, 7));
            _labEstado.setText("Se ingresó correctamente");
            _labEstado.setStyle
                    (
                            "-fx-border-color:black; " +
                                    "-fx-background-color: forestgreen; " +
                                    "-fx-text-fill:white;"
                    );
            System.out.println("el valor se ingreso correctamente");
            contador++;
            _txtContador.setText(String.valueOf(contador));
        } else if (_existe == 0)
        {
            _txtNroDosimetro.setDisable(true);
            _labEstado.setText("Ya fue ingresado o no fué enviado");
            _labEstado.setStyle
                    (
                            "-fx-border-color:darkred; " +
                                    "-fx-background-color: darkred; " +
                                    "-fx-text-fill:white;"
                    );
            System.out.println("Ya fué ingresado o no fué enviado");
        } else if (_existe > 1)
        {
            _txtNroDosimetro.setDisable(true);
            _labEstado.setText("DOSIMETRO DUPLICADO");
            _labEstado.setStyle
                    (
                            "-fx-border-color:#cccc00; " +
                                    "-fx-background-color: #cccc00; " +
                                    "-fx-text-fill:white;"
                    );
            System.out.println("DOSIMETRO DUPLICADO");
        } else
        {
            _txtNroDosimetro.setDisable(true);
            _txtNroDosimetro.setDisable(true);
            _labEstado.setText("ERROR DE CONEXION");
            _labEstado.setStyle
                    (
                            "-fx-border-color:#5200cc; " +
                                    "-fx-background-color: #5200cc; " +
                                    "-fx-text-fill:white;"
                    );
            System.out.println("ERROR DE CONEXION");
        }

        PauseTransition pause = new PauseTransition(Duration.millis(500));
        pause.setOnFinished(event -> _txtNroDosimetro.clear());
        pause.playFromStart();
    }

    private static String agregarCeros(String _numero, int _longitud)
    {
        String ceros = "";

        int cantidad = _longitud - _numero.length();

        if (cantidad >= 1)
        {
            for (int i = 0; i < cantidad; i++)
            {
                ceros += "0";
            }

            return (ceros + _numero);
        } else
        {
            return _numero;
        }
    }

}
