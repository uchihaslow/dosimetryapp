package com.aleph.controller;

import com.aleph.model.Anneal;
import com.aleph.model.AnnealReader;
import com.aleph.model.Lectura;
import com.aleph.model.LecturaReader;
import com.aleph.service.IMagazineService;
import com.aleph.service.MagazineService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class AnnealController
{
    //region Controles
    @FXML
    public AnchorPane _anchorPaneAnneal;
    @FXML
    private TableView<Anneal> _tvAnnealAceptados;
    @FXML
    private TableView<Anneal> _tvAnnealDenegados;
    @FXML
    private Button _btnIzquierdaUno;
    @FXML
    private Button _btnDerechaTodos;
    @FXML
    private Button _btnIzquierdaTodos;
    @FXML
    private Button _btnDerechaUno;
    @FXML
    private TextField _txtFileAnneal;
    @FXML
    private TextField _txtFileReport;
    @FXML
    private Button _btnExaminarReport;
    @FXML
    private Button _btnExaminarAnneal;
    @FXML
    private Button _btnAgregarPersona;

    //endregion

    IMagazineService _magazineService = new MagazineService();

    ObservableList<Anneal> _annealAceptadoList = FXCollections.observableArrayList();
    ObservableList<Anneal> _annealDenegadoList = FXCollections.observableArrayList();
    File _fileAnneal;
    File _fileReport;

    @FXML
    public void initialize()
    {
//        LlenarTabla();
//        _tvAnnealAceptados.getColumns().get(0).setVisible(false);
//        _tvAnnealAceptados.getColumns().get(0).setVisible(true);
    }

    @FXML
    public void OnActionBtnIzquierdaUno(ActionEvent event)
    {
        Anneal _anneal = _tvAnnealDenegados.getSelectionModel().getSelectedItem();
        if (_anneal != null)
        {
            _annealAceptadoList.add(_anneal);
            _tvAnnealDenegados.getItems().remove(_anneal);
        }
        _tvAnnealAceptados.getSortOrder().add(_tvAnnealAceptados.getColumns().get(0));
        _tvAnnealDenegados.getSortOrder().add(_tvAnnealDenegados.getColumns().get(0));
    }

    @FXML
    public void OnActionBtnDerechaUno(ActionEvent event)
    {

        Anneal _anneal = _tvAnnealAceptados.getSelectionModel().getSelectedItem();
        if (_anneal != null)
        {
            _annealDenegadoList.add(_anneal);
            _tvAnnealAceptados.getItems().remove(_anneal);
        }
        _tvAnnealAceptados.getSortOrder().add(_tvAnnealAceptados.getColumns().get(0));
        _tvAnnealDenegados.getSortOrder().add(_tvAnnealDenegados.getColumns().get(0));
    }

    @FXML
    public void OnActionBtnDerechaTodos(ActionEvent event)
    {
        ObservableList<Anneal> _ListaTransfer = _tvAnnealAceptados.getItems();
        ArrayList<Anneal> _listaDelete = new ArrayList<>();
        for (Anneal _dato : _ListaTransfer)
        {
            if (_dato.get_e1() >= 10.0 || _dato.get_e2() >= 10.0 || _dato.get_e3() >= 5.0 || _dato.get_e4() >= 5.0)
            {
                _annealDenegadoList.add(_dato);
//                _tvAnnealAceptados.getItems().remove(_dato);
                _listaDelete.add(_dato);
            }
        }
        _tvAnnealAceptados.getItems().removeAll(_listaDelete);
        //_tvAnnealAceptados.getItems().removeAll(_ListaTransfer);
        _tvAnnealAceptados.getSortOrder().add(_tvAnnealAceptados.getColumns().get(0));
        _tvAnnealDenegados.getSortOrder().add(_tvAnnealDenegados.getColumns().get(0));
    }

    @FXML
    public void OnActionBtnIzquierdaTodos(ActionEvent event)
    {
        ObservableList<Anneal> _ListaTransfer = _tvAnnealDenegados.getItems();
        for (Anneal _dato : _ListaTransfer)
        {
            _annealAceptadoList.add(_dato);
        }
        _tvAnnealDenegados.getItems().removeAll(_ListaTransfer);
        _tvAnnealAceptados.getSortOrder().add(_tvAnnealAceptados.getColumns().get(0));
        _tvAnnealDenegados.getSortOrder().add(_tvAnnealDenegados.getColumns().get(0));
    }

    @FXML
    public void OnActionBtnExportarExcel(ActionEvent event) throws IOException
    {
//        System.out.println("ANNEAL APROBADOS");
//
//        for (Anneal _dato : _annealAceptadoList)
//        {
//            System.out.println(
//                    _dato.get_indice() + "    "
//                            + _dato.get_badgeId() + "    "
//                            + _dato.get_e1() + "    "
//                            + _dato.get_e2() + "    "
//                            + _dato.get_e3() + "    "
//                            + _dato.get_e4() + "    "
//                            + _dato.get_reason()
//            );
////            System.out.println(_dato.get_reason());
//        }
//
//        System.out.println();
//        System.out.println("ANNEAL DENEGADOS");
//        for (Anneal _dato : _annealDenegadoList)
//        {
//            System.out.println(
//                    _dato.get_indice() + "    "
//                            + _dato.get_badgeId() + "    "
//                            + _dato.get_e1() + "    "
//                            + _dato.get_e2() + "    "
//                            + _dato.get_e3() + "    "
//                            + _dato.get_e4() + "    "
//                            + _dato.get_reason()
//            );
//        }
//
////        System.out.println("Persona Elegida");
////        System.out.println(_annealAceptadoList.get(1).get_nombre());

        //Create blank workbook


        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet spreadsheet = workbook.createSheet("Anneal Ordenado");

        //Create row object
        XSSFRow row;


//        Map<String, Object[]> _annealInfo = new TreeMap<String, Object[]>();
        Map<Integer, Object[]> _annealInfo = new TreeMap<Integer, Object[]>();

        int contador = 1;
        for (Anneal _dato : _tvAnnealAceptados.getItems())
        {
            _annealInfo.put
                    (
                            contador,
                            new Object[]
                                    {
                                            _dato.get_badgeId(),
                                            Double.toString(_dato.get_e1()),
                                            Double.toString(_dato.get_e2()),
                                            Double.toString(_dato.get_e3()),
                                            Double.toString(_dato.get_e4()),
                                            _dato.get_reason()
                                    }
                    );
            contador++;
        }

        Set<Integer> keyid = _annealInfo.keySet();

        int rowid = 0;

        for (Integer key : keyid)
        {
            row = spreadsheet.createRow(rowid++);
            Object[] objectArr = _annealInfo.get(key);
            int cellid = 0;

            for (Object obj : objectArr)
            {
                Cell cell = row.createCell(cellid++);
                cell.setCellValue(String.valueOf(obj));
            }
        }


        FileChooser fileChooser = new FileChooser();

        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xlsx)", "*.xlsx");
        fileChooser.getExtensionFilters().add(extFilter);

        LocalDateTime _now = LocalDateTime.now();
        DateTimeFormatter _dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd hh_mm_ss a");
        fileChooser.setInitialFileName("Anneal_" + _now.format(_dtf));

        File file = fileChooser.showSaveDialog(null);

        //Write the workbook in file system
//        FileOutputStream out = new FileOutputStream(new File("Writesheet.xlsx"));
        FileOutputStream out = new FileOutputStream(file);
        workbook.write(out);
        out.close();
        //System.out.println("Writesheet.xlsx written successfully");


//        int x;
//        int y;
//
//        for (int i = 0; i < _annealAceptadoList.size(); i++)
//        {
//            row = spreadsheet.createRow(i);
//
//
//            for (int j = 0; j < 6; j++)
//            {
////                Cell _cell = row.createCell();
//            }
//        }

    }

    @FXML
    public void OnActionBtnExaminarAnneal(ActionEvent actionEvent)
    {
        try
        {
            FileChooser _fc = new FileChooser();
            _fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Archivos PRN", "*.prn"));
            _fileAnneal = _fc.showOpenDialog(null);
            _txtFileAnneal.setText(_fileAnneal.getName());

            if (_fileAnneal != null && _fileReport != null)
            {
                _tvAnnealAceptados.getItems().clear();
                _tvAnnealDenegados.getItems().clear();
                ArrayList<Anneal> _listaTemp = OrdenarAnneal(AnnealReader.ReadAnneal(_fileAnneal), LecturaReader.readReport2(_fileReport));
                LlenarTabla(_listaTemp);
            } else
            {

            }
        } catch (Exception e)
        {
            System.err.println(e);
        }

    }

    @FXML
    public void OnActionBtnExaminarReport(ActionEvent event)
    {
        try
        {
            FileChooser _fc = new FileChooser();
            _fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Archivos PRN", "*.prn"));
            _fileReport = _fc.showOpenDialog(null);
            _txtFileReport.setText(_fileReport.getName());

            if (_fileAnneal != null && _fileReport != null)
            {
                _tvAnnealAceptados.getItems().clear();
                _tvAnnealDenegados.getItems().clear();
                ArrayList<Anneal> _listaTemp = OrdenarAnneal(AnnealReader.ReadAnneal(_fileAnneal), LecturaReader.readReport2(_fileReport));
                LlenarTabla(_listaTemp);
            } else
            {

            }
        } catch (Exception e)
        {
            System.err.println(e);
        }

    }

    @FXML
    public void OnActionBtnSincronizar(ActionEvent event)
    {
        ArrayList<Anneal> _annealRepetidosList = new ArrayList<>();
        boolean _existe;

        for (Anneal _dato : _annealAceptadoList)
        {
            _existe = _magazineService.findDosimetroExist(_dato.get_badgeId());
            if (_existe == false)
            {
                System.out.print(_dato.get_badgeId());
                System.out.print("    ");
                System.out.println(_dato.get_reason());
                _magazineService.addDosimetroLimpio(_dato.get_badgeId());
            } else
            {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("El dosimetro ya fué ingresado");
                alert.setHeaderText("el dosimetro es:");
                alert.setContentText("Indice: " + _dato.get_indice() + " Código: " + _dato.get_badgeId());

                alert.showAndWait();
            }

        }

        Alert _alert = new Alert(Alert.AlertType.INFORMATION);
        _alert.setTitle("Cuadro informativo");
        _alert.setHeaderText("¡Se ha registrado el anneal con exito!");
        _alert.showAndWait();


//        Alert alert = new Alert(Alert.AlertType.WARNING);
//        alert.setTitle("Exception Dialog");
//        alert.setHeaderText("Algunos dosimetros se repetieron");
//        alert.setContentText("Aquí mostramos la lista:");
//        alert.setWidth(300);
//        alert.setHeight(600);
//
//        alert.showAndWait();


        Stage _stage = (Stage) _anchorPaneAnneal.getScene().getWindow();
        _stage.close();
    }

    private ArrayList<Anneal> OrdenarAnneal(ArrayList<Anneal> _lista, ArrayList<Lectura> _listaOrdenada)
    {
        for (Anneal _datoAnneal : _lista)
        {
            _datoAnneal.set_indice(0);
            for (Lectura _datoLectura : _listaOrdenada)
            {
                if (_datoAnneal.get_badgeId().compareTo(_datoLectura.get_badgeId()) == 0)
                {
                    _datoAnneal.set_indice(_datoLectura.get_indice());
                } else
                {

                }
            }
        }
        return _lista;
    }

    private void LlenarTabla(ArrayList<Anneal> _lista)
    {
        _annealAceptadoList = FXCollections.observableArrayList(_lista);

        _tvAnnealAceptados.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_indice"));
        _tvAnnealAceptados.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_badgeId"));
        _tvAnnealAceptados.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_e1"));
        _tvAnnealAceptados.getColumns().get(3).setCellValueFactory(new PropertyValueFactory("_e2"));
        _tvAnnealAceptados.getColumns().get(4).setCellValueFactory(new PropertyValueFactory("_e3"));
        _tvAnnealAceptados.getColumns().get(5).setCellValueFactory(new PropertyValueFactory("_e4"));
        _tvAnnealAceptados.getColumns().get(6).setCellValueFactory(new PropertyValueFactory("_reason"));
        _tvAnnealAceptados.setItems(_annealAceptadoList);

        _tvAnnealDenegados.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_indice"));
        _tvAnnealDenegados.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_badgeId"));
        _tvAnnealDenegados.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_e1"));
        _tvAnnealDenegados.getColumns().get(3).setCellValueFactory(new PropertyValueFactory("_e2"));
        _tvAnnealDenegados.getColumns().get(4).setCellValueFactory(new PropertyValueFactory("_e3"));
        _tvAnnealDenegados.getColumns().get(5).setCellValueFactory(new PropertyValueFactory("_e4"));
        _tvAnnealDenegados.getColumns().get(6).setCellValueFactory(new PropertyValueFactory("_reason"));
        _tvAnnealDenegados.setItems(_annealDenegadoList);

        //region SetRowFactory
        _tvAnnealAceptados.setRowFactory(row -> new TableRow<Anneal>()
        {
            @Override
            protected void updateItem(Anneal item, boolean empty)
            {
                super.updateItem(item, empty);
                if (item == null || empty)
                {
                    setStyle("");
                } else
                {
                    if (item.get_e1() >= 10.0 || item.get_e2() >= 10.0 || item.get_e3() >= 5.0 || item.get_e4() >= 5.0 )
                    {
                        for (int i = 0; i < getChildren().size(); i++)
                        {
                            ((Labeled) getChildren().get(i)).setTextFill(Color.RED);
                        }
                    } else
                    {
                        if (getTableView().getSelectionModel().getSelectedItems().contains(item))
                        {
                            for (int i = 0; i < getChildren().size(); i++)
                            {
                                ((Labeled) getChildren().get(i)).setTextFill(Color.BLACK);
                            }
                        } else
                        {
                            for (int i = 0; i < getChildren().size(); i++)
                            {
                                ((Labeled) getChildren().get(i)).setTextFill(Color.BLACK);
                            }
                        }
                    }
                }
            }
        });

        _tvAnnealDenegados.setRowFactory(row -> new TableRow<Anneal>()
        {
            @Override
            protected void updateItem(Anneal item, boolean empty)
            {
                super.updateItem(item, empty);
                if (item == null || empty)
                {
                    setStyle("");
                } else
                {
                    if (item.get_e1() >= 10.0 || item.get_e2() >= 10.0 || item.get_e3() >= 5.0 || item.get_e4() >= 5.0 )
                    {
                        for (int i = 0; i < getChildren().size(); i++)
                        {
                            ((Labeled) getChildren().get(i)).setTextFill(Color.RED);
                        }
                    } else
                    {
                        if (getTableView().getSelectionModel().getSelectedItems().contains(item))
                        {
                            for (int i = 0; i < getChildren().size(); i++)
                            {
                                ((Labeled) getChildren().get(i)).setTextFill(Color.BLACK);
                            }
                        } else
                        {
                            for (int i = 0; i < getChildren().size(); i++)
                            {
                                ((Labeled) getChildren().get(i)).setTextFill(Color.BLACK);
                            }
                        }
                    }
                }
            }
        });
        //endregion

        _tvAnnealAceptados.getSortOrder().add(_tvAnnealAceptados.getColumns().get(0));
        _tvAnnealDenegados.getSortOrder().add(_tvAnnealDenegados.getColumns().get(0));


    }


}