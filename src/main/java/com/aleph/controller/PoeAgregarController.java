package com.aleph.controller;

import com.aleph.model.Poe;
import com.aleph.service.IPoeService;
import com.aleph.service.PoeService;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.commons.text.WordUtils;

public class PoeAgregarController
{
    @FXML
    public AnchorPane _anchorPanePoeAgregar;
    @FXML
    public Button _btnGuardar;
    @FXML
    public Button _btnCancelar;
    @FXML
    public TextField _txtDni;
    @FXML
    public TextField _txtNombres;
    @FXML
    public TextField _txtApellidos;
    @FXML
    public RadioButton _rbMasculino;
    @FXML
    public RadioButton _rbFemenino;
    @FXML
    public RadioButton _rbOtro;
    @FXML
    public Label _labDni;
    @FXML
    public Label _labNombres;
    @FXML
    public Label _labApellidos;

    IPoeService _poeSvc = new PoeService();

    @FXML
    public void initialize()
    {
        _txtDni.textProperty().addListener(new ChangeListener<String>()
        {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue)
            {
                if (!newValue.matches("\\d*"))
                {
                    _txtDni.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        _txtNombres.textProperty().addListener(new ChangeListener<String>()
        {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue)
            {
                try
                {
                    String texto = WordUtils.capitalizeFully(newValue);
                    if (!texto.matches("[a-zA-z]+[  ]+[a-zA-Z]+"))
                    {
                        texto = texto.replaceAll("^\\s+", "");
                        _txtNombres.setText(texto.replaceAll("\\s+", " "));
                    }
                } catch (Exception e)
                {
                    System.out.println("error: " + e);
                }
            }
        });

        _txtNombres.textProperty().addListener(new ChangeListener<String>()
        {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue)
            {
                if (!newValue.isEmpty())
                {
                    _txtNombres.setText(WordUtils.capitalizeFully(newValue));
                }
            }
        });

        _txtApellidos.setTextFormatter(new TextFormatter<>((change) ->
        {
            change.setText(change.getText().toUpperCase());
            return change;
        }));

        _txtApellidos.textProperty().addListener(new ChangeListener<String>()
        {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue)
            {
                try
                {
                    String texto = WordUtils.capitalizeFully(newValue);
                    //String texto = newValue;
//                if (!newValue.matches("[a-zA-z]+[  ]+[a-zA-Z]+"))
                    if (!texto.matches("[a-zA-z]+[  ]+[a-zA-Z]+"))
                    {
                        texto = texto.replaceAll("^\\s+", "");
//                     texto = newValue.replaceAll("^\\s+", "");
                        //texto = texto.replaceAll("\\s$","");
                        _txtApellidos.setText(texto.replaceAll("\\s+", " "));
//                    _txtNombres.setText(newValue.replaceAll());
//                    _txtNombres.setText(WordUtils.capitalizeFully(newValue));
                    }
//                String Texto = StringUtils.normalizeSpace(newValue);
//                _txtNombres.setText(WordUtils.capitalizeFully(newValue));
//                    if (!texto.isEmpty())
//                    {
//                        _txtApellidos.setText(WordUtils.capitalizeFully(texto));
//                    }
                } catch (Exception e)
                {
                    System.out.println("error: " + e);
                }
            }
        });
    }

    @FXML
    public void OnActionBtnGuardar(ActionEvent actionEvent)
    {
        if (!verificarCampos())
        {
            Poe _poe = new Poe();
            _poe.set_dni(_txtDni.getText());
            _poe.set_nombres(_txtNombres.getText().trim());
            _poe.set_apellidos(_txtApellidos.getText().toUpperCase());
            if (_rbMasculino.isSelected())
            {
                _poe.set_sexo('M');
            } else if (_rbFemenino.isSelected())
            {
                _poe.set_sexo('F');
            } else
            {
                _poe.set_sexo('O');
            }

            System.out.println(_poe.get_dni());
            System.out.println(_poe.get_nombres());
            System.out.println(_poe.get_apellidos());
            System.out.println(_poe.get_sexo());

            _poeSvc.addPoe(_poe);

            new AlertBox("Usuario registrado con éxito!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
            limpiarCampos();


            //Habilitar este codigo cuando se tenga todos los DNI's de los usuarios
//            if (_poeSvc.findPoeByDniExist(_poe.get_dni()))
//            {
//                new AlertBox("El dni ya existe, intente con otro", "Atención", new Alert(Alert.AlertType.WARNING));
//            } else
//            {
//                new AlertBox("Usuario registrado con éxito!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
//                limpiarCampos();
//            }

        } else
        {
            new AlertBox("Faltan datos", "Atención", new Alert(Alert.AlertType.WARNING));
        }
    }

    @FXML
    public void OnActionBtnCancelar(ActionEvent actionEvent)
    {
        Stage _stage = (Stage) _anchorPanePoeAgregar.getScene().getWindow();
        _stage.close();
    }

    private void limpiarCampos()
    {
        _txtDni.clear();
        _txtNombres.clear();
        _txtApellidos.clear();
        _rbMasculino.setSelected(false);
        _rbFemenino.setSelected(false);
        _rbOtro.setSelected(false);
    }

    public boolean verificarCampos()
    {
        boolean _error = false;
        if (_txtApellidos.getText().isEmpty())
        {
            _labApellidos.setTextFill(Color.web("#ff3333"));
            _error = true;
        } else
        {
            _labApellidos.setTextFill(Color.web("#000000"));
        }
//        if (_txtNombres.getText().isEmpty())
//        {
//            _labNombres.setTextFill(Color.web("#ff3333"));
//            _error = true;
//        } else
//        {
//            _labNombres.setTextFill(Color.web("#000000"));
//        }
        if (_txtDni.getText().isEmpty())
        {
            _labDni.setTextFill(Color.web("#ff3333"));
            _error = true;
        } else
        {
            _labDni.setTextFill(Color.web("#000000"));
        }
        if ((_rbMasculino.isSelected() || _rbFemenino.isSelected() || _rbOtro.isSelected()) == false)
        {
            _rbMasculino.setTextFill(Color.web("#ff3333"));
            _rbFemenino.setTextFill(Color.web("#ff3333"));
            _rbOtro.setTextFill(Color.web("#ff3333"));
            _error = true;
        } else
        {
            _rbMasculino.setTextFill(Color.web("#000000"));
            _rbFemenino.setTextFill(Color.web("#000000"));
            _rbOtro.setTextFill(Color.web("#000000"));
        }
        return _error;
    }
}
