package com.aleph.controller;

import com.aleph.model.Poe;
import com.aleph.service.IPoeService;
import com.aleph.service.PoeService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

public class PoeBuscarPoeController
{
    @FXML
    public AnchorPane _anchorPaneBuscarPoe;
    @FXML
    public TableView<Poe> _tvPoe;
    @FXML
    public TableColumn _tcDni;
    @FXML
    public TableColumn _tcNombres;
    @FXML
    public TableColumn _tcApellidos;
    @FXML
    public TableColumn _tcId;
    @FXML
    public TextField _txtBuscarPoe;
    @FXML
    public Button _btnSeleccionar;
    @FXML
    public RadioButton _rbNombre;
    @FXML
    public ToggleGroup tgPoe;
    @FXML
    public RadioButton _rbDni;
    @FXML
    public Button _btnBuscar;

    IPoeService _poeSvc = new PoeService();

    Poe _poe;

    public void OnActionBtnSeleccionar(ActionEvent actionEvent)
    {
        _poe = new Poe();
        _poe = _tvPoe.getSelectionModel().getSelectedItem();
        _btnSeleccionar.getScene().getWindow().hide();
    }

    public void OnActionBtnBuscar(ActionEvent actionEvent)
    {
        Buscar();
    }

    public Poe get_poe()
    {
        return _poe;
    }

    public void OnKeyPressed(KeyEvent keyEvent)
    {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
        {
            Buscar();
        }
    }

    private void Buscar()
    {
        if (_rbNombre.isSelected())
        {
            ObservableList<Poe> _poeList = FXCollections.observableArrayList(_poeSvc.findPoeByNombre(_txtBuscarPoe.getText()));
            _tcId.setCellValueFactory(new PropertyValueFactory("_poeId"));
            _tcDni.setCellValueFactory(new PropertyValueFactory("_dni"));
            _tcNombres.setCellValueFactory(new PropertyValueFactory("_nombres"));
            _tcApellidos.setCellValueFactory(new PropertyValueFactory("_apellidos"));
            _tvPoe.setItems(_poeList);
        }

        if (_rbDni.isSelected())
        {
            ObservableList<Poe> _poeList = FXCollections.observableArrayList(_poeSvc.findPoeByDni(_txtBuscarPoe.getText()));
            _tcId.setCellValueFactory(new PropertyValueFactory("_poeId"));
            _tcDni.setCellValueFactory(new PropertyValueFactory("_dni"));
            _tcNombres.setCellValueFactory(new PropertyValueFactory("_nombres"));
            _tcApellidos.setCellValueFactory(new PropertyValueFactory("_apellidos"));
            _tvPoe.setItems(_poeList);
        }
    }
}
