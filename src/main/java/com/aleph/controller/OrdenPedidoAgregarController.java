package com.aleph.controller;

import com.aleph.model.*;
import com.aleph.service.*;
import com.aleph.tools.ComboBoxAutoComplete;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Optional;

public class OrdenPedidoAgregarController
{
    private ISedeService _sedeService = new SedeService();
    private IAreaService _areaService = new AreaService();
    private IPoeService _poeService = new PoeService();
    private IGuiaService _guiaService = new GuiaService();
    private ILogService _logService = new LogService();
    private IPrestamoService _prestamoService = new PrestamoService();

    //region Controles
    @FXML
    private AnchorPane _anchorPaneEnviarOrdenPedido;
    @FXML
    private TextField _txtNumeroOrdenPedido;
    @FXML
    private ComboBox _cboxPeriodoMes;
    @FXML
    private Label _lblPeriodoYear;
    @FXML
    private Label _lblFechaOrdenPedido;
    @FXML
    private Button _btnBuscarCliente;
    @FXML
    private Label _lblRuc;
    @FXML
    private Label _lblCliente;
    @FXML
    private ComboBox _cboxSede;
    @FXML
    private ComboBox _cboxArea;
    @FXML
    private ComboBox _cboxTipoRadiacion;
    @FXML
    private ComboBox _cboxTipoDosimetro;
    @FXML
    private ComboBox _cboxTipoExposicion;
    @FXML
    private TableView<Poe> _tvUsuariosParticipantes;
    @FXML
    private TableView<Poe> _tvUsuariosNoParticipantes;
    @FXML
    private TableColumn _tcParticipaExterno;
    @FXML
    private Button _btnDerechaUno;
    @FXML
    private Button _btnIzquierdaUno;
    @FXML
    private Button _btnDerechaTodos;
    @FXML
    private Button _btnIzquierdaTodos;
    @FXML
    private Button _btnGenerarOrden;
    @FXML
    private Button _btnCancelar;
    //    @FXML
//    private CheckBox _chkboxExterno;
    @FXML
    private Text _txtContador;
    @FXML
    private TitledPane _titledPaneDetalle;

    //endregion

    //variables temporales
    ObservableList<Poe> _usuariosParticipantesList = FXCollections.observableArrayList();
    ObservableList<Poe> _usuariosNoParticipantesList = FXCollections.observableArrayList();
    ArrayList<Prestamo> _prestamoList = new ArrayList<>();
    int contador;

    private int _clienteIdRecibido;

    @FXML
    public void initialize()
    {
        //TableColumn cboxColumn = new TableColumn("Externo");
        //_tvUsuariosParticipantes.getColumns().add(cboxColumn);
        //cboxColumn.setCellValueFactory(new PropertyValueFactory<Poe, String>("_cboxExterno"));
        _tcParticipaExterno.setCellValueFactory(new PropertyValueFactory<Poe, String>("_cboxExterno"));

        fechaActual();
        LlenarComboDosimetro();
        LlenarComboExposicion();
        LlenarComboRadiacion();
        LlenarPeriodo();
    }

    // Event Listener on Button[#_btnBuscarCliente].onAction
    @FXML
    public void OnActionBtnBuscarCliente(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._ordenPedidoBuscarClientePath));
            Stage _stage = new Stage();
            _stage.initOwner(_btnBuscarCliente.getScene().getWindow());
            _stage.setScene(new Scene((Parent) _loader.load()));

            _stage.initModality(Modality.WINDOW_MODAL);
            _stage.showAndWait();

            OrdenPedidoBuscarClienteController _controller = _loader.getController();
            _lblRuc.setText(_controller.getRucSend());
            _lblCliente.setText(_controller.getRazonSocialSend());
            _clienteIdRecibido = _controller.getIdCliente();

            if (_clienteIdRecibido == -1)
            {
                _cboxSede.getItems().clear();
                _cboxArea.getItems().clear();
                _cboxSede.setDisable(true);
                _cboxArea.setDisable(true);
                _tvUsuariosParticipantes.getItems().clear();
                _tvUsuariosNoParticipantes.getItems().clear();
            } else
            {
                filtrarCBoxSedes(_clienteIdRecibido);
                _cboxSede.setDisable(false);
                _cboxArea.setDisable(true);
                _tvUsuariosParticipantes.getItems().clear();
                _tvUsuariosNoParticipantes.getItems().clear();
            }

        } catch (IOException e)
        {
            System.out.println("CLASE OrdenPedidoAgregar Metodo OnActionBtnBuscarCliente" + e.getMessage());
        }
    }

    // Event Listener on ComboBox[#_cboxSede].onAction
    @FXML
    public void OnActionCboxSede(ActionEvent event)
    {
        filtrarCBoxArea();
    }

    @FXML
    public void OnActionCboxArea(ActionEvent event)
    {
        filtrarTvPoe(event);
    }

    @FXML
    public void OnActionBtnGenerarOrden(ActionEvent event)
    {
//        if (_txtNumeroOrdenPedido.getText().isEmpty())
//        {
//            new AlertBox("el numero de Orden Pedido no puede ser vacío", "error", new Alert(Alert.AlertType.ERROR));
//        } else
//        {
//            Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
//            _alert.setTitle("Cuadro de confirmación");
//            _alert.setHeaderText("Generar Orden de Pedido");
//            _alert.setContentText("¿Quiere generar esta Orden de Pedido?");
//
//            Optional<ButtonType> _opcion = _alert.showAndWait();
//            if (_opcion.get() == ButtonType.OK)
//            {
//                Guia _guia = new Guia();
//
//                int _areaIdTemp = ((Area) (_cboxArea.getSelectionModel().getSelectedItem())).get_areaId();
//                System.out.println("area :" + _areaIdTemp);
//
//                String _nroOrdenPedidoTemp = _txtNumeroOrdenPedido.getText().toString();
//                System.out.println("nro orden pedido: " + _nroOrdenPedidoTemp);
//
//                String _fechaTemp = _lblFechaOrdenPedido.getText().toString();
//                DateTimeFormatter _dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//                LocalDate _dateTemp = LocalDate.parse(_fechaTemp, _dtf);
//                System.out.println("fecha orden pedido : " + _dateTemp.toString());
//
//                _guia.set_fkAreaId(_areaIdTemp);
//                _guia.set_nroOrdenPedido(_nroOrdenPedidoTemp);
//                _guia.set_fechaOrdenPedido(_dateTemp);
//
//                int _errorGuardar = _guiaService.addGuia(_guia);
//                if (_errorGuardar == 1)
//                {
//                    _prestamoList = new ArrayList<>();
//                    new AlertBox("Orden registrada con éxito!!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
//                    for (Poe _dato : _tvUsuariosParticipantes.getItems())
//                    {
//                        Prestamo _prestamo = new Prestamo();
//                        _prestamo.set_estado("NO ASIGNADO");
//                        _prestamo.set_poeId(_dato.get_poeId());
////                        _prestamo.set_guiaId(_guia.get_nroOrdenPedido());
//                        _prestamoList.add(_prestamo);
//                    }
//
//                    _logService.addLog("Enviado -> #" + _nroOrdenPedidoTemp);
//                    OnActionBtnCancelar(event);
//                } else
//                {
//                    new AlertBox("Hubo un error con la orden, intentar de nuevo ", "Error ", new Alert(Alert.AlertType.ERROR));
//                }
//            } else
//            {
//
//            }
//        }


//        System.out.println(_txtNumeroOrdenPedido.getText());
//
//        Locale spanishLocale = new Locale("es", "ES");
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MMMM-yyyy",spanishLocale);
//        String _date = "01-"+_cboxPeriodoMes.getValue().toString() + "-" + _lblPeriodoYear.getText();
//        LocalDate _localDate = LocalDate.parse(_date, formatter);
//        System.out.println(_localDate);
//
//        System.out.println(_lblFechaOrdenPedido.getText());
//        System.out.println("area: " + _cboxArea.getValue());
//        System.out.println("tiporad: " + _cboxTipoRadiacion.getValue());
//        System.out.println("tipodos: " + _cboxTipoDosimetro.getValue());
//        System.out.println("tipo: " + _cboxTipoExposicion.getValue());
//
//        for (Poe _usuario : _usuariosParticipantesList)
//        {
//            System.out.println(_usuario);
//        }

        Boolean _encontrado;
        if (_txtNumeroOrdenPedido.getText().isEmpty())
        {
            new AlertBox("el numero de Orden Pedido no puede ser vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else
        {
            Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
            _alert.setTitle("Cuadro de confirmación");
            _alert.setHeaderText("Generar Orden de Pedido");
            _alert.setContentText("¿Quiere generar esta Orden de Pedido?");

            Optional<ButtonType> _opcion = _alert.showAndWait();
            if (_opcion.get() == ButtonType.OK)
            {
//                _encontrado = _guiaService.findOrdenPedido(_txtNumeroOrdenPedido.getText());
//                if (_encontrado != false)
//                {
//                    new AlertBox("La Orden de Pedido ya fué ingresada", "Error", new Alert(Alert.AlertType.ERROR));
//                } else
//                {
                Locale spanishLocale = new Locale("es", "ES");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MMMM-yyyy", spanishLocale);
                String _date = "01-" + _cboxPeriodoMes.getValue().toString() + "-" + _lblPeriodoYear.getText();
                LocalDate _localDatePeriodo = LocalDate.parse(_date, formatter);

                String _fechaOrdenPedido = _lblFechaOrdenPedido.getText().toString();
                DateTimeFormatter _dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                LocalDate _localDateOrdenPedido = LocalDate.parse(_fechaOrdenPedido, _dtf);

                int _areaIdTemp = ((Area) (_cboxArea.getSelectionModel().getSelectedItem())).get_areaId();

                Guia _guia = new Guia();
                _guia.set_nroOrdenPedido(_txtNumeroOrdenPedido.getText());
                _guia.set_periodo(_localDatePeriodo);
                //Estado 0 = REGISTRADO
                _guia.set_estado(0);
                _guia.set_fechaOrdenPedido(_localDateOrdenPedido);

//                if (_chkboxExterno.isSelected())
//                    _guia.set_externo("-Ext");
//                else
//                    _guia.set_externo("");


                int _guiaId = _guiaService.addGuia(_guia, _areaIdTemp);


                for (Poe _usuario : _usuariosParticipantesList)
                {
                    Prestamo _prestamo = new Prestamo();
                    //Estado 0 = NO ASIGNADO
                    _prestamo.set_estado(0);
                    _prestamo.set_externo(false);
                    _prestamo.set_tipoRad((String) _cboxTipoRadiacion.getValue());
                    switch ((String) _cboxTipoExposicion.getValue())
                    {
                        case "Cuerpo Entero":
                            _prestamo.set_tipoExpo(1);
                            break;
                        case "Dedo Mano Derecha":
                            _prestamo.set_tipoExpo(2);
                            break;
                        case "Dedo Mano Izquierda":
                            _prestamo.set_tipoExpo(3);
                            break;
                        case "Muneca Derecha":
                            _prestamo.set_tipoExpo(4);
                            break;
                        case "Muneca Izquierda":
                            _prestamo.set_tipoExpo(5);
                            break;
                        default:
                            _prestamo.set_tipoExpo(6);
                            break;
                    }
                    _prestamo.set_tipoDosi((String) _cboxTipoDosimetro.getValue());
                    _prestamoService.addPrestamo(_prestamo, _guiaId, _usuario.get_poeId());
                    //_prestamoList.add(_prestamo);
                    if (_usuario.get_cboxExterno().isSelected())
                    {
                        Prestamo _prestamo2 = new Prestamo();
                        _prestamo2.set_estado(0);
                        _prestamo2.set_externo(true);
                        _prestamo2.set_tipoRad((String) _cboxTipoRadiacion.getValue());
                        switch ((String) _cboxTipoExposicion.getValue())
                        {
                            case "Cuerpo Entero":
                                _prestamo2.set_tipoExpo(1);
                                break;
                            case "Dedo Mano Derecha":
                                _prestamo2.set_tipoExpo(2);
                                break;
                            case "Dedo Mano Izquierda":
                                _prestamo2.set_tipoExpo(3);
                                break;
                            case "Muneca Derecha":
                                _prestamo2.set_tipoExpo(4);
                                break;
                            case "Muneca Izquierda":
                                _prestamo2.set_tipoExpo(5);
                                break;
                            default:
                                _prestamo2.set_tipoExpo(6);
                                break;
                        }
                        _prestamo2.set_tipoDosi((String) _cboxTipoDosimetro.getValue());
                        _prestamoService.addPrestamo(_prestamo2, _guiaId, _usuario.get_poeId());
                        //_prestamoList.add(_prestamo2);
                    }
                }


                //test
//                for (Prestamo dato : _prestamoList)
//                {
//                    System.out.println("dato: " + dato);
//                    System.out.println("estado: " + dato.get_estado());
//                    System.out.println("tipo dosimetro: " + dato.get_tipoDosi());
//                    System.out.println("tipo exposicion: " + dato.get_tipoExpo());
//                    System.out.println("tipo radiacion: " + dato.get_tipoRad());
//                    System.out.println("externo: " + dato.is_externo());
//                }

                //_logService.addLog("Enviado -> #" + _txtNumeroOrdenPedido.getText());

                new AlertBox("Orden ingresada con éxito", "Concluido", new Alert(Alert.AlertType.INFORMATION));

                Stage _stage = (Stage) _anchorPaneEnviarOrdenPedido.getScene().getWindow();
                _stage.close();
//                }
            }
        }
    }

    @FXML
    public void OnActionBtnCancelar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneEnviarOrdenPedido.getScene().getWindow();
        _stage.close();
    }

//    public void setCliente(String _rucRecibido, String _razonSocialRecibido, int _clienteIdRecibido)
//    {
////        _clienteIdTemp = _clienteIdRecibido;
//        _lblRuc.setText(_rucRecibido);
//        _lblCliente.setText(_razonSocialRecibido);
//        filtrarCBoxSedes(_clienteIdRecibido);
//    }

    @FXML
    public void OnActionBtnDerechaUno(ActionEvent event)
    {
        Poe _poe = _tvUsuariosParticipantes.getSelectionModel().getSelectedItem();
        if (_poe != null)
        {
            _usuariosNoParticipantesList.add(_poe);
            _tvUsuariosParticipantes.getItems().remove(_poe);
        }
        _tvUsuariosParticipantes.getSortOrder().add(_tvUsuariosParticipantes.getColumns().get(0));
        _tvUsuariosNoParticipantes.getSortOrder().add(_tvUsuariosNoParticipantes.getColumns().get(0));
        calcularContador();
    }

    @FXML
    public void OnActionBtnDerechaTodos(ActionEvent event)
    {
        ObservableList<Poe> _ListaTransfer = _tvUsuariosParticipantes.getItems();
        for (Poe _dato : _ListaTransfer)
        {
            _usuariosNoParticipantesList.add(_dato);
        }
        _tvUsuariosParticipantes.getItems().removeAll(_ListaTransfer);
        _tvUsuariosNoParticipantes.getSortOrder().add(_tvUsuariosParticipantes.getColumns().get(0));
        _tvUsuariosParticipantes.getSortOrder().add(_tvUsuariosNoParticipantes.getColumns().get(0));
        calcularContador();
    }

    @FXML
    public void OnActionBtnIzquierdaUno(ActionEvent event)
    {
        Poe _poe = _tvUsuariosNoParticipantes.getSelectionModel().getSelectedItem();
        if (_poe != null)
        {
            _usuariosParticipantesList.add(_poe);
            _tvUsuariosNoParticipantes.getItems().remove(_poe);
        }
        _tvUsuariosParticipantes.getSortOrder().add(_tvUsuariosParticipantes.getColumns().get(0));
        _tvUsuariosNoParticipantes.getSortOrder().add(_tvUsuariosNoParticipantes.getColumns().get(0));
        calcularContador();
    }

    @FXML
    public void OnActionBtnIzquierdaTodos(ActionEvent event)
    {
        ObservableList<Poe> _ListaTransfer = _tvUsuariosNoParticipantes.getItems();
        for (Poe _dato : _ListaTransfer)
        {
            _usuariosParticipantesList.add(_dato);
        }
        _tvUsuariosNoParticipantes.getItems().removeAll(_ListaTransfer);
        _tvUsuariosParticipantes.getSortOrder().add(_tvUsuariosParticipantes.getColumns().get(0));
        _tvUsuariosNoParticipantes.getSortOrder().add(_tvUsuariosNoParticipantes.getColumns().get(0));
        calcularContador();
    }

    private void fechaActual()
    {
        LocalDate _fechaActual = LocalDate.now();
        DateTimeFormatter _dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        _lblFechaOrdenPedido.setText(_fechaActual.format(_dtf));
    }


    private void filtrarCBoxSedes(int _clienteId)
    {
        try
        {
            if (_cboxSede.getItems() != null) _cboxSede.getItems().clear();
            ObservableList<Sede> _sedeList = FXCollections.observableArrayList(_sedeService.findSedeByCliente(_clienteId));
            _cboxSede.getItems().addAll(_sedeList);

            Window _stage = _cboxSede.getScene().getWindow();
            Double positionX = _stage.getX() + _titledPaneDetalle.getBoundsInParent().getMinX();
            Double positionY = _stage.getY() + _titledPaneDetalle.getBoundsInParent().getMinY();
            new ComboBoxAutoComplete<String>(_cboxSede, positionX, positionY);
        } catch (Exception e)
        {
            System.out.println("filtrarCBoxSedes :" + e);
        }
    }

    private void filtrarCBoxArea()
    {
        try
        {
            if (_cboxArea.getItems() != null) _cboxArea.getItems().clear();
            ObservableList<Area> _areaList = FXCollections
                    .observableArrayList(_areaService.findAreaBySede(((Sede) (_cboxSede.getSelectionModel().getSelectedItem())).get_sedeId()));
            _cboxArea.getItems().addAll(_areaList);

            Window _stage = _cboxArea.getScene().getWindow();
            Double positionX = _stage.getX() + _titledPaneDetalle.getBoundsInParent().getMinX();
            Double positionY = _stage.getY() + _titledPaneDetalle.getBoundsInParent().getMinY();
            new ComboBoxAutoComplete<String>(_cboxArea, positionX, positionY);
            _cboxArea.setDisable(false);
        } catch (Exception e)
        {
            System.out.println("filtrarCBoxAreas : " + e);
        }
    }

    private void filtrarTvPoe(ActionEvent event)
    {
        _tvUsuariosParticipantes.getItems().clear();
        _tvUsuariosNoParticipantes.getItems().clear();
        _usuariosParticipantesList = FXCollections.observableList(_poeService.findPoeByArea(((Area) (_cboxArea.getSelectionModel().getSelectedItem())).get_areaId()));

        for (Poe _poe : _usuariosParticipantesList)
        {
            _poe.get_cboxExterno().setOnAction(new EventHandler<ActionEvent>()
            {
                @Override
                public void handle(ActionEvent event)
                {
                    calcularContador();
                }
            });
        }

        _tvUsuariosParticipantes.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_nombreCompleto"));
        _tvUsuariosParticipantes.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_dni"));
        _tvUsuariosParticipantes.setItems(_usuariosParticipantesList);

        _tvUsuariosNoParticipantes.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_nombreCompleto"));
        _tvUsuariosNoParticipantes.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_dni"));
        _tvUsuariosNoParticipantes.setItems(_usuariosNoParticipantesList);
        calcularContador();
        OnActionBtnDerechaTodos(event);
    }

    private void LlenarComboRadiacion()
    {
        _cboxTipoRadiacion.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxTipoRadiacion.getItems().addAll("PH", "PM", "PL", "N", "B", "P");
        _cboxTipoRadiacion.getSelectionModel().select(5);
    }

    private void LlenarComboDosimetro()
    {
        _cboxTipoDosimetro.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxTipoDosimetro.getItems().addAll("I", "K");
        _cboxTipoDosimetro.getSelectionModel().select(1);
    }

    private void LlenarComboExposicion()
    {
        _cboxTipoExposicion.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxTipoExposicion.getItems().addAll
                (
                        "Cuerpo Entero",
                        "Dedo Mano Derecha",
                        "Dedo Mano Izquierda",
                        "Muneca Izquierda",
                        "Muneca Derecha",
                        "Otra parte"
                );
        _cboxTipoExposicion.getSelectionModel().select(0);
    }

    private void LlenarPeriodo()
    {
        LocalDate _now = LocalDate.now();
        _cboxPeriodoMes.getItems().clear();

        Locale spanishLocale = new Locale("es", "ES");
        DateTimeFormatter _dtfMonth = DateTimeFormatter.ofPattern("MMMM", spanishLocale);
        _cboxPeriodoMes.getItems().add(_now.format(_dtfMonth));
        _cboxPeriodoMes.getItems().add(_now.plusMonths(1).format(_dtfMonth));
        _cboxPeriodoMes.getSelectionModel().select(1);

        OnActionCboxPeriodoMes(new ActionEvent());

//        DateTimeFormatter _dtfYear = DateTimeFormatter.ofPattern("yyyy");
//        _lblPeriodoYear.setText(_now.format(_dtfYear));
    }

    public void OnActionCboxPeriodoMes(ActionEvent actionEvent)
    {
        LocalDate _now = LocalDate.now();
        if (_cboxPeriodoMes.getSelectionModel().getSelectedIndex() == 1)
        {
            DateTimeFormatter _dtfYear = DateTimeFormatter.ofPattern("yyyy");
            _lblPeriodoYear.setText(_now.plusMonths(1).format(_dtfYear));
        } else
        {
            DateTimeFormatter _dtfYear = DateTimeFormatter.ofPattern("yyyy");
            _lblPeriodoYear.setText(_now.format(_dtfYear));
        }
    }

    public void calcularContador()
    {
        contador = 0;
        ObservableList<Poe> _ListaTransfer = _tvUsuariosParticipantes.getItems();
        for (Poe _dato : _ListaTransfer)
        {
            if (_dato.get_cboxExterno().isSelected())
            {
                contador++;
            }
            contador++;
        }
        _txtContador.setText(String.valueOf(contador));
    }
}
