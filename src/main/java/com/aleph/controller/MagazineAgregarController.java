package com.aleph.controller;

import com.aleph.service.IMagazineService;
import com.aleph.service.MagazineService;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

public class MagazineAgregarController
{
    public Button _btnGuardar;
    public Button _btnCancelar;
    public TextField _txtCodigo;
    public AnchorPane _anchorPaneMagazineAgregar;

    IMagazineService _magazineService = new MagazineService();

    public void OnActionBtnGuardar(ActionEvent actionEvent)
    {
        boolean _existe;
        _existe = _magazineService.findDosimetroExist(agregarCeros(_txtCodigo.getText(), 7));
        if (_existe == false)
        {
            Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
            _alert.setTitle("Cuadro de confirmación");
            _alert.setHeaderText("Ingresar dosimetro al magazine");
            _alert.setContentText("¿Quiere continuar?");

            Optional<ButtonType> _opcion = _alert.showAndWait();
            if (_opcion.get() == ButtonType.OK)
            {
                _magazineService.addDosimetroLimpio(agregarCeros(_txtCodigo.getText(), 7));
            }
            Stage _stage = (Stage) _anchorPaneMagazineAgregar.getScene().getWindow();
            _stage.close();
        } else
        {
            Alert _alert = new Alert(Alert.AlertType.WARNING);
            _alert.setTitle("ADVERTENCIA");
            _alert.setHeaderText("El código ya fué ingresado");

            _alert.showAndWait();


//            Alert alert = new Alert(Alert.AlertType.ERROR);
//            alert.setTitle("Exception Dialog");
//            alert.setHeaderText("Look, an Exception Dialog");
//            alert.setContentText("Could not find file blabla.txt!");
//            alert.setWidth(300);
//            alert.setHeight(600);
//
//            Exception ex = new FileNotFoundException("Could not find file blablabla.txt");
//            // Create expandable Exception.
//            StringWriter sw = new StringWriter();
//            PrintWriter pw = new PrintWriter(sw);
//            ex.printStackTrace(pw);
//            String exceptionText = sw.toString();
//
//            Label label = new Label("The exception stacktrace was:");
//
//            TextArea textArea = new TextArea(exceptionText);
//            textArea.setEditable(false);
//            textArea.setWrapText(true);
//
//            textArea.setMaxWidth(Double.MAX_VALUE);
//            textArea.setMaxHeight(Double.MAX_VALUE);
//            GridPane.setVgrow(textArea, Priority.ALWAYS);
//            GridPane.setHgrow(textArea, Priority.ALWAYS);
//
//            GridPane expContent = new GridPane();
//            expContent.setMaxWidth(Double.MAX_VALUE);
//            expContent.add(label, 0, 0);
//            expContent.add(textArea, 0, 1);
//
//            // Set expandable Exception into the dialog pane.
//            alert.getDialogPane().setContent(expContent);
//
//            alert.showAndWait();
        }

    }

    public void OnActionBtnCancelar(ActionEvent actionEvent)
    {
        Stage _stage = (Stage) _anchorPaneMagazineAgregar.getScene().getWindow();
        _stage.close();
    }

    private static String agregarCeros(String _numero, int _longitud)
    {
        String ceros = "";

        int cantidad = _longitud - _numero.length();

        if (cantidad >= 1)
        {
            for (int i = 0; i < cantidad; i++)
            {
                ceros += "0";
            }

            return (ceros + _numero);
        } else
        {
            return _numero;
        }
    }
}
