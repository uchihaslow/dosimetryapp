package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.model.Usuario;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.event.ActionEvent;

import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class GuiaController
{
    @FXML
    private AnchorPane _AnchorPaneGuia;
    @FXML
    private Button _btnGuiaAsignados;
    @FXML
    private Button _btnGuiaPorAsignar;

    private Usuario _usuarioTemp;

    // Event Listener on Button[#_btnGuiaAsignados].onAction
    @FXML
    public void OnActionBtnGuiaAsignados(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._guiaAsignadosPath));
        Parent _guiaPageParent = _loader.load();

        GuiaAsignadosController controller = _loader.getController();
        controller.setUsuario(_usuarioTemp);

        Stage _guiaModalStage = new Stage();
        Scene _guiaPageScene = new Scene(_guiaPageParent);
        _guiaModalStage.setScene(_guiaPageScene);
        _guiaModalStage.initModality(Modality.WINDOW_MODAL);
        _guiaModalStage.initOwner(_AnchorPaneGuia.getScene().getWindow());
        _guiaModalStage.setResizable(false);
        _guiaModalStage.setMaximized(false);
        _guiaModalStage.show();
    }

    // Event Listener on Button[#_btnGuiaPorAsignar].onAction
    @FXML
    public void OnActionBtnGuiaPorAsignar(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._guiaPorAsignarPath));
        Parent _guiaPageParent = _loader.load();

        Stage _guiaModalStage = new Stage();
        Scene _guiaPageScene = new Scene(_guiaPageParent);
        _guiaModalStage.setScene(_guiaPageScene);
        _guiaModalStage.initModality(Modality.WINDOW_MODAL);
        _guiaModalStage.initOwner(_AnchorPaneGuia.getScene().getWindow());
        _guiaModalStage.setResizable(false);
        _guiaModalStage.setMaximized(false);
        _guiaModalStage.show();
    }

    public void setUsuario(Usuario _usuario)
    {
        _usuarioTemp = _usuario;

//        User SuperUsuario
        if (_usuarioTemp.get_access() == 1)
        {
        }
//        User Laboratorio
        if (_usuarioTemp.get_access() == 2)
        {
        }
//        User Administracion
        if (_usuarioTemp.get_access() == 3)
        {
            _btnGuiaPorAsignar.setDisable(true);
        }
    }
}
