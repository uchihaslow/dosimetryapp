package com.aleph.controller;

import com.aleph.model.Diccionario;
import com.aleph.model.Paths;
import com.aleph.model.Usuario;
import com.aleph.service.IUsuarioService;
import com.aleph.service.UsuarioService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

import javafx.event.ActionEvent;

import javafx.scene.control.PasswordField;

public class InicioController
{

    private IUsuarioService _usuarioService = new UsuarioService();

    @FXML
    private AnchorPane _anchorPaneInicio;
    @FXML
    private Button _btnIniciarSesion;
    @FXML
    private TextField _txtUsuario;
    @FXML
    private PasswordField _txtClave;

    // Event Listener on Button[#_btnIniciarSesion].onAction
    @FXML
    public void OnActionBtnIniciarSesion(ActionEvent event) throws IOException
    {
        Usuario _usuario = _usuarioService.login(_txtUsuario.getText(), _txtClave.getText());
        if (_usuario != null)
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(Paths._menuPath));
            Parent _menuPageParent = loader.load();

            //Se hace de la manera de arriba, para poder modificar
            //Parent _menuPageParent = FXMLLoader.load(getClass().getResource(Paths._menuPath));

            MenuController controller = loader.getController();
            controller.setUsuario(_usuario);

            Scene _menuPageScene = new Scene(_menuPageParent);
            Stage _mainStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            _mainStage.close();
            _mainStage.setScene(_menuPageScene);
            _mainStage.setTitle("DOSYS");
            _mainStage.centerOnScreen();
            _mainStage.setResizable(false);
            _mainStage.setMaximized(false);
            _mainStage.show();
        } else
        {
            new AlertBox(Diccionario._usuarioClaveIncorrecto, Diccionario._error, new Alert(Alert.AlertType.WARNING));
        }
    }

    @FXML
    public void OnKeyPressed(KeyEvent keyEvent) throws IOException
    {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
        {
            Usuario _usuario = _usuarioService.login(_txtUsuario.getText(), _txtClave.getText());
            if (_usuario != null)
            {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(Paths._menuPath));
                Parent _menuPageParent = loader.load();

                //Se hace de la manera de arriba, para poder modificar
                //Parent _menuPageParent = FXMLLoader.load(getClass().getResource(Paths._menuPath));

                MenuController controller = loader.getController();
                controller.setUsuario(_usuario);

                Scene _menuPageScene = new Scene(_menuPageParent);
                Stage _mainStage = new Stage();

                _mainStage.close();
                _mainStage.setScene(_menuPageScene);
                _mainStage.setTitle("DOSYS");
                _mainStage.centerOnScreen();
                _mainStage.setResizable(false);
                _mainStage.setMaximized(false);
                _mainStage.show();
                Stage _currentStage = (Stage) _anchorPaneInicio.getScene().getWindow();
                _currentStage.close();
            } else
            {
                new AlertBox(Diccionario._usuarioClaveIncorrecto, Diccionario._error, new Alert(Alert.AlertType.WARNING));
            }
        }
    }
}