package com.aleph.controller;

import com.aleph.service.DosimetroService;
import com.aleph.service.IDosimetroService;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

public class ReporteDevueltosController
{
    @FXML
    private AnchorPane _anchorPaneNoLeidos;
    @FXML
    private TableView _tvDevueltos;
    @FXML
    private TableColumn _tcDni;
    @FXML
    private TableColumn _tcNombres;
    @FXML
    private TableColumn _tcApellidos;
    @FXML
    private TableColumn _tcDosimetro;
    @FXML
    private Button _btnImprimir;
    @FXML
    private Label _labNroRuc;
    @FXML
    private Label _labRazonSocial;
    @FXML
    private Label _labFechaOrdenPedido;
    @FXML
    private TableColumn _tcFechaLlegada;

    IDosimetroService _dosimetroService = new DosimetroService();
    private ObservableList<ObservableList> _devueltosList;

    public void initialize()
    {

    }

    public void OnActionBtnImprimir(ActionEvent actionEvent)
    {

    }

    public void cargarDatos(int _guiaId)
    {
        _devueltosList = FXCollections.observableArrayList(_dosimetroService.Devueltos(_guiaId));

        _labNroRuc.setText(_devueltosList.get(0).get(0).toString());
        _labRazonSocial.setText(_devueltosList.get(0).get(1).toString());
        _labFechaOrdenPedido.setText(_devueltosList.get(0).get(2).toString());

        _tcDni.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(3).toString())
                );

        _tcNombres.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(4).toString())
                );

        _tcApellidos.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(5).toString())
                );

        _tcDosimetro.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(6).toString())
                );

        _tcFechaLlegada.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(7).toString())
                );

        for (ObservableList _row : _devueltosList)
        {
            if (_row.get(3) == null)
            {
                _row.set(3, "");
            }
            if (_row.get(4) == null)
            {
                _row.set(4, "");
            }
            if (_row.get(5) == null)
            {
                _row.set(5, "");
            }
            if (_row.get(6) == null)
            {
                _row.set(6, "");
            }
        }

        _tvDevueltos.setItems(_devueltosList);


    }
}
