package com.aleph.controller;

import com.aleph.model.Dosimetro;
import com.aleph.model.Paths;
import com.aleph.service.DosimetroService;
import com.aleph.service.IDosimetroService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

import javafx.event.ActionEvent;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class DosimetroController
{
    IDosimetroService _dosimetroService = new DosimetroService();

    @FXML
    private AnchorPane _anchorPaneDosimetroBuscar;
    @FXML
    private TableView<Dosimetro> _tvDosimetro;
    @FXML
    private TextField _txtBuscar;
    @FXML
    private Button _btnAgregar;
    @FXML
    private Button _btnBuscar;
    @FXML
    private Button _btnEliminar;
    @FXML
    private Button _btnEditar;

    // Event Listener on Button[#_btnAgregar].onAction
    @FXML
    public void OnActionBtnAgregar(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._dosimetroAgregarPath));
            Parent _dosimetroAgregarPageParent = _loader.load();

//            AreaAgregarController _controller = _loader.getController();
//            _controller.setSede((Sede) _cboxSede.getSelectionModel().getSelectedItem());

            Stage _dosimetroAgregarModalStage = new Stage();
            Scene _dosimetroAgregarPageScene = new Scene(_dosimetroAgregarPageParent);
            _dosimetroAgregarModalStage.setScene(_dosimetroAgregarPageScene);
            _dosimetroAgregarModalStage.initModality(Modality.WINDOW_MODAL);
            _dosimetroAgregarModalStage.initOwner(_anchorPaneDosimetroBuscar.getScene().getWindow());
            _dosimetroAgregarModalStage.setResizable(false);
            _dosimetroAgregarModalStage.setMaximized(false);
            _dosimetroAgregarModalStage.showAndWait();

        } catch (IOException e)
        {
            System.out.println("ERROR DosimetroController OnActionBtnAgregar: " + e);
        }
    }

    // Event Listener on Button[#_btnBuscar].onAction
    @FXML
    public void OnActionBtnBuscar(ActionEvent event)
    {
        ObservableList<Dosimetro> _dosimetroList = FXCollections.observableArrayList(_dosimetroService.findDosimetroByCodigo(_txtBuscar.getText()));
        _tvDosimetro.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_dosimetroCodigo"));
        _tvDosimetro.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_tipo"));
        _tvDosimetro.setItems(_dosimetroList);
    }

    // Event Listener on Button[#_btnEliminar].onAction
    @FXML
    public void OnActionBtnEliminar(ActionEvent event)
    {
        Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
        _alert.setTitle("Cuadro de confirmación");
        _alert.setHeaderText("Eliminar Dosimetro");
        _alert.setContentText("¿Quiere elminar este dosimetro?");

        Optional<ButtonType> _opcion = _alert.showAndWait();
        if (_opcion.get() == ButtonType.OK)
        {
            _dosimetroService.removeDosimetro(_tvDosimetro.getSelectionModel().getSelectedItem().get_dosimetroCodigo());
        } else
        {

        }
    }

    // Event Listener on Button[#_btnEditar].onAction
    @FXML
    public void OnActionBtnEditar(ActionEvent event)
    {
        try
        {
            FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._dosimetroEditarPath));
            Parent _dosimetroEditarPageParent = _loader.load();

            DosimetroEditarController _controller = _loader.getController();
            _controller.setDosimetro( _tvDosimetro.getSelectionModel().getSelectedItem());

            Stage _dosimetroEditarModalStage = new Stage();
            Scene _dosimetroEditarPageScene = new Scene(_dosimetroEditarPageParent);
            _dosimetroEditarModalStage.setScene(_dosimetroEditarPageScene);
            _dosimetroEditarModalStage.initModality(Modality.WINDOW_MODAL);
            _dosimetroEditarModalStage.initOwner(_anchorPaneDosimetroBuscar.getScene().getWindow());
            _dosimetroEditarModalStage.setResizable(false);
            _dosimetroEditarModalStage.setMaximized(false);
            _dosimetroEditarModalStage.showAndWait();
            _tvDosimetro.getItems().clear();

        } catch (IOException e)
        {
            System.out.println("ERROR DosimetroController OnActionBtnEditar: " + e);
        }
    }
}
