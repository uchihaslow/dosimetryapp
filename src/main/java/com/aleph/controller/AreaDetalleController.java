package com.aleph.controller;

import com.aleph.model.Area;
import com.aleph.model.Sede;
import com.aleph.model.TipoRadiacion;
import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import javafx.event.ActionEvent;

import javafx.scene.layout.AnchorPane;

import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

public class AreaDetalleController
{

    @FXML
    private AnchorPane _anchorPaneDetalleArea;
    @FXML
    private CheckBox _chbRayosXGAlta;
    @FXML
    private CheckBox _chbRayosXGMedia;
    @FXML
    private CheckBox _chbRayosXGBaja;
    @FXML
    private CheckBox _chbRayosXG;
    @FXML
    private CheckBox _chbNeutrones;
    @FXML
    private CheckBox _chbBeta;
    @FXML
    private TextField _txtNombreSede;
    @FXML
    private TextField _txtNombreArea;
    @FXML
    private Button _btnAceptar;
    @FXML
    private TextField _txtContacto;

    // Event Listener on Button[#_btnAceptar].onAction
    @FXML
    public void OnActionBtnAceptar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneDetalleArea.getScene().getWindow();
        _stage.close();
    }

    public void setSede(Sede _sede)
    {
        _txtNombreSede.setText(_sede.get_nombre());
    }

    public void setArea(Area _area)
    {
        _txtNombreArea.setText(_area.get_nombre());
        _txtContacto.setText(_area.get_contacto());
    }

    public void setTipoRadiacion(TipoRadiacion _tipoRadiacion)
    {
        _chbRayosXGAlta.setSelected(_tipoRadiacion.is_rayosXgAlta());
        _chbRayosXGMedia.setSelected(_tipoRadiacion.is_rayosXgMedia());
        _chbRayosXGBaja.setSelected(_tipoRadiacion.is_rayosXgBaja());
        _chbNeutrones.setSelected(_tipoRadiacion.is_neutrones());
        _chbBeta.setSelected(_tipoRadiacion.is_beta());
        _chbRayosXG.setSelected(_tipoRadiacion.is_rayosXg());
    }
}