package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.model.Usuario;
import com.aleph.service.GuiaService;
import com.aleph.service.IGuiaService;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OrdenEstadoController
{
    private Usuario _usuarioTemp;

    IGuiaService _guiaService = new GuiaService();

    @FXML
    private AnchorPane _anchorPaneGuiaAsignadas;
    @FXML
    private TableView _tvOrdenEstado;
    @FXML
    private TableColumn _tcNroGuia;
    @FXML
    private TableColumn _tcPeriodo;
    @FXML
    private TableColumn _tcNroPedido;
    @FXML
    private TableColumn _tcNombreCliente;
    @FXML
    private TableColumn _tcNombreSede;
    @FXML
    private TableColumn _tcNombreArea;
    @FXML
    private TableColumn _tcCantidadUsuarios;
    @FXML
    private ComboBox<String> _cbPeriodo;

    private ObservableList<ObservableList> _listaGuias;

    private ObservableList<ObservableList> _listaSeleccionados = FXCollections.observableArrayList();


    @FXML
    public void initialize()
    {
        FillCboxMonths();
    }

    // Event Listener on Button[#_btnShowPdf].onAction
    @FXML
    public void OnActionBtnMostrarPdf(ActionEvent event) throws IOException //throws JRException
    {
        int _guiaDownload = Integer.parseInt(((ObservableList<String>) _tvOrdenEstado.getSelectionModel().getSelectedItem()).get(0));

        /* PARA MOSTRAR UNA PANTALLA DONDE GUARDAR EL PDF

        FileChooser _fc = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
        _fc.getExtensionFilters().add(extFilter);
        _fc.setInitialFileName("Guia_Nro_" + _guiaDownload);

        File _file = _fc.showSaveDialog(null);


        _file = new File(_file.getAbsolutePath() + ".pdf");

        _guiaService.downloadFile(_guiaDownload, _file);

        */


        String _projectPath = System.getProperty("user.dir");
        final String _reportSource = (_projectPath + "/temp/");
        //final String _reportSource = (_projectPath + "/plantilla/" + "temp.pdf");
        //final String _reportSource = ("/media/windata/JavaProjects/DOSYS/plantilla/" + "temp.pdf");
        //final String _reportSource = ("/media/windata/JavaProjects/DOSYS/plantilla/");

//        if (new File(_reportSource).delete())
//        {
//            System.out.println("El fichero ha sido borrado satisfactoriamente");
//        } else
//        {
//            System.out.println("El fichero no puede ser borrado");
//        }

        File _directorio = new File(_reportSource);

        System.out.println("directory" + _directorio);

        File _file = File.createTempFile("temp_guia_" + _guiaDownload + "_", ".pdf", _directorio);

        System.out.println("Path Absolute" + _file.getAbsolutePath());

        _guiaService.downloadFile(_guiaDownload, _file);


        try
        {
            if (_file.exists())
            {
                if (Desktop.isDesktopSupported())
                {
                    new Thread
                            (
                                    () ->
                                    {
                                        try
                                        {
                                            Desktop.getDesktop().open(_file);
                                            //_file.deleteOnExit();

                                        } catch (IOException e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }
                            ).start();
                } else
                {
                    System.out.println("Awt Desktop is not supported!");
                }
            } else
            {
                System.out.println("File is not exists!");
            }
            _file.deleteOnExit();
            System.out.println("Done");
        } catch (Exception ex)
        {
            System.err.println("error: " + ex);
        }


//        JasperReport jreport = JasperPrintManager.printReport(_file.getAbsolutePath(),false);
//
//        final JasperPrint _print = JasperFillManager.fillReport(jreport, null);
//
//        JasperViewer.viewReport(_print, false);

//        if (_file.delete())
//        {
//            System.out.println("El fichero ha sido borrado satisfactoriamente");
//        } else
//        {
//            System.out.println("El fichero no puede ser borrado");
//        }

    }

    public void LlenarTvEstado()
    {
        _listaGuias = FXCollections.observableArrayList(_guiaService.listGuiaAsignadas());

        _tcNroGuia.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(0).toString())
                );

        _tcNroPedido.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(1).toString())
                );

        _tcNombreSede.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(2).toString())
                );

        _tcCantidadUsuarios.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(3).toString())
                );

        _tcPeriodo.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(7).toString())
                );

        _tcNombreCliente.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(4).toString())
                );

        _tcNombreArea.setCellValueFactory
                (
                        (Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                                param -> new SimpleStringProperty(param.getValue().get(5).toString())
                );

        _tvOrdenEstado.setItems(_listaGuias);
    }

    public void setUsuario(Usuario _usuario)
    {
        _usuarioTemp = _usuario;

//        User SuperUsuario
        if (_usuarioTemp.get_access() == 1)
        {
//            _btnGuia.setDisable(true);
//            _btnProcesamiento.setDisable(true);
//            _btnSeguimiento.setDisable(true);
        }
//        User Laboratorio
        if (_usuarioTemp.get_access() == 2)
        {

//            _btnCliente.setDisable(true);
//            _btnSede.setDisable(true);
//            _btnArea.setDisable(true);
//            _btnPoe.setDisable(true);
//            _btnDosimetro.setDisable(true);
        }
//        User Administracion
        if (_usuarioTemp.get_access() == 3)
        {
//            _btnGenEtiquetas.setDisable(true);
//            _btnGenSobre.setDisable(true);
//            _btnCambios.setDisable(true);
        }
    }

    public void BtnCargarOnAction(ActionEvent actionEvent)
    {
    }

    public void OnActionBtnExportarExcel(ActionEvent actionEvent)
    {
    }

    public void FillCboxMonths()
    {
        _cbPeriodo
                .getItems()
                .addAll("TODOS","ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
        _cbPeriodo.getSelectionModel().select(1);

    }
}
