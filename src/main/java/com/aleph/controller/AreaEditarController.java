package com.aleph.controller;

import com.aleph.model.Area;
import com.aleph.model.Sede;
import com.aleph.model.TipoRadiacion;
import com.aleph.service.AreaService;
import com.aleph.service.IAreaService;
import com.aleph.service.ITipoRadiacionService;
import com.aleph.service.TipoRadiacionService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AreaEditarController
{

    IAreaService _areaService = new AreaService();
    ITipoRadiacionService _tipoRadiacionService = new TipoRadiacionService();

    @FXML
    private AnchorPane _anchorPaneEditarArea;
    @FXML
    private CheckBox _chbRayosXGAlta;
    @FXML
    private CheckBox _chbRayosXGMedia;
    @FXML
    private CheckBox _chbRayosXGBaja;
    @FXML
    private CheckBox _chbRayosXG;
    @FXML
    private CheckBox _chbNeutrones;
    @FXML
    private CheckBox _chbBeta;
    @FXML
    private TextField _txtNombreSede;
    @FXML
    private TextField _txtNombreArea;
    @FXML
    private Button _btnActualizarArea;
    @FXML
    private Button _btnCancelar;
    @FXML
    private TextField _txtContacto;

    //variables temporales
    TipoRadiacion _tipoRadiacionTemp = new TipoRadiacion();
    Area _areaTemp = new Area();

    @FXML
    public void OnActionCheckTipoRadiacion(ActionEvent event)
    {
        if (_chbRayosXGAlta.isSelected())
        {
            _tipoRadiacionTemp.set_rayosXgAlta(true);
        } else
        {
            _tipoRadiacionTemp.set_rayosXgAlta(false);
        }
        if (_chbRayosXGMedia.isSelected())
        {
            _tipoRadiacionTemp.set_rayosXgMedia(true);
        } else
        {
            _tipoRadiacionTemp.set_rayosXgMedia(false);
        }
        if (_chbRayosXGBaja.isSelected())
        {
            _tipoRadiacionTemp.set_rayosXgBaja(true);
        } else
        {
            _tipoRadiacionTemp.set_rayosXgBaja(false);
        }
        if (_chbRayosXG.isSelected())
        {
            _tipoRadiacionTemp.set_rayosXg(true);
        } else
        {
            _tipoRadiacionTemp.set_rayosXg(false);
        }
        if (_chbNeutrones.isSelected())
        {
            _tipoRadiacionTemp.set_neutrones(true);
        } else
        {
            _tipoRadiacionTemp.set_neutrones(false);
        }
        if (_chbBeta.isSelected())
        {
            _tipoRadiacionTemp.set_beta(true);
        } else
        {
            _tipoRadiacionTemp.set_beta(false);
        }
    }

    @FXML
    public void OnActionBtnActualizarArea(ActionEvent event)
    {
        if (_txtNombreArea.getText().isEmpty())
        {
            new AlertBox("el campo nombre de area está vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else
        {
            try
            {
                _areaTemp.set_nombre(_txtNombreArea.getText());
                _areaTemp.set_contacto(_txtContacto.getText());
                _areaService.updateArea(_areaTemp);

                //_tipoRadiacionService.updateTipoRadiacion(_tipoRadiacionTemp);

                new AlertBox("Area actualizada con éxito!!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
                OnActionBtnCancelar(event);

            } catch (RuntimeException e)
            {
                System.err.println("Error OnActionBtnActualizarArea: " + e);
            }
        }

        //System.out.println(_tipoRadiacionTemp.toString());
        //System.out.println(_areaTemp.toString());
    }

    // Event Listener on Button[#_btnCancelar].onAction
    @FXML
    public void OnActionBtnCancelar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneEditarArea.getScene().getWindow();
        _stage.close();
    }

    public void setSede(Sede _sede)
    {
        _txtNombreSede.setText(_sede.get_nombre());
    }

    public void setArea(Area _area)
    {
        _txtNombreArea.setText(_area.get_nombre());
        _txtContacto.setText(_area.get_contacto());
        _areaTemp.set_areaId(_area.get_areaId());
        _areaTemp.set_nombre(_area.get_nombre());
        _areaTemp.set_contacto(_area.get_contacto());
    }

    public void setTipoRadiacion(TipoRadiacion _tipoRadiacion)
    {
        _chbRayosXGAlta.setSelected(_tipoRadiacion.is_rayosXgAlta());
        _chbRayosXGMedia.setSelected(_tipoRadiacion.is_rayosXgMedia());
        _chbRayosXGBaja.setSelected(_tipoRadiacion.is_rayosXgBaja());
        _chbNeutrones.setSelected(_tipoRadiacion.is_neutrones());
        _chbBeta.setSelected(_tipoRadiacion.is_beta());
        _chbRayosXG.setSelected(_tipoRadiacion.is_rayosXg());
        _tipoRadiacionTemp = _tipoRadiacion;
    }
}
