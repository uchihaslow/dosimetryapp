package com.aleph.controller;

import com.aleph.model.Poe;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class PoeDetalleController
{
    @FXML
    private AnchorPane _anchorPanePoeDetalle;

    @FXML
    private Button _btnAceptar;

    @FXML
    private Label _labDni;

    @FXML
    private Label _labNombres;

    @FXML
    private Label _labApellidos;

    @FXML
    private TextField _txtDni;

    @FXML
    private TextField _txtNombres;

    @FXML
    private TextField _txtApellidos;

    @FXML
    private RadioButton _rbMasculino;

    @FXML
    private ToggleGroup _tgSexo;

    @FXML
    private RadioButton _rbFemenino;

    @FXML
    private RadioButton _rbOtro;

    @FXML
    void OnActionBtnAceptar(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPanePoeDetalle.getScene().getWindow();
        _stage.close();
    }

    public void setPoe(Poe _poe)
    {
        _txtDni.setText(_poe.get_dni());
        _txtNombres.setText(_poe.get_nombres());
        _txtApellidos.setText(_poe.get_apellidos());

        switch (_poe.get_sexo())
        {
            case 'M':
                _rbMasculino.setSelected(true);
                break;
            case 'F':
                _rbFemenino.setSelected(true);
                break;
            case 'O':
                _rbOtro.setSelected(true);
                break;
        }

    }
}
