package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.model.Usuario;
import com.aleph.service.ILogService;
import com.aleph.service.LogService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class OrdenPedidoController
{

    private ILogService _logService = new LogService();

    @FXML
    private AnchorPane _anchorPaneOrdenPedido;
    @FXML
    private Button _btnEnviarOrden;
    @FXML
    private Button _btnRecibirOrden;
    @FXML
    private Button _btnEstado;

    private Usuario _usuarioTemp;

    @FXML
    public void initialize()
    {

    }

    // Event Listener on Button[#_btnEnviarOrden].onAction
    @FXML
    public void OnActionBtnEnviarOrden(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._ordenPedidoAgregarPath));
        Parent _ordenPedidoAgregarPageParent = _loader.load();

        Stage _ordenPedidoAgregarModalStage = new Stage();
        Scene _ordenPedidoAgregarPageScene = new Scene(_ordenPedidoAgregarPageParent);
        _ordenPedidoAgregarModalStage.setScene(_ordenPedidoAgregarPageScene);
        _ordenPedidoAgregarModalStage.initModality(Modality.WINDOW_MODAL);
        _ordenPedidoAgregarModalStage.initOwner(_anchorPaneOrdenPedido.getScene().getWindow());
        _ordenPedidoAgregarModalStage.setResizable(false);
        _ordenPedidoAgregarModalStage.setMaximized(false);
        _ordenPedidoAgregarModalStage.showAndWait();
        //ActualizarHistorial();
    }

    // Event Listener on Button[#_btnRecibirOrden].onAction
    @FXML
    public void OnActionBtnRecibirOrden(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._ordenPedidoRecibirOrden));
        Parent _ordenPedidoRecibirOrdenPageParent = _loader.load();

        OrdenPedidoRecibirOrdenController controller = _loader.getController();
        controller.setUsuario(_usuarioTemp);

        Stage _ordenPedidoRecibirOrdenModalStage = new Stage();
        Scene _ordenPedidoRecibirOrdenPageScene = new Scene(_ordenPedidoRecibirOrdenPageParent);
        _ordenPedidoRecibirOrdenModalStage.setScene(_ordenPedidoRecibirOrdenPageScene);
        _ordenPedidoRecibirOrdenModalStage.initModality(Modality.WINDOW_MODAL);
        _ordenPedidoRecibirOrdenModalStage.initOwner(_anchorPaneOrdenPedido.getScene().getWindow());
        _ordenPedidoRecibirOrdenModalStage.setResizable(false);
        _ordenPedidoRecibirOrdenModalStage.setMaximized(false);
        _ordenPedidoRecibirOrdenModalStage.showAndWait();
        //ActualizarHistorial();
    }

    @FXML
    public void OnActionBtnEstado(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._ordenEstadoPath));
        Parent _ordenPedidoRecibirOrdenPageParent = _loader.load();

        Stage _ordenPedidoRecibirOrdenModalStage = new Stage();
        Scene _ordenPedidoRecibirOrdenPageScene = new Scene(_ordenPedidoRecibirOrdenPageParent);
        _ordenPedidoRecibirOrdenModalStage.setScene(_ordenPedidoRecibirOrdenPageScene);
        _ordenPedidoRecibirOrdenModalStage.initModality(Modality.WINDOW_MODAL);
        _ordenPedidoRecibirOrdenModalStage.initOwner(_anchorPaneOrdenPedido.getScene().getWindow());
        _ordenPedidoRecibirOrdenModalStage.setResizable(false);
        _ordenPedidoRecibirOrdenModalStage.setMaximized(false);
        _ordenPedidoRecibirOrdenModalStage.show();
    }

    public void setUsuario(Usuario _usuario)
    {
        _usuarioTemp = _usuario;

//        User SuperUsuario
        if (_usuarioTemp.get_access() == 1)
        {
        }
//        User Laboratorio
        if (_usuarioTemp.get_access() == 2)
        {
        }
//        User Administracion
        if (_usuarioTemp.get_access() == 3)
        {

        }
    }


}
