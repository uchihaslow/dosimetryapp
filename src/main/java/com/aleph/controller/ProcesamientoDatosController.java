package com.aleph.controller;

import com.aleph.model.Paths;
import com.aleph.model.Usuario;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.event.ActionEvent;

import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class ProcesamientoDatosController
{
    private Usuario _usuarioTemp;

    @FXML
    public Button _btnMagazine;
    @FXML
    public Button _btnInformeIso;
    @FXML
    private AnchorPane _anchorPaneProcesamientoDatos;
    @FXML
    public Button _btnAnneal;
    @FXML
    public Button _btnLectura;
    @FXML
    public Button _btnReporte;
    @FXML
    public Button _btnRecibir;

    @FXML
    public void OnActionBtnAnneal(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._annealPath));
        Parent _annealParent = _loader.load();

        Stage _annealModalStage = new Stage();
        Scene _annealPageScene = new Scene(_annealParent);
        _annealModalStage.setScene(_annealPageScene);
        _annealModalStage.initModality(Modality.WINDOW_MODAL);
        _annealModalStage.initOwner(_anchorPaneProcesamientoDatos.getScene().getWindow());
        _annealModalStage.setResizable(false);
        _annealModalStage.setMaximized(false);
        _annealModalStage.show();
    }

    @FXML
    public void OnActionBtnLectura(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._lecturaPath));
        Parent _lecturaParent = _loader.load();

        Stage _lecturaModalStage = new Stage();
        Scene _lecturaPageScene = new Scene(_lecturaParent);
        _lecturaModalStage.setScene(_lecturaPageScene);
        _lecturaModalStage.initModality(Modality.WINDOW_MODAL);
        _lecturaModalStage.initOwner(_anchorPaneProcesamientoDatos.getScene().getWindow());
        _lecturaModalStage.setResizable(false);
        _lecturaModalStage.setMaximized(false);
        _lecturaModalStage.show();
    }

    @FXML
    public void OnActionBtnReporte(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._reportePath));
        Parent _reporteParent = _loader.load();

        //MagazineController _controller = _loader.getController();
        //_controller.FillMagazine();

        Stage _reporteModalStage = new Stage();
        Scene _reportePageScene = new Scene(_reporteParent);
        _reporteModalStage.setScene(_reportePageScene);
        _reporteModalStage.initModality(Modality.WINDOW_MODAL);
        _reporteModalStage.initOwner(_anchorPaneProcesamientoDatos.getScene().getWindow());
        _reporteModalStage.setResizable(false);
        _reporteModalStage.setMaximized(false);
        _reporteModalStage.show();
    }

    public void OnActionBtnMagazine(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._magazinePath));
        Parent _magazineParent = _loader.load();

        MagazineController _controller = _loader.getController();
        _controller.FillMagazine();

        Stage _MagazineModalStage = new Stage();
        Scene _MagazinePageScene = new Scene(_magazineParent);
        _MagazineModalStage.setScene(_MagazinePageScene);
        _MagazineModalStage.initModality(Modality.WINDOW_MODAL);
        _MagazineModalStage.initOwner(_anchorPaneProcesamientoDatos.getScene().getWindow());
        _MagazineModalStage.setResizable(false);
        _MagazineModalStage.setMaximized(false);
        _MagazineModalStage.show();
    }

    public void OnActionBtnInformeIso(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._informeIsoPath));
        Parent _informeIsoParent = _loader.load();

        Stage _informeModalStage = new Stage();
        Scene _informePageScene = new Scene(_informeIsoParent);
        _informeModalStage.setScene(_informePageScene);
        _informeModalStage.initModality(Modality.WINDOW_MODAL);
        _informeModalStage.initOwner(_anchorPaneProcesamientoDatos.getScene().getWindow());
        _informeModalStage.setResizable(false);
        _informeModalStage.setMaximized(false);
        _informeModalStage.show();
    }

    @FXML
    public void OnActionBtnRecibir(ActionEvent event) throws IOException
    {
        FXMLLoader _loader = new FXMLLoader(getClass().getResource(Paths._recibirPath));
        Parent _recibirParent = _loader.load();

        Stage _recibirModalStage = new Stage();
        Scene _recibirPageScene = new Scene(_recibirParent);
        _recibirModalStage.setScene(_recibirPageScene);
        _recibirModalStage.initModality(Modality.WINDOW_MODAL);
        _recibirModalStage.initOwner(_anchorPaneProcesamientoDatos.getScene().getWindow());
        _recibirModalStage.setResizable(false);
        _recibirModalStage.setMaximized(false);
        _recibirModalStage.show();
    }

    public void setUsuario(Usuario _usuario)
    {
        _usuarioTemp = _usuario;

//        User SuperUsuario
        if (_usuarioTemp.get_access() == 1)
        {

        }
//        User Laboratorio
        if (_usuarioTemp.get_access() == 2)
        {

        }
//        User Administracion
        if (_usuarioTemp.get_access() == 3)
        {
            _btnMagazine.setDisable(true);
            _btnAnneal.setDisable(true);
            _btnLectura.setDisable(true);
            //_btnReporte.setDisable(true);
            _btnInformeIso.setDisable(true);
        }
    }
}
