package com.aleph.controller;

import com.aleph.model.Responsable;
import com.aleph.model.Sede;
import com.aleph.service.IResponsableService;
import com.aleph.service.ISedeService;
import com.aleph.service.ResponsableService;
import com.aleph.service.SedeService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import javafx.event.ActionEvent;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;

public class SedeEditarController
{
    private ISedeService _sedeService = new SedeService();
    IResponsableService _responsableService = new ResponsableService();

    @FXML
    private AnchorPane _anchorPaneEditarSede;
    @FXML
    private TextField _txtCliente;
    @FXML
    private TextField _txtSedeNombre;
    @FXML
    private TextField _txtSedeDireccion;
    @FXML
    private ComboBox _cboxDistritos;
    @FXML
    private Button _btnGuardarSede;
    @FXML
    private Button _btnCancelarSede;
    @FXML
    private Button _btnAgregarContactos;
    @FXML
    public VBox _vboxContactos;

    List<Responsable> _listResponsable;
    private ObservableList<CustomResponsable> _lista = FXCollections.observableArrayList();

    //Variables temporables
    private int _sedeIdTemp;
    private int _responsableIdTemp;

    // Event Listener on Button[#_btnGuardarSede].onAction
    @FXML
    public void OnActionBtnGuardarSede(ActionEvent event)
    {
        if (_txtCliente.getText().isEmpty())
        {
            new AlertBox("El cliente no debe ser vacio", "error", new Alert(Alert.AlertType.ERROR));
        } else if (_txtSedeNombre.getText().isEmpty())
        {
            new AlertBox("el campo nombre de sede está vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else if (_txtSedeDireccion.getText().isEmpty())
        {
            new AlertBox("el campo direccion está vacío", "error", new Alert(Alert.AlertType.ERROR));
        } else
//            else if (_txtContactoNombre.getText().isEmpty())
//        {
//            new AlertBox("el campo nombre de contacto está vacío", "error", new Alert(Alert.AlertType.ERROR));
//        } else if (_txtContactoEmail.getText().isEmpty())
//        {
//            new AlertBox("el campo email de contacto está vacío", "error", new Alert(Alert.AlertType.ERROR));
//        } else if (_txtContactoTelefono.getText().isEmpty())
//        {
//            new AlertBox("el campo telefono de contacto está vacío", "error", new Alert(Alert.AlertType.ERROR));
//        } else
        {
            try
            {
                boolean _error = false;

                if (!_error)
                {
                    Sede _sede = new Sede();
                    _sede.set_sedeId(_sedeIdTemp);
                    _sede.set_nombre(_txtSedeNombre.getText());
                    _sede.set_direccion(_txtSedeDireccion.getText());
                    _sede.set_distrito((String) _cboxDistritos.getValue());

                    _sedeService.updateSede(_sede);

                    System.out.println("tamaño de lista: " + _lista.size());

                    for (int i = 0; i < _lista.size(); i++)
                    {
                        Responsable _responsable = new Responsable();
                        _responsable.set_responsableId(Integer.parseInt(_lista.get(i).get_labContactoId()));
                        _responsable.set_nombre(_lista.get(i).get_txtContactoNombre());
                        _responsable.set_email(_lista.get(i).get_txtContactoEmail());
                        _responsable.set_celular(_lista.get(i).get_txtContactoTelefono());
                        _responsable.set_habilitado(_lista.get(i).is_habilitado());
                        System.out.println(_responsable);
                        if (_responsable.get_responsableId() == 0)
                            _responsableService.addResponsable(_responsable, _sedeIdTemp);
                        else
                            _responsableService.updateResponsable(_responsable);
                    }

                    new AlertBox("Sede registrada con éxito!!", "Concluido", new Alert(Alert.AlertType.INFORMATION));
                    OnActionBtnCancelarSede(event);
                }
            } catch (RuntimeException e)
            {
                System.err.println("Error en 2: " + e);
            }
        }
    }

    // Event Listener on Button[#_btnCancelarSede].onAction
    @FXML
    public void OnActionBtnCancelarSede(ActionEvent event)
    {
        Stage _stage = (Stage) _anchorPaneEditarSede.getScene().getWindow();
        _stage.close();
    }

    @FXML
    public void OnActionBtnAgregarContacto(ActionEvent event)
    {
        CustomResponsable _row = new CustomResponsable();
        _row.set_labContactoId("0");
        _lista.add(_row);
        _vboxContactos.getChildren().clear();
        _vboxContactos.getChildren().addAll(_lista);
    }

    public void setSede(Sede _sede)
    {
        _txtCliente.setText(_sede.get_nombreCliente());
        _txtSedeNombre.setText(_sede.get_nombre());
        _txtSedeDireccion.setText(_sede.get_direccion());
        _sedeIdTemp = _sede.get_sedeId();
        LlenarComboDistritos(_sede.get_distrito());
        setResponsable(_sede.get_sedeId());
    }

    public void setResponsable(int _idSede)
    {
        _listResponsable = _responsableService.findResponsableById(_idSede);
        for (int i = 0; i < _listResponsable.size(); i++)
        {
            CustomResponsable _row = new CustomResponsable();
            _row.get_btnDeleteContacto().setOnAction(new EventHandler<ActionEvent>()
            {
                @Override
                public void handle(ActionEvent event)
                {
                    _row.get_gridPaneContactos().setVisible(false);
                    _row.get_gridPaneContactos().setPrefHeight(0);
                    _row.set_habilitado(false);
                }
            });
            _row.set_labContactoId(String.valueOf(_listResponsable.get(i).get_responsableId()));
            _row.set_txtContactoNombre(_listResponsable.get(i).get_nombre());
            _row.set_txtContactoEmail(_listResponsable.get(i).get_email());
            _row.set_txtContactoTelefono(_listResponsable.get(i).get_celular());
            _row.set_habilitado(_listResponsable.get(i).is_habilitado());
            System.out.println(_listResponsable.get(i));
            _lista.add(_row);
        }

        _vboxContactos.getChildren().addAll(_lista);
    }

    private void LlenarComboDistritos(String _opcion)
    {
        _cboxDistritos.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxDistritos.getItems().add("Otros");
        _cboxDistritos.getItems().add("Ancon");
        _cboxDistritos.getItems().add("Ate");
        _cboxDistritos.getItems().add("Barranco");
        _cboxDistritos.getItems().add("Breña");
        _cboxDistritos.getItems().add("Carabayllo");
        _cboxDistritos.getItems().add("Chaclacayo");
        _cboxDistritos.getItems().add("Chorrillos");
        _cboxDistritos.getItems().add("Cieneguilla");
        _cboxDistritos.getItems().add("Comas");
        _cboxDistritos.getItems().add("El Agustino");
        _cboxDistritos.getItems().add("Huaycan");
        _cboxDistritos.getItems().add("Independencia");
        _cboxDistritos.getItems().add("Jesus Maria");
        _cboxDistritos.getItems().add("La Molina");
        _cboxDistritos.getItems().add("La Victoria");
        _cboxDistritos.getItems().add("Lima");
        _cboxDistritos.getItems().add("Lince");
        _cboxDistritos.getItems().add("Los Olivos");
        _cboxDistritos.getItems().add("Lurigancho");
        _cboxDistritos.getItems().add("Lurin");
        _cboxDistritos.getItems().add("Magdalena del Mar");
        _cboxDistritos.getItems().add("Miraflores");
        _cboxDistritos.getItems().add("Pachacamac");
        _cboxDistritos.getItems().add("Pucusana");
        _cboxDistritos.getItems().add("Pueblo Libre");
        _cboxDistritos.getItems().add("Puente Piedra");
        _cboxDistritos.getItems().add("Punta Hermosa");
        _cboxDistritos.getItems().add("Punta Negra");
        _cboxDistritos.getItems().add("Rimac");
        _cboxDistritos.getItems().add("San Bartolo");
        _cboxDistritos.getItems().add("San Borja");
        _cboxDistritos.getItems().add("San Isidro");
        _cboxDistritos.getItems().add("San Juan de Lurigancho");
        _cboxDistritos.getItems().add("San Juan de Miraflores");
        _cboxDistritos.getItems().add("San Luis");
        _cboxDistritos.getItems().add("San Martin de Porres");
        _cboxDistritos.getItems().add("San Miguel");
        _cboxDistritos.getItems().add("Santa Anita");
        _cboxDistritos.getItems().add("Santa Maria del Mar");
        _cboxDistritos.getItems().add("Santa Rosa");
        _cboxDistritos.getItems().add("Santiago de Surco");
        _cboxDistritos.getItems().add("Surquillo");
        _cboxDistritos.getItems().add("Villa El Salvador");
        _cboxDistritos.getItems().add("Villa Maria del Triunfo");
        _cboxDistritos.getItems().add("Bellavista");
        _cboxDistritos.getItems().add("Callao");
        _cboxDistritos.getItems().add("Carmen de La Legua-Reynoso");
        _cboxDistritos.getItems().add("La Perla");
        _cboxDistritos.getItems().add("La Punta");
        _cboxDistritos.getItems().add("Ventanilla");
        _cboxDistritos.getItems().add("Mi Peru");

        switch (_opcion)
        {
            case "Ancon":
                _cboxDistritos.getSelectionModel().select(1);
                break;
            case "Ate":
                _cboxDistritos.getSelectionModel().select(2);
                break;
            case "Barranco":
                _cboxDistritos.getSelectionModel().select(3);
                break;
            case "Breña":
                _cboxDistritos.getSelectionModel().select(4);
                break;
            case "Carabayllo":
                _cboxDistritos.getSelectionModel().select(5);
                break;
            case "Chaclacayo":
                _cboxDistritos.getSelectionModel().select(6);
                break;
            case "Chorrillos":
                _cboxDistritos.getSelectionModel().select(7);
                break;
            case "Cieneguilla":
                _cboxDistritos.getSelectionModel().select(8);
                break;
            case "Comas":
                _cboxDistritos.getSelectionModel().select(9);
                break;
            case "El Agustino":
                _cboxDistritos.getSelectionModel().select(10);
                break;
            case "Huaycan":
                _cboxDistritos.getSelectionModel().select(11);
                break;
            case "Independencia":
                _cboxDistritos.getSelectionModel().select(12);
                break;
            case "Jesus Maria":
                _cboxDistritos.getSelectionModel().select(13);
                break;
            case "La Molina":
                _cboxDistritos.getSelectionModel().select(14);
                break;
            case "La Victoria":
                _cboxDistritos.getSelectionModel().select(15);
                break;
            case "Lima":
                _cboxDistritos.getSelectionModel().select(16);
                break;
            case "Lince":
                _cboxDistritos.getSelectionModel().select(17);
                break;
            case "Los Olivos":
                _cboxDistritos.getSelectionModel().select(18);
                break;
            case "Lurigancho":
                _cboxDistritos.getSelectionModel().select(19);
                break;
            case "Lurin":
                _cboxDistritos.getSelectionModel().select(20);
                break;
            case "Magdalena del Mar":
                _cboxDistritos.getSelectionModel().select(21);
                break;
            case "Miraflores":
                _cboxDistritos.getSelectionModel().select(22);
                break;
            case "Pachacamac":
                _cboxDistritos.getSelectionModel().select(23);
                break;
            case "Pucusana":
                _cboxDistritos.getSelectionModel().select(24);
                break;
            case "Pueblo Libre":
                _cboxDistritos.getSelectionModel().select(25);
                break;
            case "Puente Piedra":
                _cboxDistritos.getSelectionModel().select(26);
                break;
            case "Punta Hermosa":
                _cboxDistritos.getSelectionModel().select(27);
                break;
            case "Punta Negra":
                _cboxDistritos.getSelectionModel().select(28);
                break;
            case "Rimac":
                _cboxDistritos.getSelectionModel().select(29);
                break;
            case "San Bartolo":
                _cboxDistritos.getSelectionModel().select(30);
                break;
            case "San Borja":
                _cboxDistritos.getSelectionModel().select(31);
                break;
            case "San Isidro":
                _cboxDistritos.getSelectionModel().select(32);
                break;
            case "San Juan de Lurigancho":
                _cboxDistritos.getSelectionModel().select(33);
                break;
            case "San Juan de Miraflores":
                _cboxDistritos.getSelectionModel().select(34);
                break;
            case "San Luis":
                _cboxDistritos.getSelectionModel().select(35);
                break;
            case "San Martin de Porres":
                _cboxDistritos.getSelectionModel().select(36);
                break;
            case "San Miguel":
                _cboxDistritos.getSelectionModel().select(37);
                break;
            case "Santa Anita":
                _cboxDistritos.getSelectionModel().select(38);
                break;
            case "Santa Maria del Mar":
                _cboxDistritos.getSelectionModel().select(39);
                break;
            case "Santa Rosa":
                _cboxDistritos.getSelectionModel().select(40);
                break;
            case "Santiago de Surco":
                _cboxDistritos.getSelectionModel().select(41);
                break;
            case "Surquillo":
                _cboxDistritos.getSelectionModel().select(42);
                break;
            case "Villa El Salvador":
                _cboxDistritos.getSelectionModel().select(43);
                break;
            case "Villa Maria del Triunfo":
                _cboxDistritos.getSelectionModel().select(44);
                break;
            case "Bellavista":
                _cboxDistritos.getSelectionModel().select(45);
                break;
            case "Callao":
                _cboxDistritos.getSelectionModel().select(46);
                break;
            case "Carmen de La Legua-Reynoso":
                _cboxDistritos.getSelectionModel().select(47);
                break;
            case "La Perla":
                _cboxDistritos.getSelectionModel().select(48);
                break;
            case "La Punta":
                _cboxDistritos.getSelectionModel().select(49);
                break;
            case "Ventanilla":
                _cboxDistritos.getSelectionModel().select(50);
                break;
            case "Mi Peru":
                _cboxDistritos.getSelectionModel().select(51);
                break;
            default:
                _cboxDistritos.getSelectionModel().select(0);
                break;
        }
    }
}
