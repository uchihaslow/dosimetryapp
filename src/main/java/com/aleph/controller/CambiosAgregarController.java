package com.aleph.controller;

import com.aleph.model.Poe;
import com.aleph.model.Prestamo;
import com.aleph.service.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import java.util.Optional;

public class CambiosAgregarController
{

    private IPoeService _poeService = new PoeService();
    private IPrestamoService _prestamoService = new PrestamoService();
    private IMagazineService _magazineService = new MagazineService();

    @FXML
    private AnchorPane _anchorPaneCambiosAgregar;
    @FXML
    private TableView<Poe> _tvUsuarios;
    @FXML
    private TextField _txtDosimetro;
    @FXML
    private Button _btnAgregar;
    @FXML
    private ComboBox _cboxTipoRadiacion;
    @FXML
    private ComboBox _cboxTipoDosimetro;
    @FXML
    private ComboBox _cboxTipoExposicion;
    @FXML
    private CheckBox _chkboxExterno;

    //variables auxiliares
    private int guiaIdAux;
    private int cantidadAux;

    @FXML
    public void initialize()
    {
        LlenarComboDosimetro();
        LlenarComboExposicion();
        LlenarComboRadiacion();
    }

    @FXML
    void OnActionBtnAgregar(ActionEvent event)
    {
        Alert _alert = new Alert(Alert.AlertType.CONFIRMATION);
        _alert.setTitle("Cuadro de confirmación");
        _alert.setHeaderText("Agregar Usuario-Dosimetro");
        _alert.setContentText("¿Quiere agregarlo?");

        Prestamo _prestamo = new Prestamo();
        _prestamo.set_externo(_chkboxExterno.isSelected() ? true : false);
        _prestamo.set_estado(2);
        _prestamo.set_tipoRad((String) _cboxTipoRadiacion.getValue());
        switch ((String) _cboxTipoExposicion.getValue())
        {
            case "Cuerpo Entero":
                _prestamo.set_tipoExpo(1);
                break;
            case "Dedo Mano Derecha":
                _prestamo.set_tipoExpo(2);
                break;
            case "Dedo Mano Izquierda":
                _prestamo.set_tipoExpo(3);
                break;
            case "Muneca Derecha":
                _prestamo.set_tipoExpo(4);
                break;
            case "Muneca Izquierda":
                _prestamo.set_tipoExpo(5);
                break;
            default:
                _prestamo.set_tipoExpo(6);
                break;
        }
        _prestamo.set_tipoDosi((String) _cboxTipoDosimetro.getValue());

        Optional<ButtonType> _opcion = _alert.showAndWait();
        if (_opcion.get() == ButtonType.OK)
        {
            _prestamoService.cambioDosimetro(_prestamo, guiaIdAux, _tvUsuarios.getSelectionModel().getSelectedItem().get_poeId(), agregarCeros(_txtDosimetro.getText(), 7));
            //_magazineService.deshabilitarDosimetro(agregarCeros(_txtDosimetro.getText(), 7));
            cantidadAux++;
            new AlertBox("Asignacion agregada", "Concluido", new Alert(Alert.AlertType.INFORMATION));
            _btnAgregar.getScene().getWindow().hide();
        }
    }

    private void LlenarComboRadiacion()
    {
        _cboxTipoRadiacion.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxTipoRadiacion.getItems().addAll("PH", "PM", "PL", "N", "B", "P");
        _cboxTipoRadiacion.getSelectionModel().select(5);
    }

    private void LlenarComboDosimetro()
    {
        _cboxTipoDosimetro.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxTipoDosimetro.getItems().addAll("I", "K");
        _cboxTipoDosimetro.getSelectionModel().select(1);
    }

    private void LlenarComboExposicion()
    {
        _cboxTipoExposicion.getItems().clear();
        //ObservableList<String> _items = FXCollections.observableArrayList();
        _cboxTipoExposicion.getItems().addAll
                (
                        "Cuerpo Entero",
                        "Dedo Mano Derecha",
                        "Dedo Mano Izquierda",
                        "Muneca Izquierda",
                        "Muneca Derecha",
                        "Otra parte"
                );
        _cboxTipoExposicion.getSelectionModel().select(0);
    }

    public void setGuiaIdAux(int guiaId)
    {
        this.guiaIdAux = guiaId;
        llenarTvUsuarios();
    }

    public void llenarTvUsuarios()
    {
        ObservableList<Poe> _poeList = FXCollections.observableArrayList(_poeService.findPoeByGuia2(guiaIdAux));
        _tvUsuarios.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_dni"));
        _tvUsuarios.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_nombreCompleto"));
        _tvUsuarios.setItems(_poeList);
    }

    public int getCantidadAux()
    {
        return cantidadAux;
    }

    public void setCantidadAux(int cantidadAux)
    {
        this.cantidadAux = cantidadAux;
    }

    private static String agregarCeros(String _numero, int _longitud)
    {
        String ceros = "";

        int cantidad = _longitud - _numero.length();

        if (cantidad >= 1)
        {
            for (int i = 0; i < cantidad; i++)
            {
                ceros += "0";
            }

            return (ceros + _numero);
        } else
        {
            return _numero;
        }
    }
}
