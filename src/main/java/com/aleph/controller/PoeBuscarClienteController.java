package com.aleph.controller;

import com.aleph.model.Cliente;
import com.aleph.service.ClienteService;
import com.aleph.service.IClienteService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

public class PoeBuscarClienteController
{
    private IClienteService _clienteService = new ClienteService();

    @FXML
    private AnchorPane _anchorPaneSeleccionarPoe;
    @FXML
    private TableView<Cliente> _tvCliente;
    @FXML
    private TextField _txtBuscarCliente;
    @FXML
    private Button _btnSeleccionar;
    @FXML
    private RadioButton _rbRuc;
    @FXML
    private ToggleGroup _tgGrupoClientes;
    @FXML
    private RadioButton _rbRazonSocial;
    @FXML
    private Button _btnBuscar;

    //variables temporales
    private String _rucSend;

    private String _razonSocialSend;

    private int _idClienteSend;

    public String getRucSend()
    {
        return _rucSend;
    }

    public String getRazonSocialSend()
    {
        return _razonSocialSend;
    }

    public Integer getIdCliente()
    {
        return _idClienteSend;
    }

    // Event Listener on Button[#_btnSeleccionar].onAction
    @FXML
    public void OnActionBtnSeleccionar(ActionEvent event)
    {
        _rucSend = _tvCliente.getSelectionModel().getSelectedItem().get_ruc();
        _razonSocialSend = _tvCliente.getSelectionModel().getSelectedItem().get_razonSocial();
        _idClienteSend = _tvCliente.getSelectionModel().getSelectedItem().get_clienteId();
        _btnSeleccionar.getScene().getWindow().hide();
    }

    // Event Listener on Button[#_btnBuscar].onAction
    @FXML
    public void OnActionBtnBuscar(ActionEvent event)
    {
        Buscar();
    }

    private void Buscar()
    {
        if (_rbRazonSocial.isSelected())
        {
            ObservableList<Cliente> _clienteList = FXCollections.observableArrayList(_clienteService.findClienteByNombre(_txtBuscarCliente.getText()));
            _tvCliente.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_ruc"));
            _tvCliente.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_razonSocial"));
            _tvCliente.setItems(_clienteList);
        }
        if (_rbRuc.isSelected())
        {
            ObservableList<Cliente> _clienteList = FXCollections.observableArrayList(_clienteService.findClienteByRuc(_txtBuscarCliente.getText()));
            _tvCliente.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_ruc"));
            _tvCliente.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_razonSocial"));
            _tvCliente.setItems(_clienteList);
        }
    }

    public void OnKeyPressed(KeyEvent keyEvent)
    {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
        {
            Buscar();
        }
    }
}
