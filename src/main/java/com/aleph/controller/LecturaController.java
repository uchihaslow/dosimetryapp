package com.aleph.controller;

import com.aleph.model.Lectura;
import com.aleph.model.LecturaReader;
import com.aleph.service.ILecturaService;
import com.aleph.service.IPrestamoService;
import com.aleph.service.LecturaService;
import com.aleph.service.PrestamoService;
import com.aleph.tools.DosimetroRecibido;
import com.aleph.tools.Month;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class LecturaController
{
    IPrestamoService _prestamoService = new PrestamoService();
    ILecturaService _lecturaService = new LecturaService();

    @FXML
    private AnchorPane _anchorPaneLecturas;
    @FXML
    private TableView<Lectura> _tvReport;
    @FXML
    private TextField _txtFileReport;
    @FXML
    private Button _btnExaminarReport;
    @FXML
    private Button _btnSincronizar;
    @FXML
    public ComboBox<Month> _cboxPeriodoMes;
    @FXML
    public ComboBox _cboxPeriodoAnio;
    @FXML
    public Button _btnCargar;

    File _fileReport;
    ObservableList<Lectura> _listaLecturas = FXCollections.observableArrayList();

    ArrayList<DosimetroRecibido> _listDosimetrosRecibidos = new ArrayList();

    @FXML
    public void initialize()
    {
        LlenarComboMes();
        LlenarComboAnio();
    }

    @FXML
    public void OnActionBtnExaminarReport(ActionEvent event)
    {
        try
        {
            FileChooser _fc = new FileChooser();
            List<String> _extensiones = new ArrayList<>();
            _extensiones.add("*.prn");
            _extensiones.add("*.PRN");

            _fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Archivos PRN", _extensiones));
            _fileReport = _fc.showOpenDialog(null);
            _txtFileReport.setText(_fileReport.getName());


        } catch (Exception e)
        {
            System.err.println(e);
        }
    }

    @FXML
    public void OnActionBtnSincronizar(ActionEvent event)
    {
        boolean _existe;

        for (Lectura _dato : _listaLecturas)
        {
            _existe = _prestamoService.findPrestamoRecibidoExist(_dato.get_badgeId());
            if (_existe == true)
            {
                System.out.print(_dato.get_badgeId());
                System.out.print("    ");
                System.out.print(_dato.get_cristalino());
                System.out.print("    ");
                System.out.print(_dato.get_efectiva());
                System.out.print("    ");
                System.out.println(_dato.get_piel());

                _lecturaService.addLectura(_dato);
                _prestamoService.changeEstadoLeido(_dato.get_badgeId());
            } else
            {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Alerta");
                alert.setHeaderText("el dosímetro ya fué leído o no fué asignado:");
                alert.setContentText("Indice: " + _dato.get_indice() + " Código: " + _dato.get_badgeId());

                alert.showAndWait();
            }
        }

        Alert _alert = new Alert(Alert.AlertType.INFORMATION);
        _alert.setTitle("Cuadro informativo");
        _alert.setHeaderText("Proceso terminado");
        _alert.showAndWait();

        Stage _stage = (Stage) _anchorPaneLecturas.getScene().getWindow();
        _stage.close();
    }

    private void LlenarTabla(ArrayList<Lectura> _lista)
    {
        _listaLecturas = FXCollections.observableArrayList(_lista);

        _tvReport.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("_badgeId"));
        _tvReport.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("_efectiva"));
        _tvReport.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("_cristalino"));
        _tvReport.getColumns().get(3).setCellValueFactory(new PropertyValueFactory("_piel"));
        _tvReport.getColumns().get(4).setCellValueFactory(new PropertyValueFactory("_radiacion"));
        _tvReport.setItems(_listaLecturas);
    }

    public static double Redondear(double numero, int digitos)
    {
        int cifras = (int) Math.pow(10, digitos);
        return Math.rint(numero * cifras) / cifras;
    }

    @FXML
    public void OnActionCboxPeriodoMes(ActionEvent actionEvent)
    {

    }

    @FXML
    public void OnActionCboxPeriodoAnio(ActionEvent actionEvent)
    {

    }

    @FXML
    public void OnActionBtnCargar(ActionEvent actionEvent)
    {

        System.out.println(_cboxPeriodoMes.getValue().get_posicion());
        System.out.println(_cboxPeriodoAnio.getValue());
        String mes = String.valueOf(_cboxPeriodoMes.getValue().get_posicion());
        String year = String.valueOf(_cboxPeriodoAnio.getValue());

        _listDosimetrosRecibidos = _prestamoService.listDosimetrosRecibidos("01/"+mes+"/"+year);
        System.out.println("01/"+mes+"/"+year);

        for (int i = 0; i < _listDosimetrosRecibidos.size(); i++)
        {
            System.out.println(_listDosimetrosRecibidos.get(i));
        }

        if (_fileReport != null)
        {
            _tvReport.getItems().clear();
            ArrayList<Lectura> _listaTemp = LecturaReader.readReport(_fileReport,_listDosimetrosRecibidos);
            LlenarTabla(_listaTemp);

            DecimalFormat df = new DecimalFormat("#.##");

            for (Lectura _dato : _listaLecturas)
            {
                _dato.set_cristalino(Redondear(_dato.get_cristalino() / 100, 2));
                _dato.set_efectiva(Redondear(_dato.get_efectiva() / 100, 2));
                _dato.set_piel(Redondear(_dato.get_piel() / 100, 2));
            }
        }
    }

    private void LlenarComboMes()
    {
        _cboxPeriodoMes.getItems().clear();
        _cboxPeriodoMes.setItems(getMeses());

//        _cboxPeriodoMes.setCellFactory(new Callback<ListView<Month>, ListCell<Month>>()
//        {
//            @Override
//            public ListCell<Month> call(ListView<Month> param)
//            {
//                ListCell<Month> cell = new ListCell<Month>()
//                {
//                    @Override
//                    protected void updateItem(Month item, boolean empty)
//                    {
//                        super.updateItem(item, empty);
//                        if (item != null)
//                        {
//                            setText(item.get_nombre());
//                            System.out.println("mes: " + item.get_nombre() + "  posicion: " + item.get_posicion());
//                        } else
//                        {
//                            setText(null);
//                        }
//
//                    }
//                };
//                return cell;
//            }
//        });
        _cboxPeriodoMes.getSelectionModel().select(0);
    }

    private void LlenarComboAnio()
    {
        _cboxPeriodoAnio.getItems().clear();
        _cboxPeriodoAnio.setItems(getAnios());

        _cboxPeriodoAnio.getSelectionModel().select(1);
    }

    private static ObservableList<Month> getMeses()
    {
        ObservableList<Month> _data = FXCollections.observableArrayList();

        _data.add(new Month("ENE", "01"));
        _data.add(new Month("FEB", "02"));
        _data.add(new Month("MAR", "03"));
        _data.add(new Month("ABR", "04"));
        _data.add(new Month("MAY", "05"));
        _data.add(new Month("JUN", "06"));
        _data.add(new Month("JUL", "07"));
        _data.add(new Month("AGO", "08"));
        _data.add(new Month("SEP", "09"));
        _data.add(new Month("OCT", "10"));
        _data.add(new Month("NOV", "11"));
        _data.add(new Month("DIC", "12"));

        return _data;
    }

    private static ObservableList<String> getAnios()
    {
        ObservableList<String> _data = FXCollections.observableArrayList();

        _data.add("2018");
        _data.add("2019");
        _data.add("2020");
        _data.add("2021");
        _data.add("2022");
        _data.add("2023");
        _data.add("2024");

        return _data;
    }
}