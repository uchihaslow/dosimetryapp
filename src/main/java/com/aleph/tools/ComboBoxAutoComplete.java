package com.aleph.tools;

import com.aleph.model.Paths;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Window;

import java.io.IOException;
import java.text.Normalizer;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Uses a combobox tooltip as the suggestion for auto complete and updates the
 * combo box itens accordingly <br />
 * It does not work with space, space and escape cause the combobox to hide and
 * clean the filter ... Send me a PR if you want it to work with all characters
 * -> It should be a custom controller - I KNOW!
 *
 */
public class ComboBoxAutoComplete<T>
{

    private ComboBox<T> cmb;
    String filter = "";
    private ObservableList<T> originalItems;
    Double posX;
    Double posY;


    public ComboBoxAutoComplete(ComboBox<T> cmb)
    {
        this.cmb = cmb;
        originalItems = FXCollections.observableArrayList(cmb.getItems());
        cmb.setTooltip(new Tooltip());
        cmb.setOnKeyPressed(this::handleOnKeyPressed);
        cmb.setOnHidden(this::handleOnHiding);
    }

    public ComboBoxAutoComplete(ComboBox<T> cmb, Double posX, Double posY)
    {
        this.cmb = cmb;
        originalItems = FXCollections.observableArrayList(cmb.getItems());
        cmb.setTooltip(new Tooltip());
        cmb.setOnKeyPressed(this::handleOnKeyPressed);
        cmb.setOnHidden(this::handleOnHiding);
        this.posX = posX;
        this.posY = posY;
    }

    public void handleOnKeyPressed(KeyEvent e)
    {
        ObservableList<T> filteredList = FXCollections.observableArrayList();
        KeyCode code = e.getCode();

        if (code.isLetterKey())
        {
            filter += e.getText();
        }
        if (code == KeyCode.BACK_SPACE && filter.length() > 0)
        {
            filter = filter.substring(0, filter.length() - 1);
            cmb.getItems().setAll(originalItems);
        }
        if (code == KeyCode.ESCAPE)
        {
            filter = "";
        }
        if (filter.length() == 0)
        {
            filteredList = originalItems;
            cmb.getTooltip().hide();
        } else
        {
            Stream<T> itens = cmb.getItems().stream();
            String txtUsr = unaccent(filter.toString().toLowerCase());
            itens.filter(el -> unaccent(el.toString().toLowerCase()).contains(txtUsr)).forEach(filteredList::add);
            cmb.getTooltip().setText(txtUsr);
            Window stage = cmb.getScene().getWindow();

            //System.out.println(cmb.getParent());

            FXMLLoader loader = new FXMLLoader(getClass().getResource(Paths._poeAsignacionPath));

            //try
            //{
            //    AnchorPane pane = loader.load();
            //} catch (IOException e1)
            //{
            //    e1.printStackTrace();
            //}
            //Map<String, Object> fxmlNamespace = loader.getNamespace();
            //TitledPane titlePaneSede = (TitledPane) fxmlNamespace.get("_titlePaneSede");

            //System.out.println("tp: " + titlePaneSede.getBoundsInParent().getMinX());
            //System.out.println("tp: " + titlePaneSede.getBoundsInParent().getMinY());


            //double posX = stage.getX() + cmb.getBoundsInParent().getMinX();
            //double posX = stage.getX() + titlePaneSede.getBoundsInParent().getMinX();
            //double posY = stage.getY() + cmb.getBoundsInParent().getMinY();
            //double posY = stage.getY() + titlePaneSede.getBoundsInParent().getMinY();

            cmb.getTooltip().show(stage, posX, posY);
            cmb.show();
        }
        cmb.getItems().setAll(filteredList);
    }

    public void handleOnHiding(Event e)
    {
        filter = "";
        cmb.getTooltip().hide();
        T s = cmb.getSelectionModel().getSelectedItem();
        cmb.getItems().setAll(originalItems);
        cmb.getSelectionModel().select(s);
    }

    private String unaccent(String s)
    {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

}