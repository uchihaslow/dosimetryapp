package com.aleph.tools;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

public class IntArray implements Array
{
    private int[] _array;

    public IntArray(int[] _array)
    {
        if (_array == null)
        {
            throw new IllegalArgumentException("el parametro _array no deberia ser null");
        }
        this._array = _array;
    }

    @Override
    public String getBaseTypeName() throws SQLException
    {
        return "int4";
    }

    @Override
    public int getBaseType() throws SQLException
    {
        return Types.INTEGER;
    }

    @Override
    public Object getArray() throws SQLException
    {
        return null;
    }

    @Override
    public Object getArray(Map<String, Class<?>> map) throws SQLException
    {
        return null;
    }

    @Override
    public Object getArray(long index, int count) throws SQLException
    {
        return null;
    }

    @Override
    public Object getArray(long index, int count, Map<String, Class<?>> map) throws SQLException
    {
        return null;
    }

    @Override
    public ResultSet getResultSet() throws SQLException
    {
        return null;
    }

    @Override
    public ResultSet getResultSet(Map<String, Class<?>> map) throws SQLException
    {
        return null;
    }

    @Override
    public ResultSet getResultSet(long index, int count) throws SQLException
    {
        return null;
    }

    @Override
    public ResultSet getResultSet(long index, int count, Map<String, Class<?>> map) throws SQLException
    {
        return null;
    }

    @Override
    public void free() throws SQLException
    {

    }

    @Override
    public String toString()
    {
        String result = "{";
        for (int i = 0; i < this._array.length; ++i)
        {
            if (i > 0)
            {
                result += ",";
            }
            result += this._array[i];
        }
        result += "}";
        return result;
    }
}
