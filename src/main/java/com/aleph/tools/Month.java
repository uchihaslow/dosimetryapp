package com.aleph.tools;

public class Month
{
    private String _nombre;
    private String _posicion;

    public Month(String _nombre, String _posicion)
    {
        this._nombre = _nombre;
        this._posicion = _posicion;
    }

    public String get_nombre()
    {
        return _nombre;
    }

    public void set_nombre(String _nombre)
    {
        this._nombre = _nombre;
    }

    public String get_posicion()
    {
        return _posicion;
    }

    public void set_posicion(String _posicion)
    {
        this._posicion = _posicion;
    }

    @Override
    public String toString()
    {
        return _nombre;

    }
}
