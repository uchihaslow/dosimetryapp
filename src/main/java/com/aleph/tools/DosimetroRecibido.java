package com.aleph.tools;

public class DosimetroRecibido
{
    private int _prestamoId;
    private String _tipoRad;
    private String _tipoDos;
    private int _tipoExp;
    private String _dosimetroId;

    public DosimetroRecibido()
    {

    }

    public int get_prestamoId()
    {
        return _prestamoId;
    }

    public void set_prestamoId(int _prestamoId)
    {
        this._prestamoId = _prestamoId;
    }

    public String get_tipoRad()
    {
        return _tipoRad;
    }

    public void set_tipoRad(String _tipoRad)
    {
        this._tipoRad = _tipoRad;
    }

    public String get_tipoDos()
    {
        return _tipoDos;
    }

    public void set_tipoDos(String _tipoDos)
    {
        this._tipoDos = _tipoDos;
    }

    public int get_tipoExp()
    {
        return _tipoExp;
    }

    public void set_tipoExp(int _tipoExp)
    {
        this._tipoExp = _tipoExp;
    }

    public String get_dosimetroId()
    {
        return _dosimetroId;
    }

    public void set_dosimetroId(String _dosimetroId)
    {
        this._dosimetroId = _dosimetroId;
    }

    @Override
    public String toString()
    {
        return "DosimetroRecibido{" +
                "_prestamoId=" + _prestamoId +
                ", _tipoRad='" + _tipoRad + '\'' +
                ", _tipoDos='" + _tipoDos + '\'' +
                ", _tipoExp=" + _tipoExp +
                ", _dosimetroId='" + _dosimetroId + '\'' +
                '}';
    }
}
